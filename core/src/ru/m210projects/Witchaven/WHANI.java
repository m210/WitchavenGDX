package ru.m210projects.Witchaven;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Items.WALLPIKETYPE;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Potions.randompotion;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHFX.*;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.addhealth;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.startredflash;

public class WHANI {

    public static void animateobjs(PLAYER plr) {
        Sector plrsec = boardService.getSector(plr.getSector());
        if (plrsec == null) {
            return;
        }

        ListNode<Sprite> next;
        if (game.WH2) {
            for (ListNode<Sprite> node = boardService.getStatNode(SHARDOFGLASS); node != null; node = next) {
                next = node.getNext();
                final int i = node.getIndex();

                Sprite spr = node.get();
                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                switch (spr.getExtra()) {
                    case 1:
                        spr.setZvel(spr.getZvel() + (TICSPERFRAME << 3));
                        movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                                ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 0);
                        break;
                    case 2:
                        spr.setZvel(spr.getZvel() + (TICSPERFRAME << 5));
                        movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 1,
                                ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 1, spr.getZvel(), 4 << 8, 4 << 8, 0);
                        break;
                    case 3:
                        spr.setZvel(spr.getZvel() - (TICSPERFRAME << 5));
                        movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 2,
                                ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 2, spr.getZvel(), 4 << 8, 4 << 8, 0);
                        if (spr.getLotag() < 0) {
                            spr.setLotag(30);
                            spr.setExtra(2);
                        }
                        break;
                }
                if (spr.getLotag() < 0) {
                    engine.deletesprite(i);
                }
            }

            for (ListNode<Sprite> node = boardService.getStatNode(SPARKSUP); node != null; node = next) {
                next = node.getNext();
                Sprite spr = node.get();
                final int i = node.getIndex();

                int osectnum = spr.getSectnum();

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < -100) {
                    spr.setLotag(-100);
                }

                if (spr.getLotag() < 0) {
                    spr.setZvel(spr.getZvel() - (TICSPERFRAME << 4));
                    spr.setAng((short) ((spr.getAng() + (TICSPERFRAME << 2)) & 2047));

                    movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                            ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 1);

                    if (osectnum != spr.getSectnum()) {
                        spr.setX(sparksx);
                        spr.setY(sparksy);
                        spr.setZ(sparksz);
                        spr.setAng((short) ((engine.krand() % 2047) & 2047));
                        spr.setZvel(0);
                        engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                    }
                }
            }

            for (ListNode<Sprite> node = boardService.getStatNode(SPARKSDN); node != null; node = next) {
                next = node.getNext();
                Sprite spr = node.get();
                final int i = node.getIndex();

                int osectnum = spr.getSectnum();

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < -100) {
                    spr.setLotag(-100);
                }

                if (spr.getLotag() < 0) {
                    spr.setZvel(spr.getZvel() + (TICSPERFRAME << 4));
                    spr.setAng((short) ((spr.getAng() + (TICSPERFRAME << 2)) & 2047));

                    movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                            ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 1);

                    if (osectnum != spr.getSectnum()) {
                        spr.setX(sparksx);
                        spr.setY(sparksy);
                        spr.setZ(sparksz);
                        spr.setAng((short) ((engine.krand() % 2047) & 2047));
                        spr.setZvel(0);
                        engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                    }
                }
            }

            for (ListNode<Sprite> node = boardService.getStatNode(SPARKS); node != null; node = next) {
                next = node.getNext();
                Sprite spr = node.get();
                final int i = node.getIndex();

                int osectnum = spr.getSectnum();
                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < -100) {
                    spr.setLotag(-100);
                }

                if (spr.getLotag() < 0) {
                    spr.setAng((short) ((spr.getAng() + (TICSPERFRAME << 2)) & 2047));

                    movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                            ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 1);

                    if (osectnum != spr.getSectnum()) {
                        spr.setX(sparksx);
                        spr.setY(sparksy);
                        spr.setZ(sparksz);
                        spr.setAng((short) ((engine.krand() % 2047) & 2047));
                        spr.setZvel(0);
                        engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                    }
                }
            }

            for (ListNode<Sprite> node = boardService.getStatNode(STONETOFLESH); node != null; node = next) {
                next = node.getNext();
                Sprite spr = node.get();
                final int i = node.getIndex();

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    switch (spr.getPicnum()) {
                        case STONEGONZOCHM:
                            spr.setPicnum(GONZOGHM);
                            enemy[GONZOTYPE].info.set(spr);
                            spr.setDetail(GONZOTYPE);
                            spr.setXrepeat(24);
                            spr.setYrepeat(24);
                            spr.setClipdist(32);
                            engine.changespritestat(i, FACE);
                            spr.setHitag(adjusthp(100));
                            spr.setLotag(100);
                            spr.setCstat(spr.getCstat() | 0x101);
                            spr.setExtra(0);
                            break;
                        case STONEGONZOBSH:
                        case STONEGONZOGSH:
                            spr.setPicnum(GONZOGSH);
                            enemy[GONZOTYPE].info.set(spr);
                            spr.setDetail(GONZOTYPE);
                            spr.setXrepeat(24);
                            spr.setYrepeat(24);
                            spr.setClipdist(32);
                            engine.changespritestat(i, FACE);
                            spr.setHitag(adjusthp(100));
                            spr.setLotag(100);
                            spr.setCstat(spr.getCstat() | 0x101);
                            spr.setExtra(0);
                            break;
                        case STONEGRONDOVAL:
                            spr.setPicnum(GRONHAL);
                            enemy[GRONTYPE].info.set(spr);
                            spr.setDetail(GRONTYPE);
                            spr.setXrepeat(30);
                            spr.setYrepeat(30);
                            spr.setClipdist(64);
                            engine.changespritestat(i, FACE);
                            spr.setHitag(adjusthp(300));
                            spr.setLotag(100);
                            spr.setCstat(spr.getCstat() | 0x101);
                            spr.setExtra(4);
                            break;
                        case STONEGONZOBSW2:
                        case STONEGONZOBSW:
                            spr.setPicnum(NEWGUY);
                            enemy[NEWGUYTYPE].info.set(spr);
                            spr.setDetail(NEWGUYTYPE);
                            spr.setXrepeat(26);
                            spr.setYrepeat(26);
                            spr.setClipdist(48);
                            engine.changespritestat(i, FACE);
                            spr.setHitag(adjusthp(100));
                            spr.setLotag(100);
                            spr.setCstat(spr.getCstat() | 0x101);
                            spr.setExtra(30);
                            break;
                    }
                }
            }

            for (ListNode<Sprite> node = boardService.getStatNode(SHADE); node != null; node = next) {
                next = node.getNext();
                Sprite spr = node.get();
                final int i = node.getIndex();
                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() < 0) {
                    spr.setPicnum(GONZOBSHDEAD);
                    spr.setShade(31);
                    spr.setCstat(0x303);
                    spr.setPal(0);
                    spr.setExtra(12);
                    newstatus(i, EVILSPIRIT);
                }
            }

            for (ListNode<Sprite> node = boardService.getStatNode(EVILSPIRIT); node != null; node = next) {
                next = node.getNext();
                Sprite spr = node.get();
                final int i = node.getIndex();
                if (spr.getPicnum() >= (GONZOBSHDEAD - 8)) {
                    int extra = spr.getExtra() - 1;
                    spr.setExtra(extra);
                    if (extra <= 0) {
                        spr.setPicnum(spr.getPicnum() - 1);
                        spr.setExtra(12);
                    }
                } else {
                    int j = engine.insertsprite(spr.getSectnum(), FACE);
                    Sprite spr2 = boardService.getSprite(j);
                    if (spr2 != null) {
                        enemy[GONZOTYPE].info.set(spr2);
                        spr2.setX(spr.getX());
                        spr2.setY(spr.getY());
                        spr2.setZ(spr.getZ());
                        spr2.setCstat(0x303);
                        spr2.setPicnum(GONZOGSH);
                        spr2.setShade(31);
                        spr2.setPal(0);
                        spr2.setXrepeat(spr.getXrepeat());
                        spr2.setYrepeat(spr.getYrepeat());
                        spr2.setOwner(0);
                        spr2.setLotag(40);
                        spr2.setHitag(0);
                        spr2.setDetail(GONZOTYPE);
                    }
                    engine.deletesprite(i);
                }
            }

            for (ListNode<Sprite> node = boardService.getStatNode(TORCHFRONT); node != null; node = next) {
                next = node.getNext();
                Sprite spr = node.get();
                final int i = node.getIndex();
                playertorch = spr.getLotag() - TICSPERFRAME;
                if (plr.getSelectedgun() > 4) {
                    playertorch = -1;
                }
                spr.setLotag(playertorch);
                engine.setsprite(i, plr.getX(), plr.getY(), plr.getZ());
                final int osectnum = spr.getSectnum();
                int j = (torchpattern[lockclock % 38]);
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    continue;
                }

                osec.setCeilingshade((byte) ((osec.getCeilingshade() + j) >> 1));
                osec.setFloorshade((byte) ((osec.getFloorshade() + j) >> 1));
                for (ListNode<Wall> wn = osec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setShade((byte) ((wal.getShade() + j) >> 1));
                }

                movesprite(i,
                        ((EngineUtils.sin(((short) plr.getAng() + 512) & 2047)) << TICSPERFRAME) << 8,
                        ((EngineUtils.sin((short) plr.getAng() & 2047)) << TICSPERFRAME) << 8, 0, 4 << 8, 4 << 8, 0);
                osec = boardService.getSector(osectnum);
                if (osec == null) {
                    continue;
                }

                spr.setCstat(spr.getCstat() | 0x8000);
                show2dsprite.clearBit(i);

                if (osectnum != spr.getSectnum()) {
                    osec.setCeilingshade((byte) j);
                    osec.setFloorshade((byte) j);
                    for (ListNode<Wall> wn = osec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade((byte) j);
                    }
                }
                if (spr.getLotag() < 0) {
                    engine.deletesprite(i);

                    // set back to normall
                    plrsec.setCeilingshade(ceilingshadearray[plr.getSector()]);
                    plrsec.setFloorshade(floorshadearray[plr.getSector()]);
                    for (ListNode<Wall> wn = plrsec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade(wallshadearray[wn.getIndex()]);
                    }

                    Sector oplrsec = boardService.getSector(plr.getOldsector());
                    if (oplrsec != null) {
                        oplrsec.setCeilingshade(ceilingshadearray[plr.getOldsector()]);
                        oplrsec.setFloorshade(floorshadearray[plr.getOldsector()]);
                        for (ListNode<Wall> wn = oplrsec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            wal.setShade(wallshadearray[wn.getIndex()]);
                        }
                    }

                    osec.setCeilingshade(ceilingshadearray[osectnum]);
                    osec.setFloorshade(floorshadearray[osectnum]);
                    for (ListNode<Wall> wn = osec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade(wallshadearray[wn.getIndex()]);
                    }

                    osec = boardService.getSector(plr.getOldsector());
                    if (osec != null) {
                        osec.setCeilingshade(ceilingshadearray[osectnum]);
                        osec.setFloorshade(floorshadearray[osectnum]);
                        for (ListNode<Wall> wn = osec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            wal.setShade(wallshadearray[wn.getIndex()]);
                        }
                    }
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(PULLTHECHAIN); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            final int i = node.getIndex();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                spr.setPicnum(spr.getPicnum() + 1);
                spr.setLotag(24);
                if (spr.getPicnum() == PULLCHAIN3 || spr.getPicnum() == SKULLPULLCHAIN3) {
                    spr.setLotag(0);
                    engine.changespritestat(i, (short) 0);
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(ANIMLEVERDN); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            final int i = node.getIndex();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                spr.setPicnum(spr.getPicnum() + 1);
                spr.setLotag(24);
                if (spr.getPicnum() == LEVERDOWN) {
                    spr.setLotag(60);
                    engine.changespritestat(i, (short) 0);
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(ANIMLEVERUP); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            final int i = node.getIndex();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                spr.setPicnum(spr.getPicnum() - 1);
                spr.setLotag(24);
                if (spr.getPicnum() == LEVERUP) {
                    spr.setLotag(1);
                    engine.changespritestat(i, (short) 0);
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(WARPFX); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            final int i = node.getIndex();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                spr.setLotag(12);
                spr.setPicnum(spr.getPicnum() + 1);
                if (spr.getPicnum() == ANNIHILATE + 5) {
                    engine.deletesprite(i);
                }
            }
        }

        // FLOCKSPAWN
        for (ListNode<Sprite> node = boardService.getStatNode(FLOCKSPAWN); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            final int i = node.getIndex();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                spr.setExtra(spr.getExtra() - 1);
                spr.setLotag((short) (engine.krand() & 48 + 24));
                bats(plr, i);
                if (spr.getExtra() == 0) {
                    engine.changespritestat(i, (short) 0);
                }
            }
        }

        // FLOCK
        for (ListNode<Sprite> node = boardService.getStatNode(FLOCK); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            final int i = node.getIndex();
            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            switch (spr.getExtra()) {
                case 0: // going out of the cave
                    if (spr.getLotag() < 0) {
                        spr.setExtra(1);
                        spr.setLotag(512);
                    } else {
                        int movestat = movesprite(i,
                                ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                                ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
                        engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                        if (movestat != 0) {
                            spr.setAng((short) (engine.krand() & 2047));
                        }
                    }
                    break;
                case 1: // flying in circles
                    if (spr.getLotag() < 0) {
                        Sprite spr2 = boardService.getSprite(spr.getHitag());
                        if (spr2 != null) {
                            spr.setExtra(2);
                            spr.setLotag(512);
                            spr.setAng((short) (((EngineUtils.getAngle(spr2.getX() - spr.getX(),
                                    spr2.getY() - spr.getY()) & 2047) - 1024) & 2047));
                        }
                    } else {
                        spr.setZ(spr.getZ() - (TICSPERFRAME << 4));
                        spr.setAng(((spr.getAng() + (TICSPERFRAME << 2)) & 2047));
                        int movestat = movesprite(i,
                                ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                                ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
                        engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                        if (movestat != 0) {
                            spr.setAng((engine.krand() & 2047));
                        }
                    }
                    break;
                case 2: // fly to roof and get deleted
                    if (spr.getLotag() < 0) {
                        if (i == lastbat && batsnd != -1) {
                            stopsound(batsnd);
                            batsnd = -1;
                        }
                        engine.deletesprite(i);
                        continue;
                    } else {
                        int movestat = movesprite(i,
                                ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                                ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
                        engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                        if ((movestat & HIT_TYPE_MASK) == HIT_SECTOR) {// Hits a ceiling / floor
                            if (i == lastbat && batsnd != -1) {
                                stopsound(batsnd);
                                batsnd = -1;
                            }
                            engine.deletesprite(i);
                            continue;
                        }
                        if (movestat != 0) {
                            spr.setAng((short) (engine.krand() & 2047));
                        }
                    }
                    break;
            }
        }

        // TORCHLIGHT
        for (ListNode<Sprite> node = boardService.getStatNode(TORCHLIGHT); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            Sector osec =  boardService.getSector(spr.getSectnum());

            if (osec != null) {
                int j = (torchpattern[lockclock % 38]);
                osec.setCeilingshade((byte) j);
                osec.setFloorshade((byte) j);
                for (ListNode<Wall> wn = osec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setShade((byte) j);
                }
            }
        }

        // GLOWLIGHT
        for (ListNode<Sprite> node = boardService.getStatNode(GLOWLIGHT); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            Sector osec = boardService.getSector(spr.getSectnum());
            if (osec != null) {
                int j = (torchpattern[lockclock % 38]);
                osec.setFloorshade((byte) j);
                for (ListNode<Wall> wn = osec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setShade((byte) j);
                }
            }
        }

        // BOB
        for (ListNode<Sprite> node = boardService.getStatNode(BOB); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            spr.setZ(spr.getZ() + (EngineUtils.sin((lockclock << 4) & 2047) >> 6));
        }

        // LIFT UP
        for (ListNode<Sprite> node = boardService.getStatNode(LIFTUP); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();

            switch (spr.getLotag()) {
                case 1821: {
                    game.pInt.setsprinterpolate(i, spr);
                    spr.setZ(spr.getZ() - (TICSPERFRAME << 6));
                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                    Sector osec = boardService.getSector(spr.getSectnum());
                    if (osec == null) {
                        continue;
                    }

                    if (spr.getZ() <= osec.getCeilingz() + 32768) {
                        stopsound(cartsnd);
                        cartsnd = -1;
                        playsound_loc(S_CLUNK, spr.getX(), spr.getY());
                        engine.changespritestat(i, (short) 0);
                        spr.setLotag(1820);
                        spr.setZ(osec.getCeilingz() + 32768);
                    }
                    break;}
                case 1811: {
                    game.pInt.setsprinterpolate(i, spr);
                    spr.setZ(spr.getZ() - (TICSPERFRAME << 6));
                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                    Sector osec = boardService.getSector(spr.getSectnum());
                    if (osec == null) {
                        continue;
                    }

                    if (spr.getZ() <= osec.getCeilingz() + 65536) {
                        engine.changespritestat(i, (short) 0);
                        spr.setLotag(1810);
                        spr.setZ(osec.getCeilingz() + 65536);
                    }
                    break;}
                case 1801:{
                    game.pInt.setsprinterpolate(i, spr);
                    spr.setZ(spr.getZ() - (TICSPERFRAME << 6));
                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                    Sector osec = boardService.getSector(spr.getSectnum());
                    if (osec == null) {
                        continue;
                    }

                    if (spr.getZ() <= osec.getCeilingz() + 65536) {
                        engine.changespritestat(i, (short) 0);
                        spr.setLotag(1800);
                        spr.setZ(osec.getCeilingz() + 65536);
                    }
                    break;}
            }
        }

        // LIFT DN
        for (ListNode<Sprite> node = boardService.getStatNode(LIFTDN); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();

            int ironbarmove;
            switch (spr.getLotag()) {
                case 1820: {
                    game.pInt.setsprinterpolate(i, spr);
                    ironbarmove = TICSPERFRAME << 6;
                    spr.setZ(spr.getZ() + ironbarmove);
                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                    Sector osec = boardService.getSector(spr.getSectnum());
                    if (osec == null) {
                        break;
                    }

                    if (spr.getZ() >= (osec.getFloorz() - 32768)) {
                        stopsound(cartsnd);
                        cartsnd = -1;
                        playsound_loc(S_CLUNK, spr.getX(), spr.getY());
                        engine.changespritestat(i, (short) 0);
                        spr.setLotag(1821);
                        spr.setZ(osec.getFloorz() - 32768);
                    }
            }     break;
                case 1810: {
                    game.pInt.setsprinterpolate(i, spr);
                    ironbarmove = TICSPERFRAME << 6;
                    spr.setZ(spr.getZ() + ironbarmove);
                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                    Sector osec = boardService.getSector(spr.getSectnum());
                    if (osec == null) {
                        break;
                    }

                    if (spr.getZ() >= osec.getFloorz()) {
                        engine.changespritestat(i, (short) 0);
                        spr.setLotag(1811);
                        spr.setZ(osec.getFloorz());
                    }
                }    break;
                case 1800: {
                    game.pInt.setsprinterpolate(i, spr);
                    ironbarmove = TICSPERFRAME << 6;
                    spr.setZ(spr.getZ() + ironbarmove);
                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                    Sector osec = boardService.getSector(spr.getSectnum());
                    if (osec == null) {
                        break;
                    }

                    if (spr.getZ() >= osec.getFloorz()) {
                        engine.changespritestat(i, (short) 0);
                        spr.setLotag(1801);
                        spr.setZ(osec.getFloorz());
                    }
                }    break;
            }
        }

        // MASPLASH
        for (ListNode<Sprite> node = boardService.getStatNode(MASPLASH); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            game.pInt.setsprinterpolate(i, spr);
            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            spr.setZ(osec.getFloorz() + (engine.getTile(spr.getPicnum()).getHeight() << 8));
            engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

            if (spr.getLotag() <= 0) {
                if ((spr.getPicnum() >= SPLASHAROO && spr.getPicnum() < LASTSPLASHAROO)
                        || (spr.getPicnum() >= LAVASPLASH && spr.getPicnum() < LASTLAVASPLASH)
                        || (spr.getPicnum() >= SLIMESPLASH && spr.getPicnum() < LASTSLIMESPLASH)) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(8);
                } else {
                    engine.deletesprite(i);
                }
            }
        }

        // SHATTER
        for (ListNode<Sprite> node = boardService.getStatNode(SHATTER); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);

            if (spr.getLotag() < 0) {
                spr.setPicnum(spr.getPicnum() + 1);
                spr.setLotag(12);
            }
            if (spr.getPicnum() == FSHATTERBARREL + 2) {
                engine.changespritestat(i, (short) 0);
            }
        }

        // FIRE
        for (ListNode<Sprite> node = boardService.getStatNode(FIRE); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            spr.setLotag(spr.getLotag() - TICSPERFRAME);

            if (spr.getZ() < osec.getFloorz()) {
                spr.setZ(spr.getZ() + (TICSPERFRAME << 8));
            }
            if (spr.getZ() > osec.getFloorz()) {
                spr.setZ(osec.getFloorz());
            }

            if (spr.getLotag() < 0) {
                switch (spr.getPicnum()) {
                    case LFIRE:
                        spr.setPicnum(SFIRE);
                        spr.setLotag(2047);
                        break;
                    case SFIRE:
                        engine.deletesprite(i);
                        continue;
                }
            }

            if (checkdist(i, plr.getX(), plr.getY(), plr.getZ())) {
                addhealth(plr, -1);
                flashflag = 1;
                startredflash(10);
            }
        }

        // FALL
        for (ListNode<Sprite> node = boardService.getStatNode(FALL); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2,
                    CLIPMASK0);

            if (spr.getZ() < zr_florz) {
                spr.setZvel(spr.getZvel() + (TICSPERFRAME << 9));
            }

            int hitobject = movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                    ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 0);
            engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            if (spr.getPicnum() == FBARRELFALL || spr.getPicnum() >= BOULDER && spr.getPicnum() <= BOULDER + 3
                    && (checkdist(i, plr.getX(), plr.getY(), plr.getZ()))) {
                addhealth(plr, -50);
                startredflash(50);
            }

            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            if ((hitobject & HIT_TYPE_MASK) == HIT_SECTOR) {
                if (osec.getFloorpicnum() == WATER) {
                    makemonstersplash(SPLASHAROO, i);
                }

                if (spr.getPicnum() == FBARRELFALL) {
                    newstatus(i, SHATTER);
                    spr.setLotag(12);
                } else {
                    if (spr.getPicnum() == TORCH) {
                        for (int k = 0; k < 16; k++) {
                            makeafire(i, 0);
                        }
                        engine.deletesprite(i);
                        break;
                    }
                    engine.changespritestat(i, 0);
                }
                spr.setHitag(0);
            }
        }

        // SHOVE
        for (ListNode<Sprite> node = boardService.getStatNode(SHOVE); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2,
                    CLIPMASK0);

            if (spr.getZ() < zr_florz) {
                spr.setZvel(spr.getZvel() + (TICSPERFRAME << 5));
            }

            int hitobject = movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                    ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 0);

            engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            if (spr.getZ() >= osec.getFloorz()) {
                if (osec.getFloorpicnum() == WATER
                        || osec.getFloorpicnum() == FLOORMIRROR) {
                    makemonstersplash(SPLASHAROO, i);
                }
                newstatus(i, BROKENVASE);
                continue;
            }

            if ((hitobject & HIT_TYPE_MASK) == HIT_SECTOR) {
                newstatus(i, BROKENVASE);
                continue;
            }

            if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) { // Bullet hit a sprite
                if (spr.getOwner() != hitobject) {
                    boolean hitdamage = damageactor(plr, hitobject, i);
                    if (hitdamage) {
                        newstatus(i, BROKENVASE);
                    }
                }
            }
        }

        // PUSH
        for (ListNode<Sprite> node = boardService.getStatNode(PUSH); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2,
                    CLIPMASK0);

            if (spr.getZ() < zr_florz) {
                spr.setZvel(spr.getZvel() + (TICSPERFRAME << 1));
            }

            // clip type was 1
            int hitobject = movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                    ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 0);

            engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            if (spr.getLotag() < 0 || (hitobject & HIT_TYPE_MASK) == HIT_WALL) {
                spr.setLotag(0);
                engine.changespritestat(i, 0);
                if (spr.getZ() < osec.getFloorz()) {
                    spr.setZvel(spr.getZvel() + 256);
                    engine.changespritestat(i, FALL);
                }
            }
        }

        // DORMANT
        for (ListNode<Sprite> node = boardService.getStatNode(DORMANT); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();

            if (game.WH2) {
                Sector osec = boardService.getSector(spr.getSectnum());
                if (osec != null) {
                    int j = (torchpattern[lockclock % 38]);
                    osec.setCeilingshade((byte) j);
                    osec.setFloorshade((byte) j);
                    for (ListNode<Wall> wn = osec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade((byte) j);
                    }
                    spr.setLotag(spr.getLotag() - TICSPERFRAME);
                    if (spr.getLotag() < 0) {
                        newstatus(i, ACTIVE);
                    }
                }
            } else {
                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                spr.setXrepeat(2);
                spr.setYrepeat(2);
                if (spr.getLotag() < 0) {
                    newstatus(i, ACTIVE);
                }
            }
        }

        // ACTIVE
        for (ListNode<Sprite> node = boardService.getStatNode(ACTIVE); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            if (game.WH2) {
                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                int j = (torchpattern[lockclock % 38]);
                osec.setCeilingshade((byte) j);
                osec.setFloorshade((byte) j);
                for (ListNode<Wall> wn = osec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setShade((byte) j);
                }
                if (spr.getLotag() < 0) {
                    osec.setCeilingshade(ceilingshadearray[spr.getSectnum()]);
                    osec.setFloorshade(floorshadearray[spr.getSectnum()]);
                    for (ListNode<Wall> wn = osec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade(wallshadearray[wn.getIndex()]);
                    }
                    newstatus(i, DORMANT);
                }
            } else {
                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                spr.setXrepeat(48);
                spr.setYrepeat(32);
                if (spr.getLotag() < 0) {
                    newstatus(i, DORMANT);
                }
            }
        }

        aiProcess();

        // New missile code
        for (ListNode<Sprite> node = boardService.getStatNode(MISSILE); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            spr.setLotag(spr.getLotag() - TICSPERFRAME);

            int daz = spr.getZvel();
            switch (spr.getPicnum()) {
                case WH1THROWPIKE:
                case WH2THROWPIKE:
                case FATSPANK:
                case MONSTERBALL:
                case FIREBALL:
                case PLASMA:
                    if ((spr.getPicnum() == WH1THROWPIKE && game.WH2) || (spr.getPicnum() == WH2THROWPIKE && !game.WH2)) {
                        break;
                    }

                    Sector osec =  boardService.getSector(spr.getSectnum());
                    if (osec == null) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (spr.getPicnum() == MONSTERBALL && engine.krand() % 100 > 90) {
                        if (spr.getLotag() < 200) {
                            trailingsmoke(i, false);
                        }
                    }
                    spr.setZ(spr.getZ() + spr.getZvel());
                    if (spr.getZ() < osec.getCeilingz() + (4 << 8)) {
                        spr.setZ(osec.getCeilingz() + (4 << 8));
                        spr.setZvel((short) -(spr.getZvel() >> 1));
                    }
                    if (spr.getZ() > osec.getFloorz() - (4 << 8)) {
                        spr.setZ(osec.getFloorz() - (4 << 8));
                        if (osec.getFloorpicnum() == WATER || osec.getFloorpicnum() == SLIME
                                || osec.getFloorpicnum() == FLOORMIRROR) {
                            if (spr.getPicnum() == FISH) {
                                spr.setZ(osec.getFloorz());
                            } else {
                                if (engine.krand() % 100 > 60) {
                                    makemonstersplash(SPLASHAROO, i);
                                }
                            }
                        }

//					if (spr.picnum != THROWPIKE) { //XXX
                        engine.deletesprite(i);
                        continue;
//					}
                    }
                    daz = (((spr.getZvel()) * TICSPERFRAME) >> 3);
                    break;
                case BULLET:
                    daz = spr.getZvel();
                    break;
            } // switch

            int hitobject;
            if (spr.getPicnum() == THROWPIKE) {
                spr.setCstat(0);
                hitobject = movesprite(i,
                        ((EngineUtils.sin((spr.getExtra() + 512) & 2047)) * TICSPERFRAME) << 6,
                        ((EngineUtils.sin(spr.getExtra() & 2047)) * TICSPERFRAME) << 6, daz, 4 << 8, 4 << 8, 1);
                spr.setCstat(21);
            } else {
                hitobject = movesprite(i,
                        ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 6, // was 3
                        ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 6, // was 3
                        daz, 4 << 8, 4 << 8, 1);
            }

            if (hitobject != 0 && spr.getPicnum() == MONSTERBALL) {
                if (spr.getOwner() == plr.getSprite().getOwner()) {
                    explosion2(i, spr.getX(), spr.getY(), spr.getZ(), i);
                } else {
                    explosion(i, spr.getX(), spr.getY(), spr.getZ(), i);
                }
            }

            if ((hitobject & HIT_TYPE_MASK) == HIT_SECTOR) { // Hits a ceiling / floor
                if (spr.getPicnum() == THROWPIKE) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setDetail(WALLPIKETYPE);
                    engine.changespritestat(i, (short) 0);

                    continue;
                }
                engine.deletesprite(i);
                continue;
            } else if ((hitobject & HIT_TYPE_MASK) == HIT_WALL) { // hit a wall

                if (spr.getPicnum() == MONSTERBALL) {
                    if (spr.getOwner() == plr.getSprite().getOwner()) {
                        explosion2(i, spr.getX(), spr.getY(), spr.getZ(), i);
                    } else {
                        explosion(i, spr.getX(), spr.getY(), spr.getZ(), i);
                    }
                }
                if (spr.getPicnum() == THROWPIKE) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setDetail(WALLPIKETYPE);
                    engine.changespritestat(i, (short) 0);

                    continue;
                }
                engine.deletesprite(i);
                continue;
            } else if (spr.getLotag() < 0 && spr.getPicnum() == PLASMA) {
                hitobject = 1;
            }

            if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) { // Bullet hit a sprite
                if (spr.getPicnum() == MONSTERBALL) {
                    if (spr.getOwner() == plr.getSprite().getOwner()) {
                        explosion2(i, spr.getX(), spr.getY(), spr.getZ(), i);
                    } else {
                        explosion(i, spr.getX(), spr.getY(), spr.getZ(), i);
                    }
                }

                if (spr.getOwner() != hitobject) {
                    boolean hitdamage = damageactor(plr, hitobject, i);
                    if (hitdamage) {
                        engine.deletesprite(i);
                        continue;
                    }
                }
            }

            if (hitobject != 0 || spr.getLotag() < 0) {
                int pic = spr.getPicnum();
                switch (pic) {
                    case PLASMA:
                    case FATSPANK:
                    case MONSTERBALL:
                    case FIREBALL:
                    case BULLET:
                    case WH1THROWPIKE:
                    case WH2THROWPIKE:
                        if ((spr.getPicnum() == WH1THROWPIKE && game.WH2)
                                || (spr.getPicnum() == WH2THROWPIKE && !game.WH2)) {
                            break;
                        }

                        engine.deletesprite(i);
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(JAVLIN); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            int daz = spr.getZvel();
            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (isBlades(spr.getPicnum())) {
                spr.setZ(spr.getZ() - spr.getZvel());
                if (spr.getZ() < osec.getCeilingz() + (4 << 8)) {
                    spr.setZ(osec.getCeilingz() + (4 << 8));
                    spr.setZvel((short) -(spr.getZvel() >> 1));
                }
                if (spr.getZ() > osec.getFloorz() - (4 << 8)) {
                    spr.setZ(osec.getFloorz() - (4 << 8));
                    if (osec.getFloorpicnum() == WATER || osec.getFloorpicnum() == SLIME
                            || osec.getFloorpicnum() == FLOORMIRROR) {
                        if (engine.krand() % 100 > 60) {
                            makemonstersplash(SPLASHAROO, i);
                        }
                    }
                    engine.deletesprite(i);
                    continue;
                }

                if (game.WH2) {
                    daz = spr.getZvel();
                } else {
                    daz = (((spr.getZvel()) * TICSPERFRAME) >> 3);
                }
            }

            spr.setCstat(0);

            int hitobject = movesprite(i, ((EngineUtils.sin((spr.getExtra() + 512) & 2047)) * TICSPERFRAME) << 6,
                    ((EngineUtils.sin(spr.getExtra() & 2047)) * TICSPERFRAME) << 6, daz, 4 << 8, 4 << 8, 0);

            osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            if (spr.getPicnum() == WALLARROW || spr.getPicnum() == THROWHALBERD) {
                spr.setCstat(0x11);
            } else if (spr.getPicnum() == DART) {
                spr.setCstat(0x10);
            } else {
                spr.setCstat(0x15);
            }

            if ((hitobject & HIT_TYPE_MASK) == HIT_SECTOR) { // Hits a ceiling / floor
                // EG Bugfix 17 Aug 2014: Since the game thinks that a javlin hitting the
                // player's pike axe is a
                // floor/ceiling hit rather than a sprite hit, we'll need to check if the JAVLIN
                // is
                // actually in the floor/ceiling before going inactive.
                if (spr.getZ() <= osec.getCeilingz()
                        && spr.getZ() >= osec.getFloorz()) {
                    if (spr.getPicnum() == THROWPIKE) {
                        spr.setPicnum(spr.getPicnum() + 1);
                        spr.setDetail(WALLPIKETYPE);
                    }

                    engine.changespritestat(i, INACTIVE); // EG Note: RAF.H gives this a nice name, so use it
                }
                continue;
            } else if ((hitobject & HIT_TYPE_MASK) == HIT_WALL) { // hit a wall

                if (spr.getPicnum() == THROWPIKE) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setDetail(WALLPIKETYPE);
                }

                engine.changespritestat(i, INACTIVE);
                continue;
            }

            if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) { // Bullet hit a sprite
                int j = (hitobject & HIT_INDEX_MASK); // j is the spritenum that the bullet (spritenum i) hit
                Sprite spr2 = boardService.getSprite(j);

                boolean hitdamage = damageactor(plr, hitobject, i);
                if (hitdamage) {
                    continue;
                }

                if (spr2 != null && isBlades(spr2.getPicnum())) {
                    engine.deletesprite(i);
                    continue;
                }
            }

            if (hitobject != 0) {
                engine.deletesprite(i);
            }
        }

        // CHUNK O WALL

        for (ListNode<Sprite> node = boardService.getStatNode(CHUNKOWALL); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            spr.setZvel(spr.getZvel() + (TICSPERFRAME << 2));
            movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                    ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 1);
            engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            if (spr.getExtra() == 0) {
                if (spr.getLotag() < 0) {
                    spr.setLotag(8);
                    spr.setPicnum(spr.getPicnum() + 1);
                    if (spr.getPicnum() == SMOKEFX + 3) {
                        engine.deletesprite(i);
                    }
                }
            } else {
                if (spr.getLotag() < 0) {
                    engine.deletesprite(i);
                }
            }
        }

        // CHUNK O MEAT
        for (ListNode<Sprite> node = boardService.getStatNode(CHUNKOMEAT); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);

            spr.setZ(spr.getZ() + spr.getZvel());

            spr.setZvel(spr.getZvel() + (TICSPERFRAME << 4));
            int daz = spr.getZvel();
            int xvel = ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3;
            int yvel = ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3;

            if (spr.getPicnum() == BONECHUNK1 && spr.getPicnum() == BONECHUNKEND) {
                daz >>= 1;
                xvel >>= 1;
                yvel >>= 1;
            }

            int movestat = movesprite(i, xvel, yvel, daz, 4 << 8, 4 << 8, 1);
            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            if ((movestat & HIT_TYPE_MASK) == HIT_SECTOR) {
                if (osec.getFloorpicnum() == WATER || osec.getFloorpicnum() == SLIME
                        || osec.getFloorpicnum() == FLOORMIRROR) {
                    if (spr.getPicnum() == FISH) {
                        spr.setZ(osec.getFloorz());
                    } else {
                        if (engine.krand() % 100 > 60) {
                            makemonstersplash(SPLASHAROO, i);
                        }
                    }
                    spr.setLotag(-1);
                } else {
                    /* EG: Add check for parallax sky */
                    if (spr.getPicnum() >= BONECHUNK1 && spr.getPicnum() <= BONECHUNKEND
                            || (daz >= zr_ceilz && (osec.getCeilingstat() & 1) != 0)) {
                        engine.deletesprite(i);
                    } else {
                        spr.setCstat(spr.getCstat() | 0x0020);
                        spr.setLotag(1200);
                        newstatus(i, BLOOD);
                    }
                }
            } else if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                if (spr.getPicnum() >= BONECHUNK1 && spr.getPicnum() <= BONECHUNKEND) {
                    engine.deletesprite(i);
                } else {
                    spr.setLotag(600);
                    newstatus(i, DRIP);
                }
            }

            if (spr.getLotag() < 0) {
                engine.deletesprite(i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(BLOOD); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                if (spr.getZ() < osec.getFloorz()) {
                    spr.setLotag(600);
                    spr.setZvel(0);
                    newstatus(i, DRIP);
                } else {
                    engine.deletesprite(i);
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(DEVILFIRE); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();

            if (plr.getInvisibletime() < 0) {
                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag((short) (engine.krand() & 120 + 360));
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                            spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                        // JSA_NEW
                        playsound_loc(S_FIREBALL, spr.getX(), spr.getY());
                        castspell(plr, i);
                    }
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(DRIP); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();



            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            spr.setZ(spr.getZ() + spr.getZvel());
            int dax = 0;
            int day = 0;
            spr.setZvel(spr.getZvel() + (TICSPERFRAME << 1));
            int daz = (((spr.getZvel()) * TICSPERFRAME) << 1);
            int movestat = movesprite(i, dax, day, daz, 4 << 8, 4 << 8, 1);

            if ((movestat & HIT_TYPE_MASK) == HIT_SECTOR) {
                spr.setLotag(1200);
                newstatus(i, BLOOD);
            }
            if (spr.getLotag() < 0) {
                engine.deletesprite(i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(SMOKE); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getXrepeat() >= TICSPERFRAME) { // 09.06.2024 was spr.xrepeat > 1
                int size = spr.getXrepeat() - TICSPERFRAME;
                spr.setXrepeat(size);
                spr.setYrepeat(size);
            }

            if (spr.getLotag() < 0) {
                engine.deletesprite(i);
            }
        }

        if (!game.WH2) {
            for (ListNode<Sprite> node = boardService.getStatNode(EXPLO); node != null; node = next) {
                next = node.getNext();
                final int i = node.getIndex();
                Sprite spr = node.get();
                Sector osec =  boardService.getSector(spr.getSectnum());
                if (osec == null) {
                    continue;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                spr.setX(spr.getX() + ((spr.getXvel() * TICSPERFRAME) >> 5));
                spr.setY(spr.getY() + ((spr.getYvel() * TICSPERFRAME) >> 5));
                spr.setZ(spr.getZ() - ((spr.getZvel() * TICSPERFRAME) >> 6));

                spr.setZvel(spr.getZvel() + (TICSPERFRAME << 4));

                if (spr.getZ() < osec.getCeilingz() + (4 << 8)) {
                    spr.setZ(osec.getCeilingz() + (4 << 8));
                    spr.setZvel((short) -(spr.getZvel() >> 1));
                }
                if (spr.getZ() > osec.getFloorz() - (4 << 8)) {
                    spr.setZ(osec.getFloorz() - (4 << 8));
                    spr.setZvel((short) -(spr.getZvel() >> 1));
                }

                spr.setXrepeat(spr.getXrepeat() + TICSPERFRAME);
                spr.setYrepeat(spr.getYrepeat() + TICSPERFRAME);

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (engine.krand() % 100 > 90) {
                    int j = engine.insertsprite(spr.getSectnum(), SMOKE);
                    Sprite spr2 = boardService.getSprite(j);
                    if (spr2 != null) {
                        spr2.setX(spr.getX());
                        spr2.setY(spr.getY());
                        spr2.setZ(spr.getZ());
                        spr2.setCstat(0x03);
                        spr2.setCstat(spr2.getCstat() & ~3);
                        spr2.setPicnum(SMOKEFX);
                        spr2.setShade(0);
                        spr2.setPal(0);
                        spr2.setXrepeat(spr.getXrepeat());
                        spr2.setYrepeat(spr.getYrepeat());

                        spr2.setOwner(spr.getOwner());
                        spr2.setLotag(256);
                        spr2.setHitag(0);
                    }
                }

                if (spr.getLotag() < 0) {
                    engine.deletesprite(i);
                }
            }
        } else {
            for (ListNode<Sprite> node = boardService.getStatNode(EXPLO); node != null; node = next) {
                next = node.getNext();
                final int i = node.getIndex();
                Sprite spr = node.get();

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                spr.setPicnum(spr.getPicnum() + 1);
                if (spr.getLotag() < 0) {
                    spr.setLotag(12);
                }

                ListNode<Sprite> sectNode = boardService.getSectNode(spr.getSectnum());
                while (sectNode != null) {
                    ListNode<Sprite> nextSect = sectNode.getNext();

                    Sprite tspr = sectNode.get();
                    long dx = klabs(spr.getX() - tspr.getX()); // x distance to sprite
                    long dy = klabs(spr.getY() - tspr.getY()); // y distance to sprite
                    long dz = klabs((spr.getZ() >> 8) - (tspr.getZ() >> 8)); // z distance to sprite
                    int dh = engine.getTile(tspr.getPicnum()).getHeight() >> 1; // height of sprite
                    if (dx + dy < PICKDISTANCE && dz - dh <= getPickHeight()) {
                        if (tspr.getOwner() != 4096) {
                            switch (tspr.getDetail()) {
                                case SKELETONTYPE:
                                case KOBOLDTYPE:
                                case IMPTYPE:
                                case NEWGUYTYPE:
                                case KURTTYPE:
                                case GONZOTYPE:
                                case GRONTYPE:
                                    if (tspr.getHitag() > 0) {
                                        tspr.setHitag(tspr.getHitag() - (TICSPERFRAME << 4));
                                        if (tspr.getHitag() < 0) {
                                            newstatus(sectNode.getIndex(), DIE);
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    sectNode = nextSect;
                }

                if (spr.getPicnum() == EXPLOEND) {
                    engine.deletesprite(i);
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(BROKENVASE); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                spr.setPicnum(spr.getPicnum() + 1);
                spr.setLotag(18);

                if (spr.getPicnum() == (SHATTERVASE + 6) || spr.getPicnum() == (SHATTERVASE2 + 6)
                        || spr.getPicnum() == (SHATTERVASE3 + 6)) {
                    engine.changespritestat(i, (short) 0);
                } else {
                    switch (spr.getPicnum()) {
                        case FSHATTERBARREL + 2:
                            randompotion(i);
                            engine.changespritestat(i, (short) 0);
                            break;
                        case STAINGLASS1 + 6:
                        case STAINGLASS2 + 6:
                        case STAINGLASS3 + 6:
                        case STAINGLASS4 + 6:
                        case STAINGLASS5 + 6:
                        case STAINGLASS6 + 6:
                        case STAINGLASS7 + 6:
                        case STAINGLASS8 + 6:
                        case STAINGLASS9 + 6:
                            engine.changespritestat(i, (short) 0);
                            break;
                    }
                }
            }
        }

        // Go through explosion sprites
        for (ListNode<Sprite> node = boardService.getStatNode(FX); node != null; node = next) {
            next = node.getNext();
            final int i = node.getIndex();
            Sprite spr = node.get();
            Sector osec =  boardService.getSector(spr.getSectnum());
            if (osec == null) {
                continue;
            }

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (// spr.picnum == PLASMA ||
                    spr.getPicnum() == BULLET || spr.getPicnum() == EXPLOSION || spr.getPicnum() == FIREBALL
                            || spr.getPicnum() == MONSTERBALL || spr.getPicnum() == FATSPANK) {

                // spr.z+=spr.zvel;
                spr.setZvel(spr.getZvel() + (TICSPERFRAME << 5));
                if (spr.getZ() < osec.getCeilingz() + (4 << 8)) {
                    spr.setZ(osec.getCeilingz() + (4 << 8));
                    spr.setZvel((short) -(spr.getZvel() >> 1));
                }
                if (spr.getZ() > osec.getFloorz() - (4 << 8) && spr.getPicnum() != EXPLOSION) {
                    spr.setZ(osec.getFloorz() - (4 << 8));
                    spr.setZvel(0);
                    spr.setLotag(4);
                }
                int dax = (((spr.getXvel()) * TICSPERFRAME) >> 3);
                int day = (((spr.getYvel()) * TICSPERFRAME) >> 3);
                int daz = ((spr.getZvel()) * TICSPERFRAME);
                movesprite(i, dax, day, daz, 4 << 8, 4 << 8, 1);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                osec =  boardService.getSector(spr.getSectnum());
                if (osec == null) {
                    continue;
                }
            }

            int movestat = 0;
            if (spr.getPicnum() == ICECUBE && spr.getZ() < osec.getFloorz()) {
                spr.setZ(spr.getZ() + spr.getZvel());

                spr.setZvel(spr.getZvel() + (TICSPERFRAME << 4));
                movestat = movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                        ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 1);
                osec =  boardService.getSector(spr.getSectnum());
                if (osec == null) {
                    continue;
                }
            }

            if (spr.getLotag() < 0 || movestat != 0) {
                if (spr.getPicnum() == PLASMA || spr.getPicnum() == EXPLOSION || spr.getPicnum() == FIREBALL
                        || spr.getPicnum() == MONSTERBALL || spr.getPicnum() == FATSPANK
                        || spr.getPicnum() == ICECUBE) {
                    engine.deletesprite(i);
                    continue;
                }
            }

            if (spr.getZ() + (8 << 8) >= osec.getFloorz() && spr.getPicnum() == ICECUBE
                    || movestat != 0) {
                spr.setZ(osec.getFloorz());
                engine.changespritestat(i, (short) 0);
                if (osec.getFloorpicnum() == WATER || osec.getFloorpicnum() == SLIME
                        || osec.getFloorpicnum() == FLOORMIRROR) {
                    if (spr.getPicnum() == FISH) {
                        spr.setZ(osec.getFloorz());
                    } else {
                        if (engine.krand() % 100 > 60) {
                            makemonstersplash(SPLASHAROO, i);
                            engine.deletesprite(i);
                        }
                    }
                } else {
                    if (spr.getLotag() < 0) {
                        engine.deletesprite(i);
                    }
                }
            }
        }
    }

    public static boolean isBlades(int pic) {
        return pic == THROWPIKE || pic == WALLARROW || pic == DART || pic == HORIZSPIKEBLADE || pic == THROWHALBERD;
    }
}
