package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.FileEntry;

import static ru.m210projects.Witchaven.Menu.WHMenuUserContent.episodeManager;

public interface EpisodeEntry {

    FileEntry getFileEntry(); // directory entry file or group (parent group) when map files placed

    Group getGroup();

    class Addon extends FileEntry implements EpisodeEntry {

        private final Group group;
        private final FileEntry entry;

        public Addon(Group group, FileEntry groupEntry) {
            super(groupEntry);
            this.group = group;
            this.entry = groupEntry;
        }

        @Override
        public String getName() {
            EpisodeInfo gameInfo = episodeManager.getEpisode(this);
            if (gameInfo != null) {
                return "Addon:" + gameInfo.Title;
            }
            return "Broken entry";
        }

        @Override
        public FileEntry getFileEntry() {
            return entry;
        }

        @Override
        public Group getGroup() {
            return group;
        }

    }
}
