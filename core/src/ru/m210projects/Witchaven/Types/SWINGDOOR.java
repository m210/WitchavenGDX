package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

public class SWINGDOOR {
    public final int[] wall = new int[8];
    public int sector;
    public int angopen;
    public int angclosed;
    public int angopendir;
    public int ang;
    public int anginc;
    public final int[] x = new int[8];
    public final int[] y = new int[8];

    public void copy(SWINGDOOR src) {
        System.arraycopy(src.wall, 0, wall, 0, 8);

        sector = src.sector;
        angopen = src.angopen;
        angclosed = src.angclosed;
        angopendir = src.angopendir;
        ang = src.ang;
        anginc = src.anginc;

        System.arraycopy(src.x, 0, x, 0, 8);
        System.arraycopy(src.y, 0, y, 0, 8);
    }

    public void set(InputStream is) throws IOException {
        for (int j = 0; j < 8; j++) {
            wall[j] = StreamUtils.readShort(is);
        }
        sector = StreamUtils.readShort(is);
        angopen = StreamUtils.readShort(is);
        angclosed = StreamUtils.readShort(is);
        angopendir = StreamUtils.readShort(is);
        ang = StreamUtils.readShort(is);
        anginc = StreamUtils.readShort(is);
        for (int j = 0; j < 8; j++) {
            x[j] = StreamUtils.readInt(is);
        }
        for (int j = 0; j < 8; j++) {
            y[j] = StreamUtils.readInt(is);
        }
    }
}
