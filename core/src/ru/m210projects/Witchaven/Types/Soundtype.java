package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.Architecture.common.audio.Source;
import ru.m210projects.Build.Architecture.common.audio.SourceListener;

public class Soundtype implements SourceListener {
    public Source hVoice;
    public int priority;
    public int sndnum;
    public int x, y;
    public float loop;

    @Override
    public void onStop() {
        sndnum = -1;
        priority = 0;
        x = 0;
        y = 0;

        if (hVoice == null) {
            return;
        }

        hVoice.stop();
        hVoice = null;
        loop = 0;
    }
}
