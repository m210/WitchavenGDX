// This file is part of WitchavenGDX.
// Copyright (C) 2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// LSPGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// LSPGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with WitchavenGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Witchaven.Main;
import ru.m210projects.Witchaven.Screens.DemoScreen;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static java.lang.Math.min;
import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.game;
import static ru.m210projects.Witchaven.Menu.WHMenuUserContent.episodeManager;
import static ru.m210projects.Witchaven.Potions.MAXPOTIONS;
import static ru.m210projects.Witchaven.Weapons.MAXWEAPONS;

public class DemoFile {

    public int rcnt;
    public Input[][] recsync;

    public int reccnt;
    public int version;
    public int map, skill;
    public PlayerDemoData playerData;
    public Main.UserFlag userFlag;
    public EpisodeInfo addon;

    public DemoFile(InputStream is) throws Exception {
        rcnt = 0;
        addon = null;

        String header = StreamUtils.readString(is, DemoScreen.header.length());
        if (!header.equalsIgnoreCase(DemoScreen.header)) {
            throw new Exception("Wrong header: " + header);
        }

        reccnt = StreamUtils.readInt(is);
        version = StreamUtils.readUnsignedByte(is);

        if (version != GDXBYTEVERSION && version != GDXCURRENTBYTEVERSION) {
            throw new Exception("Wrong version!");
        }

        if (reccnt == 0) {
            throw new Exception("reccnt == 0");
        }

        userFlag = Main.UserFlag.parseValue(StreamUtils.readByte(is));
        if (userFlag == Main.UserFlag.Addon) {
            String addonFileName = StreamUtils.readDataString(is).toLowerCase();
            addon = episodeManager.getEpisode(game.getCache().getGameDirectory().getEntry(FileUtils.getPath(addonFileName)));
        }

        map = StreamUtils.readUnsignedByte(is);
        skill = StreamUtils.readUnsignedByte(is);

        playerData = new PlayerDemoData();
        playerData.readObject(is);

        recsync = new Input[reccnt][MAXPLAYERS];
        int rccnt = 0;
        for (int c = 0; c <= reccnt / RECSYNCBUFSIZ; c++) {
            int l = min(reccnt - rccnt, RECSYNCBUFSIZ);

            for (int rcnt = rccnt; rcnt < rccnt + l; rcnt++) {
                for (int i = 0; i < 1; i++) {
                    recsync[rcnt][i] = new Input().readObject(is);
                }
            }
            rccnt += RECSYNCBUFSIZ;
        }
    }

    public static class PlayerDemoData {

        private final int[] weapon = new int[MAXWEAPONS];
        private final int[] ammo = new int[MAXWEAPONS];
        private final int[] preenchantedweapon = new int[MAXWEAPONS];
        private final int[] preenchantedammo = new int[MAXWEAPONS];
        private final int[] potion = new int[MAXPOTIONS]; // bottles
        private final int[] treasure = new int[MAXTREASURES]; // rings (and keys here)
        private final int[] orb = new int[MAXNUMORBS]; // scrolls
        private final int[] orbammo = new int[MAXNUMORBS]; // scroll count

        private int lvl;
        private int score;
        private int health;
        private int maxhealth;
        private int armor; // armor amount
        private int armortype;
        private int shieldpoints; // shield amount
        private int shieldtype; // shield visual model
        private int poisoned;

        public void readObject(InputStream is) throws IOException {
            for (int i = 0; i < MAXWEAPONS; i++) {
                this.weapon[i] = StreamUtils.readInt(is);
                this.ammo[i] = StreamUtils.readInt(is);
                this.preenchantedweapon[i] = StreamUtils.readInt(is);
                this.preenchantedammo[i] = StreamUtils.readInt(is);
            }

            for (int i = 0; i < MAXPOTIONS; i++) {
                this.potion[i] = StreamUtils.readInt(is);
            }

            for (int i = 0; i < MAXTREASURES; i++) {
                this.treasure[i] = StreamUtils.readInt(is);
            }

            for (int i = 0; i < MAXNUMORBS; i++) {
                this.orb[i] = StreamUtils.readInt(is);
                this.orbammo[i] = StreamUtils.readInt(is);
            }

            this.lvl = StreamUtils.readInt(is);
            this.score = StreamUtils.readInt(is);
            this.health = StreamUtils.readInt(is);
            this.maxhealth = StreamUtils.readInt(is);
            this.armor = StreamUtils.readInt(is);
            this.armortype = StreamUtils.readInt(is);
            this.shieldpoints = StreamUtils.readInt(is);
            this.shieldtype = StreamUtils.readInt(is);
            this.poisoned = StreamUtils.readInt(is);
        }

        public void writeObject(OutputStream os) throws IOException {
            for (int i = 0; i < MAXWEAPONS; i++) {
                StreamUtils.writeInt(os, this.weapon[i]);
                StreamUtils.writeInt(os, this.ammo[i]);
                StreamUtils.writeInt(os, this.preenchantedweapon[i]);
                StreamUtils.writeInt(os, this.preenchantedammo[i]);
            }

            for (int i = 0; i < MAXPOTIONS; i++) {
                StreamUtils.writeInt(os, this.potion[i]);
            }

            for (int i = 0; i < MAXTREASURES; i++) {
                StreamUtils.writeInt(os, this.treasure[i]);
            }

            for (int i = 0; i < MAXNUMORBS; i++) {
                StreamUtils.writeInt(os, this.orb[i]);
                StreamUtils.writeInt(os, this.orbammo[i]);
            }

            StreamUtils.writeInt(os, this.lvl);
            StreamUtils.writeInt(os, this.score);
            StreamUtils.writeInt(os, this.health);
            StreamUtils.writeInt(os, this.maxhealth);
            StreamUtils.writeInt(os, this.armor);
            StreamUtils.writeInt(os, this.armortype);
            StreamUtils.writeInt(os, this.shieldpoints);
            StreamUtils.writeInt(os, this.shieldtype);
            StreamUtils.writeInt(os, this.poisoned);
        }

        public PlayerDemoData setFrom(PLAYER plr) {
            System.arraycopy(plr.getWeapon(), 0, this.weapon, 0, MAXWEAPONS);
            System.arraycopy(plr.getAmmo(), 0, this.ammo, 0, MAXWEAPONS);
            System.arraycopy(plr.getPreenchantedweapon(), 0, this.preenchantedweapon, 0, MAXWEAPONS);
            System.arraycopy(plr.getPreenchantedammo(), 0, this.preenchantedammo, 0, MAXWEAPONS);
            System.arraycopy(plr.getPotion(), 0, this.potion, 0, MAXPOTIONS);
            System.arraycopy(plr.getTreasure(), 0, this.treasure, 0, MAXTREASURES);
            System.arraycopy(plr.getOrb(), 0, this.orb, 0, MAXNUMORBS);
            System.arraycopy(plr.getOrbammo(), 0, this.orbammo, 0, MAXNUMORBS);

            this.lvl = plr.getLvl();
            this.score = plr.getScore();
            this.health = plr.getHealth();
            this.maxhealth = plr.getMaxhealth();
            this.armor = plr.getArmor();
            this.armortype = plr.getArmortype();
            this.shieldpoints = plr.getShieldpoints();
            this.shieldtype = plr.getShieldtype();
            this.poisoned = plr.getPoisoned();

            return this;
        }

        public void applyTo(PLAYER plr) {
            System.arraycopy(this.weapon, 0, plr.getWeapon(), 0, MAXWEAPONS);
            System.arraycopy(this.ammo, 0, plr.getAmmo(), 0, MAXWEAPONS);
            System.arraycopy(this.preenchantedweapon, 0, plr.getPreenchantedweapon(), 0, MAXWEAPONS);
            System.arraycopy(this.preenchantedammo, 0, plr.getPreenchantedammo(), 0, MAXWEAPONS);
            System.arraycopy(this.potion, 0, plr.getPotion(), 0, MAXPOTIONS);
            System.arraycopy(this.treasure, 0, plr.getTreasure(), 0, MAXTREASURES);
            System.arraycopy(this.orb, 0, plr.getOrb(), 0, MAXNUMORBS);
            System.arraycopy(this.orbammo, 0, plr.getOrbammo(), 0, MAXNUMORBS);

            plr.setLvl(this.lvl);
            plr.setScore(this.score);
            plr.setHealth(this.health);
            plr.setMaxhealth(this.maxhealth);
            plr.setArmor(this.armor);
            plr.setArmortype(this.armortype);
            plr.setShieldpoints(this.shieldpoints);
            plr.setShieldtype(this.shieldtype);
            plr.setPoisoned(this.poisoned);
        }
    }
}
