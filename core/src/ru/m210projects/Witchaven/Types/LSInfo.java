package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

public class LSInfo {
    public int level;
    public int skill;
    public String info;
    public String date;
    public String addonfile;

    public void read(InputStream is) throws IOException {
        level = StreamUtils.readInt(is);
        skill = StreamUtils.readInt(is);
        update();
    }

    public void update() {
        info = "Map:" + level + " / Skill:" + skill;
    }

    public void clear() {
        skill = 0;
        level = 0;
        info = "Empty slot";
        date = null;
        addonfile = null;
    }
}
