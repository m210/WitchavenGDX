package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.BitMap;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Witchaven.Animate.MAXANIMATES;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.game;
import static ru.m210projects.Witchaven.Menu.WHMenuUserContent.episodeManager;
import static ru.m210projects.Witchaven.Whldsv.*;

public class SafeLoader {

    public Entry boardfilename;

    public int gAnimationCount = 0;
    public final ANIMATION[] gAnimationData = new ANIMATION[MAXANIMATES];
    public final PLAYER[] plr = new PLAYER[MAXPLAYERS];

    public final short[] pskyoff = new short[MAXPSKYTILES];
    public short pskybits;
    public int parallaxyscale;

    public short connecthead;
    public final short[] connectpoint2 = new short[MAXPLAYERS];

    public int randomseed;

    //    public boolean gUserMap;
    public int mapon, skill;

    public int thunderflash;
    public int thundertime;

    public int kills;
    public int killcnt;
    public int treasurescnt;
    public int treasuresfound;
    public int hours, minutes, seconds;

    public int visibility;
    public byte automapping;
    public final BitMap show2dsector = new BitMap(MAXSECTORSV7);
    public final BitMap show2dwall = new BitMap(MAXWALLSV7);
    public final BitMap show2dsprite = new BitMap(MAXSPRITESV7);

    //MapInfo

    public Sector[] sector;
    public Wall[] wall;
    public List<Sprite> sprite;

    //WHFX

    public final short[] skypanlist = new short[64];
    public short skypancnt;
    public final short[] lavadrylandsector = new short[32];
    public short lavadrylandcnt;
    public final short[] bobbingsectorlist = new short[16];
    public short bobbingsectorcnt;

    public final short[] revolveclip = new short[16];
    public final short[] revolvesector = new short[4];
    public final short[] revolveang = new short[4];
    public short revolvecnt;
    public final int[][] revolvex = new int[4][32];
    public final int[][] revolvey = new int[4][32];
    public final int[] revolvepivotx = new int[4];
    public final int[] revolvepivoty = new int[4];


    //WHTAG

    public final short[] warpsectorlist = new short[64];
    public short warpsectorcnt;
    public final short[] xpanningsectorlist = new short[16];
    public short xpanningsectorcnt;
    public final short[] ypanningwalllist = new short[128];
    public short ypanningwallcnt;
    public final short[] floorpanninglist = new short[64];
    public short floorpanningcnt;
    public final SWINGDOOR[] swingdoor = new SWINGDOOR[MAXSWINGDOORS];
    public short swingcnt;

    public final short[] dragsectorlist = new short[16];
    public final short[] dragxdir = new short[16];
    public final short[] dragydir = new short[16];
    public short dragsectorcnt;
    public final int[] dragx1 = new int[16];
    public final int[] dragy1 = new int[16];
    public final int[] dragx2 = new int[16];
    public final int[] dragy2 = new int[16];
    public final int[] dragfloorz = new int[16];

    public final short[] ironbarsector = new short[16];
    public short ironbarscnt;
    public final int[] ironbarsgoal1 = new int[16];
    public final int[] ironbarsgoal2 = new int[16];
    public final short[] ironbarsdone = new short[16];
    public final short[] ironbarsanim = new short[16];
    public final int[] ironbarsgoal = new int[16];

    public int floormirrorcnt;
    public final short[] floormirrorsector = new short[64];
    public final short[] arrowsprite = new short[ARROWCOUNTLIMIT];
    public final short[] throwpikesprite = new short[THROWPIKELIMIT];
    public final byte[] ceilingshadearray = new byte[MAXSECTORS];
    public final byte[] floorshadearray = new byte[MAXSECTORS];
    public final byte[] wallshadearray = new byte[MAXWALLS];

    public int dropshieldcnt;
    public boolean droptheshield;

    public int warp_on;

    public EpisodeInfo addon;
    public String addonFileName;
    private String message;

    public SafeLoader() {
        for (int i = 0; i < MAXPLAYERS; i++) {
            plr[i] = new PLAYER();
        }

        for (int i = 0; i < MAXANIMATES; i++) {
            gAnimationData[i] = new ANIMATION();
        }

        for (int i = 0; i < MAXSWINGDOORS; i++) {
            swingdoor[i] = new SWINGDOOR();
        }
    }

    public boolean load(InputStream is) throws IOException {
        StreamUtils.skip(is, SAVETIME + SAVENAME);

        addon = null;
        addonFileName = null;
        message = null;

        mapon = StreamUtils.readInt(is);
        skill = StreamUtils.readInt(is);

        LoadGDXBlock(is);
        StuffLoad(is);
        MapLoad(is);
        SectorLoad(is);
        AnimationLoad(is);

        if (is.available() != 0) {
            return false;
        }


        if (warp_on == 2) { // try to find addon
            addon = findAddon(addonFileName);

            if (addon == null) {
                message = "Can't find user episode file: " + boardfilename;
                warp_on = 1;

                mapon = 0;
            }
        }

        return true;
    }

    public void SectorLoad(InputStream is) throws IOException {
        for (int i = 0; i < 64; i++) {
            skypanlist[i] = StreamUtils.readShort(is);
        }
        skypancnt = StreamUtils.readShort(is);
        for (int i = 0; i < 32; i++) {
            lavadrylandsector[i] = StreamUtils.readShort(is);
        }
        lavadrylandcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            dragsectorlist[i] = StreamUtils.readShort(is);
            dragxdir[i] = StreamUtils.readShort(is);
            dragydir[i] = StreamUtils.readShort(is);
            dragx1[i] = StreamUtils.readInt(is);
            dragy1[i] = StreamUtils.readInt(is);
            dragx2[i] = StreamUtils.readInt(is);
            dragy2[i] = StreamUtils.readInt(is);
            dragfloorz[i] = StreamUtils.readInt(is);
        }
        dragsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            bobbingsectorlist[i] = StreamUtils.readShort(is);
        }
        bobbingsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            warpsectorlist[i] = StreamUtils.readShort(is);
        }
        warpsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            xpanningsectorlist[i] = StreamUtils.readShort(is);
        }
        xpanningsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 128; i++) {
            ypanningwalllist[i] = StreamUtils.readShort(is);
        }
        ypanningwallcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 64; i++) {
            floorpanninglist[i] = StreamUtils.readShort(is);
        }
        floorpanningcnt = StreamUtils.readShort(is);

        swingcnt = StreamUtils.readShort(is);
        for (int i = 0; i < MAXSWINGDOORS; i++) {
            swingdoor[i].set(is);
        }

        revolvecnt = StreamUtils.readShort(is);
        for (int i = 0; i < 4; i++) {
            revolvesector[i] = StreamUtils.readShort(is);
            revolveang[i] = StreamUtils.readShort(is);
            for (int j = 0; j < 32; j++) {
                revolvex[i][j] = StreamUtils.readInt(is);
                revolvey[i][j] = StreamUtils.readInt(is);
            }
            revolvepivotx[i] = StreamUtils.readInt(is);
            revolvepivoty[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 16; i++) {
            revolveclip[i] = StreamUtils.readShort(is);
        }

        ironbarscnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            ironbarsector[i] = StreamUtils.readShort(is);
            ironbarsgoal[i] = StreamUtils.readInt(is);
            ironbarsgoal1[i] = StreamUtils.readInt(is);
            ironbarsgoal2[i] = StreamUtils.readInt(is);
            ironbarsdone[i] = StreamUtils.readShort(is);
            ironbarsanim[i] = StreamUtils.readShort(is);
        }
    }

    public void AnimationLoad(InputStream is) throws IOException {
        for (int i = 0; i < MAXANIMATES; i++) {
            short index = StreamUtils.readShort(is);
            byte type = StreamUtils.readByte(is);
            gAnimationData[i].id = index;
            gAnimationData[i].type = type;
            gAnimationData[i].ptr = null;
            gAnimationData[i].goal = StreamUtils.readInt(is);
            gAnimationData[i].vel = StreamUtils.readInt(is);
            gAnimationData[i].acc = StreamUtils.readInt(is);
        }
        gAnimationCount = StreamUtils.readInt(is);
    }

    public void StuffLoad(InputStream is) throws IOException {
        kills = StreamUtils.readInt(is);
        killcnt = StreamUtils.readInt(is);
        treasurescnt = StreamUtils.readInt(is);
        treasuresfound = StreamUtils.readInt(is);
        hours = StreamUtils.readInt(is);
        minutes = StreamUtils.readInt(is);
        seconds = StreamUtils.readInt(is);

        visibility = StreamUtils.readInt(is);
        thunderflash = StreamUtils.readInt(is);
        thundertime = StreamUtils.readInt(is);
        randomseed = StreamUtils.readInt(is);

        show2dsector.readObject(is);
        show2dwall.readObject(is);
        show2dsprite.readObject(is);
        automapping = StreamUtils.readByte(is);

        pskybits = StreamUtils.readShort(is);
        parallaxyscale = StreamUtils.readInt(is);
        for (int i = 0; i < MAXPSKYTILES; i++) {
            pskyoff[i] = StreamUtils.readShort(is);
        }

        connecthead = StreamUtils.readShort(is);
        for (int i = 0; i < MAXPLAYERS; i++) {
            connectpoint2[i] = StreamUtils.readShort(is);
        }

        for (int i = 0; i < MAXPLAYERS; i++) {
            plr[i].readObject(is);
        }

        dropshieldcnt = StreamUtils.readInt(is);
        droptheshield = StreamUtils.readUnsignedByte(is) == 1;

        //WH2
        floormirrorcnt = StreamUtils.readInt(is);
        for (int i = 0; i < 64; i++) {
            floormirrorsector[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < ARROWCOUNTLIMIT; i++) {
            arrowsprite[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < THROWPIKELIMIT; i++) {
            throwpikesprite[i] = StreamUtils.readShort(is);
        }
        int len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            ceilingshadearray[i] = (byte) StreamUtils.readUnsignedByte(is);
            floorshadearray[i] = (byte) StreamUtils.readUnsignedByte(is);
        }
        len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            wallshadearray[i] = (byte) StreamUtils.readUnsignedByte(is);
        }
    }

    public void MapLoad(InputStream is) throws IOException {
        sector = new Sector[StreamUtils.readInt(is)];
        for (int i = 0; i < sector.length; i++) {
            sector[i] = new Sector().readObject(is);
        }

        wall = new Wall[StreamUtils.readInt(is)];
        for (int i = 0; i < wall.length; i++) {
            wall[i] = new Wall().readObject(is);
        }

        int numSprites = StreamUtils.readInt(is);
        sprite = new ArrayList<>(numSprites * 2);

        for (int i = 0; i < numSprites; i++) {
            sprite.add(new Sprite().readObject(is));
        }
    }

    public void LoadGDXBlock(InputStream is) throws IOException {
        StreamUtils.skip(is, SAVESCREENSHOTSIZE);

        warp_on = StreamUtils.readUnsignedByte(is);
        if (warp_on == 2) {
            addonFileName = StreamUtils.readDataString(is).toLowerCase();
        }

        boardfilename = game.getCache().getEntry(StreamUtils.readDataString(is), true);
    }

    public EpisodeInfo LoadGDXHeader(InputStream is) throws IOException {
        warp_on = 0;
        addon = null;
        addonFileName = null;
        boardfilename = DUMMY_ENTRY;

        StreamUtils.skip(is, SAVETIME + SAVENAME);

        mapon = StreamUtils.readInt(is);
        skill = StreamUtils.readInt(is);
        message = null;

        LoadGDXBlock(is);

        if (warp_on == 2) { // try to find addon
            addon = findAddon(addonFileName);
        }
        return addon;
    }

    private EpisodeInfo findAddon(String addonFileName) {
        try {
            FileEntry addonEntry = game.getCache().getGameDirectory().getEntry(FileUtils.getPath(addonFileName));
            if (addonEntry.exists()) {
                return episodeManager.getEpisode(addonEntry);
            }
        } catch (Exception e) {
            Console.out.println(e.toString(), OsdColor.RED);
        }
        return null;
    }

    public String getMessage() {
        return message;
    }
}
