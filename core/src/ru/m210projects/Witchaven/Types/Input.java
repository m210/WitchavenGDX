package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.Pattern.BuildNet.NetInput;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Input implements NetInput {

    public float angvel;
    public float horiz;
    public int fvel, svel;
    public int bits;

    @Override
    public void reset() {
        this.fvel = 0;
        this.svel = 0;
        this.angvel = 0;
        this.bits = 0;
        this.horiz = 0;
    }

    @Override
    public NetInput Copy(NetInput src) {
        Input input = (Input) src;

        this.fvel = input.fvel;
        this.svel = input.svel;
        this.angvel = input.angvel;
        this.bits = input.bits;
        this.horiz = input.horiz;

        return this;
    }

    @Override
    public int GetInput(byte[] p, int offset, NetInput oldInput) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int PutInput(byte[] p, int offset, NetInput oldInput) {
        // TODO Auto-generated method stub
        return 0;
    }

    public Input readObject(InputStream is) throws IOException {
        angvel = StreamUtils.readFloat(is);
        horiz = StreamUtils.readFloat(is);
        fvel = StreamUtils.readInt(is);
        svel = StreamUtils.readInt(is);
        bits = StreamUtils.readInt(is);
        return this;
    }

    public void writeObject(ByteArrayOutputStream os) throws IOException {
        StreamUtils.writeFloat(os, angvel);
        StreamUtils.writeFloat(os, horiz);
        StreamUtils.writeInt(os, fvel);
        StreamUtils.writeInt(os, svel);
        StreamUtils.writeInt(os, bits);
    }
}
