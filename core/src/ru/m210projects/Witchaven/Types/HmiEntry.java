package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.Architecture.common.audio.MidiDevice;
import ru.m210projects.Build.filehandle.InputStreamProvider;

import static ru.m210projects.Witchaven.Types.HMIMIDIP.hmpinit;

public class HmiEntry extends WHSoundEntry {
    public HmiEntry(InputStreamProvider provider, String name, int prioritize, int offset, int size) {
        super(provider, String.format("%s.%s", name, MidiDevice.MIDI_EXTENSION), prioritize, offset, size);
    }

    @Override
    public byte[] getBytes() {
        return hmpinit(super.getBytes());
    }
}
