package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Witchaven.Globals.MAXNUMORBS;
import static ru.m210projects.Witchaven.Globals.MAXTREASURES;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Potions.MAXPOTIONS;
import static ru.m210projects.Witchaven.Weapons.MAXWEAPONS;

public class PLAYER {

    private int shootgunzvel;
    private int ihaveflag = 0;
    private int justplayed = 0;
    private int lopoint = 0;
    private int walktoggle = 0;
    private int runningtime = 0;
    private int oldhoriz;
    private int damage_vel;
    private int damage_svel;
    private int damage_angvel;
    private int dahand; // #GDX 04.06.2024

    private int dropshieldcnt;
    private boolean droptheshield;
    private int weapondrop;
    private int weapondropgoal;

    private transient final Input pInput;
    private int x;
    private int y;
    private int z;
    private float ang;
    private float horiz;
    private transient float jumphoriz;
    private int height;
    private transient int hvel;
    private int sector;
    private int oldsector;
    private int spritenum;
    private transient boolean keytoggle;
    private int flags;
    private final int[] weapon = new int[MAXWEAPONS];
    private final int[] preenchantedweapon = new int[MAXWEAPONS];
    private final int[] ammo = new int[MAXWEAPONS];
    private final int[] preenchantedammo = new int[MAXWEAPONS];
    private final int[] orbammo = new int[MAXNUMORBS];
    private final int[] treasure = new int[MAXTREASURES];
    private final int[] orbactive = new int[MAXNUMORBS];
    private final int[] orb = new int[MAXNUMORBS];
    private final int[] potion = new int[MAXPOTIONS];
    private int lvl;
    private int score;
    private int health;
    private int maxhealth;
    private int armor;
    private int armortype;
    private int onsomething;
    private int fallz;

    private boolean dead;
    private short turnAround;

    private int shadowtime;
    private int helmettime;
    private int scoretime;
    private int vampiretime;

    private int selectedgun;
    private int currweapon;
    private transient int currweapontics;
    private transient int currweaponanim;
    private transient int currweaponframe;
    private transient int currweaponfired;
    private int currweaponattackstyle;
    private int currweaponflip;
    private int hasshot;

    private int currentpotion;
    private int strongtime;
    private int manatime;
    private int invisibletime = -1;

    private int orbshot;
    private transient int spellbooktics;
    private transient int spellbook;
    private int spellbookflip;
    private int nightglowtime;

    private int showbook;
    private int showbooktype;
    private int showbookflip;
    private int showbookanim;
    private int currentorb;

    private int spelltime;

    private int shieldpoints;
    private int shieldtype;

    private int poisoned;
    private int poisontime;
    private int shockme = -1;
    private int invincibletime;

    private int spiked;
    private int spiketics;
    private int spikeframe;
    private int currspikeframe;
    private boolean godMode;
    private boolean noclip;

    public PLAYER() {
        pInput = new Input();
    }


    public void copy(PLAYER src) {
        setX(src.getX());
        setY(src.getY());
        setZ(src.getZ());
        setAng(src.getAng());
        setHoriz(src.getHoriz());
        setJumphoriz(src.getJumphoriz());
        setHeight(src.getHeight());
        setHvel(src.getHvel());
        setSector(src.getSector());
        setOldsector(src.getOldsector());
        setSpritenum(src.getSpritenum());
        setKeytoggle(src.isKeytoggle());
        setFlags(src.getFlags());
        System.arraycopy(src.getWeapon(), 0, getWeapon(), 0, MAXWEAPONS);
        System.arraycopy(src.getAmmo(), 0, getAmmo(), 0, MAXWEAPONS);
        System.arraycopy(src.getPreenchantedweapon(), 0, getPreenchantedweapon(), 0, MAXWEAPONS);
        System.arraycopy(src.getPreenchantedammo(), 0, getPreenchantedammo(), 0, MAXWEAPONS);
        System.arraycopy(src.getOrbammo(), 0, getOrbammo(), 0, MAXNUMORBS);
        System.arraycopy(src.getOrbactive(), 0, getOrbactive(), 0, MAXNUMORBS);
        System.arraycopy(src.getOrb(), 0, getOrb(), 0, MAXNUMORBS);
        System.arraycopy(src.getTreasure(), 0, getTreasure(), 0, MAXTREASURES);
        System.arraycopy(src.getPotion(), 0, getPotion(), 0, MAXPOTIONS);

        setLvl(src.getLvl());
        setScore(src.getScore());
        setHealth(src.getHealth());
        setMaxhealth(src.getMaxhealth());
        setArmor(src.getArmor());
        setArmortype(src.getArmortype());
        setOnsomething(src.getOnsomething());
        setFallz(src.getFallz());

        setDead(src.isDead());
        setTurnAround(src.getTurnAround());

        setShadowtime(src.getShadowtime());
        setHelmettime(src.getHelmettime());
        setScoretime(src.getScoretime());

        setSelectedgun(src.getSelectedgun());
        setCurrweapon(src.getCurrweapon());
        setCurrweapontics(src.getCurrweapontics());
        setCurrweaponanim(src.getCurrweaponanim());
        setCurrweaponframe(src.getCurrweaponframe());
        setCurrweaponfired(src.getCurrweaponfired());
        setCurrweaponattackstyle(src.getCurrweaponattackstyle());
        setCurrweaponflip(src.getCurrweaponflip());
        setHasshot(src.getHasshot());

        setCurrentpotion(src.getCurrentpotion());
        setStrongtime(src.getStrongtime());
        setManatime(src.getManatime());
        setInvisibletime(src.getInvisibletime());

        setOrbshot(src.getOrbshot());
        setSpellbooktics(src.getSpellbooktics());
        setSpellbook(src.getSpellbook());
        setSpellbookflip(src.getSpellbookflip());
        setNightglowtime(src.getNightglowtime());

        setShowbook(src.getShowbook());
        setShowbooktype(src.getShowbooktype());
        setShowbookflip(src.getShowbookflip());
        setShowbookanim(src.getShowbookanim());
        setCurrentorb(src.getCurrentorb());

        setSpelltime(src.getSpelltime());
        setShieldtype(src.getShieldtype());

        setShockme(src.getShockme());
        setInvincibletime(src.getInvincibletime());

        setSpiketics(src.getSpiketics());
        setSpikeframe(src.getSpikeframe());
        setCurrspikeframe(src.getCurrspikeframe());

        setShieldpoints(src.getShieldpoints());
        setVampiretime(src.getVampiretime());
        setPoisoned(src.getPoisoned());
        setPoisontime(src.getPoisontime());
        setSpiked(src.getSpiked());

        setGodMode(src.isGodMode());
        setNoclip(src.isNoclip());
    }

    public Sprite getSprite() {
        return boardService.getSprite(getSpritenum());
    }

    public int getCurrentorb() {
        if (currentorb < 0 || currentorb >= MAXNUMORBS) {
            return 0;
        }
        return currentorb;
    }

    public void setCurrentorb(int currentorb) {
        this.currentorb = currentorb;
    }

    public PLAYER readObject(InputStream is) throws IOException {
        setX(StreamUtils.readInt(is));
        setY(StreamUtils.readInt(is));
        setZ(StreamUtils.readInt(is));
        setAng(StreamUtils.readFloat(is));
        setHoriz(StreamUtils.readFloat(is));
        setJumphoriz(StreamUtils.readFloat(is));
        setHeight(StreamUtils.readInt(is));
        setHvel(StreamUtils.readInt(is));
        setSector(StreamUtils.readShort(is));
        setOldsector(StreamUtils.readShort(is));
        setSpritenum(StreamUtils.readShort(is));
        setKeytoggle(StreamUtils.readBoolean(is));
        setFlags(StreamUtils.readInt(is));
        for(int i = 0; i < MAXWEAPONS; i++) {
            getWeapon()[i] = StreamUtils.readInt(is);
            getAmmo()[i] = StreamUtils.readInt(is);
            getPreenchantedweapon()[i] = StreamUtils.readInt(is);
            getPreenchantedammo()[i] = StreamUtils.readInt(is);
        }
        for(int i = 0; i < MAXNUMORBS; i++) {
            getOrbammo()[i] = StreamUtils.readInt(is);
            getOrbactive()[i] = StreamUtils.readInt(is);
            getOrb()[i] = StreamUtils.readInt(is);
        }
        for(int i = 0; i < MAXTREASURES; i++)
            getTreasure()[i] = StreamUtils.readInt(is);
        for(int i = 0; i < MAXPOTIONS; i++)
            getPotion()[i] = StreamUtils.readInt(is);

        setLvl(StreamUtils.readInt(is));
        setScore(StreamUtils.readInt(is));
        setHealth(StreamUtils.readInt(is));
        setMaxhealth(StreamUtils.readInt(is));
        setArmor(StreamUtils.readInt(is));
        setArmortype(StreamUtils.readInt(is));
        setOnsomething(StreamUtils.readInt(is));
        setFallz(StreamUtils.readInt(is));

        setDead(StreamUtils.readBoolean(is));
        setTurnAround(StreamUtils.readShort(is));

        setShadowtime(StreamUtils.readInt(is));
        setHelmettime(StreamUtils.readInt(is));
        setScoretime(StreamUtils.readInt(is));

        setSelectedgun(StreamUtils.readInt(is));
        setCurrweapon(StreamUtils.readInt(is));
        setCurrweapontics(StreamUtils.readInt(is));
        setCurrweaponanim(StreamUtils.readInt(is));
        setCurrweaponframe(StreamUtils.readInt(is));
        setCurrweaponfired(StreamUtils.readInt(is));
        setCurrweaponattackstyle(StreamUtils.readInt(is));
        setCurrweaponflip(StreamUtils.readInt(is));
        setHasshot(StreamUtils.readInt(is));

        setCurrentpotion(StreamUtils.readInt(is));
        setStrongtime(StreamUtils.readInt(is));
        setManatime(StreamUtils.readInt(is));
        setInvisibletime(StreamUtils.readInt(is));

        setOrbshot(StreamUtils.readInt(is));
        setSpellbooktics(StreamUtils.readInt(is));
        setSpellbook(StreamUtils.readInt(is));
        setSpellbookflip(StreamUtils.readInt(is));
        setNightglowtime(StreamUtils.readInt(is));

        setShowbook(StreamUtils.readInt(is));
        setShowbooktype(StreamUtils.readInt(is));
        setShowbookflip(StreamUtils.readInt(is));
        setShowbookanim(StreamUtils.readInt(is));
        currentorb= StreamUtils.readInt(is);

        setSpelltime(StreamUtils.readInt(is));
        setShieldtype(StreamUtils.readInt(is));

        setShockme(StreamUtils.readInt(is));
        setInvincibletime(StreamUtils.readInt(is));

        setSpiketics(StreamUtils.readInt(is));
        setSpikeframe(StreamUtils.readInt(is));
        setCurrspikeframe(StreamUtils.readInt(is));

        setShieldpoints(StreamUtils.readShort(is));
        setVampiretime(StreamUtils.readShort(is));
        if (getVampiretime() > 7200)
            setVampiretime(0); /* Discard vampiretime if it's out of line */
        setPoisoned(StreamUtils.readShort(is));
        setPoisontime(StreamUtils.readShort(is));
        setSpiked(StreamUtils.readShort(is));
        setGodMode(StreamUtils.readBoolean(is));
        setNoclip(StreamUtils.readBoolean(is));

        setShootgunzvel(StreamUtils.readInt(is));
        setIhaveflag(StreamUtils.readInt(is));
        setJustplayed(StreamUtils.readInt(is));
        setLopoint(StreamUtils.readInt(is));
        setWalktoggle(StreamUtils.readInt(is));
        setRunningtime(StreamUtils.readInt(is));
        setOldhoriz(StreamUtils.readInt(is));
        setDamage_vel(StreamUtils.readInt(is));
        setDamage_svel(StreamUtils.readInt(is));
        setDamage_angvel(StreamUtils.readInt(is));

        setDahand(StreamUtils.readInt(is));
        setDropshieldcnt(StreamUtils.readInt(is));
        setDroptheshield(StreamUtils.readBoolean(is));
        setWeapondrop(StreamUtils.readInt(is));
        setWeapondropgoal(StreamUtils.readInt(is));

        return this;
    }

    public void writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, getX());
        StreamUtils.writeInt(os, getY());
        StreamUtils.writeInt(os, getZ());
        StreamUtils.writeFloat(os, getAng());
        StreamUtils.writeFloat(os, getHoriz());
        StreamUtils.writeFloat(os, getJumphoriz());
        StreamUtils.writeInt(os, getHeight());
        StreamUtils.writeInt(os, getHvel());
        StreamUtils.writeShort(os, getSector());
        StreamUtils.writeShort(os, getOldsector());
        StreamUtils.writeShort(os, getSpritenum());
        StreamUtils.writeBoolean(os, isKeytoggle());
        StreamUtils.writeInt(os, getFlags());
        for (int i = 0; i < MAXWEAPONS; i++) {
            StreamUtils.writeInt(os, getWeapon()[i]);
            StreamUtils.writeInt(os, getAmmo()[i]);
            StreamUtils.writeInt(os, getPreenchantedweapon()[i]);
            StreamUtils.writeInt(os, getPreenchantedammo()[i]);
        }
        for (int i = 0; i < MAXNUMORBS; i++) {
            StreamUtils.writeInt(os, getOrbammo()[i]);
            StreamUtils.writeInt(os, getOrbactive()[i]);
            StreamUtils.writeInt(os, getOrb()[i]);
        }
        for (int i = 0; i < MAXTREASURES; i++) {
            StreamUtils.writeInt(os, getTreasure()[i]);
        }
        for (int i = 0; i < MAXPOTIONS; i++) {
            StreamUtils.writeInt(os, getPotion()[i]);
        }

        StreamUtils.writeInt(os, getLvl());
        StreamUtils.writeInt(os, getScore());
        StreamUtils.writeInt(os, getHealth());
        StreamUtils.writeInt(os, getMaxhealth());
        StreamUtils.writeInt(os, getArmor());
        StreamUtils.writeInt(os, getArmortype());
        StreamUtils.writeInt(os, getOnsomething());
        StreamUtils.writeInt(os, getFallz());

        StreamUtils.writeBoolean(os, isDead());
        StreamUtils.writeShort(os, getTurnAround());

        StreamUtils.writeInt(os, getShadowtime());
        StreamUtils.writeInt(os, getHelmettime());
        StreamUtils.writeInt(os, getScoretime());

        StreamUtils.writeInt(os, getSelectedgun());
        StreamUtils.writeInt(os, getCurrweapon());
        StreamUtils.writeInt(os, getCurrweapontics());
        StreamUtils.writeInt(os, getCurrweaponanim());
        StreamUtils.writeInt(os, getCurrweaponframe());
        StreamUtils.writeInt(os, getCurrweaponfired());
        StreamUtils.writeInt(os, getCurrweaponattackstyle());
        StreamUtils.writeInt(os, getCurrweaponflip());
        StreamUtils.writeInt(os, getHasshot());

        StreamUtils.writeInt(os, getCurrentpotion());
        StreamUtils.writeInt(os, getStrongtime());
        StreamUtils.writeInt(os, getManatime());
        StreamUtils.writeInt(os, getInvisibletime());

        StreamUtils.writeInt(os, getOrbshot());
        StreamUtils.writeInt(os, getSpellbooktics());
        StreamUtils.writeInt(os, getSpellbook());
        StreamUtils.writeInt(os, getSpellbookflip());
        StreamUtils.writeInt(os, getNightglowtime());

        StreamUtils.writeInt(os, getShowbook());
        StreamUtils.writeInt(os, getShowbooktype());
        StreamUtils.writeInt(os, getShowbookflip());
        StreamUtils.writeInt(os, getShowbookanim());
        StreamUtils.writeInt(os, currentorb);

        StreamUtils.writeInt(os, getSpelltime());
        StreamUtils.writeInt(os, getShieldtype());

        StreamUtils.writeInt(os, getShockme());
        StreamUtils.writeInt(os, getInvincibletime());

        StreamUtils.writeInt(os, getSpiketics());
        StreamUtils.writeInt(os, getSpikeframe());
        StreamUtils.writeInt(os, getCurrspikeframe());

        StreamUtils.writeShort(os, getShieldpoints());
        StreamUtils.writeShort(os, getVampiretime());

        StreamUtils.writeShort(os, getPoisoned());
        StreamUtils.writeShort(os, getPoisontime());
        StreamUtils.writeShort(os, getSpiked());
        StreamUtils.writeBoolean(os, isGodMode());
        StreamUtils.writeBoolean(os, isNoclip());

        StreamUtils.writeInt(os, getShootgunzvel());
        StreamUtils.writeInt(os, getIhaveflag());
        StreamUtils.writeInt(os, getJustplayed());
        StreamUtils.writeInt(os, getLopoint());
        StreamUtils.writeInt(os, getWalktoggle());
        StreamUtils.writeInt(os, getRunningtime());
        StreamUtils.writeInt(os, getOldhoriz());
        StreamUtils.writeInt(os, getDamage_vel());
        StreamUtils.writeInt(os, getDamage_svel());
        StreamUtils.writeInt(os, getDamage_angvel());

        StreamUtils.writeInt(os, getDahand());
        StreamUtils.writeInt(os, getDropshieldcnt());
        StreamUtils.writeBoolean(os, isDroptheshield());
        StreamUtils.writeInt(os, getWeapondrop());
        StreamUtils.writeInt(os, getWeapondropgoal());
    }

    public int getShootgunzvel() {
        return shootgunzvel;
    }

    public void setShootgunzvel(int shootgunzvel) {
        this.shootgunzvel = shootgunzvel;
    }

    public int getIhaveflag() {
        return ihaveflag;
    }

    public void setIhaveflag(int ihaveflag) {
        this.ihaveflag = ihaveflag;
    }

    public int getJustplayed() {
        return justplayed;
    }

    public void setJustplayed(int justplayed) {
        this.justplayed = justplayed;
    }

    public int getLopoint() {
        return lopoint;
    }

    public void setLopoint(int lopoint) {
        this.lopoint = lopoint;
    }

    public int getWalktoggle() {
        return walktoggle;
    }

    public void setWalktoggle(int walktoggle) {
        this.walktoggle = walktoggle;
    }

    public int getRunningtime() {
        return runningtime;
    }

    public void setRunningtime(int runningtime) {
        this.runningtime = runningtime;
    }

    public int getOldhoriz() {
        return oldhoriz;
    }

    public void setOldhoriz(int oldhoriz) {
        this.oldhoriz = oldhoriz;
    }

    public int getDamage_vel() {
        return damage_vel;
    }

    public void setDamage_vel(int damage_vel) {
        this.damage_vel = damage_vel;
    }

    public int getDamage_svel() {
        return damage_svel;
    }

    public void setDamage_svel(int damage_svel) {
        this.damage_svel = damage_svel;
    }

    public int getDamage_angvel() {
        return damage_angvel;
    }

    public void setDamage_angvel(int damage_angvel) {
        this.damage_angvel = damage_angvel;
    }

    public int getDahand() {
        return dahand;
    }

    public void setDahand(int dahand) {
        this.dahand = dahand;
    }

    public int getDropshieldcnt() {
        return dropshieldcnt;
    }

    public void setDropshieldcnt(int dropshieldcnt) {
        this.dropshieldcnt = dropshieldcnt;
    }

    public boolean isDroptheshield() {
        return droptheshield;
    }

    public void setDroptheshield(boolean droptheshield) {
        this.droptheshield = droptheshield;
    }

    public int getWeapondrop() {
        return weapondrop;
    }

    public void setWeapondrop(int weapondrop) {
        this.weapondrop = weapondrop;
    }

    public int getWeapondropgoal() {
        return weapondropgoal;
    }

    public void setWeapondropgoal(int weapondropgoal) {
        this.weapondropgoal = weapondropgoal;
    }

    public Input getInput() {
        return pInput;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public float getAng() {
        return ang;
    }

    public void setAng(float ang) {
        this.ang = ang;
    }

    public float getHoriz() {
        return horiz;
    }

    public void setHoriz(float horiz) {
        this.horiz = horiz;
    }

    public float getJumphoriz() {
        return jumphoriz;
    }

    public void setJumphoriz(float jumphoriz) {
        this.jumphoriz = jumphoriz;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHvel() {
        return hvel;
    }

    public void setHvel(int hvel) {
        this.hvel = hvel;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public int getOldsector() {
        return oldsector;
    }

    public void setOldsector(int oldsector) {
        this.oldsector = oldsector;
    }

    public int getSpritenum() {
        return spritenum;
    }

    public void setSpritenum(int spritenum) {
        this.spritenum = spritenum;
    }

    public boolean isKeytoggle() {
        return keytoggle;
    }

    public void setKeytoggle(boolean keytoggle) {
        this.keytoggle = keytoggle;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public int[] getWeapon() {
        return weapon;
    }

    public int[] getPreenchantedweapon() {
        return preenchantedweapon;
    }

    public int[] getAmmo() {
        return ammo;
    }

    public int[] getPreenchantedammo() {
        return preenchantedammo;
    }

    public int[] getOrbammo() {
        return orbammo;
    }

    public int[] getTreasure() {
        return treasure;
    }

    public int[] getOrbactive() {
        return orbactive;
    }

    public int[] getOrb() {
        return orb;
    }

    public int[] getPotion() {
        return potion;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMaxhealth() {
        return maxhealth;
    }

    public void setMaxhealth(int maxhealth) {
        this.maxhealth = maxhealth;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getArmortype() {
        return armortype;
    }

    public void setArmortype(int armortype) {
        this.armortype = armortype;
    }

    public int getOnsomething() {
        return onsomething;
    }

    public void setOnsomething(int onsomething) {
        this.onsomething = onsomething;
    }

    public int getFallz() {
        return fallz;
    }

    public void setFallz(int fallz) {
        this.fallz = fallz;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public short getTurnAround() {
        return turnAround;
    }

    public void setTurnAround(short turnAround) {
        this.turnAround = turnAround;
    }

    public int getShadowtime() {
        return shadowtime;
    }

    public void setShadowtime(int shadowtime) {
        this.shadowtime = shadowtime;
    }

    public int getHelmettime() {
        return helmettime;
    }

    public void setHelmettime(int helmettime) {
        this.helmettime = helmettime;
    }

    public int getScoretime() {
        return scoretime;
    }

    public void setScoretime(int scoretime) {
        this.scoretime = scoretime;
    }

    public int getVampiretime() {
        return vampiretime;
    }

    public void setVampiretime(int vampiretime) {
        this.vampiretime = vampiretime;
    }

    public int getSelectedgun() {
        return selectedgun;
    }

    public void setSelectedgun(int selectedgun) {
        this.selectedgun = selectedgun;
    }

    public int getCurrweapon() {
        return currweapon;
    }

    public void setCurrweapon(int currweapon) {
        this.currweapon = currweapon;
    }

    public int getCurrweapontics() {
        return currweapontics;
    }

    public void setCurrweapontics(int currweapontics) {
        this.currweapontics = currweapontics;
    }

    public int getCurrweaponanim() {
        return currweaponanim;
    }

    public void setCurrweaponanim(int currweaponanim) {
        this.currweaponanim = currweaponanim;
    }

    public int getCurrweaponframe() {
        return currweaponframe;
    }

    public void setCurrweaponframe(int currweaponframe) {
        this.currweaponframe = currweaponframe;
    }

    public int getCurrweaponfired() {
        return currweaponfired;
    }

    public void setCurrweaponfired(int currweaponfired) {
        this.currweaponfired = currweaponfired;
    }

    public int getCurrweaponattackstyle() {
        return currweaponattackstyle;
    }

    public void setCurrweaponattackstyle(int currweaponattackstyle) {
        this.currweaponattackstyle = currweaponattackstyle;
    }

    public int getCurrweaponflip() {
        return currweaponflip;
    }

    public void setCurrweaponflip(int currweaponflip) {
        this.currweaponflip = currweaponflip;
    }

    public int getHasshot() {
        return hasshot;
    }

    public void setHasshot(int hasshot) {
        this.hasshot = hasshot;
    }

    public int getCurrentpotion() {
        return currentpotion;
    }

    public void setCurrentpotion(int currentpotion) {
        this.currentpotion = currentpotion;
    }

    public int getStrongtime() {
        return strongtime;
    }

    public void setStrongtime(int strongtime) {
        this.strongtime = strongtime;
    }

    public int getManatime() {
        return manatime;
    }

    public void setManatime(int manatime) {
        this.manatime = manatime;
    }

    public int getInvisibletime() {
        return invisibletime;
    }

    public void setInvisibletime(int invisibletime) {
        this.invisibletime = invisibletime;
    }

    public int getOrbshot() {
        return orbshot;
    }

    public void setOrbshot(int orbshot) {
        this.orbshot = orbshot;
    }

    public int getSpellbooktics() {
        return spellbooktics;
    }

    public void setSpellbooktics(int spellbooktics) {
        this.spellbooktics = spellbooktics;
    }

    public int getSpellbook() {
        return spellbook;
    }

    public void setSpellbook(int spellbook) {
        this.spellbook = spellbook;
    }

    public int getSpellbookflip() {
        return spellbookflip;
    }

    public void setSpellbookflip(int spellbookflip) {
        this.spellbookflip = spellbookflip;
    }

    public int getNightglowtime() {
        return nightglowtime;
    }

    public void setNightglowtime(int nightglowtime) {
        this.nightglowtime = nightglowtime;
    }

    public int getShowbook() {
        return showbook;
    }

    public void setShowbook(int showbook) {
        this.showbook = showbook;
    }

    public int getShowbooktype() {
        return showbooktype;
    }

    public void setShowbooktype(int showbooktype) {
        this.showbooktype = showbooktype;
    }

    public int getShowbookflip() {
        return showbookflip;
    }

    public void setShowbookflip(int showbookflip) {
        this.showbookflip = showbookflip;
    }

    public int getShowbookanim() {
        return showbookanim;
    }

    public void setShowbookanim(int showbookanim) {
        this.showbookanim = showbookanim;
    }

    public int getSpelltime() {
        return spelltime;
    }

    public void setSpelltime(int spelltime) {
        this.spelltime = spelltime;
    }

    public int getShieldpoints() {
        return shieldpoints;
    }

    public void setShieldpoints(int shieldpoints) {
        this.shieldpoints = shieldpoints;
    }

    public int getShieldtype() {
        return shieldtype;
    }

    public void setShieldtype(int shieldtype) {
        this.shieldtype = shieldtype;
    }

    public int getPoisoned() {
        return poisoned;
    }

    public void setPoisoned(int poisoned) {
        this.poisoned = poisoned;
    }

    public int getPoisontime() {
        return poisontime;
    }

    public void setPoisontime(int poisontime) {
        this.poisontime = poisontime;
    }

    public int getShockme() {
        return shockme;
    }

    public void setShockme(int shockme) {
        this.shockme = shockme;
    }

    public int getInvincibletime() {
        return invincibletime;
    }

    public void setInvincibletime(int invincibletime) {
        this.invincibletime = invincibletime;
    }

    public int getSpiked() {
        return spiked;
    }

    public void setSpiked(int spiked) {
        this.spiked = spiked;
    }

    public int getSpiketics() {
        return spiketics;
    }

    public void setSpiketics(int spiketics) {
        this.spiketics = spiketics;
    }

    public int getSpikeframe() {
        return spikeframe;
    }

    public void setSpikeframe(int spikeframe) {
        this.spikeframe = spikeframe;
    }

    public int getCurrspikeframe() {
        return currspikeframe;
    }

    public void setCurrspikeframe(int currspikeframe) {
        this.currspikeframe = currspikeframe;
    }

    public boolean isGodMode() {
        return godMode;
    }

    public void setGodMode(boolean godMode) {
        this.godMode = godMode;
    }

    public boolean isNoclip() {
        return noclip;
    }

    public void setNoclip(boolean noclip) {
        this.noclip = noclip;
    }
}
