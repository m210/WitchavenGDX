package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.m210projects.Witchaven.Main.game;

public class EpisodeManager {

    private final Map<String, EpisodeInfo> episodeCache = new HashMap<>();

    public void putEpisode(EpisodeInfo gameInfo) {
        episodeCache.put(getHashKey(gameInfo.getEpisodeEntry().getFileEntry()), gameInfo);
    }

    private String getHashKey(FileEntry entry) {
        return entry.getRelativePath().toString().toUpperCase(Locale.ROOT);
    }

    public EpisodeInfo getEpisode(FileEntry entry) {
        EpisodeInfo addon = episodeCache.get(getHashKey(entry));
        if (addon != null) {
            return addon;
        }

        addon = checkCustomAddon(entry);
        if (addon != null) {
            episodeCache.put(getHashKey(addon.getEpisodeEntry().getFileEntry()), addon);
            Console.out.println("Found addon: " + addon.Title + " ( " + entry.getName() + " )");
        }

        return addon;
    }

    public EpisodeInfo checkCustomAddon(FileEntry fileEntry) {
        if (!fileEntry.exists()) {
            return null;
        }

        Group gr = fileEntry.isDirectory() ? fileEntry.getDirectory() : game.cache.newGroup(fileEntry);
        List<Entry> mapList = gr.stream().filter(e -> e.isExtension("map")).sorted(Entry::compareTo).collect(Collectors.toList());
        int nCount = 0;
        EpisodeInfo ep = new EpisodeInfo(fileEntry.getName(), new EpisodeEntry.Addon(gr, fileEntry));
        char[] mapname = "level1.map".toCharArray();
        for (Entry entry : mapList) {
            if (String.copyValueOf(mapname).equalsIgnoreCase(entry.getName())) {
                ep.gMapInfo.add(new MapInfo(entry.getName(), fileEntry.getName() + ": Map" + (nCount + 1)));
                mapname = ("level" + (1 + ++nCount) + ".map").toCharArray();
            }
        }

        if (ep.maps() > 0) {
            return ep;
        }

        // If one map episode
        if (!fileEntry.isDirectory() && mapList.size() == 1) {
            for (Entry entry : mapList) {
                ep.gMapInfo.add(new MapInfo(entry.getName(), entry.getName()));
            }
            return ep;
        }

        return null;
    }
}
