package ru.m210projects.Witchaven.Types;

import java.util.ArrayList;
import java.util.List;

public class EpisodeInfo {

    public final String Title;
    private final EpisodeEntry.Addon episodeEntry;
    public final boolean packed;
    public final List<MapInfo> gMapInfo;

    public EpisodeInfo(String title, EpisodeEntry.Addon episodeEntry) {
        this.gMapInfo = new ArrayList<>();
        this.episodeEntry = episodeEntry;
        this.Title = title;
        this.packed = !episodeEntry.getFileEntry().isDirectory();
    }

    public EpisodeEntry.Addon getEpisodeEntry() {
        return episodeEntry;
    }

    public int maps() {
        return gMapInfo.size();
    }

    public String getPath() {
        return episodeEntry.getFileEntry().getRelativePath().toString();
    }

    public String getMapName(int num) {
        MapInfo map = getMap(num);
        if (map != null) {
            return map.title;
        }

        return null;
    }

    public MapInfo getMap(int num) {
        return (num - 1) < gMapInfo.size() ? gMapInfo.get(num - 1) : null;
    }
}
