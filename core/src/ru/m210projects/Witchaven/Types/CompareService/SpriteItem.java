package ru.m210projects.Witchaven.Types.CompareService;

import ru.m210projects.Build.Types.Sprite;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Engine.MAXSECTORS;
import static ru.m210projects.Witchaven.Main.boardService;

public class SpriteItem extends StructItem<Sprite> {

    public SpriteItem(Sprite objectStruct, int index) {
        super(objectStruct, index);

        objectStruct.setShade(objectStruct.getShade());
        objectStruct.setDetail((byte) objectStruct.getDetail());
        objectStruct.setXoffset((byte) objectStruct.getXoffset());
        objectStruct.setYoffset((byte) objectStruct.getYoffset());
        if (objectStruct.getSectnum() == boardService.getSectorCount()) {
            objectStruct.setSectnum(MAXSECTORS);
        }
    }

    @Override
    protected Sprite readObject(InputStream is) throws IOException {
        return new Sprite().readObject(is);
    }

    @Override
    public void write(OutputStream os) throws IOException {
        objectStruct.writeObject(os);
    }
}
