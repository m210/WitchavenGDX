package ru.m210projects.Witchaven.Types.CompareService;

import ru.m210projects.Witchaven.Types.PLAYER;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Witchaven.WHPLR.player;

public class PlayerStructItem extends StructItem<PLAYER> {

    public PlayerStructItem(PLAYER objectStruct, int index) {
        super(objectStruct, index);
    }

    @Override
    protected PLAYER readObject(InputStream is) throws IOException {
        return new PLAYER().readObject(is);
    }

    @Override
    public void write(OutputStream os) throws IOException {
        player[index].writeObject(os);
    }
}
