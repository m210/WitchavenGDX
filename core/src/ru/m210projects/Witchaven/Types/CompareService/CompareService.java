package ru.m210projects.Witchaven.Types.CompareService;

import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Witchaven.Factory.WHEngine;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.WHPLR.player;

public class CompareService {

    private static OutputStream writer;
    private static InputStream reader;
    private static Type type;
    private static Path path;

    public static void prepare(Path path, Type t) {
        type = t;
        CompareService.path = path;
        try {
            if (type == Type.Memory) {
                if (writer != null) {
                    writer.close();
                }
                writer = new ByteArrayOutputStream();
            } else if (type == Type.Write) {
                if (writer != null) {
                    writer.close();
                }
                writer = new BufferedOutputStream(Files.newOutputStream(path));
            } else {
                if (reader != null) {
                    reader.close();
                }
                reader = new BufferedInputStream(Files.newInputStream(path));
            }
        } catch (FileNotFoundException | NoSuchFileException fnf) {
            reader = null;
            writer = null;

            System.err.println(path + " is not found!");
        } catch (IOException e) {
            reader = null;
            writer = null;
            e.printStackTrace();
        }
    }

    @Deprecated
    public static boolean conditionCheck() {
        return gDemoScreen.demfile.rcnt >= 944; // && gDemoScreen.demfile.rcnt <= 180;
    }

    @Deprecated
    public static boolean conditionUpdate() {
        if (conditionCheck()) {
            CompareService.update(gDemoScreen.demfile.rcnt);
            return true;
        }
        return false;
    }

    public static void update(int cnt) {
        if (type == null) {
            return;
        }

        switch (type) {
            case Read:
                if (reader != null) {
                    try {
                        Packet packet = new Packet(cnt);
                        if (!packet.compareRead(reader)) {
//                            throw new Error("Unsync at " + packet.rcnt + " gFrameClock = " + gFrameClock);
                            System.err.println("Unsync at " + packet.rcnt + " in map");
                        } else {
                            System.out.println("\033[32mDemo file " + packet.rcnt + " is OK\033[0m");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        reader = null;
                    }
                }
                break;
            case Write:
            case Memory:
                if (writer != null) {
                    try {
                        Packet packet = new Packet(cnt);
                        packet.write(writer);
                        // System.out.println("\033[32mWrite file " + boardfilename + " at " + packet.rcnt + "\033[0m");
                    } catch (IOException e) {
                        e.printStackTrace();
                        writer = null;
                    }
                }
                break;
        }
    }

    public static void close() {
        if (type == null) {
            return;
        }

        if (writer != null) {
            try {
                if (type == Type.Memory) {
                    BufferedOutputStream os = new BufferedOutputStream(Files.newOutputStream(path));
                    StreamUtils.writeBytes(os, ((ByteArrayOutputStream) writer).toByteArray());
                    os.close();
                }
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public enum Type {
        Read, Write, Memory
    }

    public static class Packet {
        private final int rcnt;
        private final CompareItem[] items;

        public Packet(int rcnt) {
            this.rcnt = rcnt;

            List<CompareItem> list = new ArrayList<>();
            list.add(new IntegerItem(engine.getrand(), "bseed"));
//            list.add(new IntegerItem(lockclock, "lockclock"));
//            list.add(new StringItem(WHEngine.randomStack, "randomStack"));
//            WHEngine.randomStack = "";
//
//            list.add(new PlayerStructItem(player[0], 0));
//            list.add(new SpriteListItem());
//
//            for (int i = 0; i < boardService.getWallCount(); i++) {
//                Wall wall = new Wall();
//                wall.set(boardService.getWall(i));
//                list.add(new WallItem(wall, i));
//            }
//
//            for (int i = 0; i < boardService.getSectorCount(); i++) {
//                Sector sector = new Sector();
//                sector.set(boardService.getSector(i));
//                list.add(new SectorItem(sector, i));
//            }
//
//            list.add(new SpriteLinkedListItem());
            this.items = new CompareItem[list.size()];
            list.toArray(items);
        }

        public boolean compareRead(InputStream is) throws IOException {
            boolean[] equals = new boolean[items.length + 1];

            int file_rcnt = StreamUtils.readInt(is);
            equals[0] = rcnt == file_rcnt;
            for (int i = 0; i < items.length; i++) {
                equals[i + 1] = items[i].compare(is);
            }

            for (boolean equal : equals) {
                if (!equal) {
                    return false;
                }
            }
            return true;
        }

        public void write(OutputStream os) throws IOException {
            StreamUtils.writeInt(os, rcnt);
            for (CompareItem item : items) {
                item.write(os);
            }
        }
    }
}
