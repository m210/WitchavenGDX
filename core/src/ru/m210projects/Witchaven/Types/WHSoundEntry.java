package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.filehandle.InputStreamProvider;
import ru.m210projects.Build.filehandle.grp.GrpEntry;

public class WHSoundEntry extends GrpEntry {

    private final int prioritize;

    public WHSoundEntry(InputStreamProvider provider, String name, int prioritize, int offset, int size) {
        super(provider, name, offset, size);
        this.prioritize = prioritize;
    }

    public int getPrioritize() {
        return prioritize;
    }
}
