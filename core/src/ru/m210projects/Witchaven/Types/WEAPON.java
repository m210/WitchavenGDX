package ru.m210projects.Witchaven.Types;

public class WEAPON {
    public final int daweapontics;
    public final int daweaponframe;
    public final int currx;
    public final int curry;

    public WEAPON(int daweapontics, int daweaponframe, int currx, int curry) {
        this.daweapontics = daweapontics;
        this.daweaponframe = daweaponframe;
        this.currx = currx;
        this.curry = curry;
    }
}
