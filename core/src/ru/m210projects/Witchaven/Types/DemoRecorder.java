package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Witchaven.Main;
import ru.m210projects.Witchaven.Screens.DemoScreen;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.WHPLR.mapon;
import static ru.m210projects.Witchaven.WHPLR.player;

public class DemoRecorder {

    private final ByteArrayOutputStream baos;
    private final FileOutputStream os;
    private final Path filepath;
    public int reccnt;
    public int totalreccnt;
    public int recversion;

    public DemoRecorder(FileOutputStream os, Path fn, int ver) throws IOException {
        this.filepath = fn;
        Console.out.println("Start recording to " + fn);

        StreamUtils.writeString(os, DemoScreen.header);
        StreamUtils.writeInt(os, 0);
        StreamUtils.writeByte(os, ver);

        byte warp_on = 0;
        if (mUserFlag == Main.UserFlag.Addon)
            warp_on = (byte) ((gCurrentEpisode != null) ? 1 : 2);
        if (mUserFlag == Main.UserFlag.UserMap)
            warp_on = 2;
        StreamUtils.writeByte(os, warp_on);

        if (warp_on == 1) {
            String path = gCurrentEpisode.getPath();
            StreamUtils.writeDataString(os, path);
        }

        StreamUtils.writeByte(os, mapon);
        StreamUtils.writeByte(os, difficulty);

        DemoFile.PlayerDemoData playerData = new DemoFile.PlayerDemoData().setFrom(player[myconnectindex]);
        playerData.writeObject(os);

        recversion = ver;
        totalreccnt = 0;
        reccnt = 0;

        this.os = os;
        this.baos = new ByteArrayOutputStream(RECSYNCBUFSIZ);
    }

    public void record() {
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            try {
                PLAYER pp = player[i];
                pp.getInput().writeObject(baos);

                reccnt++;
                totalreccnt++;

                if (reccnt >= RECSYNCBUFSIZ) {
                    os.write(baos.toByteArray());
                    baos.reset();
                    reccnt = 0;
                }
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
                close();
            }
        }
    }

    public void close() {
        try {
            if (baos.size() != 0) {
                os.write(baos.toByteArray());
            }
            StreamUtils.seek(os, DemoScreen.header.length());
            StreamUtils.writeInt(os, totalreccnt);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Console.out.println("Stop recording");
        Directory dir = game.getCache().getGameDirectory();

        Entry entry = dir.addEntry(filepath);
        if (entry.exists()) {
            List<Entry> demos = gDemoScreen.demofiles.computeIfAbsent(dir, e -> new ArrayList<>());
            demos.add(entry);
        }
    }
}
