package ru.m210projects.Witchaven.Types;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.InputStreamProvider;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;

public class WHGroup implements Group {

    private final String name;
    protected final Map<Integer, Entry> entries;

    public WHGroup(String name, InputStreamProvider provider, Class<? extends WHSoundEntry> entryClass) throws IOException {
        this.name = name.toUpperCase();
        int numFiles = 1024 / 3;
        int[] list = new int[1024];
        try (InputStream is = new BufferedInputStream(provider.newInputStream())) {
            StreamUtils.skip(is, is.available() - 4096);
            this.entries = new LinkedHashMap<>(numFiles);
            for (int i = 0; i < 1024; i++) {
                list[i] = StreamUtils.readInt(is);
            }
        }

        Constructor<? extends WHSoundEntry> entryConstructor;
        try {
            entryConstructor = entryClass.getConstructor(InputStreamProvider.class, String.class, int.class, int.class, int.class);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }

        for (int i = 0; i < numFiles; i++) {
            String fileName = String.format("%s%d", name, i).toUpperCase();
            int offset = list[i * 3] * 4096;
            int size = list[(i * 3) + 1];
            int prioritize = list[(i * 3) + 2];
            if (offset == 0 && size == 0) {
                continue;
            }

            try {
                Entry entry = entryConstructor.newInstance(provider, fileName, prioritize, offset, size);
                entry.setParent(this);
                entries.put(i, entry);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Entry getEntry(int index) {
        Entry entry;
        synchronized (this) {
            entry = entries.getOrDefault(index, DUMMY_ENTRY);
        }
        return entry;
    }

    public Entry getEntry(String name) {
        Objects.requireNonNull(name, "name");
        Entry entry;
        synchronized (this) {
            entry = entries.getOrDefault(Integer.parseInt(name), DUMMY_ENTRY);
        }
        return entry;
    }

    @Override
    public synchronized int getSize() {
        return entries.size();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public synchronized List<Entry> getEntries() {
        return new ArrayList<>(entries.values());
    }

}
