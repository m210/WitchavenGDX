package ru.m210projects.Witchaven;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.Pattern.Tools.SaveManager;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Witchaven.Factory.WHRenderer;
import ru.m210projects.Witchaven.Menu.MenuCorruptGame;
import ru.m210projects.Witchaven.Types.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Witchaven.Animate.*;
import static ru.m210projects.Witchaven.Factory.WHMenuHandler.CORRUPTLOAD;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Menu.WHMenuUserContent.checkEpisodeResources;
import static ru.m210projects.Witchaven.Menu.WHMenuUserContent.resetEpisodeResources;
import static ru.m210projects.Witchaven.Types.ANIMATION.*;
import static ru.m210projects.Witchaven.WHFX.*;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.showmessage;
import static ru.m210projects.Witchaven.WHTAG.*;

public class Whldsv {

    public static final String savsign = "WHVN";
    public static final int gdxSave = 100;
    public static final int currentGdxSave = 105; // v1.16 == 104
    public static final int SAVETIME = 8;
    public static final int SAVENAME = 32;
    public static final int SAVESCREENSHOTSIZE = 160 * 100;
    public static final char[] filenum = new char[4];
    public static boolean gQuickSaving;
    public static boolean gAutosaveRequest;
    public static final LSInfo lsInf = new LSInfo();
    public static FileEntry lastload;
    public static int quickslot = 0;
    public static final SafeLoader loader = new SafeLoader();

    public static void FindSaves(Directory dir) {
        for (Entry file : dir) {
            if (file.isExtension("sav") && file instanceof FileEntry) {
                try (InputStream is = file.getInputStream()) {
                    String signature = StreamUtils.readString(is, 4);
                    if (signature.isEmpty()) {
                        continue;
                    }

                    if (signature.equals(savsign)) {
                        int nVersion = StreamUtils.readShort(is);
                        if (nVersion >= gdxSave) {
                            long time = StreamUtils.readLong(is);
                            String savname = StreamUtils.readString(is, SAVENAME);
                            game.pSavemgr.add(savname, time, (FileEntry) file);
                        }
                    }
                } catch (Exception ignored) {
                }
            }
        }
        game.pSavemgr.sort();
    }

    public static int lsReadLoadData(FileEntry file) {
        if (file.exists()) {
            ArtEntry pic = engine.getTile(SaveManager.Screenshot);
            if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                pic = engine.allocatepermanenttile(SaveManager.Screenshot, 160, 100);
            }

            try (InputStream is = file.getInputStream()) {
                int nVersion = checkSave(is) & 0xFFFF;
                lsInf.clear();

                if (nVersion == currentGdxSave) {
                    lsInf.date = game.date.getDate(StreamUtils.readLong(is));
                    StreamUtils.skip(is, SAVENAME);

                    lsInf.read(is);

                    ((DynamicArtEntry) pic).copyData(StreamUtils.readBytes(is, SAVESCREENSHOTSIZE));

                    lsInf.addonfile = null;
                    if (StreamUtils.readByte(is) == 2) {
                        String addon = FileUtils.getFullName(StreamUtils.readDataString(is).toLowerCase());
                        if (!addon.isEmpty()) {
                            lsInf.addonfile = "Addon: " + addon;
                        }
                    }

                    return 1;
                } else {
                    lsInf.info = "Incompatible ver. " + nVersion + " != " + currentGdxSave;
                    return -1;
                }
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }
        lsInf.clear();
        return -1;
    }

    public static String makeNum(int num) {
        filenum[3] = (char) ((num % 10) + 48);
        filenum[2] = (char) (((num / 10) % 10) + 48);
        filenum[1] = (char) (((num / 100) % 10) + 48);
        filenum[0] = (char) (((num / 1000) % 10) + 48);

        return new String(filenum);
    }

    public static int checkSave(InputStream is) throws IOException {
        String signature = StreamUtils.readString(is, 4);
        if (!signature.equals(savsign)) {
            return 0;
        }

        return StreamUtils.readShort(is);
    }

    public static boolean checkfile(InputStream is) throws IOException {
        int nVersion = checkSave(is);
        if (nVersion != currentGdxSave) {
            return false;
        }

        return loader.load(is);
    }

    public static void load() {
        gDemoScreen.onLoad();

        stopallsounds();

        mapon = loader.mapon;
        difficulty = loader.skill;

        LoadGDXBlock();
        LoadStuff();
        MapLoad();
        SectorLoad();
        AnimationLoad();

        pyrn = myconnectindex;

        game.doPrecache(() -> {
            game.nNetMode = NetMode.Single;
            if (mUserFlag == UserFlag.Addon) {
                boardfilename = game.getCache().getEntry(gCurrentEpisode.gMapInfo.get(mapon - 1).path, true);
            }
//            sndCheckUserMusic(boardfilename);

            if (mUserFlag != UserFlag.UserMap) {
                startmusic(mapon - 1);
            } else {
                sndStopMusic();
            }

            game.changeScreen(gGameScreen);

            PLAYER plr = player[pyrn];
            plr.setHvel(0);
            plr.setDamage_angvel(0);
            plr.setDamage_svel(0);
            plr.setDamage_vel(0);

            game.gPaused = false;
            engine.getTimer().setTotalClock(lockclock);
            game.pNet.ototalclock = lockclock;

            showmessage("Game loaded", 360);

            System.gc();

            game.pNet.ResetTimers();
            game.pNet.ready2send = true;
        });
    }

    public static boolean loadgame(FileEntry fil) {
        if (fil.exists()) {
            try (InputStream is = fil.getInputStream()) {
                boolean status = checkfile(is);
                if (status) {
                    load();
                    if (lastload == null || !lastload.exists()) {
                        lastload = fil;
                    }
                }
                return status;
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }

        return false;
    }

    public static void LoadGDXBlock() {
        boardfilename = loader.boardfilename;
        if (loader.warp_on == 0) {
            mUserFlag = UserFlag.None;
            resetEpisodeResources(gOriginalEpisode);
        } else if (loader.warp_on == 1) {
            mUserFlag = UserFlag.UserMap;
            resetEpisodeResources(null);
        } else if (loader.warp_on == 2) {
            mUserFlag = UserFlag.Addon;
            checkEpisodeResources(loader.addon);
        }
    }

    public static void SectorLoad() {
        System.arraycopy(loader.skypanlist, 0, skypanlist, 0, 64);
        skypancnt = loader.skypancnt;
        System.arraycopy(loader.lavadrylandsector, 0, lavadrylandsector, 0, 32);
        lavadrylandcnt = loader.lavadrylandcnt;
        System.arraycopy(loader.dragsectorlist, 0, dragsectorlist, 0, 16);
        System.arraycopy(loader.dragxdir, 0, dragxdir, 0, 16);
        System.arraycopy(loader.dragydir, 0, dragydir, 0, 16);
        System.arraycopy(loader.dragx1, 0, dragx1, 0, 16);
        System.arraycopy(loader.dragy1, 0, dragy1, 0, 16);
        System.arraycopy(loader.dragx2, 0, dragx2, 0, 16);
        System.arraycopy(loader.dragy2, 0, dragy2, 0, 16);
        System.arraycopy(loader.dragfloorz, 0, dragfloorz, 0, 16);

        dragsectorcnt = loader.dragsectorcnt;
        System.arraycopy(loader.bobbingsectorlist, 0, bobbingsectorlist, 0, 16);
        bobbingsectorcnt = loader.bobbingsectorcnt;

        System.arraycopy(loader.warpsectorlist, 0, warpsectorlist, 0, 16);

        warpsectorcnt = loader.warpsectorcnt;
        System.arraycopy(loader.xpanningsectorlist, 0, xpanningsectorlist, 0, 16);
        xpanningsectorcnt = loader.xpanningsectorcnt;
        System.arraycopy(loader.ypanningwalllist, 0, ypanningwalllist, 0, 128);
        ypanningwallcnt = loader.ypanningwallcnt;
        System.arraycopy(loader.floorpanninglist, 0, floorpanninglist, 0, 64);
        floorpanningcnt = loader.floorpanningcnt;

        swingcnt = loader.swingcnt;
        for (int i = 0; i < MAXSWINGDOORS; i++) {
            swingdoor[i].copy(loader.swingdoor[i]);
        }

        revolvecnt = loader.revolvecnt;
        System.arraycopy(loader.revolvesector, 0, revolvesector, 0, 4);
        System.arraycopy(loader.revolveang, 0, revolveang, 0, 4);
        System.arraycopy(loader.revolvepivotx, 0, revolvepivotx, 0, 4);
        System.arraycopy(loader.revolvepivoty, 0, revolvepivoty, 0, 4);
        for (int i = 0; i < 4; i++) {
            System.arraycopy(loader.revolvex[i], 0, revolvex[i], 0, 32);
            System.arraycopy(loader.revolvey[i], 0, revolvey[i], 0, 32);
        }
        System.arraycopy(loader.revolveclip, 0, revolveclip, 0, 16);

        ironbarscnt = loader.ironbarscnt;
        System.arraycopy(loader.ironbarsector, 0, ironbarsector, 0, 16);
        System.arraycopy(loader.ironbarsgoal, 0, ironbarsgoal, 0, 16);
        System.arraycopy(loader.ironbarsgoal1, 0, ironbarsgoal1, 0, 16);
        System.arraycopy(loader.ironbarsgoal2, 0, ironbarsgoal2, 0, 16);
        System.arraycopy(loader.ironbarsdone, 0, ironbarsdone, 0, 16);
        System.arraycopy(loader.ironbarsanim, 0, ironbarsanim, 0, 16);
    }

    public static void LoadStuff() {
        kills = loader.kills;
        killcnt = loader.killcnt;
        treasurescnt = loader.treasurescnt;
        treasuresfound = loader.treasuresfound;
        hours = loader.hours;
        minutes = loader.minutes;
        seconds = loader.seconds;

        visibility = loader.visibility;
        thunderflash = loader.thunderflash;
        thundertime = loader.thundertime;
        engine.srand(loader.randomseed);

        show2dsector.copy(loader.show2dsector);
        show2dwall.copy(loader.show2dwall);
        show2dsprite.copy(loader.show2dsprite);
        automapping = loader.automapping;

        pskybits = loader.pskybits;
        WHRenderer renderer = game.getRenderer();
        renderer.setParallaxScale(loader.parallaxyscale);
        System.arraycopy(loader.pskyoff, 0, pskyoff, 0, MAXPSKYTILES);
        System.arraycopy(pskyoff, 0, zeropskyoff, 0, MAXPSKYTILES);

        connecthead = loader.connecthead;
        System.arraycopy(loader.connectpoint2, 0, connectpoint2, 0, MAXPLAYERS);

        for (int i = 0; i < MAXPLAYERS; i++) {
            player[i].copy(loader.plr[i]);
        }

        player[myconnectindex].setDropshieldcnt(loader.dropshieldcnt);
        player[myconnectindex].setDroptheshield(loader.droptheshield);

        floormirrorcnt = loader.floormirrorcnt;
        System.arraycopy(loader.floormirrorsector, 0, floormirrorsector, 0, 64);
        System.arraycopy(loader.arrowsprite, 0, arrowsprite, 0, ARROWCOUNTLIMIT);
        System.arraycopy(loader.throwpikesprite, 0, throwpikesprite, 0, THROWPIKELIMIT);
        System.arraycopy(loader.ceilingshadearray, 0, ceilingshadearray, 0, MAXSECTORS);
        System.arraycopy(loader.floorshadearray, 0, floorshadearray, 0, MAXSECTORS);
        System.arraycopy(loader.wallshadearray, 0, wallshadearray, 0, MAXWALLS);
    }

    public static void MapLoad() {
        PLAYER plr = player[myconnectindex];
        boardService.setBoard(new Board(new BuildPos(plr.getX(), plr.getY(), plr.getZ(), (int) plr.getAng(), plr.getSector()), loader.sector, loader.wall, loader.sprite));
    }

    public static void AnimationLoad() {
        for (int i = 0; i < MAXANIMATES; i++) {
            gAnimationData[i].id = loader.gAnimationData[i].id;
            gAnimationData[i].type = loader.gAnimationData[i].type;
            gAnimationData[i].ptr = loader.gAnimationData[i].ptr;
            gAnimationData[i].goal = loader.gAnimationData[i].goal;
            gAnimationData[i].vel = loader.gAnimationData[i].vel;
            gAnimationData[i].acc = loader.gAnimationData[i].acc;
        }
        gAnimationCount = loader.gAnimationCount;

        for (int i = gAnimationCount - 1; i >= 0; i--) {
            ANIMATION gAnm = gAnimationData[i];
            Object object = (gAnm.ptr = getobject(gAnm.id, gAnm.type));
            switch (gAnm.type) {
                case WALLX:
                case WALLY:
                    game.pInt.setwallinterpolate(gAnm.id, (Wall) object);
                    break;
                case FLOORZ:
                    game.pInt.setfloorinterpolate(gAnm.id, (Sector) object);
                    break;
                case CEILZ:
                    game.pInt.setceilinterpolate(gAnm.id, (Sector) object);
                    break;
            }
        }
    }

    public static void quickload() {
        if (numplayers > 1) {
            return;
        }
        System.err.println("load");

        final FileEntry loadFile = game.pSavemgr.getLast();
        if (canLoad(loadFile)) {
            game.changeScreen(gLoadScreen);
            gLoadScreen.init(() -> {
                if (!loadgame(loadFile)) {
                    game.setPrevScreen();
                    if (game.isCurrentScreen(gGameScreen)) {
                        showmessage("Incompatible version of saved game found!", 360);
                    }
                }
            });
        }
    }

    public static boolean canLoad(FileEntry fil) {
        return fil.load(is -> {
            int nVersion = checkSave(is) & 0xFFFF;
            if (nVersion != currentGdxSave) {
                if (nVersion >= gdxSave) {
                    final EpisodeInfo addon = loader.LoadGDXHeader(is);
                    final EpisodeInfo ep = addon != null ? addon : gOriginalEpisode;
                    if (ep != null && ep.getMap(loader.mapon) != null && loader.skill >= 0 && loader.skill < 5
                            && loader.warp_on != 1) { // not usermap
                        MenuCorruptGame menu = (MenuCorruptGame) game.menu.mMenus[CORRUPTLOAD];
                        menu.setRunnable(() -> gGameScreen.newgame(ep, loader.mapon, loader.skill));
                        game.menu.mOpen(menu, -1);
                    }
                }
                throw new IOException("Wrong file version");
            }
        });
    }

    private static void save(OutputStream os, String savename, long time) throws IOException {
        SaveHeader(os, savename, time);
        SaveGDXBlock(os); // ok
        StuffSave(os);
        MapSave(os);
        SectorSave(os);
        AnimationSave(os);
    }

    public static void savegame(Directory dir, String savename, String filename) {
        FileEntry file = dir.getEntry(filename);
        if (file.exists()) {
            if (!file.delete()) {
                showmessage("Game not saved. Access denied!", 360);
                return;
            }
        }

        Path path = dir.getPath().resolve(filename);
        try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(path))) {
            long time = game.date.getCurrentDate();
            save(os, savename, time);

            file = dir.addEntry(path);
            if (file.exists()) {
                game.pSavemgr.add(savename, time, file);
                lastload = file;
                showmessage("GAME SAVED", 360);
            } else {
                throw new FileNotFoundException(filename);
            }
        } catch (Exception e) {
            showmessage("Game not saved! " + e, 360);
        }
    }

    public static void SaveHeader(OutputStream os, String savename, long time) throws IOException {
        SaveVersion(os, currentGdxSave);
        StreamUtils.writeLong(os, time);
        StreamUtils.writeString(os, savename, SAVENAME);
        StreamUtils.writeInt(os, mapon);
        StreamUtils.writeInt(os, difficulty);
    }

    public static void SaveGDXBlock(OutputStream os) throws IOException {
        StreamUtils.writeBytes(os, gGameScreen.captBuffer);

        byte warp_on = 0;
        if (mUserFlag == UserFlag.UserMap) {
            warp_on = 1;
        }
        if (mUserFlag == UserFlag.Addon) {
            warp_on = 2;
        }

        StreamUtils.writeByte(os, warp_on);
        if (warp_on == 2) {// user episode
            StreamUtils.writeDataString(os, gCurrentEpisode.getPath());
        }

        if (boardfilename != null && boardfilename.exists()) {
            if (boardfilename instanceof FileEntry) {
                StreamUtils.writeDataString(os, ((FileEntry) boardfilename).getPath().toString());
            } else {
                StreamUtils.writeDataString(os, boardfilename.getName());
            }
        } else {
            StreamUtils.writeDataString(os, "");
        }
        gGameScreen.captBuffer = null;
    }

    public static void SectorSave(OutputStream os) throws IOException {
        for (int i = 0; i < 64; i++) {
            StreamUtils.writeShort(os, skypanlist[i]);
        }
        StreamUtils.writeShort(os, skypancnt);
        for (int i = 0; i < 32; i++) {
            StreamUtils.writeShort(os, lavadrylandsector[i]);
        }
        StreamUtils.writeShort(os, lavadrylandcnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, dragsectorlist[i]);
            StreamUtils.writeShort(os, dragxdir[i]);
            StreamUtils.writeShort(os, dragydir[i]);
            StreamUtils.writeInt(os, dragx1[i]);
            StreamUtils.writeInt(os, dragy1[i]);
            StreamUtils.writeInt(os, dragx2[i]);
            StreamUtils.writeInt(os, dragy2[i]);
            StreamUtils.writeInt(os, dragfloorz[i]);
        }
        StreamUtils.writeShort(os, dragsectorcnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, bobbingsectorlist[i]);
        }
        StreamUtils.writeShort(os, bobbingsectorcnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, warpsectorlist[i]);
        }
        StreamUtils.writeShort(os, warpsectorcnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, xpanningsectorlist[i]);
        }
        StreamUtils.writeShort(os, xpanningsectorcnt);
        for (int i = 0; i < 128; i++) {
            StreamUtils.writeShort(os, ypanningwalllist[i]);
        }
        StreamUtils.writeShort(os, ypanningwallcnt);
        for (int i = 0; i < 64; i++) {
            StreamUtils.writeShort(os, floorpanninglist[i]);
        }
        StreamUtils.writeShort(os, floorpanningcnt);
        StreamUtils.writeShort(os, swingcnt);
        for (int i = 0; i < MAXSWINGDOORS; i++) {
            for (int j = 0; j < 8; j++) {
                StreamUtils.writeShort(os, (short) swingdoor[i].wall[j]);
            }
            StreamUtils.writeShort(os, (short) swingdoor[i].sector);
            StreamUtils.writeShort(os, (short) swingdoor[i].angopen);
            StreamUtils.writeShort(os, (short) swingdoor[i].angclosed);
            StreamUtils.writeShort(os, (short) swingdoor[i].angopendir);
            StreamUtils.writeShort(os, (short) swingdoor[i].ang);
            StreamUtils.writeShort(os, (short) swingdoor[i].anginc);
            for (int j = 0; j < 8; j++) {
                StreamUtils.writeInt(os, swingdoor[i].x[j]);
            }
            for (int j = 0; j < 8; j++) {
                StreamUtils.writeInt(os, swingdoor[i].y[j]);
            }
        }

        StreamUtils.writeShort(os, revolvecnt);
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeShort(os, revolvesector[i]);
            StreamUtils.writeShort(os, revolveang[i]);
            for (int j = 0; j < 32; j++) {
                StreamUtils.writeInt(os, revolvex[i][j]);
                StreamUtils.writeInt(os, revolvey[i][j]);
            }
            StreamUtils.writeInt(os, revolvepivotx[i]);
            StreamUtils.writeInt(os, revolvepivoty[i]);
        }
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, revolveclip[i]);
        }

        StreamUtils.writeShort(os, ironbarscnt);
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, ironbarsector[i]);
            StreamUtils.writeInt(os, ironbarsgoal[i]);
            StreamUtils.writeInt(os, ironbarsgoal1[i]);
            StreamUtils.writeInt(os, ironbarsgoal2[i]);
            StreamUtils.writeShort(os, ironbarsdone[i]);
            StreamUtils.writeShort(os, ironbarsanim[i]);
        }
    }

    public static void StuffSave(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, kills);
        StreamUtils.writeInt(os, killcnt);
        StreamUtils.writeInt(os, treasurescnt);
        StreamUtils.writeInt(os, treasuresfound);
        StreamUtils.writeInt(os, hours);
        StreamUtils.writeInt(os, minutes);
        StreamUtils.writeInt(os, seconds);

        StreamUtils.writeInt(os, visibility);
        StreamUtils.writeInt(os, thunderflash);
        StreamUtils.writeInt(os, thundertime);
        StreamUtils.writeInt(os, engine.getrand());

        show2dsector.writeObject(os);
        show2dwall.writeObject(os);
        show2dsprite.writeObject(os);
        StreamUtils.writeByte(os, automapping);

        StreamUtils.writeShort(os, pskybits);
        WHRenderer renderer = game.getRenderer();
        StreamUtils.writeInt(os, renderer.getParallaxScale());
        for (int i = 0; i < MAXPSKYTILES; i++) {
            StreamUtils.writeShort(os, pskyoff[i]);
        }

        StreamUtils.writeShort(os, connecthead);
        for (int i = 0; i < MAXPLAYERS; i++) {
            StreamUtils.writeShort(os, connectpoint2[i]);
        }

        for (int i = 0; i < MAXPLAYERS; i++) {
            player[i].writeObject(os);
        }

        StreamUtils.writeInt(os, player[myconnectindex].getDropshieldcnt());
        StreamUtils.writeByte(os, player[myconnectindex].isDroptheshield() ? (byte) 1 : 0);

        // WH2
        StreamUtils.writeInt(os, floormirrorcnt);
        for (int i = 0; i < 64; i++) {
            StreamUtils.writeShort(os, floormirrorsector[i]);
        }
        for (int i = 0; i < ARROWCOUNTLIMIT; i++) {
            StreamUtils.writeShort(os, arrowsprite[i]);
        }
        for (int i = 0; i < THROWPIKELIMIT; i++) {
            StreamUtils.writeShort(os, throwpikesprite[i]);
        }

        StreamUtils.writeInt(os, MAXSECTORS);
        for (int i = 0; i < MAXSECTORS; i++) {
            StreamUtils.writeByte(os, ceilingshadearray[i]);
            StreamUtils.writeByte(os, floorshadearray[i]);
        }
        StreamUtils.writeInt(os, MAXWALLS);
        for (int i = 0; i < MAXWALLS; i++) {
            StreamUtils.writeByte(os, wallshadearray[i]);
        }
    }

    public static void MapSave(OutputStream os) throws IOException {
        Board board = boardService.getBoard();
        Sector[] sectors = board.getSectors();
        StreamUtils.writeInt(os, sectors.length);
        for (Sector s : sectors) {
            s.writeObject(os);
        }

        Wall[] walls = board.getWalls();
        StreamUtils.writeInt(os, walls.length);
        for (Wall wal : walls) {
            wal.writeObject(os);
        }

        List<Sprite> sprites = board.getSprites();
        StreamUtils.writeInt(os, sprites.size());
        for (Sprite s : sprites) {
            s.writeObject(os);
        }
    }

    public static void SaveVersion(OutputStream os, int nVersion) throws IOException {
        StreamUtils.writeString(os, savsign);
        StreamUtils.writeShort(os, nVersion);
    }

    public static void AnimationSave(OutputStream os) throws IOException {
        for (int i = 0; i < MAXANIMATES; i++) {
            StreamUtils.writeShort(os, gAnimationData[i].id);
            StreamUtils.writeByte(os, gAnimationData[i].type);
            StreamUtils.writeInt(os, gAnimationData[i].goal);
            StreamUtils.writeInt(os, gAnimationData[i].vel);
            StreamUtils.writeInt(os, gAnimationData[i].acc);
        }
        StreamUtils.writeInt(os, gAnimationCount);
    }

    public static void quicksave() {
        if (player[myconnectindex].getHealth() != 0) {
            gQuickSaving = true;
        }
    }
}
