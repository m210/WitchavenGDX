package ru.m210projects.Witchaven;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Strhandler.toCharArray;
import static ru.m210projects.Witchaven.AI.AIGonzo.deaddude;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Items.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Screens.DemoScreen.checkCodeVersion;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHANI.isBlades;
import static ru.m210projects.Witchaven.WHFX.justwarpedfx;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.*;

public class WHOBJ {

//    public final static int eg_onyx_effect = 1;
    public static final int NORMALCLIP = 0;
    public static final int PROJECTILECLIP = 1;
    public static final int CLIFFCLIP = 2;
    public static int justwarpedcnt = 0;
    public static byte flashflag = 0x00;
    public static final short[] torchpattern = {2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 6, 6, 4, 4, 6, 6, 4, 4, 6, 6, 4, 4, 6, 6, 4, 4,
            6, 6, 5, 5, 4, 4, 3, 3, 3, 2, 2, 2};
    // EG 19 Aug 2017 - Try to prevent monsters teleporting back and forth wildly
    public static int monsterwarptime;

    // see if picked up any objects?
    // JSA 4_27 play appropriate sounds pending object picked up

    public static short adjusthp(int hp) {
        if (checkCodeVersion(game.getScreen(), 101)) {
            float factor = (float) (engine.krand() % 20) / 100;
            int howhard = difficulty;

            if (engine.krand() % 100 > 50) {
                return (short) ((hp * (factor + 1)) * howhard);
            }
            return (short) ((hp - (hp * (factor))) * howhard);
        }

        engine.krand();
        engine.krand();

        return (short) (hp * difficulty);
    }

    public static void timerprocess(PLAYER plr) {
        Renderer renderer = game.getRenderer();
        if (justwarpedfx > 0) {
            justwarpedfx -= TICSPERFRAME;
            justwarpedcnt += TICSPERFRAME << 6;

            if (justwarpedfx <= 0) {
                justwarpedcnt = 0;
            }
        }

        if (plr.getPoisoned() == 1) {
            if (plr.getPoisontime() >= 0) {
                plr.setPoisontime(plr.getPoisontime() - TICSPERFRAME);

                addhealth(plr, 0);

                if (plr.getPoisontime() < 0) {
                    startredflash(50);
                    addhealth(plr, -10);
                    plr.setPoisontime(7200);
                }
            }
        }

        // EG 19 Aug 2017 - Try to prevent monsters teleporting back and forth wildly
        if (monsterwarptime > 0) {
            monsterwarptime -= TICSPERFRAME;
        }

        if (plr.getVampiretime() > 0) {
            plr.setVampiretime(plr.getVampiretime() - TICSPERFRAME);
        }

        if (plr.getShockme() >= 0) {
            plr.setShockme(plr.getShockme() - TICSPERFRAME);
        }

        if (plr.getHelmettime() > 0) {
            plr.setHelmettime(plr.getHelmettime() - TICSPERFRAME);
        }

        if (displaytime > 0) {
            displaytime -= TICSPERFRAME;
        }

        if (plr.getShadowtime() >= 0) {
            plr.setShadowtime(plr.getShadowtime() - TICSPERFRAME);
        }

        if (plr.getNightglowtime() >= 0) {
            plr.setNightglowtime(plr.getNightglowtime() - TICSPERFRAME);
            visibility = 256;
            if (plr.getNightglowtime() < 0) {
                visibility = 1024;
            }
        }

        if (plr.getStrongtime() >= 0) {
            plr.setStrongtime(plr.getStrongtime() - TICSPERFRAME);
            whitecount = 10;
        }

        if (plr.getInvisibletime() >= 0) {
            plr.setInvisibletime(plr.getInvisibletime() - TICSPERFRAME);
        }

        if (plr.getInvincibletime() >= 0) {
            plr.setInvincibletime(plr.getInvincibletime() - TICSPERFRAME);
            whitecount = 10;
        }

        if (plr.getManatime() >= 0) {
            plr.setManatime(plr.getManatime() - TICSPERFRAME);
            redcount = 20;
        }

        if (plr.getSpiked() != 0) {
            plr.setSpiketics(plr.getSpiketics() - TICSPERFRAME);
        }

        if (displaytime <= 0) {
            if (plr.getManatime() > 0) {
                if (plr.getManatime() < 512) {
                    if ((plr.getManatime() % 64) > 32) {
                        return;
                    }
                }
                game.getFont(1).drawTextScaled(renderer, 18, 24, toCharArray("FIRE RESISTANCE"), 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            } else if (plr.getPoisoned() == 1) {
                game.getFont(1).drawTextScaled(renderer, 18, 24, toCharArray("POISONED"), 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            } else if (plr.getOrbactive()[5] > 0) {
                if (plr.getOrbactive()[5] < 512) {
                    if ((plr.getOrbactive()[5] % 64) > 32) {
                        return;
                    }
                }
                game.getFont(1).drawTextScaled(renderer, 18, 24, toCharArray("FLYING"), 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            } else if (plr.getVampiretime() > 0) {
                game.getFont(1).drawTextScaled(renderer, 18, 24, toCharArray("ORNATE HORN"), 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            }
        }
    }

    public static int getPickHeight() {
        if (game.WH2) {
            return PICKHEIGHT2;
        } else {
            return PICKHEIGHT;
        }
    }

    public static void processobjs(PLAYER plr) {
        if (plr.getSector() < 0 || plr.getSector() >= boardService.getSectorCount()) {
            return;
        }

        ListNode<Sprite> node = boardService.getSectNode(plr.getSector());
        while (node != null) {
            ListNode<Sprite> nextNode = node.getNext();
            Sprite spr = node.get();

            int dx = klabs(plr.getX() - spr.getX()); // x distance to sprite
            int dy = klabs(plr.getY() - spr.getY()); // y distance to sprite
            int dz = klabs((plr.getZ() >> 8) - (spr.getZ() >> 8)); // z distance to sprite
            int dh = engine.getTile(spr.getPicnum()).getHeight() >> 1; // height of sprite
            if (dx + dy < PICKDISTANCE && dz - dh <= getPickHeight()) {
                if (isItemSprite(spr)) {
                    items[(spr.getDetail() & 0xFF) - ITEMSBASE].pickup(plr, (short) node.getIndex());
                }

                if (spr.getPicnum() >= EXPLOSTART && spr.getPicnum() <= EXPLOEND && spr.getOwner() != plr.getSprite().getOwner()) {
                    if (plr.getManatime() < 1) {
                        addhealth(plr, -1);
                    }
                }
            }
            node = nextNode;
        }
    }

    public static void newstatus(final int sn, int seq) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }
        
        switch (seq) {
            case AMBUSH:
                engine.changespritestat(sn, AMBUSH);
                break;
            case LAND:
                engine.changespritestat(sn, LAND);
                break;
            case EVILSPIRIT:
                engine.changespritestat(sn, EVILSPIRIT);
                spr.setLotag((short) (120 + (engine.krand() & 64)));
                break;
            case PATROL:
                engine.changespritestat(sn, PATROL);
                break;
            case WARPFX:
                engine.changespritestat(sn, WARPFX);
                spr.setLotag(12);
                break;
            case NUKED:
                engine.changespritestat(sn, NUKED);
                if (!game.WH2) {
                    spr.setLotag(24);
                }
                break;
            case BROKENVASE:
                engine.changespritestat(sn, BROKENVASE);
                switch (spr.getPicnum()) {
                    case VASEA:
                        playsound_loc(S_GLASSBREAK1 + (engine.krand() % 3), spr.getX(), spr.getY());
                        spr.setPicnum(SHATTERVASE);
                        break;
                    case VASEB:
                        playsound_loc(S_GLASSBREAK1 + (engine.krand() % 3), spr.getX(), spr.getY());
                        spr.setPicnum(SHATTERVASE2);
                        break;
                    case VASEC:
                        playsound_loc(S_GLASSBREAK1 + (engine.krand() % 3), spr.getX(), spr.getY());
                        spr.setPicnum(SHATTERVASE3);
                        break;
                    case STAINGLASS1:
                    case STAINGLASS2:
                    case STAINGLASS3:
                    case STAINGLASS4:
                    case STAINGLASS5:
                    case STAINGLASS6:
                    case STAINGLASS7:
                    case STAINGLASS8:
                    case STAINGLASS9:
                        spr.setPicnum(spr.getPicnum() + 1);
                        SND_Sound(S_BIGGLASSBREAK1 + (engine.krand() % 3));
                        break;
                    case FBARRELFALL:
                    case BARREL:
                        playsound_loc(S_BARRELBREAK, spr.getX(), spr.getY());
                        spr.setPicnum(FSHATTERBARREL);
                        break;
                }
                spr.setLotag(12);
                spr.setCstat(spr.getCstat() & ~3);
                break;
            case DRAIN:
                engine.changespritestat(sn, DRAIN);
                spr.setLotag(24);
                spr.setPal(7);
                break;
            case ANIMLEVERDN:
                playsound_loc(S_PULLCHAIN1, spr.getX(), spr.getY());
                spr.setPicnum(LEVERUP);
                engine.changespritestat(sn, ANIMLEVERDN);
                spr.setLotag(24);
                break;
            case ANIMLEVERUP:
                playsound_loc(S_PULLCHAIN1, spr.getX(), spr.getY());
                spr.setPicnum(LEVERDOWN);
                engine.changespritestat(sn, ANIMLEVERUP);
                spr.setLotag(24);
                break;
            case SKULLPULLCHAIN1:
            case PULLTHECHAIN:
                playsound_loc(S_PULLCHAIN1, spr.getX(), spr.getY());
                engine.changespritestat(sn, PULLTHECHAIN);
                SND_Sound(S_CHAIN1);
                spr.setLotag(24);
                break;
            case FROZEN:
                // JSA_NEW
                playsound_loc(S_FREEZE, spr.getX(), spr.getY());
                engine.changespritestat(sn, FROZEN);
                spr.setLotag(3600);
                break;
            case DEVILFIRE:
                engine.changespritestat(sn, DEVILFIRE);
                spr.setLotag((short) (engine.krand() & 120 + 360));
                break;
            case DRIP:
                engine.changespritestat(sn, DRIP);
                break;
            case BLOOD:
                engine.changespritestat(sn, BLOOD);
                break;
            case WAR:
                engine.changespritestat(sn, WAR);
                break;
            case PAIN:
                spr.setLotag(36);
                switch (spr.getDetail()) {
                    case DEMONTYPE:
                        spr.setLotag(24);
                        playsound_loc(S_GUARDIANPAIN1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        spr.setPicnum(DEMON - 1);
                        engine.changespritestat(sn, PAIN);
                        break;
                    case NEWGUYTYPE:
                        spr.setLotag(24);
                        spr.setPicnum(NEWGUYPAIN);
                        engine.changespritestat(sn, PAIN);
                        playsound_loc(S_AGM_PAIN1, spr.getX(), spr.getY());
                        break;

                    case KURTTYPE:
                        spr.setLotag(24);
                        spr.setPicnum(GONZOCSWPAIN);
                        engine.changespritestat(sn, PAIN);
                        playsound_loc(S_GRONPAINA + (engine.krand() % 3), spr.getX(), spr.getY());
                        break;

                    case GONZOTYPE:
                        spr.setLotag(24);
                        switch (spr.getPicnum()) {
                            case KURTSTAND:
                            case KURTKNEE:
                            case KURTAT:
                            case KURTPUNCH:
                            case KURTREADY:
                            case KURTREADY + 1:
                            case GONZOCSW:
                                spr.setPicnum(GONZOCSWPAIN);
                                playsound_loc(S_GRONPAINA + (engine.krand() % 3), spr.getX(), spr.getY());
                                break;
                            case GONZOGSW:
                                spr.setPicnum(GONZOGSWPAIN);
                                playsound_loc(S_GRONPAINA + (engine.krand() % 3), spr.getX(), spr.getY());
                                break;
                            case GONZOGHM:
                                spr.setPicnum(GONZOGHMPAIN);
                                playsound_loc(S_GRONPAINA + (engine.krand() % 3), spr.getX(), spr.getY());
                                break;
                            case GONZOGSH:
                                spr.setPicnum(GONZOGSHPAIN);
                                playsound_loc(S_GRONPAINA, spr.getX(), spr.getY());
                                break;
                            default:
                                engine.changespritestat(sn, FLEE);
                                break;
                        }
                        engine.changespritestat(sn, PAIN);
                        break;
                    case KATIETYPE:
                        spr.setPicnum(KATIEPAIN);
                        engine.changespritestat(sn, PAIN);
                        break;
                    case JUDYTYPE:
                        spr.setLotag(24);
                        spr.setPicnum(JUDY);
                        engine.changespritestat(sn, PAIN);
                        break;
                    case FATWITCHTYPE:
                        spr.setLotag(24);
                        spr.setPicnum(FATWITCHDIE);
                        engine.changespritestat(sn, PAIN);
                        break;
                    case SKULLYTYPE:
                        spr.setLotag(24);
                        spr.setPicnum(SKULLYDIE);
                        engine.changespritestat(sn, PAIN);
                        break;
                    case GUARDIANTYPE:
                        spr.setLotag(24);
                        // spr.picnum=GUARDIANATTACK;
                        playsound_loc(S_GUARDIANPAIN1 + (engine.krand() % 2), spr.getX(), spr.getY());

                        if (game.WH2) {
                            spr.setPicnum(GUARDIAN);
                        } else {
                            spr.setPicnum(GUARDIANCHAR);
                        }
                        engine.changespritestat(sn, PAIN);
                        break;
                    case GRONTYPE:
                        spr.setLotag(24);
                        engine.changespritestat(sn, PAIN);
                        playsound_loc(S_GRONPAINA + engine.krand() % 3, spr.getX(), spr.getY());

                        if (spr.getPicnum() == GRONHAL || spr.getPicnum() == GRONHALATTACK) {
                            spr.setPicnum(GRONHALPAIN);
                        } else if (spr.getPicnum() == GRONSW || spr.getPicnum() == GRONSWATTACK) {
                            spr.setPicnum(GRONSWPAIN);
                        } else if (spr.getPicnum() == GRONMU || spr.getPicnum() == GRONMUATTACK) {
                            spr.setPicnum(GRONMUPAIN);
                        }
                        break;
                    case KOBOLDTYPE:
                        spr.setPicnum(KOBOLDDIE);
                        engine.changespritestat(sn, PAIN);
                        playsound_loc(S_KPAIN1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        break;
                    case DEVILTYPE:
                        playsound_loc(S_MPAIN1, spr.getX(), spr.getY());
                        spr.setPicnum(DEVILPAIN);
                        engine.changespritestat(sn, PAIN);
                        break;
                    case FREDTYPE:
                        spr.setPicnum(FREDPAIN);
                        engine.changespritestat(sn, PAIN);
                        // EG: Sounds for Fred (currently copied from ogre)
                        playsound_loc(S_KPAIN1 + (engine.rand() % 2), spr.getX(), spr.getY());
                        break;
                    case GOBLINTYPE:
                    case IMPTYPE:
                        if (game.WH2 && (spr.getPicnum() == IMP || spr.getPicnum() == IMPATTACK)) {
                            spr.setLotag(24);
                            spr.setPicnum(IMPPAIN);
                            engine.changespritestat(sn, PAIN);
                        } else {
                            spr.setPicnum(GOBLINPAIN);
                            engine.changespritestat(sn, PAIN);
                            playsound_loc(S_GOBPAIN1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        }
                        break;
                    case MINOTAURTYPE:
                        spr.setPicnum(MINOTAURPAIN);
                        engine.changespritestat(sn, PAIN);
                        playsound_loc(S_MPAIN1, spr.getX(), spr.getY());
                        break;
                    default:
                        engine.changespritestat(sn, FLEE);
                        break;
                }
                break;
            case FLOCKSPAWN:
                spr.setLotag(36);
                spr.setExtra(10);
                engine.changespritestat(sn, FLOCKSPAWN);
                break;
            case FLOCK:
                spr.setLotag(128);
                spr.setExtra(0);
                spr.setPal(0);
                engine.changespritestat(sn, FLOCK);
                break;
            case FINDME:
                spr.setLotag(360);
                if (spr.getPicnum() == RAT) {
                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                    engine.changespritestat(sn, FLEE);
                } else {
                    engine.changespritestat(sn, FINDME);
                }
                break;
            case SKIRMISH:
                spr.setLotag(60);
                if (spr.getPicnum() == RAT) {
                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                    engine.changespritestat(sn, FLEE);
                } else {
                    engine.changespritestat(sn, SKIRMISH);
                }
                break;
            case CHILL:
                spr.setLotag(60);
                engine.changespritestat(sn, CHILL);
                break;
            case WITCHSIT:
                spr.setLotag(12);
                engine.changespritestat(sn, WITCHSIT);
                break;
            case DORMANT:
                spr.setLotag((short) (engine.krand() & 2047 + 2047));
                break;
            case ACTIVE:
                spr.setLotag(360);
                break;
            case FLEE:
                switch (spr.getDetail()) {
                    case GONZOTYPE:

                        switch (spr.getPicnum()) {
                            case GONZOCSWAT:
                            case KURTSTAND:
                            case KURTKNEE:
                            case KURTAT:
                            case KURTPUNCH:
                                spr.setPicnum(GONZOCSW);
                                break;
                            case GONZOGSWAT:
                                spr.setPicnum(GONZOGSW);
                                break;
                            case GONZOGHMAT:
                                spr.setPicnum(GONZOGHM);
                                break;
                            case GONZOGSHAT:
                                spr.setPicnum(GONZOGSH);
                                break;
                        }
                        break;
                    case NEWGUYTYPE:
                        spr.setPicnum(NEWGUY);
                        break;
                    case KURTTYPE:
                        spr.setPicnum(GONZOCSW);
                        break;
                    case GRONTYPE:
                        if (spr.getPicnum() == GRONHALATTACK) {
                            spr.setPicnum(GRONHAL);
                        } else if (spr.getPicnum() == GRONMUATTACK) {
                            spr.setPicnum(GRONMU);
                        } else if (spr.getPicnum() == GRONSWATTACK) {
                            spr.setPicnum(GRONSW);
                        }
                        break;
                    case DEVILTYPE:
                        spr.setPicnum(DEVIL);
                        break;
                    case KOBOLDTYPE:
                        spr.setPicnum(KOBOLD);
                        break;
                    case MINOTAURTYPE:
                        spr.setPicnum(MINOTAUR);
                        break;
                    case SKELETONTYPE:
                        spr.setPicnum(SKELETON);
                        break;
                    case FREDTYPE:
                        spr.setPicnum(FRED);
                        break;
                    case GOBLINTYPE:
                        spr.setPicnum(GOBLIN);
                        break;
                }

                engine.changespritestat(sn, FLEE);
                if (!game.WH2 && spr.getPicnum() == DEVILATTACK && spr.getPicnum() == DEVIL) {
                    spr.setLotag((short) (120 + (engine.krand() & 360)));
                } else {
                    spr.setLotag(60);
                }
                break;
            case BOB:
                engine.changespritestat(sn, BOB);
                break;
            case LIFTUP:
                if (cartsnd == -1) {
                    playsound_loc(S_CLUNK, spr.getX(), spr.getY());
                    cartsnd = playsound(S_CHAIN1, spr.getX(), spr.getY(), 5);
                }

                engine.changespritestat(sn, LIFTUP);
                break;
            case LIFTDN:
                if (cartsnd == -1) {
                    playsound_loc(S_CLUNK, spr.getX(), spr.getY());
                    cartsnd = playsound(S_CHAIN1, spr.getX(), spr.getY(), 5);
                }

                engine.changespritestat(sn, LIFTDN);
                break;
            case SHOVE:
                spr.setLotag(128);
                engine.changespritestat(sn, SHOVE);
                break;
            case SHATTER:
                engine.changespritestat(sn, SHATTER);
                if (spr.getPicnum() == FBARRELFALL) {
                    spr.setPicnum(FSHATTERBARREL);
                }
                break;
            case YELL:
                engine.changespritestat(sn, YELL);
                spr.setLotag(12);
                break;
            case ATTACK2: //WH1
                if (game.WH2) {
                    break;
                }
                spr.setLotag(40);
                spr.setCstat(spr.getCstat() | 1);
                engine.changespritestat(sn, ATTACK2);
                spr.setPicnum(DRAGONATTACK2);
                playsound_loc(S_DRAGON1 + (engine.krand() % 3), spr.getX(), spr.getY());
            case ATTACK:
                spr.setLotag(64);
                spr.setCstat(spr.getCstat() | 1);
                engine.changespritestat(sn, ATTACK);
                switch (spr.getDetail()) {
                    case NEWGUYTYPE:
                        if (spr.getExtra() > 20) {
                            spr.setPicnum(NEWGUYCAST);
                            spr.setLotag(24);
                        } else if (spr.getExtra() > 10) {
                            spr.setPicnum(NEWGUYBOW);
                        } else if (spr.getExtra() > 0) {
                            spr.setPicnum(NEWGUYMACE);
                        } else {
                            spr.setPicnum(NEWGUYPUNCH);
                        }
                        break;
                    case GONZOTYPE:
                    case KURTTYPE:
                        switch (spr.getPicnum()) {
                            case GONZOCSW:
                                if (spr.getExtra() > 10) {
                                    spr.setPicnum(GONZOCSWAT);
                                } else if (spr.getExtra() > 0) {
                                    spr.setPicnum(KURTREADY);
                                    spr.setLotag(12);
                                } else {
                                    spr.setPicnum(KURTPUNCH);
                                }
                                break;
                            case GONZOGSW:
                                spr.setPicnum(GONZOGSWAT);
                                break;
                            case GONZOGHM:
                                spr.setPicnum(GONZOGHMAT);
                                break;
                            case GONZOGSH:
                                spr.setPicnum(GONZOGSHAT);
                                break;
                        }
                        break;
                    case KATIETYPE:
                        if ((engine.krand() % 10) > 4) {
                            playsound_loc(S_JUDY1, spr.getX(), spr.getY());
                        }
                        spr.setPicnum(KATIEAT);
                        break;
                    case DEMONTYPE:
                        playsound_loc(S_GUARDIAN1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        spr.setPicnum(DEMON);
                        break;
                    case GRONTYPE:
                        if (spr.getPicnum() == GRONHAL) {
                            spr.setPicnum(GRONHALATTACK);
                        } else if (spr.getPicnum() == GRONMU) {
                            spr.setPicnum(GRONMUATTACK);
                        } else if (spr.getPicnum() == GRONSW) {
                            spr.setPicnum(GRONSWATTACK);
                        }
                        break;
                    case KOBOLDTYPE:
                        spr.setPicnum(KOBOLDATTACK);
                        if (engine.krand() % 10 > 4) {
                            playsound_loc(S_KSNARL1 + (engine.krand() % 4), spr.getX(), spr.getY());
                        }
                        break;
                    case DRAGONTYPE:
                        if ((engine.krand() % 10) > 3) {
                            playsound_loc(S_DRAGON1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        }

                        spr.setPicnum(DRAGONATTACK);
                        break;
                    case DEVILTYPE:
                        if ((engine.krand() % 10) > 4) {
                            playsound_loc(S_DEMON1 + (engine.krand() % 5), spr.getX(), spr.getY());
                        }

                        spr.setPicnum(DEVILATTACK);
                        break;
                    case FREDTYPE:
                        spr.setPicnum(FREDATTACK);
                        /* EG: Sounds for Fred (currently copied from Ogre) */
                        if (engine.rand() % 10 > 4) { // playsound_loc
                            playsound_loc(S_KSNARL1 + (engine.rand() % 4), spr.getX(), spr.getY());
                        }
                        break;
                    case SKELETONTYPE:
                        spr.setPicnum(SKELETONATTACK);
                        break;
                    case IMPTYPE:
                        spr.setLotag(92);
                        if ((engine.krand() % 10) > 5) {
                            playsound_loc(S_IMPGROWL1 + (engine.krand() % 3), spr.getX(), spr.getY());
                        }
                        spr.setPicnum(IMPATTACK);
                        break;
                    case GOBLINTYPE:
                        if ((engine.krand() % 10) > 5) {
                            playsound_loc(S_GOBLIN1 + (engine.krand() % 3), spr.getX(), spr.getY());
                        }
                        spr.setPicnum(GOBLINATTACK);
                        break;
                    case MINOTAURTYPE:
                        if ((engine.krand() % 10) > 4) {
                            playsound_loc(S_MSNARL1 + (engine.krand() % 3), spr.getX(), spr.getY());
                        }

                        spr.setPicnum(MINOTAURATTACK);
                        break;
                    case SKULLYTYPE:
                        spr.setPicnum(SKULLYATTACK);
                        break;
                    case FATWITCHTYPE:
                        if ((engine.krand() % 10) > 4) {
                            playsound_loc(S_FATLAUGH, spr.getX(), spr.getY());
                        }
                        spr.setPicnum(FATWITCHATTACK);
                        break;
                    case JUDYTYPE:
                        // spr.cstat=0;
                        if (engine.krand() % 2 == 0) {
                            spr.setPicnum(JUDYATTACK1);
                        } else {
                            spr.setPicnum(JUDYATTACK2);
                        }
                        break;
                    case WILLOWTYPE:
                        playsound_loc(S_WISP + (engine.krand() % 2), spr.getX(), spr.getY());
                        spr.setPal(7);
                        break;
                    case GUARDIANTYPE:
                        playsound_loc(S_GUARDIAN1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        spr.setPicnum(GUARDIANATTACK);
                        break;
                }
                break;
            case FACE:
                engine.changespritestat(sn, FACE);
                break;
            case STAND:
                engine.changespritestat(sn, FACE);
                spr.setLotag(0);
                break;
            case CHASE:
                if (spr.getPicnum() == RAT) {
                    engine.changespritestat(sn, FLEE);
                } else {
                    engine.changespritestat(sn, CHASE);
                }
                spr.setLotag(256);
                switch (spr.getDetail()) {
                    case NEWGUYTYPE:
                        spr.setPicnum(NEWGUY);
                        break;
                    case KATIETYPE:
                        spr.setPicnum(KATIE);
                        break;
                    case DEMONTYPE:
                        spr.setPicnum(DEMON);
                        break;
                    case KURTTYPE:
                        spr.setPicnum(GONZOCSW);
                        break;
                    case GONZOTYPE:
                        switch (spr.getPicnum()) {
                            case GONZOCSWAT:
                            case KURTSTAND:
                            case KURTKNEE:
                            case KURTAT:
                            case KURTPUNCH:
                                spr.setPicnum(GONZOCSW);
                                break;
                            case GONZOGSWAT:
                                spr.setPicnum(GONZOGSW);
                                break;
                            case GONZOGHMAT:
                                spr.setPicnum(GONZOGHM);
                                break;
                            case GONZOGSHAT:
                                spr.setPicnum(GONZOGSH);
                                break;
                        }
                        break;
                    case GRONTYPE:
                        if (spr.getPicnum() == GRONHALATTACK) {
                            if (spr.getExtra() > 2) {
                                spr.setPicnum(GRONHAL);
                            } else {
                                spr.setPicnum(GRONMU);
                            }
                        } else if (spr.getPicnum() == GRONSWATTACK) {
                            spr.setPicnum(GRONSW);
                        } else if (spr.getPicnum() == GRONMUATTACK) {
                            if (spr.getExtra() > 0) {
                                spr.setPicnum(GRONMU);
                            } else {
                                spr.setPicnum(GRONSW);
                            }
                        }
                        break;
                    case KOBOLDTYPE:
                        spr.setPicnum(KOBOLD);
                        break;
                    case DRAGONTYPE:
                        spr.setPicnum(DRAGON);
                        break;
                    case DEVILTYPE:
                        spr.setPicnum(DEVIL);
                        break;
                    case FREDTYPE:
                        spr.setPicnum(FRED);
                        break;
                    case SKELETONTYPE:
                        spr.setPicnum(SKELETON);
                        break;
                    case GOBLINTYPE:
                    case IMPTYPE:
                        if (game.WH2 && spr.getPicnum() == IMPATTACK) {
                            if (engine.krand() % 10 > 2) {
                                spr.setPicnum(IMP);
                            }
                        } else {
                            if (engine.krand() % 10 > 2) {
                                playsound_loc(S_GOBLIN1 + (engine.krand() % 3), spr.getX(), spr.getY());
                            }

                            spr.setPicnum(GOBLIN);
                        }
                        break;
                    case MINOTAURTYPE:
                        // JSA_DEMO3
                        playsound_loc(S_MSNARL1 + (engine.krand() % 4), spr.getX(), spr.getY());
                        spr.setPicnum(MINOTAUR);
                        break;
                    case SKULLYTYPE:
                        spr.setPicnum(SKULLY);
                        break;
                    case FATWITCHTYPE:
                        spr.setPicnum(FATWITCH);
                        break;
                    case JUDYTYPE:
                        spr.setPicnum(JUDY);
                        break;
                    case GUARDIANTYPE:
                        spr.setPicnum(GUARDIAN);
                        break;
                    case WILLOWTYPE:
                        spr.setPal(6);
                        break;
                }
                break;
            case MISSILE:
                engine.changespritestat(sn, MISSILE);
                break;
            case CAST:
                engine.changespritestat(sn, CAST);
                spr.setLotag(12);

                if (spr.getPicnum() == GRONHALATTACK
                        || spr.getPicnum() == GONZOCSWAT
                        || spr.getPicnum() == NEWGUY) {
                    spr.setLotag(24);
                } else if (spr.getPicnum() == GRONMUATTACK) {
                    spr.setLotag(36);
                }
                break;
            case FX:
                engine.changespritestat(sn, FX);
                break;
            case DIE:
                if (spr.getStatnum() == DIE || spr.getStatnum() == DEAD) //already dying
                {
                    break;
                }

                if (spr.getDetail() != GONZOTYPE || spr.getShade() != 31) {
                    spr.setCstat(spr.getCstat() & ~3);
                } else {
                    spr.setCstat(spr.getCstat() & ~1);
                }
                switch (spr.getDetail()) {
                    case NEWGUYTYPE:
                        spr.setLotag(20);
                        spr.setPicnum(NEWGUYDIE);
                        playsound_loc(S_AGM_DIE1 + (engine.krand() % 3), spr.getX(), spr.getY());
                        break;
                    case KURTTYPE:
                    case GONZOTYPE:
                        spr.setLotag(20);
                        playsound_loc(S_GRONDEATHA + engine.krand() % 3, spr.getX(), spr.getY());
                        switch (spr.getPicnum()) {
                            case KURTSTAND:
                            case KURTKNEE:
                            case KURTAT:
                            case KURTPUNCH:
                            case KURTREADY:
                            case KURTREADY + 1:
                            case GONZOCSW:
                            case GONZOCSWAT:
                            case GONZOCSWPAIN:
                                spr.setPicnum(GONZOCSWPAIN);
                                break;
                            case GONZOGSW:
                            case GONZOGSWAT:
                            case GONZOGSWPAIN:
                                spr.setPicnum(GONZOGSWPAIN);
                                break;
                            case GONZOGHM:
                            case GONZOGHMAT:
                            case GONZOGHMPAIN:
                                spr.setPicnum(GONZOGHMPAIN);
                                break;
                            case GONZOGSH:
                            case GONZOGSHAT:
                            case GONZOGSHPAIN:
                                spr.setPicnum(GONZOGSHPAIN);
                                break;
                            case GONZOBSHPAIN:
                                spr.setPicnum(GONZOBSHPAIN);
                                if (spr.getShade() > 30) {
                                    trailingsmoke(sn, false);
                                    engine.deletesprite(sn);
                                    return;
                                }
                                break;
                            default:
                                spr.setLotag(20);
                                spr.setPicnum(GONZOGSWPAIN);
                                return;
                        }
                        break;
                    case KATIETYPE:
                        playsound_loc(S_JUDYDIE, spr.getX(), spr.getY());
                        spr.setLotag(20);
                        spr.setPicnum(KATIEPAIN);
                        break;
                    case DEMONTYPE:
                        playsound_loc(S_GUARDIANDIE, spr.getX(), spr.getY());
                        explosion(sn, spr.getX(), spr.getY(), spr.getZ(), spr.getOwner());
                        engine.deletesprite(sn);
                        addscore(aiGetPlayerTarget(sn), 1500);
                        kills++;
                        return;
                    case GRONTYPE:
                        spr.setLotag(20);
                        playsound_loc(S_GRONDEATHA + engine.krand() % 3, spr.getX(), spr.getY());
                        if (spr.getPicnum() == GRONHAL || spr.getPicnum() == GRONHALATTACK || spr.getPicnum() == GRONHALPAIN) {
                            spr.setPicnum(GRONHALDIE);
                        } else if (spr.getPicnum() == GRONSW || spr.getPicnum() == GRONSWATTACK || spr.getPicnum() == GRONSWPAIN) {
                            spr.setPicnum(GRONSWDIE);
                        } else if (spr.getPicnum() == GRONMU || spr.getPicnum() == GRONMUATTACK || spr.getPicnum() == GRONMUPAIN) {
                            spr.setPicnum(GRONMUDIE);
                        } else {
                            spr.setPicnum(GRONDIE);
                        }
                        break;
                    case FISHTYPE:
                    case RATTYPE:
                        spr.setLotag(20);
                        break;
                    case KOBOLDTYPE:
                        playsound_loc(S_KDIE1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        spr.setLotag(20);
                        spr.setPicnum(KOBOLDDIE);
                        break;
                    case DRAGONTYPE:
                        playsound_loc(S_DEMONDIE1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        spr.setLotag(20);
                        spr.setPicnum(DRAGONDIE);

                        break;
                    case DEVILTYPE:
                        playsound_loc(S_DEMONDIE1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        spr.setLotag(20);
                        spr.setPicnum(DEVILDIE);
                        break;
                    case FREDTYPE:
                        spr.setLotag(20);
                        spr.setPicnum(FREDDIE);
                        /* EG: Sounds for Fred (currently copied from Ogre) */
                        playsound_loc(S_KDIE1 + (engine.rand() % 2), spr.getX(), spr.getY());
                        break;
                    case SKELETONTYPE:
                        playsound_loc(S_SKELETONDIE, spr.getX(), spr.getY());
                        spr.setLotag(20);
                        spr.setPicnum(SKELETONDIE);
                        break;
                    case IMPTYPE:
                        playsound_loc(S_IMPDIE1 + (engine.krand() % 2), spr.getX(), spr.getY());
                        spr.setLotag(20);
                        spr.setPicnum(IMPDIE);
                        break;
                    case GOBLINTYPE:
                        playsound_loc(S_GOBDIE1 + (engine.krand() % 3), spr.getX(), spr.getY());
                        spr.setLotag(20);
                        spr.setPicnum(GOBLINDIE);
                        break;
                    case MINOTAURTYPE:
                        playsound_loc(S_MDEATH1, spr.getX(), spr.getY());
                        spr.setLotag(10);
                        spr.setPicnum(MINOTAURDIE);
                        break;
                    case SPIDERTYPE:
                        spr.setLotag(10);
                        spr.setPicnum(SPIDERDIE);
                        break;
                    case SKULLYTYPE:
                        spr.setLotag(20);
                        spr.setPicnum(SKULLYDIE);
                        playsound_loc(S_SKULLWITCHDIE, spr.getX(), spr.getY());
                        break;
                    case FATWITCHTYPE:
                        spr.setLotag(20);
                        spr.setPicnum(FATWITCHDIE);
                        playsound_loc(S_FATWITCHDIE, spr.getX(), spr.getY());
                        break;
                    case JUDYTYPE:
                        spr.setLotag(20);
                        if (mapon < 24) {
                            for (int j = 0; j < 8; j++) {
                                trailingsmoke(sn, true);
                            }
                            engine.deletesprite(sn);
                            return;
                        } else {
                            spr.setPicnum(JUDYDIE);
                            playsound_loc(S_JUDYDIE, spr.getX(), spr.getY());
                        }
                        break;
                    case GUARDIANTYPE:
                        playsound_loc(S_GUARDIANDIE, spr.getX(), spr.getY());
                        for (int j = 0; j < 4; j++) {
                            explosion(sn, spr.getX(), spr.getY(), spr.getZ(), spr.getOwner());
                        }
                        engine.deletesprite(sn);
                        addscore(aiGetPlayerTarget(sn), 1500);
                        kills++;
                        return;
                    case WILLOWTYPE:
                        playsound_loc(S_WILLOWDIE, spr.getX(), spr.getY());
                        spr.setPal(0);
                        spr.setLotag(20);
                        spr.setPicnum(WILLOWEXPLO);
                        break;
                }
                engine.changespritestat(sn, DIE);
                break;

            case RESURECT:
                spr.setLotag(7200);
                switch (spr.getPicnum()) {
                    case GONZOBSHDEAD:
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 85);
                        spr.setDetail(GONZOTYPE);
                        break;
                    case NEWGUYDEAD:
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 55);
                        spr.setDetail(NEWGUYTYPE);
                        break;
                    case GONZOCSWDEAD:
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 55);
                        spr.setDetail(GONZOTYPE);
                        break;
                    case GONZOGSWDEAD:
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 105);
                        spr.setDetail(GONZOTYPE);
                        break;
                    case GONZOGHMDEAD:
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 100);
                        spr.setDetail(GONZOTYPE);
                        break;
                    case GONZOGSHDEAD:
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 110);
                        spr.setDetail(GONZOTYPE);
                        break;
                    case KATIEDEAD:
                        trailingsmoke(sn, true);
                        spr.setPicnum(KATIEDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        spawnhornskull(sn);
                        addscore(aiGetPlayerTarget(sn), 5000);
                        spr.setDetail(KATIETYPE);
                        break;
                    case DEVILDEAD:
                        trailingsmoke(sn, true);
                        spr.setPicnum(DEVILDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 70);
                        spr.setDetail(DEVILTYPE);
                        break;
                    case IMPDEAD:
                        spr.setPicnum(IMPDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 115);
                        spr.setDetail(IMPTYPE);
                        break;
                    case KOBOLDDEAD:
                        spr.setPicnum(KOBOLDDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        spr.setDetail(KOBOLDTYPE);
                        if (game.WH2) {
                            switch (spr.getPal()) {
                                case 0:
                                    addscore(aiGetPlayerTarget(sn), 25);
                                    break;
                                case 7:
                                    addscore(aiGetPlayerTarget(sn), 40);
                                    break;
                            }
                            addscore(aiGetPlayerTarget(sn), 10);
                            break;
                        }


                        addscore(aiGetPlayerTarget(sn), 10);
                        break;
                    case DRAGONDEAD:
                        spr.setPicnum(DRAGONDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 4000);
                        spr.setDetail(DRAGONTYPE);
                        break;
                    case FREDDEAD:
                        spr.setPicnum(FREDDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 40);
                        spr.setDetail(FREDTYPE);
                        break;
                    case GOBLINDEAD:
                        spr.setPicnum(GOBLINDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 25);
                        spr.setDetail(GOBLINTYPE);
                        break;
                    case MINOTAURDEAD:
                        spr.setPicnum(MINOTAURDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), game.WH2 ? 95 : 170);
                        spr.setDetail(MINOTAURTYPE);
                        break;
                    case SPIDERDEAD:
                        spr.setPicnum(SPIDERDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 5);
                        spr.setDetail(SPIDERTYPE);
                        break;
                    case SKULLYDEAD:
                        spr.setPicnum(SKULLYDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 1000);
                        spr.setDetail(SKULLYTYPE);
                        break;
                    case FATWITCHDEAD:
                        spr.setPicnum(FATWITCHDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 900);
                        spr.setDetail(FATWITCHTYPE);
                        break;
                    case JUDYDEAD:
                        spr.setPicnum(JUDYDEAD);
                        spr.setCstat(spr.getCstat() & ~3);
                        engine.changespritestat(sn, RESURECT);
                        addscore(aiGetPlayerTarget(sn), 7000);
                        spr.setDetail(JUDYTYPE);
                        break;
                    default:
                        if (spr.getPicnum() == SKELETONDEAD) {
                            spr.setPicnum(SKELETONDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, RESURECT);
                            spr.setDetail(SKELETONTYPE);
                            addscore(aiGetPlayerTarget(sn), game.WH2 ? 20 : 10);
                        } else if (spr.getPicnum() == GRONDEAD) {
                            spr.setPicnum(GRONDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            spr.setExtra(3);
                            spr.setDetail(GRONTYPE);
                            engine.changespritestat(sn, RESURECT);
                            if (game.WH2) {
                                switch (spr.getPal()) {
                                    case 0:
                                        addscore(aiGetPlayerTarget(sn), 125);
                                        break;
                                    case 10:
                                        addscore(aiGetPlayerTarget(sn), 90);
                                        break;
                                    case 11:
                                        addscore(aiGetPlayerTarget(sn), 115);
                                        break;
                                    case 12:
                                        addscore(aiGetPlayerTarget(sn), 65);
                                        break;
                                }
                            } else {
                                addscore(aiGetPlayerTarget(sn), 200);
                            }
                        }
                        break;
                }
                break;

            case DEAD:
                spr.setDetail(0);
                if (spr.getPicnum() == SKELETONDEAD) {
                    spr.setPicnum(SKELETONDEAD);
                    spr.setCstat(spr.getCstat() & ~3);
                    engine.changespritestat(sn, DEAD);
                    if (game.WH2) {
                        addscore(aiGetPlayerTarget(sn), 70);
                        monsterweapon(sn);
                    }
                } else if (spr.getPicnum() == FISH || spr.getPicnum() == RAT) {
                    spr.setCstat(spr.getCstat() & ~3);
                    engine.changespritestat(sn, DEAD);
                    addscore(aiGetPlayerTarget(sn), 5);
                } else if (spr.getPicnum() == GRONDEAD) {
                    spr.setPicnum(GRONDEAD);
                    spr.setCstat(spr.getCstat() & ~3);
                    engine.changespritestat(sn, DEAD);
                    if (game.WH2) {
                        switch (spr.getPal()) {
                            case 0:
                                addscore(aiGetPlayerTarget(sn), 125);
                                break;
                            case 10:
                                addscore(aiGetPlayerTarget(sn), 90);
                                break;
                            case 11:
                                addscore(aiGetPlayerTarget(sn), 115);
                                break;
                            case 12:
                                addscore(aiGetPlayerTarget(sn), 65);
                                break;
                        }
                    } else {
                        addscore(aiGetPlayerTarget(sn), 200);
                    }
                    monsterweapon(sn);
                } else {
                    switch (spr.getPicnum()) {
                        case GONZOBSHDEAD:
                            if (netgame) {
                                break;
                            }
                            spr.setPicnum(GONZOBSHDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            if (spr.getPal() == 4) {
                                engine.changespritestat(sn, SHADE);
                                deaddude(sn);
                            } else {
                                engine.changespritestat(sn, DEAD);
                                if (spr.getShade() < 25) {
                                    monsterweapon(sn);
                                }
                            }
                            addscore(aiGetPlayerTarget(sn), 85);
                            break;
                        case GONZOCSWDEAD:
                            if (netgame) {
                                break;
                            }
                            spr.setPicnum(GONZOCSWDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            if (spr.getPal() == 4) {
                                engine.changespritestat(sn, SHADE);
                                deaddude(sn);
                            } else {
                                engine.changespritestat(sn, DEAD);
                                monsterweapon(sn);
                            }
                            addscore(aiGetPlayerTarget(sn), 55);
                            break;
                        case GONZOGSWDEAD:
                            if (netgame) {
                                break;
                            }
                            spr.setPicnum(GONZOGSWDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            if (spr.getPal() == 4) {
                                engine.changespritestat(sn, SHADE);
                                deaddude(sn);
                            } else {
                                engine.changespritestat(sn, DEAD);
                                monsterweapon(sn);
                            }
                            addscore(aiGetPlayerTarget(sn), 105);
                            break;
                        case GONZOGHMDEAD:
                            if (netgame) {
                                break;
                            }
                            spr.setPicnum(GONZOGHMDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            if (spr.getPal() == 4) {
                                engine.changespritestat(sn, SHADE);
                                deaddude(sn);
                            } else {
                                engine.changespritestat(sn, DEAD);
                                monsterweapon(sn);
                            }
                            addscore(aiGetPlayerTarget(sn), 100);
                            break;
                        case NEWGUYDEAD:
                            if (netgame) {
                                break;
                            }
                            spr.setPicnum(NEWGUYDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            monsterweapon(sn);
                            addscore(aiGetPlayerTarget(sn), 50);
                            break;
                        case GONZOGSHDEAD:
                            if (netgame) {
                                break;
                            }
                            spr.setPicnum(GONZOGSHDEAD);
                            if (spr.getShade() != 31) {
                                spr.setCstat(spr.getCstat() & ~3);
                            } else {
                                spr.setCstat(spr.getCstat() & ~1);
                            }
                            if (spr.getPal() == 4) {
                                engine.changespritestat(sn, SHADE);
                                deaddude(sn);
                            } else {
                                engine.changespritestat(sn, DEAD);
                                monsterweapon(sn);
                            }
                            addscore(aiGetPlayerTarget(sn), 110);
                            break;
                        case KATIEDEAD:
                            if (netgame) {
                                break;
                            }
                            trailingsmoke(sn, true);
                            spr.setPicnum(DEVILDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            spawnhornskull(sn);
                            addscore(aiGetPlayerTarget(sn), 500);
                            break;
                        case IMPDEAD:
                            if (!game.WH2) {
                                break;
                            }
                            spr.setPicnum(IMPDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            addscore(aiGetPlayerTarget(sn), 115);
                            monsterweapon(sn);
                            break;
                        case KOBOLDDEAD:
                            spr.setPicnum(KOBOLDDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            addscore(aiGetPlayerTarget(sn), 10);
                            break;
                        case DRAGONDEAD:
                            spr.setPicnum(DRAGONDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            addscore(aiGetPlayerTarget(sn), 4000);
                            break;
                        case DEVILDEAD:
                            trailingsmoke(sn, true);
                            spr.setPicnum(DEVILDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            addscore(aiGetPlayerTarget(sn), game.WH2 ? 70 : 50);
                            if (game.WH2) {
                                monsterweapon(sn);
                            }
                            break;
                        case FREDDEAD:
                            spr.setPicnum(FREDDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            addscore(aiGetPlayerTarget(sn), 40);
                            break;
                        case GOBLINDEAD:
                            spr.setPicnum(GOBLINDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);

                            PLAYER p = aiGetPlayerTarget(sn);
                            if (p != null) {
                                addscore(p, 25);
                            }

                            if ((engine.krand() % 100) > 60) {
                                monsterweapon(sn);
                            }
                            break;
                        case MINOTAURDEAD:
                            spr.setPicnum(MINOTAURDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            addscore(aiGetPlayerTarget(sn), game.WH2 ? 95 : 70);
                            if ((engine.krand() % 100) > 60) {
                                monsterweapon(sn);
                            }
                            break;
                        case SPIDERDEAD:
                            spr.setPicnum(SPIDERDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            addscore(aiGetPlayerTarget(sn), 5);
                            break;
                        case SKULLYDEAD:
                            spr.setPicnum(SKULLYDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            addscore(aiGetPlayerTarget(sn), 100);
                            break;
                        case FATWITCHDEAD:
                            spr.setPicnum(FATWITCHDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            addscore(aiGetPlayerTarget(sn), 900);
                            break;
                        case JUDYDEAD:
                            spr.setPicnum(JUDYDEAD);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, DEAD);
                            spawnapentagram(sn);
                            addscore(aiGetPlayerTarget(sn), 7000);
                            break;
                        case WILLOWEXPLO + 2:
                            spr.setPal(0);
                            spr.setCstat(spr.getCstat() & ~3);
                            engine.changespritestat(sn, (short) 0);
                            engine.deletesprite(sn);
                            addscore(aiGetPlayerTarget(sn), game.WH2 ? 15 : 150);
                            return;
                    }
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(),
                        (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                if ((zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR) {
                    Sector sec = boardService.getSector(spr.getSectnum());
                    if (sec != null) {
                        if (spr.getSectnum() != MAXSECTORS && (sec.getFloorpicnum() == WATER
                                || sec.getFloorpicnum() == SLIME)) {
                            if (spr.getPicnum() == MINOTAURDEAD) {
                                spr.setZ(spr.getZ() + (8 << 8));
                                engine.setsprite(sn, spr.getX(), spr.getY(), spr.getZ());
                                sec = boardService.getSector(spr.getSectnum());
                            }
                        }

                        if (sec != null && (sec.getFloorpicnum() == LAVA
                                || sec.getFloorpicnum() == LAVA1
                                || sec.getFloorpicnum() == LAVA2)) {
                            trailingsmoke(sn, true);
                            engine.deletesprite(sn);
                        }
                    }
                }
                break;
        }
        //
        // the control variable for monster release
        //
    }

    public static void makeafire(int i, int ignoredFireType) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), FIRE);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        game.pInt.setsprinterpolate(j, spr2);

        spr2.setX(spr.getX() + (engine.krand() & 1024) - 512);
        spr2.setY(spr.getY() + (engine.krand() & 1024) - 512);
        spr2.setZ(spr.getZ());

        spr2.setCstat(0);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);

        spr2.setShade(0);

        spr2.setClipdist(64);
        spr2.setOwner(spr.getOwner());
        spr2.setLotag(2047);
        spr2.setHitag(0);
        engine.changespritestat(j, FIRE);
    }

    public static void explosion(int i, int x, int y, int z, int ignoredOwner) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), EXPLO);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        boolean isWH2 = game.WH2;

        if (!isWH2) {
            spr2.setX(x + (engine.krand() & 1024) - 512);
            spr2.setY(y + (engine.krand() & 1024) - 512);
            spr2.setZ(z);
        } else {
            spr2.setX(x);
            spr2.setY(y);
            spr2.setZ(z + (16 << 8));
        }

        game.pInt.setsprinterpolate(j, spr2);

        spr2.setCstat(0); // Hitscan does not hit smoke on wall
        spr2.setCstat(spr2.getCstat() & ~3);
        spr2.setShade(-15);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
        spr2.setAng((short) (engine.krand() & 2047));
        spr2.setXvel((short) ((engine.krand() & 511) - 256));
        spr2.setYvel((short) ((engine.krand() & 511) - 256));
        spr2.setZvel((short) ((engine.krand() & 511) - 256));
        spr2.setOwner(spr.getOwner());
        spr2.setHitag(0);
        spr2.setPal(0);
        if (!isWH2) {
            spr2.setPicnum(MONSTERBALL);
            spr2.setLotag(256);
        } else {
            spr2.setPicnum(EXPLOSTART);
            spr2.setLotag(12);
        }
    }

    public static void explosion2(int i, int x, int y, int z, int ignoredOwner) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), EXPLO);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        boolean isWH2 = game.WH2;

        if (!isWH2) {
            spr2.setX(x + (engine.krand() & 256) - 128);
            spr2.setY(y + (engine.krand() & 256) - 128);
            spr2.setZ(z);
        } else {
            spr2.setX(x);
            spr2.setY(y);
            spr2.setZ(z + (16 << 8));
        }

        game.pInt.setsprinterpolate(j, spr2);

        spr2.setCstat(0);
        spr2.setCstat(spr2.getCstat() & ~3);

        spr2.setShade(-25);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
        spr2.setAng((short) (engine.krand() & 2047));
        spr2.setXvel((short) ((engine.krand() & 256) - 128));
        spr2.setYvel((short) ((engine.krand() & 256) - 128));
        spr2.setZvel((short) ((engine.krand() & 256) - 128));
        spr2.setOwner(spr.getOwner());
        spr2.setHitag(0);
        spr2.setPal(0);

        if (!isWH2) {
            spr2.setPicnum(MONSTERBALL);
            spr2.setLotag(128);
        } else {
            spr2.setPicnum(EXPLOSTART);
            spr2.setLotag(12);
        }

    }

    public static void trailingsmoke(int i, boolean ball) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), SMOKE);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ());

        game.pInt.setsprinterpolate(j, spr2);
        spr2.setCstat(0x03);
        spr2.setCstat(spr2.getCstat() & ~3);
        spr2.setPicnum(SMOKEFX);
        spr2.setShade(0);
        if (ball) {
            spr2.setXrepeat(128);
            spr2.setYrepeat(128);
        } else {
            spr2.setXrepeat(32);
            spr2.setYrepeat(32);
        }
        spr2.setPal(0);

        spr2.setOwner(spr.getOwner());
        spr2.setLotag(256);
        spr2.setHitag(0);
    }

    public static void icecubes(int i, int x, int y, int ignoredZ, int ignoredOwner) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), FX);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return;
        }

        spr2.setX(x);
        spr2.setY(y);

        spr2.setZ(sec.getFloorz() - (getPlayerHeight() << 8) + (engine.krand() & 4096));

        game.pInt.setsprinterpolate(j, spr2);

        spr2.setCstat(0); // Hitscan does not hit smoke on wall
        spr2.setPicnum(ICECUBE);
        spr2.setShade(-16);
        spr2.setXrepeat(16);
        spr2.setYrepeat(16);

        spr2.setAng((short) (((engine.krand() & 1023) - 1024) & 2047));
        spr2.setXvel((short) ((engine.krand() & 1023) - 512));
        spr2.setYvel((short) ((engine.krand() & 1023) - 512));
        spr2.setZvel((short) ((engine.krand() & 1023) - 512));

        spr2.setPal(6);
        spr2.setOwner(spr.getOwner());

        if (game.WH2) {
            spr2.setLotag(2048);
        } else {
            spr2.setLotag(999);
        }
        spr2.setHitag(0);

    }

    public static boolean damageactor(PLAYER plr, int hitobject, final int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return false;
        }

        final int j = (hitobject & HIT_INDEX_MASK); // j is the spritenum that the bullet (spritenum i) hit
        if (j == plr.getSpritenum() && spr.getOwner() == plr.getSprite().getOwner()) {
            return false;
        }

        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return false;
        }

        if (j == plr.getSpritenum() && spr.getOwner() != plr.getSprite().getOwner()) {
            if (plr.getInvincibletime() > 0 || plr.isGodMode()) {
                engine.deletesprite(i);
                return false;
            }
            // EG 17 Oct 2017: Mass backport of RULES.CFG behavior for resist/onyx ring
            // EG 21 Aug 2017: New RULES.CFG behavior in place of the old #ifdef
            if (plr.getManatime() > 0) {
                if (/* eg_resist_blocks_traps && */spr.getPicnum() != FATSPANK && spr.getPicnum() != PLASMA) {
                    engine.deletesprite(i);
                    return false;
                }
                // Use "fixed" version: EXPLOSION and MONSTERBALL are the fire attacks, account
                // for animation
                // FATSPANK is right in the middle of this group of tiles, so still keep an
                // exception for it.
                else if ((spr.getPicnum() >= EXPLOSION && spr.getPicnum() <= (MONSTERBALL + 2)
                        && spr.getPicnum() != FATSPANK)) {
                    engine.deletesprite(i);
                    return false;
                }
            }
            // EG 21 Aug 2017: onyx ring
            else if (plr.getTreasure()[TONYXRING] == 1 /*&& eg_onyx_effect != 0*/
                    && ((spr.getPicnum() < EXPLOSION || spr.getPicnum() > MONSTERBALL + 2)
                    && spr.getPicnum() != PLASMA)) {

//				if (eg_onyx_effect == 1 || (eg_onyx_effect == 2 && ((engine.krand() & 32) > 16))) {
                engine.deletesprite(i);
                return false;
//				}
            }
            // EG 21 Aug 2017: Move this here so as not to make ouch sounds unless pain is
            // happening
            if ((engine.krand() & 9) == 0) {
                playsound_loc(S_PLRPAIN1 + (engine.rand() % 2), spr.getX(), spr.getY());
            }

            if (game.WH2 && spr.getPicnum() == DART) {
                plr.setPoisoned(1);
                plr.setPoisontime(7200);
                showmessage("Poisoned", 360);
            }

            if (!netgame) {
                if (spr.getPicnum() == PLASMA) {
                    addhealth(plr, -((engine.krand() & 15) + 15));
                } else if (spr.getPicnum() == FATSPANK) {
                    playsound_loc(S_GORE1A + (engine.krand() % 3), plr.getX(), plr.getY());
                    addhealth(plr, -((engine.krand() & 10) + 10));
                    if ((engine.krand() % 100) > 90) {
                        plr.setPoisoned(1);
                        plr.setPoisontime(7200);
                        showmessage("Poisoned", 360);
                    }
                } else if (spr.getPicnum() == THROWPIKE) {
                    addhealth(plr, -((engine.krand() % 10) + 5));
                } else {
                    addhealth(plr, -((engine.krand() & 20) + 5));
                }
            }

            startredflash(10);
            /* EG 2017 - Trap fix */
            engine.deletesprite(i);
            return true;
        }

        if (j != plr.getSpritenum() && !netgame) // Les 08/11/95
        {
            if (spr.getOwner() != j) {

//				public static final int DEMONTYPE = 1; XXX
//				public static final int DRAGONTYPE = 3;
//				public static final int FISHTYPE = 5;
//				public static final int JUDYTYPE = 12;
//				public static final int RATTYPE = 18;
//				public static final int SKULLYTYPE = 20;

                switch (spr2.getDetail()) {
                    case NEWGUYTYPE:
                    case KURTTYPE:
                    case GONZOTYPE:
                    case KATIETYPE:
                    case GRONTYPE:
                    case KOBOLDTYPE:
                    case DEVILTYPE:
                    case FREDTYPE:
                    case GOBLINTYPE:
                    case IMPTYPE:
                    case MINOTAURTYPE:
                    case SPIDERTYPE:
                    case SKELETONTYPE:
                    case FATWITCHTYPE:
                    case WILLOWTYPE:
                    case GUARDIANTYPE:
                        if (isBlades(spr.getPicnum())) {
                            spr2.setHitag(spr2.getHitag() - 30);
                            if (spr.getPicnum() == THROWPIKE) {
                                if ((engine.krand() % 2) != 0) {
                                    playsound_loc(S_GORE1A + engine.krand() % 2, spr.getX(), spr.getY());
                                }
                            }
                        } else {
                            switch (spr.getPicnum()) {
                                case PLASMA:
                                case MONSTERBALL:
                                    spr2.setHitag(spr2.getHitag() - 40);
                                    break;
                                case FATSPANK:
                                case BULLET:
                                    spr2.setHitag(spr2.getHitag() - 10);
                                    break;
                                case FIREBALL:
                                    if (!game.WH2) {
                                        spr2.setHitag(spr2.getHitag() - 3);
                                    }
                                    break;
                                case DISTORTIONBLAST:
                                    spr2.setHitag(10);
                                    break;
                                case BARREL:
                                    spr2.setHitag(spr2.getHitag() - 100);
                                    break;
                            }
                        }

                        if (spr2.getHitag() <= 0) {
                            newstatus(j, DIE);
                            engine.deletesprite(i);
                        } else {
                            newstatus(j, PAIN);
                        }
                        return true;
                }

                switch (spr2.getDetail()) {
                    case GONZOTYPE:
                    case NEWGUYTYPE:
                    case KATIETYPE:
                    case GRONTYPE:
                    case KOBOLDTYPE:
                    case DEVILTYPE:
                    case FREDTYPE:
                    case GOBLINTYPE:
                    case MINOTAURTYPE:
                    case SPIDERTYPE:
                    case SKELETONTYPE:
                    case IMPTYPE:
                        // JSA_NEW //why is this here it's in whplr
                        // raf because monsters could shatter a guy thats been frozen
                        if (spr2.getPal() == 6) {
                            for (int k = 0; k < 32; k++) {
                                icecubes(j, spr2.getX(), spr2.getY(), spr2.getZ(), j);
                            }
                            // EG 26 Oct 2017: Move this here from medusa (anti multi-freeze exploit)
                            addscore(plr, 100);
                            engine.deletesprite(j);
                        }
                        return true;
                }

                switch (spr2.getPicnum()) {
                    case BARREL:
                    case VASEA:
                    case VASEB:
                    case VASEC:
                    case STAINGLASS1:
                    case STAINGLASS2:
                    case STAINGLASS3:
                    case STAINGLASS4:
                    case STAINGLASS5:
                    case STAINGLASS6:
                    case STAINGLASS7:
                    case STAINGLASS8:
                    case STAINGLASS9:
                        spr2.setHitag(0);
                        spr2.setLotag(0);
                        newstatus(j, BROKENVASE);
                        break;
                    default:
                        engine.deletesprite(i);
                        return true;
                }
            }
        }

        return false;
    }

    public static int movesprite(int spritenum, int dx, int dy, int dz, int ceildist, int flordist, int cliptype) {
        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null || spr.getStatnum() == MAXSTATUS) {
            return (-1);
        }

        game.pInt.setsprinterpolate(spritenum, spr);

        int dcliptype = 0;
        switch (cliptype) {
            case NORMALCLIP:
            case CLIFFCLIP:
                dcliptype = CLIPMASK0;
                break;
            case PROJECTILECLIP:
                dcliptype = CLIPMASK1;
                break;
        }

        int zoffs = 0;
        if ((spr.getCstat() & 128) == 0) {
            zoffs = -((engine.getTile(spr.getPicnum()).getHeight() * spr.getYrepeat()) << 1);
        }

        int dasectnum = spr.getSectnum(); // Can't modify sprite sectors directly becuase of linked lists
        int daz = spr.getZ() + zoffs; // Must do this if not using the new centered centering (of course)
        int retval = engine.clipmove(spr.getX(), spr.getY(), daz, dasectnum, dx, dy, (spr.getClipdist()) << 2, ceildist, flordist,
                dcliptype);

        if (clipmove_sectnum != -1) {
            spr.setX(clipmove_x);
            spr.setY(clipmove_y);
            daz = clipmove_z;
            dasectnum = clipmove_sectnum;
        }

        engine.pushmove(spr.getX(), spr.getY(), daz, dasectnum, spr.getClipdist() << 2, ceildist, flordist, CLIPMASK0);
        if (pushmove_sectnum != -1) {
            spr.setX(pushmove_x);
            spr.setY(pushmove_y);
            dasectnum = pushmove_sectnum;
        }

        if ((dasectnum != spr.getSectnum()) && (dasectnum >= 0)) {
            engine.changespritesect(spritenum, dasectnum);
        }

        // Set the blocking bit to 0 temporarly so getzrange doesn't pick up
        // its own sprite
        int tempshort = spr.getCstat();
        spr.setCstat(spr.getCstat() & ~1);
        engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, dcliptype);
        spr.setCstat(tempshort);

        daz = spr.getZ() + zoffs + dz;
        if ((daz <= zr_ceilz) || (daz > zr_florz)) {
            if (retval != 0) {
                return (retval);
            }
            return (HIT_SECTOR | dasectnum);
        }
        spr.setZ((daz - zoffs));
        return (retval);
    }

    public static void trowajavlin(int s) {
        Sprite spr = boardService.getSprite(s);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), JAVLIN);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ());// - (40 << 8);

        game.pInt.setsprinterpolate(j, spr2);

        spr2.setCstat(21);

        switch (spr.getLotag()) {
            case 91:
                spr2.setPicnum(WALLARROW);
                spr2.setAng((short) (((spr.getAng() + 2048) - 512) & 2047));
                spr2.setXrepeat(16);
                spr2.setYrepeat(48);
                spr2.setClipdist(24);
                break;
            case 92:
                spr2.setPicnum(DART);
                spr2.setAng((short) (((spr.getAng() + 2048) - 512) & 2047));
                spr2.setXrepeat(64);
                spr2.setYrepeat(64);
                spr2.setClipdist(16);
                break;
            case 93:
                spr2.setPicnum(HORIZSPIKEBLADE);
                spr2.setAng((short) (((spr.getAng() + 2048) - 512) & 2047));
                spr2.setXrepeat(16);
                spr2.setYrepeat(48);
                spr2.setClipdist(32);
                break;
            case 94:
                spr2.setPicnum(THROWPIKE);
                spr2.setAng((short) (((spr.getAng() + 2048) - 512) & 2047));
                spr2.setXrepeat(24);
                spr2.setYrepeat(24);
                spr2.setClipdist(32);
                break;
        }

        spr2.setExtra(spr.getAng());
        spr2.setShade(-15);
        spr2.setXvel((short) ((engine.krand() & 256) - 128));
        spr2.setYvel((short) ((engine.krand() & 256) - 128));
        spr2.setZvel((short) ((engine.krand() & 256) - 128));

        spr2.setOwner(0);
        spr2.setLotag(0);
        spr2.setHitag(0);
        spr2.setPal(0);
    }

    public static void spawnhornskull(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), 0);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ() - (24 << 8));

        game.pInt.setsprinterpolate(j, spr2);

        spr2.setShade(-15);
        spr2.setCstat(0);
        spr2.setCstat(spr2.getCstat() & ~3);
        spr2.setPal(0);
        spr2.setPicnum(HORNEDSKULL);
        spr2.setDetail(HORNEDSKULLTYPE);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
    }

    public static void spawnapentagram(int sn) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), 0);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ() - (8 << 8));

        game.pInt.setsprinterpolate(j, spr2);

        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
        spr2.setPal(0);
        spr2.setShade(-15);
        spr2.setCstat(0);
        spr2.setClipdist(64);
        spr2.setLotag(0);
        spr2.setHitag(0);
        spr2.setExtra(0);
        spr2.setPicnum(PENTAGRAM);
        spr2.setDetail(PENTAGRAMTYPE);

        engine.setsprite(j, spr2.getX(), spr2.getY(), spr2.getZ());
    }
}
