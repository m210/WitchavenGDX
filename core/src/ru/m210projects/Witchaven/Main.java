package ru.m210projects.Witchaven;

import org.jetbrains.annotations.NotNull;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.LogSender;
import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Types.MemLog;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.filehandle.grp.GrpFile;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.CommandResponse;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.commands.OsdCallback;
import ru.m210projects.Build.osd.commands.OsdValueRange;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Witchaven.Factory.*;
import ru.m210projects.Witchaven.Menu.GameMenu;
import ru.m210projects.Witchaven.Menu.MainMenu;
import ru.m210projects.Witchaven.Menu.MenuCorruptGame;
import ru.m210projects.Witchaven.Screens.*;
import ru.m210projects.Witchaven.Types.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.NORMAL;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Witchaven.Animate.initanimations;
import static ru.m210projects.Witchaven.Factory.WHMenuHandler.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Menu.WHMenuUserContent.episodeManager;
import static ru.m210projects.Witchaven.WH2Names.FLOORMIRROR;
import static ru.m210projects.Witchaven.WHFX.*;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.sndInit;
import static ru.m210projects.Witchaven.WHSND.stopallsounds;
import static ru.m210projects.Witchaven.WHScreen.initpaletteshifts;
import static ru.m210projects.Witchaven.WHScreen.showmessage;
import static ru.m210projects.Witchaven.WHTAG.delayitem;
import static ru.m210projects.Witchaven.WHTAG.swingdoor;
import static ru.m210projects.Witchaven.Whldsv.FindSaves;
import static ru.m210projects.Witchaven.Whmap.loadnewlevel;

public class Main extends BuildGame {

    public static final String appdefwh1 = "whgdx.def";
    public static final String appdefwh2 = "wh2gdx.def";

    public static MenuScreen gMenuScreen;
    public static LoadingScreen gLoadScreen;
    public static GameScreen gGameScreen;
    public static VictoryScreen gVictoryScreen;
    public static CutsceneScreen gCutsceneScreen;
    public static StatisticsScreen gStatisticsScreen;
    public static DemoScreen gDemoScreen;

    public static UserFlag mUserFlag = UserFlag.None;

    public static Main game;
    public static WHEngine engine;
    public static BoardService boardService;
    public static Config cfg;
    public final boolean WH2;
    public WHMenuHandler menu;

    public final Runnable main = new Runnable() {
        @Override
        public void run() {
            stopallsounds();
            gDemoScreen.onStopRecord();

            if (numplayers > 1 || gDemoScreen.demofiles.isEmpty() || cfg.gDemoSeq == 0 || !gDemoScreen.showDemo(cache.getGameDirectory()))
                changeScreen(gMenuScreen);

            if (!menu.gShowMenu) {
                menu.mOpen(menu.mMenus[MAIN], -1);
            }
        }
    };

    public Main(List<String> args, GameConfig cfg, String name, String version, boolean release, boolean isWH2) throws IOException {
        super(args, cfg, name, version, release);
        WH2 = isWH2;
        game = this;
        Main.cfg = (Config) cfg;
    }

    @Override
    protected MessageScreen createMessage(String header, String text, MessageType type) {
        return new WHMessageScreen(this, header, text, type);
    }

    @Override
    public void onDropEntry(FileEntry entry) {
        if (!entry.isExtension("map")) {
            return;
        }

        Console.out.println("Start dropped map: " + entry.getName());
        gGameScreen.newgame(entry, 0, difficulty);
    }

    @Override
    @NotNull
    public WHRenderer getRenderer() {
        Renderer renderer = super.getRenderer();
        if (renderer instanceof WHRenderer) {
            return (WHRenderer) renderer;
        }
        return new WHDummyRenderer();
    }

    @Override
    public boolean init() throws Exception {
        boardService = engine.getBoardService();
        Names.init();

        initlava();
        initwater();

        ConsoleInit();

        visibility = 1024;

        parallaxtype = 1;

        if (WH2) {
            ArtEntry artEntry = engine.getTile(FLOORMIRROR);
            if (artEntry instanceof DynamicArtEntry) {
                ((DynamicArtEntry) artEntry).clearData();
            }

            for (int j = 0; j < 256; j++) {
                tempbuf[j] = (byte) ((j + 32) & 0xFF);     // remap colors for screwy palette sectors
            }
            PaletteManager paletteManager = engine.getPaletteManager();
            paletteManager.makePalookup(16, tempbuf, 0, 0, 0, 1);
            for (int j = 0; j < 256; j++) {
                tempbuf[j] = (byte) j;
            }
            paletteManager.makePalookup(17, tempbuf, 24, 24, 24, 1);

            for (int j = 0; j < 256; j++) {
                tempbuf[j] = (byte) j;          // (j&31)+32;
            }
            paletteManager.makePalookup(18, tempbuf, 8, 8, 48, 1);
        }

        readpalettetable();

        for (int i = 0; i < MAXSECTORS; i++) {
            delayitem[i] = new Delayitem();
        }

        sndInit();

        initstruct();

        initpaletteshifts();
        InitOriginalEpisodes();

        Console.out.println("Initializing def-scripts...");
        cache.loadGdxDef(baseDef, WH2 ? appdefwh2 : appdefwh1, "whgdx.dat");

        Directory gameDir = cache.getGameDirectory();

        if (pCfg.isAutoloadFolder()) {
            Console.out.println("Initializing autoload folder");
            for (Entry file : gameDir.getDirectory(gameDir.getEntry("autoload"))) {
                switch (file.getExtension()) {
                    case "PK3":
                    case "ZIP":
                        Group group = cache.newGroup(file);
                        Entry def = group.getEntry(WH2 ? appdefwh2 : appdefwh1);
                        if (def.exists()) {
                            cache.addGroup(group, NORMAL); // HIGH?
                            baseDef.loadScript(file.getName(), def);
                        }
                        break;
                    case "DEF":
                        baseDef.loadScript(file.getName(), file);
                        break;
                }
            }
        }


        FileEntry filgdx = gameDir.getEntry(WH2 ? appdefwh2 : appdefwh1);
        if (filgdx.exists()) {
            baseDef.loadScript(filgdx);
        }
        this.setDefs(baseDef);

        FindSaves(getUserDirectory());
        InitCutscenes();

        gLoadScreen = new LoadingScreen(this);
        gGameScreen = new GameScreen(this);
        gDemoScreen = new DemoScreen(this);
        gVictoryScreen = new VictoryScreen();
        gCutsceneScreen = new CutsceneScreen(this);
        gStatisticsScreen = new StatisticsScreen(this);

        menu.mMenus[MAIN] = new MainMenu(this);
        menu.mMenus[GAME] = new GameMenu(this);
        menu.mMenus[CORRUPTLOAD] = new MenuCorruptGame(this);
        gMenuScreen = new MenuScreen(this);

        gDemoScreen.checkDemoEntry(gameDir);

        System.gc();
        MemLog.log("create");

        return true;
    }

    public String getGameDef() {
        return  WH2 ? appdefwh2 : appdefwh1;
    }

    private void InitOriginalEpisodes() {
        final String[] sMapName;

        if (WH2) {
            sMapName = new String[]{
                    "Antechamber of asmodeus",
                    "Halls of ragnoth",
                    "Lokis tomb",
                    "Forsaken realm",
                    "Eye of midian",
                    "Dungeon of disembowlment",
                    "Stronghold of chaos",
                    "Jaws of venom",
                    "Descent into doom",
                    "Hack n sniff",
                    "Straits of perdition",
                    "Plateau of insanity",
                    "Crypt of decay",
                    "Mausoleum of madness",
                    "Gateway into oblivion",
                    "Lungs of hell"
            };
        } else {
            sMapName = new String[48];
            for (int i = 0; i < sMapName.length; i++) {
                sMapName[i] = "Map " + (i + 1);
            }
        }

        Directory dir = cache.getGameDirectory();
        gOriginalEpisode = new EpisodeInfo("Original",  new EpisodeEntry.Addon(dir, dir.getDirectoryEntry()));
        for (int i = 0; i < sMapName.length; i++) {
            gOriginalEpisode.gMapInfo.add(new MapInfo("level" + (i + 1) + ".map", sMapName[i]));
        }
        episodeManager.putEpisode(gOriginalEpisode);
    }

    private void initstruct() {
        for (int i = 0; i < MAXSWINGDOORS; i++) {
            swingdoor[i] = new SWINGDOOR();
        }

        initanimations();
    }

    @Override
    public GameProcessor createGameProcessor() {
        return new WHControl(this);
    }

    @Override
    public BuildFactory getFactory() {
        return new WHFactory(this);
    }

    public void ConsoleInit() {
        Console.out.println("Initializing on-screen display system");

        Console.out.getPrompt().setVersion(getTitle(), OsdColor.YELLOW, 10);

        OsdCallback.OsdRunnable cmdGod = argv -> {
            if (isCurrentScreen(gGameScreen)) {
                player[myconnectindex].setGodMode(!player[myconnectindex].isGodMode());
                if (player[myconnectindex].isGodMode()) {
                    Console.out.println("God mode: On");
                } else {
                    Console.out.println("God mode: Off");
                }
            } else {
                Console.out.println("god: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        };

        Console.out.registerCommand(new OsdCallback("god", "", cmdGod));
        Console.out.registerCommand(new OsdCallback("darkness", "", cmdGod));

        Console.out.registerCommand(new OsdCallback("noclip",
                "", argv -> {
            if (isCurrentScreen(gGameScreen)) {
                player[myconnectindex].setNoclip(!player[myconnectindex].isNoclip());
                if (player[myconnectindex].isNoclip()) {
                    Console.out.println("Noclip mode: On");
                } else {
                    Console.out.println("Noclip mode: Off");
                }
            } else {
                Console.out.println("Noclip: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdCallback("showmap",
                "", argv -> {
            if (isCurrentScreen(gGameScreen)) {
                Arrays.fill(show2dsector.getArray(), (byte) 255);
                showmessage("showmap: on", 360);
            } else {
                Console.out.println("showmap: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdCallback("scooter",
                "", argv -> {
            if (isCurrentScreen(gGameScreen)) {
                PLAYER plr = player[pyrn];
                plr.getWeapon()[1] = 1;
                plr.getAmmo()[1] = 45; //DAGGER
                plr.getWeapon()[2] = 1;
                plr.getAmmo()[2] = 55; //MORNINGSTAR
                plr.getWeapon()[3] = 1;
                plr.getAmmo()[3] = 50; //SHORT SWORD
                plr.getWeapon()[4] = 1;
                plr.getAmmo()[4] = 80; //BROAD SWORD
                plr.getWeapon()[5] = 1;
                plr.getAmmo()[5] = 100; //BATTLE AXE
                plr.getWeapon()[6] = 1;
                plr.getAmmo()[6] = 50; // BOW
                plr.getWeapon()[7] = 2;
                plr.getAmmo()[7] = 40; //PIKE
                plr.getWeapon()[8] = 1;
                plr.getAmmo()[8] = 250; //TWO HANDED
                plr.getWeapon()[9] = 1;
                plr.getAmmo()[9] = 50;
                plr.setSelectedgun(4);
                plr.setCurrweapon(4);

                showmessage("All weapons and ammo", 360);
            } else {
                Console.out.println("scooter: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdCallback("mommy",
                "", argv -> {
            if (isCurrentScreen(gGameScreen)) {
                PLAYER plr = player[pyrn];
                Arrays.fill(plr.getPotion(), 9);
                showmessage("All Potions and Spells", 360);
            } else {
                Console.out.println("mommy: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));
        Console.out.registerCommand(new OsdCallback("wango",
                "", argv -> {
            if (isCurrentScreen(gGameScreen)) {
                PLAYER plr = player[pyrn];
                for (int i = 0; i < 8; i++) {
                    plr.getOrb()[i] = 1;
                    plr.getOrbammo()[i] = 9;
                }
                plr.setHealth(0);
                addhealth(plr, 200);
                plr.setArmor(150);
                plr.setArmortype(3);
                plr.setLvl(7);
                plr.setMaxhealth(200);
                plr.getTreasure()[TBRASSKEY] = 1;
                plr.getTreasure()[TBLACKKEY] = 1;
                plr.getTreasure()[TGLASSKEY] = 1;
                plr.getTreasure()[TIVORYKEY] = 1;

                showmessage("Full Health, Armor, Scrolls; Bonus EXP", 360);
            } else {
                Console.out.println("wango: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        OsdCallback.OsdRunnable cmdPowerup = argv -> {
            if (isCurrentScreen(gGameScreen)) {
                PLAYER plr = player[pyrn];
                weaponpowerup(plr);
            } else {
                Console.out.println("powerup: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        };

        Console.out.registerCommand(new OsdCallback("powerup", "", cmdPowerup));
        Console.out.registerCommand(new OsdCallback("enchant", "", cmdPowerup));

        Console.out.registerCommand(new OsdValueRange("changemap",
                "",2, 25) {
            @Override
            public float getValue() {
                return mapon;
            }

            @Override
            protected void setCheckedValue(float value) {
                mapon = (int) value;
                loadnewlevel(mapon);
            }
        });
    }

    public void readpalettetable() {
        int num_tables, lookup_num;

        Entry fp = cache.getEntry("lookup.dat", true);
        if (fp.exists()) {
            try (InputStream is = fp.getInputStream()) {
                num_tables = StreamUtils.readUnsignedByte(is);
                for (int j = 0; j < num_tables; j++) {
                    lookup_num = StreamUtils.readUnsignedByte(is);
                    engine.getPaletteManager().makePalookup(lookup_num, StreamUtils.readBytes(is, 256), 0, 0, 0, 1);
                }
            } catch (IOException e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }
    }

    private void InitCutscenes() {
        Console.out.println("Initializing cutscenes");
        GrpFile group = new GrpFile("Cutscenes");

        Set<Group> groupList = new LinkedHashSet<>();
        Directory gameDir = cache.getGameDirectory();
        Directory smkDir = gameDir.getDirectory(gameDir.getEntry("smk"));
        if (!smkDir.isEmpty()) {
            groupList.add(smkDir);
        }
        groupList.add(gameDir);

        for (Group gr : groupList) {
            gr.stream().filter(e -> e.isExtension("smk")).forEach(group::addEntry);
        }

        if (!group.isEmpty()) {
            cache.addGroup(group, NORMAL);
        } else {
            Console.out.println("Cutscenes were not found", OsdColor.YELLOW);
        }
    }

    @Override
    public void dispose() {
        gDemoScreen.onStopRecord();
        super.dispose();
    }

    @Override
    public void show() {
        gDemoScreen.onStopRecord();
        if (gCutsceneScreen.init("intro.smk")) {
            changeScreen(gCutsceneScreen.setSkipping(main).escSkipping(true));
        } else {
            main.run();
        }
    }

    @Override
    public DefaultPrecacheScreen getPrecacheScreen(Runnable readyCallback, PrecacheListener listener) {
        return new PrecacheScreen(readyCallback, listener);
    }

    @Override
    public LogSender getLogSender() {
        return new LogSender(this) {
            @Override
            public byte[] reportData() {
                String report = "boardfilename " + boardfilename;

                report += "\r\n";
                if (player[0] != null) {
                    report += "PlayerX " + player[0].getX();
                    report += "\r\n";
                    report += "PlayerY " + player[0].getY();
                    report += "\r\n";
                    report += "PlayerZ " + player[0].getZ();
                    report += "\r\n";
                }

                return report.getBytes();
            }
        };
    }

    public enum UserFlag {
        None, UserMap, Addon;

        public static UserFlag parseValue(byte index) {
            switch (index) {
                case 1:
                    return Addon;
                case 2:
                    return UserMap;
                default:
                    return None;
            }
        }
    }

}

