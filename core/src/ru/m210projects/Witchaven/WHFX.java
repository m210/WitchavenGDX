package ru.m210projects.Witchaven;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Types.DefaultScreenFade;
import ru.m210projects.Build.Render.Types.ScreenFade;
import ru.m210projects.Build.Types.Point;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Factory.WHRenderer;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Build.Engine.visibility;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Witchaven.Animate.setanimation;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Types.ANIMATION.FLOORZ;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.showmessage;
import static ru.m210projects.Witchaven.WHTAG.*;
import static ru.m210projects.Witchaven.Whmap.loadnewlevel;

public class WHFX {

    public static final String WHITE_SCREEN_NAME = "WHITE";
    public static final String RED_SCREEN_NAME = "RED";
    public static final String GREEN_SCREEN_NAME = "GREEN";
    public static final String BLUE_SCREEN_NAME = "BLUE";

    public final static ScreenFade WHITE_DAC = new DefaultScreenFade(WHITE_SCREEN_NAME);
    public final static ScreenFade RED_DAC = new DefaultScreenFade(RED_SCREEN_NAME);
    public final static ScreenFade GREEN_DAC = new DefaultScreenFade(GREEN_SCREEN_NAME);
    public final static ScreenFade BLUE_DAC = new DefaultScreenFade(BLUE_SCREEN_NAME);

    public final static ScreenFade[] SCREEN_DAC_ARRAY = {
            WHITE_DAC, RED_DAC, GREEN_DAC, BLUE_DAC
    };

    public static final short[] skypanlist = new short[64];
    public static short skypancnt;
    public static final short[] lavadrylandsector = new short[32];
    public static short lavadrylandcnt;
    public static final short[] bobbingsectorlist = new short[16];
    public static short bobbingsectorcnt;

    public static int justwarpedfx = 0;
    public static int lastbat = -1;

    public static final short[] revolveclip = new short[16];
    public static final short[] revolvesector = new short[4];
    public static final short[] revolveang = new short[4];
    public static short revolvecnt;
    public static final int[][] revolvex = new int[4][32];
    public static final int[][] revolvey = new int[4][32];
    public static final int[] revolvepivotx = new int[4];
    public static final int[] revolvepivoty = new int[4];
    public static int warpx, warpy, warpz, warpang;
    public static int warpsect;

    public static int scarytime = -1;
    public static int scarysize = 0;

//    static int revolvesyncstat;
    static short revolvesyncang, revolvesyncrotang;
    static int revolvesyncx, revolvesyncy;
    static int thunderflash;
    static int thundertime;

    public static void initlava() // XXX
    {

    }

    public static void initwater() // XXX
    {

    }

//    public static void skypanfx() {
//        for (int i = 0; i < skypancnt; i++) {
//            boardService.getSector(skypanlist[i]).setCeilingxpanning((short) -((lockclock >> 2) & 255));
//        }
//    }

    public static void panningfx() {
        for (int i = 0; i < floorpanningcnt; i++) {
            Sector sec = boardService.getSector(floorpanninglist[i]);
            if (sec == null) {
                continue;
            }

            int whichdir = sec.getLotag() - 80;

            switch (whichdir) {
                case 0:
                    sec.setFloorypanning((short) ((lockclock >> 2) & 255));
                    break;
                case 1:
                    sec.setFloorxpanning((short) -((lockclock >> 2) & 255));
                    sec.setFloorypanning((short) ((lockclock >> 2) & 255));
                    break;
                case 2:
                    sec.setFloorxpanning((short) -((lockclock >> 2) & 255));
                    break;
                case 3:
                    sec.setFloorxpanning((short) -((lockclock >> 2) & 255));
                    sec.setFloorypanning((short) -((lockclock >> 2) & 255));
                    break;
                case 4:
                    sec.setFloorypanning((short) -((lockclock >> 2) & 255));
                    break;
                case 5:
                    sec.setFloorxpanning((short) ((lockclock >> 2) & 255));
                    sec.setFloorypanning((short) -((lockclock >> 2) & 255));
                    break;
                case 6:
                    sec.setFloorxpanning((short) ((lockclock >> 2) & 255));
                    break;
                case 7:
                    sec.setFloorxpanning((short) ((lockclock >> 2) & 255));
                    sec.setFloorypanning((short) ((lockclock >> 2) & 255));
                    break;
                default:
                    sec.setFloorxpanning(0);
                    sec.setFloorypanning(0);
                    break;
            }
        }

        for (int i = 0; i < xpanningsectorcnt; i++) {
            Sector sec = boardService.getSector(xpanningsectorlist[i]);
            if (sec == null) {
                continue;
            }

            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                wal.setXpanning((short) ((lockclock >> 2) & 255));
            }
        }

        for (int i = 0; i < ypanningwallcnt; i++) {
            Wall wal = boardService.getWall(ypanningwalllist[i]);
            if (wal != null) {
                wal.setYpanning(~(lockclock & 255));
            }
        }
    }

    public static void revolvefx() {
        PLAYER plr = player[pyrn];
        for (int i = 0; i < revolvecnt; i++) {
            Sector sec = boardService.getSector(revolvesector[i]);
            if (sec == null) {
                continue;
            }

            int startwall = sec.getWallptr();
            int endwall = (startwall + sec.getWallnum() - 1);

            revolveang[i] = (short) ((revolveang[i] + 2048 - ((TICSPERFRAME) << 1)) & 2047);
            for (int k = startwall; k <= endwall; k++) {
                Point out = EngineUtils.rotatepoint(revolvepivotx[i], revolvepivoty[i], revolvex[i][k - startwall],
                        revolvey[i][k - startwall], revolveang[i]);
                int dax = out.getX();
                int day = out.getY();
                engine.dragpoint(k, dax, day);
            }

            if (plr.getSector() == revolvesector[i]) {
                revolvesyncang = (short) plr.getAng();
                revolvesyncrotang = 0;
                revolvesyncx = plr.getX();
                revolvesyncy = plr.getY();
                revolvesyncrotang = (short) ((revolvesyncrotang + 2048 - ((TICSPERFRAME) << 1)) & 2047);
                Point out = EngineUtils.rotatepoint(revolvepivotx[i], revolvepivoty[i], revolvesyncx, revolvesyncy,
                        revolvesyncrotang);
                viewBackupPlayerLoc(pyrn);
                plr.setX(out.getX());
                plr.setY(out.getY());
                plr.setAng(((revolvesyncang + revolvesyncrotang) & 2047));
            }
        }
    }

    public static void bobbingsector() {
        for (int i = 0; i < bobbingsectorcnt; i++) {
            Sector sec = boardService.getSector(bobbingsectorlist[i]);
            if (sec == null) {
                continue;
            }
            sec.setFloorz(sec.getFloorz() + (EngineUtils.sin((lockclock << 4) & 2047) >> 6));
        }
    }

    public static void teleporter() {
        final PLAYER plr = player[pyrn];

        for (int i = 0; i < warpsectorcnt; i++) {
            Sector sec = boardService.getSector(warpsectorlist[i]);
            if (sec == null) {
                continue;
            }

            int j = ((lockclock & 127) >> 2);
            if (j >= 16) {
                j = 31 - j;
            }

            sec.setCeilingshade((byte) j);
            sec.setFloorshade((byte) j);
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                wal.setShade((byte) j);
            }
        }

        Sector sec = boardService.getSector(plr.getSector());
        if (sec == null) {
            return;
        }

        if (sec.getLotag() == 10) {
            if (plr.getSector() != plr.getOldsector()) {
                int daang = (short) plr.getAng();
                warpfxsprite(plr.getSpritenum());
                warp(plr.getX(), plr.getY(), plr.getZ(), daang, plr.getSector());
                viewBackupPlayerLoc(pyrn);
                plr.setX(warpx);
                plr.setY(warpy);
                plr.setZ(warpz);
                daang = (short) warpang;
                plr.setSector(warpsect);
                warpfxsprite(plr.getSpritenum());
                plr.setAng(daang);
                justwarpedfx = 48;
                playsound_loc(S_WARP, plr.getX(), plr.getY());
                engine.setsprite(plr.getSpritenum(), plr.getX(), plr.getY(), plr.getZ() + (32 << 8));
            }
        }

        if (sec.getLotag() == 4002) {
            if (plr.getSector() != plr.getOldsector()) {
                if (plr.getTreasure()[TPENTAGRAM] == 1) {
                    plr.getTreasure()[TPENTAGRAM] = 0;
                    if (mUserFlag == UserFlag.UserMap) {
                        game.changeScreen(gMenuScreen);
                        return;
                    }
                    switch (sec.getHitag()) {
                        case 1: // NEXTLEVEL
                            justteleported = true;

                            if (game.WH2) {
                                gStatisticsScreen.show(plr, () -> {
                                    mapon++;
                                    playsound_loc(S_CHAINDOOR1, plr.getX(), plr.getY());
                                    playertorch = 0;
                                    playsound_loc(S_WARP, plr.getX(), plr.getY());
                                    loadnewlevel(mapon);
                                });
                                break;
                            }

                            mapon++;
                            playsound_loc(S_CHAINDOOR1, plr.getX(), plr.getY());
                            playertorch = 0;
                            playsound_loc(S_WARP, plr.getX(), plr.getY());
                            loadnewlevel(mapon);
                            break;
                        case 2: // ENDOFDEMO
                            playsound_loc(S_THUNDER1, plr.getX(), plr.getY());
                            justteleported = true;
                            game.changeScreen(gVictoryScreen);
                            break;
                    }
                } else {
                    // player need pentagram to teleport
                    showmessage("ITEM NEEDED", 360);
                }
            }
        }
    }

    public static void warp(int x, int y, int z, int daang, int dasector) {
        Sector warpSec = boardService.getSector(dasector);
        if (warpSec == null) {
            return;
        }

        warpx = x;
        warpy = y;
        warpz = z;
        warpang = daang;
        warpsect = dasector;

        for (int i = 0; i < warpsectorcnt; i++) {
            Sector sec = boardService.getSector(warpsectorlist[i]);
            if (sec != null && sec.getHitag() == warpSec.getHitag() && warpsectorlist[i] != warpsect) {
                warpsect = warpsectorlist[i];
                warpSec = sec;
                break;
            }
        }

        // find center of sector
        int dax = 0, day = 0;
        Wall w = null;
        int startwall = warpSec.getWallptr();
        int endwall = (startwall + warpSec.getWallnum() - 1);
        for (ListNode<Wall> wn = warpSec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wal = wn.get();
            dax += wal.getX();
            day += wal.getY();
            if (wal.getNextsector() >= 0) {
                w = wal;
            }
        }
        warpx = dax / (endwall - startwall + 1);
        warpy = day / (endwall - startwall + 1);
        warpz = warpSec.getFloorz() - (32 << 8);
        warpsect = engine.updatesector(warpx, warpy, warpsect);
        if (w != null) {
            dax = ((w.getX() + w.getWall2().getX()) >> 1);
            day = ((w.getY() + w.getWall2().getY()) >> 1);
            warpang = EngineUtils.getAngle(dax - warpx, day - warpy);
        }
    }

    public static void warpsprite(int spritenum) {
        // EG 19 Aug 2017 - Try to prevent monsters teleporting back and forth wildly
        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null || monsterwarptime > 0) {
            return;
        }
        int dasectnum = spr.getSectnum();
        warpfxsprite(spritenum);
        warp(spr.getX(), spr.getY(), spr.getZ(), spr.getAng(), dasectnum);
        spr.setX(warpx);
        spr.setY(warpy);
        spr.setZ(warpz);
        spr.setAng((short) warpang);

        warpfxsprite(spritenum);
        engine.setsprite(spritenum, spr.getX(), spr.getY(), spr.getZ());

        // EG 19 Aug 2017 - Try to prevent monsters teleporting back and forth wildly
        monsterwarptime = 120;
    }

    public static void ironbars() {
        for (int i = 0; i < ironbarscnt; i++) {
            if (ironbarsdone[i] == 1) {
                final int spritenum = ironbarsanim[i];
                Sprite spr = boardService.getSprite(spritenum);
                if (spr != null) {
                    switch (spr.getHitag()) {
                        case 1:
                            game.pInt.setsprinterpolate(spritenum, spr);
                            spr.setAng(spr.getAng() + (TICSPERFRAME << 1));
                            if (spr.getAng() > 2047) {
                                spr.setAng(spr.getAng() - 2047);
                            }
                            ironbarsgoal[i] += TICSPERFRAME << 1;
                            engine.setsprite(spritenum, spr.getX(), spr.getY(), spr.getZ());
                            if (ironbarsgoal[i] > 512) {
                                ironbarsgoal[i] = 0;
                                spr.setHitag(2);
                                ironbarsdone[i] = 0;
                            }
                            break;
                        case 2:
                            game.pInt.setsprinterpolate(spritenum, spr);
                            spr.setAng(spr.getAng() - (TICSPERFRAME << 1));
                            if (spr.getAng() < 0) {
                                spr.setAng(spr.getAng() + 2047);
                            }
                            ironbarsgoal[i] += TICSPERFRAME << 1;
                            engine.setsprite(spritenum, spr.getX(), spr.getY(), spr.getZ());
                            if (ironbarsgoal[i] > 512) {
                                ironbarsgoal[i] = 0;
                                spr.setHitag(1);
                                ironbarsdone[i] = 0;
                            }
                            break;
                        case 3:
                            game.pInt.setsprinterpolate(spritenum, spr);
                            spr.setZ(spr.getZ() - (TICSPERFRAME << 4));
                            if (spr.getZ() < ironbarsgoal[i]) {
                                spr.setZ(ironbarsgoal[i]);
                                spr.setHitag(4);
                                ironbarsdone[i] = 0;
                                ironbarsgoal[i] = spr.getZ() + 6000;
                            }
                            engine.setsprite(spritenum, spr.getX(), spr.getY(), spr.getZ());
                            break;
                        case 4:
                            game.pInt.setsprinterpolate(spritenum, spr);
                            spr.setZ(spr.getZ() + (TICSPERFRAME << 4));
                            if (spr.getZ() > ironbarsgoal[i]) {
                                spr.setZ(ironbarsgoal[i]);
                                spr.setHitag(3);
                                ironbarsdone[i] = 0;
                                ironbarsgoal[i] = spr.getZ() - 6000;
                            }
                            engine.setsprite(spritenum, spr.getX(), spr.getY(), spr.getZ());
                            break;
                    }
                }
            }
        }
    }

    public static void sectorsounds() {
        if (cfg.isNoSound()) {
            return;
        }

        PLAYER plr = player[pyrn];
        Sector sec = boardService.getSector(plr.getSector());
        if (sec == null) {
            return;
        }

        int secExtra = sec.getExtra() & 0xFFFF;
        if (secExtra != 0) {
            if ((secExtra & 32768) != 0) { // loop on/off sector
                if ((secExtra & 1) != 0) { // turn loop on if lsb is 1
                    int index = (secExtra & ~0x8001) >> 1;
                    if (index < ambsoundarray.length && ambsoundarray[index].hsound == -1) {
                        ambsoundarray[index].hsound = playsound(ambsoundarray[index].soundnum, 0, 0, -1);
                    }
                } else { // turn loop off if lsb is 0 and its playing
                    int index = (secExtra & ~0x8000) >> 1;
                    if (index < ambsoundarray.length && ambsoundarray[index].hsound != -1) {
                        stopsound(ambsoundarray[index].hsound);
                        ambsoundarray[index].hsound = -1;
                    }
                }
            } else {
                if (plr.getZ() <= sec.getFloorz() - (8 << 8)) {
                    playsound_loc(secExtra, plr.getX(), plr.getY());
                }
            }
        }
    }

    public static void scaryprocess() {
        if (engine.krand() % 32768 > 32500 && engine.krand() % 32768 > 32500 && scarytime < 0) {
            scarytime = 180;
            scarysize = 30;
            SND_Sound(S_SCARYDUDE);
        }

        if (scarytime >= 0) {
            scarytime -= TICSPERFRAME << 1;
            scarysize += TICSPERFRAME << 1;
        }
    }

    public static void dofx() {
        WHRenderer renderer = game.getRenderer();
        byte[] gotpic = renderer.getGotPic();
        lavadryland();
        scaryprocess();
        if (revolvecnt > 0) {
            revolvefx();
        }
        panningfx();
        teleporter();
        bobbingsector();
        if (ironbarscnt > 0) {
            ironbars();
        }

        if ((gotpic[ANILAVA >> 3] & (1 << (ANILAVA & 7))) > 0) {
            gotpic[ANILAVA >> 3] &= ~(1 << (ANILAVA & 7));
//			if (waloff[ANILAVA] != null)
//			movelava(waloff[ANILAVA]); XXX
        }
        if ((gotpic[HEALTHWATER >> 3] & (1 << (HEALTHWATER & 7))) > 0) {
            gotpic[HEALTHWATER >> 3] &= ~(1 << (HEALTHWATER & 7));
//			if (waloff[HEALTHWATER] != null) XXX
//				movewater(waloff[HEALTHWATER]);
        }
        thesplash();
        thunder();
        cracks();
        if (game.WH2) {
            PLAYER plr = player[0];
            Sector sec = boardService.getSector(plr.getSector());
            if (sec != null && sec.getLotag() == 50 && sec.getHitag() > 0) {
                weaponpowerup(plr);
            }
        }

        if (player[pyrn].getPoisoned() != 0) {
            int tilt = mulscale(EngineUtils.sin((3 * lockclock) & 2047), 20, 16);
            if (tilt != 0) {
                renderer.setdrunk(tilt);
            }
        } else {
            renderer.setdrunk(0);
        }
    }

    public static void thunder() {
        WHRenderer renderer = game.getRenderer();
        byte[] gotpic = renderer.getGotPic();
        if (thunderflash == 0) {
            visibility = 1024;
            if ((gotpic[SKY >> 3] & (1 << (SKY & 7))) > 0) {
                gotpic[SKY >> 3] &= ~(1 << (SKY & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            } else if ((gotpic[SKY2 >> 3] & (1 << (SKY2 & 7))) > 0) {
                gotpic[SKY2 >> 3] &= (byte) ~(1 << (SKY2 & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            } else if ((gotpic[SKY3 >> 3] & (1 << (SKY3 & 7))) > 0) {
                gotpic[SKY3 >> 3] &= ~(1 << (SKY3 & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            } else if ((gotpic[SKY4 >> 3] & (1 << (SKY4 & 7))) > 0) {
                gotpic[SKY4 >> 3] &= ~(1 << (SKY4 & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            } else if ((gotpic[SKY5 >> 3] & (1 << (SKY5 & 7))) > 0) {
                gotpic[SKY5 >> 3] &= ~(1 << (SKY5 & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            } else if ((gotpic[SKY6 >> 3] & (1 << (SKY6 & 7))) > 0) {
                gotpic[SKY6 >> 3] &= ~(1 << (SKY6 & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            } else if ((gotpic[SKY7 >> 3] & (1 << (SKY7 & 7))) > 0) {
                gotpic[SKY7 >> 3] &= ~(1 << (SKY7 & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            } else if ((gotpic[SKY8 >> 3] & (1 << (SKY8 & 7))) > 0) {
                gotpic[SKY8 >> 3] &= ~(1 << (SKY8 & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            } else if ((gotpic[SKY9 >> 3] & (1 << (SKY9 & 7))) > 0) {
                gotpic[SKY9 >> 3] &= ~(1 << (SKY9 & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            } else if ((gotpic[SKY10 >> 3] & (1 << (SKY10 & 7))) > 0) {
                gotpic[SKY10 >> 3] &= ~(1 << (SKY10 & 7));
                if (engine.rand() % 32768 > 32700) {
                    thunderflash = 1;
                    thundertime = 120;
                }
            }
        } else {
            thundertime -= TICSPERFRAME;
            if (thundertime < 0) {
                thunderflash = 0;
                SND_Sound(S_THUNDER1 + (engine.rand() % 4));
                visibility = 1024;
            }
        }

        if (thunderflash == 1) {
            int val = engine.rand() % 4;
            switch (val) {
                case 0:
                    visibility = 2048;
                    break;
                case 1:
                    visibility = 1024;
                    break;
                case 2:
                    visibility = 512;
                    break;
                case 3:
                    visibility = 256;
                    break;
                default:
                    visibility = 4096;
                    break;
            }
        }
    }

    public static void thesplash() {
        PLAYER plr = player[pyrn];
        Sector sec = boardService.getSector(plr.getSector());
        if (sec == null) {
            return;
        }

        if (sec.getFloorpicnum() == WATER || sec.getFloorpicnum() == LAVA
                || sec.getFloorpicnum() == SLIME) {
            if (plr.getOnsomething() == 0) {
                return;
            }

            if (plr.getSector() != plr.getOldsector()) {
                switch (sec.getFloorpicnum()) {
                    case WATER:
                        makeasplash(SPLASHAROO, plr);
                        break;
                    case SLIME:
                        if (game.WH2) {
                            makeasplash(SLIMESPLASH, plr);
                        } else {
                            makeasplash(SPLASHAROO, plr);
                        }
                        break;
                    case LAVA:
                        makeasplash(LAVASPLASH, plr);
                        break;
                }
            }
        }
    }

    public static void makeasplash(int picnum, PLAYER plr) {
        int j = engine.insertsprite(plr.getSector(), MASPLASH);
        Sprite spr = boardService.getSprite(j);
        if (spr == null) {
            return;
        }

        Sector sec = boardService.getSector(plr.getSector());
        if (sec == null) {
            return;
        }


        spr.setX(plr.getX());
        spr.setY(plr.getY());
        spr.setZ(sec.getFloorz() + (engine.getTile(picnum).getHeight() << 8));
        spr.setCstat(0); // Hitscan does not hit other bullets
        spr.setPicnum(picnum);
        spr.setShade(0);
        spr.setPal(0);
        spr.setXrepeat(64);
        spr.setYrepeat(64);
        spr.setOwner(0);
        spr.setClipdist(16);
        spr.setLotag(8);
        spr.setHitag(0);

        switch (picnum) {
            case SPLASHAROO:
            case SLIMESPLASH:
                if (!game.WH2 && picnum == SLIMESPLASH) {
                    break;
                }

                playsound_loc(S_SPLASH1 + (engine.krand() % 3), spr.getX(), spr.getY());
                break;
            case LAVASPLASH:
                break;
        }

        movesprite(j, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
    }

    public static void makemonstersplash(int picnum, final int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null || spr.getPicnum() == FISH) {
            return;
        }

        WHRenderer renderer = game.getRenderer();
        int j = engine.insertsprite(spr.getSectnum(), MASPLASH);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(sec.getFloorz() + (engine.getTile(picnum).getHeight() << 8));
        spr2.setCstat(0); // Hitscan does not hit other bullets
        spr2.setPicnum((short) picnum);
        spr2.setShade(0);

        if (sec.getFloorpal() == 9) {
            spr2.setPal(9);
        } else {
            spr2.setPal(0);
        }

        if (spr.getPicnum() == RAT) {
            spr2.setXrepeat(40);
            spr2.setYrepeat(40);
        } else {
            spr2.setXrepeat(64);
            spr2.setYrepeat(64);
        }
        spr2.setOwner(0);
        spr2.setClipdist(16);
        spr2.setLotag(8);
        spr2.setHitag(0);
        game.pInt.setsprinterpolate(j, spr2);
        byte[] gotpic = renderer.getGotPic();

        // JSA 5/3 start
        switch (picnum) {
            case SPLASHAROO:
            case SLIME:
                if ((engine.krand() % 2) != 0) {
                    if ((gotpic[WATER >> 3] & (1 << (WATER & 7))) > 0) {
                        gotpic[WATER >> 3] &= ~(1 << (WATER & 7));
                        if ((engine.rand() % 2) != 0) {
                            playsound_loc(S_SPLASH1 + (engine.rand() % 3), spr2.getX(), spr2.getY());
                        }
                    }
                }
                if ((engine.krand() % 2) != 0) {
                    if ((gotpic[SLIME >> 3] & (1 << (SLIME & 7))) > 0) {
                        gotpic[SLIME >> 3] &= ~(1 << (SLIME & 7));
                        if ((engine.rand() % 2) != 0) {
                            playsound_loc(S_SPLASH1 + (engine.rand() % 3), spr2.getX(), spr2.getY());
                        }
                    }
                }
                break;
            case LAVASPLASH:
                break;
        }
    }

    public static void bats(PLAYER ignoredPlr, final int k) {
        Sprite spr = boardService.getSprite(k);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), FLOCK);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ());
        spr2.setCstat(0);
        spr2.setPicnum(BAT);
        spr2.setShade(0);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
        spr2.setAng((short) ((spr.getAng() + (engine.krand() & 128 - 256)) & 2047));
        spr2.setOwner((short) k);
        spr2.setClipdist(16);
        spr2.setLotag(128);
        spr2.setHitag((short) k);
        spr2.setExtra(0);
        game.pInt.setsprinterpolate(j, spr2);

        newstatus(j, FLOCK);

        if (spr.getExtra() == 1) {
            lastbat = j;
        }
    }

    public static void cracks() {
        PLAYER plr = player[0];
        Sector sec = boardService.getSector(plr.getSector());
        if (sec == null) {
            return;
        }

        int datag = sec.getLotag();
        if (floorpanningcnt < 64) {
            if (datag >= 3500 && datag <= 3599) {
                sec.setHitag(0);
                int daz = sec.getFloorz() + (1024 * (sec.getLotag() - 3500));
                if ((setanimation(plr.getSector(), daz, 32, 0, FLOORZ)) >= 0) {
                    sec.setFloorpicnum(LAVA1);
                    sec.setFloorshade(-25);
                    SND_Sound(S_CRACKING);
                }
                sec.setLotag(80);
                floorpanninglist[floorpanningcnt++] = (short) plr.getSector();
            }
        }

        if (datag >= 5100 && datag <= 5199) {
            sec.setHitag(0);
            sec.setLotag(0);
        }

        if (datag >= 5200 && datag <= 5299) {
            sec.setHitag(0);
            sec.setLotag(0);
        }

        if (datag == 3001) {
            sec.setLotag(0);
            for (int k = 0; k < MAXSPRITES; k++) {
                Sprite spr = boardService.getSprite(k);
                if (spr != null && sec.getHitag() == spr.getHitag()) {
                    spr.setLotag(36);
                    spr.setZvel((short) (engine.krand() & 1024 + 512));
                    newstatus(k, SHOVE);
                }
            }
        }
    }

    public static void lavadryland() {
        PLAYER plr = player[pyrn];

        for (int k = 0; k < lavadrylandcnt; k++) {
            int s = lavadrylandsector[k];
            Sector sec = boardService.getSector(s);
            if (sec != null && plr.getSector() == s && sec.getLotag() > 0) {
                sec.setHitag(0);
                switch (sec.getFloorpicnum()) {
                    case LAVA:
                    case ANILAVA:
                    case LAVA1:
                        sec.setFloorpicnum(COOLLAVA);
                        break;
                    case SLIME:
                        sec.setFloorpicnum(DRYSLIME);
                        break;
                    case WATER:
                    case HEALTHWATER:
                        sec.setFloorpicnum(DRYWATER);
                        break;
                    case LAVA2:
                        sec.setFloorpicnum(COOLLAVA2);
                        break;
                }
                sec.setLotag(0);
            }
        }

    }

    public static void warpfxsprite(final int s) {
        PLAYER plr = player[pyrn];
        Sprite spr = boardService.getSprite(s);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), WARPFX);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ() - (32 << 8));

        spr2.setCstat(0);

        spr2.setPicnum(ANNIHILATE);
        short daang;
        if (s == plr.getSpritenum()) {
            daang = (short) plr.getAng();
            spr2.setAng(daang);
        } else {
            daang = spr.getAng();
            spr2.setAng(daang);
        }

        spr2.setXrepeat(48);
        spr2.setYrepeat(48);
        spr2.setClipdist(16);

        spr2.setExtra(0);
        spr2.setShade(-31);
        spr2.setXvel((short) ((engine.krand() & 256) - 128));
        spr2.setYvel((short) ((engine.krand() & 256) - 128));
        spr2.setZvel((short) ((engine.krand() & 256) - 128));
        spr2.setOwner((short) s);
        spr2.setLotag(12);
        spr2.setHitag(0);
        spr2.setPal(0);

        int daz = (((spr2.getZvel()) * TICSPERFRAME) >> 3);

        movesprite(j, ((EngineUtils.sin((daang + 512) & 2047)) * TICSPERFRAME) << 3,
                ((EngineUtils.sin(daang & 2047)) * TICSPERFRAME) << 3, daz, 4 << 8, 4 << 8, 1);
    }

    public static void weaponpowerup(PLAYER plr) {
        showmessage("Weapons enchanted", 360);
        for (int i = 0; i < 10; i++) {
            if (plr.getWeapon()[i] != 0 && plr.getWeapon()[i] != 3) {
                plr.getPreenchantedweapon()[i] = plr.getWeapon()[i];
                plr.getPreenchantedammo()[i] = plr.getAmmo()[i];
                plr.getWeapon()[i] = 3;
                switch (difficulty) {
                    case 0:
                        plr.getAmmo()[i] = 25;
                        break;
                    case 1:
                        plr.getAmmo()[i] = 20;
                        break;
                    case 2:
                    case 3:
                        plr.getAmmo()[i] = 10;
                        break;
                }

                Sector sec = boardService.getSector(plr.getSector());
                if (sec != null && sec.getHitag() > 0) {
                    sec.setHitag(sec.getHitag() - 1);
                    if (sec.getHitag() == 0) {
                        ListNode<Sprite> sectNode = boardService.getSectNode(plr.getSector());
                        while (sectNode != null) {
                            ListNode<Sprite> next = sectNode.getNext();
                            if (sectNode.get().getPicnum() == CONE) {
                                engine.deletesprite(sectNode.getIndex());
                            } else if (sectNode.get().getPicnum() == SPARKBALL) {
                                engine.deletesprite(sectNode.getIndex());
                            }
                            sectNode = next;
                        }
                    }
                }
            }
        }
    }

    public static void makesparks(int i, int type) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = -1;
        switch (type) {
            case 1:
                j = engine.insertsprite(spr.getSectnum(), SPARKS);
                break;
            case 2:
                j = engine.insertsprite(spr.getSectnum(), SPARKSUP);
                break;
            case 3:
                j = engine.insertsprite(spr.getSectnum(), SPARKSDN);
                break;
        }

        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ());
        spr2.setCstat(0);
        spr2.setPicnum(SPARKBALL);
        spr2.setShade(0);
        spr2.setXrepeat(24);
        spr2.setYrepeat(24);
        spr2.setAng((short) ((engine.krand() % 2047) & 2047));
        spr2.setOwner(0);
        spr2.setClipdist(16);
        spr2.setLotag((short) (engine.krand() % 100));
        spr2.setHitag(0);
        spr2.setExtra(0);
        spr2.setPal(0);
    }

    public static void shards(final int i, int type) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), SHARDOFGLASS);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX() + (((engine.krand() % 512) - 256) << 2));
        spr2.setY(spr.getY() + (((engine.krand() % 512) - 256) << 2));
        spr2.setZ(spr.getZ() - (getPlayerHeight() << 8) + (((engine.krand() % 48) - 16) << 7));
        spr2.setZvel((short) (engine.krand() % 256));
        spr2.setCstat(0);
        spr2.setPicnum((short) (SHARD + (engine.krand() % 3)));
        spr2.setShade(0);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
        spr2.setAng((short) (spr.getAng() + ((engine.krand() % 512) - 256) & 2047));
        spr2.setOwner((short) i);
        spr2.setClipdist(16);
        spr2.setLotag((short) (120 + (engine.krand() % 100)));
        spr2.setHitag(0);
        spr2.setExtra((short) type);
        spr2.setPal(0);
    }
}
