package ru.m210projects.Witchaven;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;
import ru.m210projects.Witchaven.Types.WEAPON;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClampAngle;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Screens.DemoScreen.checkCodeVersion;
import static ru.m210projects.Witchaven.Spellbooks.*;
import static ru.m210projects.Witchaven.Types.CompareService.CompareService.conditionUpdate;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.showmessage;
import static ru.m210projects.Witchaven.WHScreen.startredflash;
import static ru.m210projects.Witchaven.WHTAG.operatesector;

public class Weapons {

    public static final int MAXWEAPONS = 10;
    public static final WEAPON[] spikeanimtics = {new WEAPON(10, DIESPIKE, 136, 145),
            new WEAPON(10, DIESPIKE + 1, 136, 124), new WEAPON(10, DIESPIKE + 2, 136, 100),
            new WEAPON(10, DIESPIKE + 3, 136, 70), new WEAPON(10, DIESPIKE + 4, 136, 50)};
    public static final WEAPON[][] wh2throwanimtics = {
            // MUTWOHANDS  - scare spell
            {new WEAPON(1, 0, 127, 170), new WEAPON(10, MUTWOHANDS + 1, 0, 128), new WEAPON(10, MUTWOHANDS + 2, 0, 93),
                    new WEAPON(10, MUTWOHANDS + 3, 0, 83), new WEAPON(10, MUTWOHANDS + 4, 0, 72), new WEAPON(10, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 6, 10, 96), new WEAPON(10, MUTWOHANDS + 7, 43, 109), new WEAPON(10, MUTWOHANDS + 8, 69, 113),
                    new WEAPON(10, MUTWOHANDS + 9, 65, 115), new WEAPON(10, MUTWOHANDS + 10, 64, 117), new WEAPON(10, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)
            },

            // MUTWOHANDS  - night vision
            {new WEAPON(10, ZFIREBALL, 0, 177), new WEAPON(10, ZFIREBALL + 1, 0, 137), new WEAPON(10, ZFIREBALL + 2, 48, 82),
                    new WEAPON(10, ZFIREBALL + 3, 127, 41), new WEAPON(10, ZFIREBALL + 4, 210, 9), new WEAPON(10, ZFIREBALL + 5, 284, 26),
                    new WEAPON(10, ZFIREBALL + 6, 213, 63), new WEAPON(10, ZFIREBALL + 7, 147, 99), new WEAPON(10, ZFIREBALL + 8, 91, 136),
                    new WEAPON(10, ZFIREBALL + 9, 46, 183), new WEAPON(1, 0, 127, 170), new WEAPON(1, 0, 127, 170),
                    new WEAPON(1, 0, 127, 170)
            },

            // MUTWOHANDS  - freeze
//		     {new WEAPON(1, 0, 127, 170), new WEAPON(10, ZFREEZE + 1, 0, 51), new WEAPON(10, ZFREEZE + 2, 0, 71),
//		     new WEAPON(10, ZFREEZE + 3, 4, 85), new WEAPON(10, ZFREEZE + 4, 32, 78), new WEAPON(10, ZFREEZE + 5, 51, 80),
//		     new WEAPON(10, ZFREEZE + 6, 50, 80), new WEAPON(10, ZFREEZE + 7, 49, 89), new WEAPON(10, ZFREEZE + 8, 49, 89),
//		     new WEAPON(10, ZFREEZE + 9, 49, 98), new WEAPON(10, ZFREEZE + 10, 47, 105), new WEAPON(10, ZFREEZE + 11, 48, 121),
//		     new WEAPON(1, 0, 127, 170)
            {new WEAPON(1, 0, 127, 170), new WEAPON(10, MUTWOHANDS + 1, 0, 128), new WEAPON(10, MUTWOHANDS + 2, 0, 93),
                    new WEAPON(10, MUTWOHANDS + 3, 0, 83), new WEAPON(10, MUTWOHANDS + 4, 0, 72), new WEAPON(10, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 6, 10, 96), new WEAPON(10, MUTWOHANDS + 7, 43, 109), new WEAPON(10, MUTWOHANDS + 8, 69, 113),
                    new WEAPON(10, MUTWOHANDS + 9, 65, 115), new WEAPON(10, MUTWOHANDS + 10, 64, 117), new WEAPON(10, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)
            },

            // MUTWOHANDS  - magic arrow
            {new WEAPON(1, 0, 127, 170), new WEAPON(10, MUTWOHANDS + 1, 0, 128), new WEAPON(10, MUTWOHANDS + 2, 0, 93),
                    new WEAPON(10, MUTWOHANDS + 3, 0, 83), new WEAPON(10, MUTWOHANDS + 4, 0, 72), new WEAPON(10, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 6, 10, 96), new WEAPON(10, MUTWOHANDS + 7, 43, 109), new WEAPON(10, MUTWOHANDS + 8, 69, 113),
                    new WEAPON(10, MUTWOHANDS + 9, 65, 115), new WEAPON(10, MUTWOHANDS + 10, 64, 117), new WEAPON(10, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)
            },
            // MUTWOHANDS  - open door
            {new WEAPON(15, MUTWOHANDS, 19, 155), new WEAPON(15, MUTWOHANDS + 1, 0, 128), new WEAPON(15, MUTWOHANDS + 2, 0, 93),
                    new WEAPON(15, MUTWOHANDS + 3, 0, 83), new WEAPON(15, MUTWOHANDS + 4, 0, 72), new WEAPON(15, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(15, MUTWOHANDS + 6, 10, 96), new WEAPON(15, MUTWOHANDS + 7, 43, 109), new WEAPON(15, MUTWOHANDS + 8, 69, 113),
                    new WEAPON(15, MUTWOHANDS + 9, 65, 115), new WEAPON(15, MUTWOHANDS + 10, 64, 117), new WEAPON(15, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)
            },

            // MUMEDUSA    - fly
            {new WEAPON(10, ZLIGHT, 0, 177), new WEAPON(10, ZLIGHT + 1, 0, 137), new WEAPON(10, ZLIGHT + 2, 48, 82),
                    new WEAPON(10, ZLIGHT + 3, 127, 41), new WEAPON(10, ZLIGHT + 4, 210, 9), new WEAPON(10, ZLIGHT + 5, 284, 26),
                    new WEAPON(10, ZLIGHT + 6, 213, 63), new WEAPON(10, ZLIGHT + 7, 147, 99), new WEAPON(10, ZLIGHT + 8, 91, 136),
                    new WEAPON(10, ZLIGHT + 9, 46, 183), new WEAPON(1, 0, 127, 170), new WEAPON(1, 0, 127, 170),
                    new WEAPON(1, 0, 127, 170)
            },

            // MUTWOHANDS  - fireball
            {new WEAPON(1, 0, 127, 170), new WEAPON(10, ZFIREBALL + 1, 0, 137), new WEAPON(10, ZFIREBALL + 2, 48, 82),
                    new WEAPON(10, ZFIREBALL + 3, 127, 41), new WEAPON(10, ZFIREBALL + 4, 210, 9), new WEAPON(10, ZFIREBALL + 5, 284, 26),
                    new WEAPON(10, ZFIREBALL + 6, 213, 63), new WEAPON(10, ZFIREBALL + 7, 147, 99), new WEAPON(10, ZFIREBALL + 8, 91, 136),
                    new WEAPON(10, ZFIREBALL + 9, 46, 183), new WEAPON(1, 0, 127, 170), new WEAPON(1, 0, 127, 170),
                    new WEAPON(1, 0, 127, 170)
            },

            // MUTWOHANDS  - nuke
            {new WEAPON(1, 0, 127, 170), new WEAPON(10, MUTWOHANDS + 1, 0, 128), new WEAPON(10, MUTWOHANDS + 2, 0, 93),
                    new WEAPON(10, MUTWOHANDS + 3, 0, 83), new WEAPON(10, MUTWOHANDS + 4, 0, 72), new WEAPON(10, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 6, 10, 96), new WEAPON(10, MUTWOHANDS + 7, 43, 109), new WEAPON(10, MUTWOHANDS + 8, 69, 113),
                    new WEAPON(10, MUTWOHANDS + 9, 65, 115), new WEAPON(10, MUTWOHANDS + 10, 64, 117), new WEAPON(10, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)
            }
    };
    public static final WEAPON[][] throwanimtics = {
            // MUTWOHANDS
            {new WEAPON(10, MUTWOHANDS, 19, 155), new WEAPON(10, MUTWOHANDS + 1, 0, 128),
                    new WEAPON(10, MUTWOHANDS + 2, 0, 93), new WEAPON(10, MUTWOHANDS + 3, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 4, 0, 72), new WEAPON(10, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 6, 10, 96), new WEAPON(10, MUTWOHANDS + 7, 43, 109),
                    new WEAPON(10, MUTWOHANDS + 8, 69, 113), new WEAPON(10, MUTWOHANDS + 9, 65, 115),
                    new WEAPON(10, MUTWOHANDS + 10, 64, 117), new WEAPON(10, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)},
            // MUMEDUSA
            {new WEAPON(10, MUMEDUSA, 0, 177), new WEAPON(10, MUMEDUSA + 1, 0, 137),
                    new WEAPON(10, MUMEDUSA + 2, 48, 82), new WEAPON(10, MUMEDUSA + 3, 127, 41),
                    new WEAPON(10, MUMEDUSA + 4, 210, 9), new WEAPON(10, MUMEDUSA + 5, 284, 26),
                    new WEAPON(10, MUMEDUSA + 6, 213, 63), new WEAPON(10, MUMEDUSA + 7, 147, 99),
                    new WEAPON(10, MUMEDUSA + 8, 91, 136), new WEAPON(10, MUMEDUSA + 9, 46, 183),
                    new WEAPON(1, 0, 127, 170), new WEAPON(1, 0, 127, 170), new WEAPON(1, 0, 127, 170)},
            // BMUTWOHANDS
            {new WEAPON(10, MUTWOHANDS, 19, 155), new WEAPON(10, MUTWOHANDS + 1, 0, 128),
                    new WEAPON(10, MUTWOHANDS + 2, 0, 93), new WEAPON(10, MUTWOHANDS + 3, 0, 83),
                    new WEAPON(10, BMUTWOHANDS, 0, 74), new WEAPON(10, BMUTWOHANDS + 1, 0, 97),
                    new WEAPON(10, BMUTWOHANDS + 2, 10, 109), new WEAPON(10, BMUTWOHANDS + 3, 43, 113),
                    new WEAPON(10, BMUTWOHANDS + 4, 69, 115), new WEAPON(10, BMUTWOHANDS + 5, 65, 117),
                    new WEAPON(10, BMUTWOHANDS + 6, 64, 117), new WEAPON(10, BMUTWOHANDS + 7, 63, 117),
                    new WEAPON(1, 0, 127, 170)},
            // MUTWOHANDS
            {new WEAPON(10, MUTWOHANDS, 19, 155), new WEAPON(10, MUTWOHANDS + 1, 0, 128),
                    new WEAPON(10, MUTWOHANDS + 2, 0, 93), new WEAPON(10, MUTWOHANDS + 3, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 4, 0, 72), new WEAPON(10, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 6, 10, 96), new WEAPON(10, MUTWOHANDS + 7, 43, 109),
                    new WEAPON(10, MUTWOHANDS + 8, 69, 113), new WEAPON(10, MUTWOHANDS + 9, 65, 115),
                    new WEAPON(10, MUTWOHANDS + 10, 64, 117), new WEAPON(10, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)},
            // MUTWOHANDS
            {new WEAPON(15, MUTWOHANDS, 19, 155), new WEAPON(15, MUTWOHANDS + 1, 0, 128),
                    new WEAPON(15, MUTWOHANDS + 2, 0, 93), new WEAPON(15, MUTWOHANDS + 3, 0, 83),
                    new WEAPON(15, MUTWOHANDS + 4, 0, 72), new WEAPON(15, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(15, MUTWOHANDS + 6, 10, 96), new WEAPON(15, MUTWOHANDS + 7, 43, 109),
                    new WEAPON(15, MUTWOHANDS + 8, 69, 113), new WEAPON(15, MUTWOHANDS + 9, 65, 115),
                    new WEAPON(15, MUTWOHANDS + 10, 64, 117), new WEAPON(15, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)},
            // MUMEDUSA
            {new WEAPON(10, MUMEDUSA, 0, 177), new WEAPON(10, MUMEDUSA + 1, 0, 137),
                    new WEAPON(10, MUMEDUSA + 2, 48, 82), new WEAPON(10, MUMEDUSA + 3, 127, 41),
                    new WEAPON(10, MUMEDUSA + 4, 210, 9), new WEAPON(10, MUMEDUSA + 5, 284, 26),
                    new WEAPON(10, MUMEDUSA + 6, 213, 63), new WEAPON(10, MUMEDUSA + 7, 147, 99),
                    new WEAPON(10, MUMEDUSA + 8, 91, 136), new WEAPON(10, MUMEDUSA + 9, 46, 183),
                    new WEAPON(1, 0, 127, 170), new WEAPON(1, 0, 127, 170), new WEAPON(1, 0, 127, 170)},
            // MUTWOHANDS
            {new WEAPON(10, MUTWOHANDS, 19, 155), new WEAPON(10, MUTWOHANDS + 1, 0, 128),
                    new WEAPON(10, MUTWOHANDS + 2, 0, 93), new WEAPON(10, MUTWOHANDS + 3, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 4, 0, 72), new WEAPON(10, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 6, 10, 96), new WEAPON(10, MUTWOHANDS + 7, 43, 109),
                    new WEAPON(10, MUTWOHANDS + 8, 69, 113), new WEAPON(10, MUTWOHANDS + 9, 65, 115),
                    new WEAPON(10, MUTWOHANDS + 10, 64, 117), new WEAPON(10, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)},
            // MUTWOHANDS
            {new WEAPON(10, MUTWOHANDS, 19, 155), new WEAPON(10, MUTWOHANDS + 1, 0, 128),
                    new WEAPON(10, MUTWOHANDS + 2, 0, 93), new WEAPON(10, MUTWOHANDS + 3, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 4, 0, 72), new WEAPON(10, MUTWOHANDS + 5, 0, 83),
                    new WEAPON(10, MUTWOHANDS + 6, 10, 96), new WEAPON(10, MUTWOHANDS + 7, 43, 109),
                    new WEAPON(10, MUTWOHANDS + 8, 69, 113), new WEAPON(10, MUTWOHANDS + 9, 65, 115),
                    new WEAPON(10, MUTWOHANDS + 10, 64, 117), new WEAPON(10, MUTWOHANDS + 11, 63, 117),
                    new WEAPON(1, 0, 127, 170)}};
    public static final WEAPON[] cockanimtics = {new WEAPON(24, 0, 10, 10), new WEAPON(12, BOWREADYEND + 1, 101, 115),
            new WEAPON(12, BOWREADYEND + 2, 112, 0), new WEAPON(12, BOWREADYEND + 3, 115, 0),
            new WEAPON(12, BOWREADYEND + 4, 75, 13)};
    // SCOTT
    public static final WEAPON[] zcockanimtics = {new WEAPON(24, 0, 10, 10), new WEAPON(12, BOWREADYEND + 1, 101, 115),
            new WEAPON(12, BOWREADYEND + 2, 112, 0), new WEAPON(12, BOWREADYEND + 3, 115, 0),
            new WEAPON(12, ZBOWWALK, 75, 13)};
    // SCOTT
    public static final WEAPON[][] zreadyanimtics = {
            // FIST
            {new WEAPON(10, RFIST, 216, 180), new WEAPON(10, RFIST, 216, 170), new WEAPON(10, RFIST, 216, 160),
                    new WEAPON(10, RFIST, 216, 150), new WEAPON(10, RFIST, 216, 140), new WEAPON(10, RFIST, 216, 130),
                    new WEAPON(10, RFIST, 216, 124), new WEAPON(1, RFIST, 216, 124), new WEAPON(1, RFIST, 216, 124),
                    new WEAPON(1, RFIST, 216, 122), new WEAPON(1, RFIST, 216, 122), new WEAPON(1, RFIST, 216, 122),
                    new WEAPON(1, 0, 147, 76)},
            // ZKNIFE
            {new WEAPON(10, ZKNIFEREADY, 69, 171), new WEAPON(10, ZKNIFEREADY + 1, 11, 146),
                    new WEAPON(10, ZKNIFEREADY + 2, 25, 146), new WEAPON(10, ZKNIFEREADY + 3, 35, 158),
                    new WEAPON(10, ZKNIFEREADY + 4, 38, 158), new WEAPON(10, ZKNIFEREADY + 5, 16, 157),
                    new WEAPON(10, ZKNIFEREADY + 6, 37, 102), new WEAPON(10, ZKNIFEREADY + 7, 239, 63),
                    new WEAPON(10, ZKNIFEREADY + 8, 214, 85), new WEAPON(10, ZKNIFEREADY + 9, 206, 110),
                    new WEAPON(10, ZKNIFEREADY + 10, 217, 108), new WEAPON(10, ZKNIFEREADY + 11, 204, 95),
                    new WEAPON(1, 0, 147, 76)},
            // ZSHORTREADY
            {new WEAPON(12, ZSHORTREADY, 79, 169), new WEAPON(12, ZSHORTREADY + 1, 95, 115),
                    new WEAPON(12, ZSHORTREADY + 2, 94, 93), new WEAPON(12, ZSHORTREADY + 3, 156, 77),
                    new WEAPON(12, ZSHORTREADY + 4, 218, 64), new WEAPON(12, ZSHORTREADY + 5, 224, 57),
                    new WEAPON(8, ZSHORTREADY + 6, 251, 54), new WEAPON(1, ZSHORTREADY + 7, 243, 92),
                    new WEAPON(1, ZSHORTREADY + 7, 243, 92), new WEAPON(1, ZSHORTREADY + 7, 243, 92),
                    new WEAPON(1, ZSHORTREADY + 7, 243, 92), new WEAPON(1, ZSHORTREADY + 7, 243, 92),
                    new WEAPON(1, 0, 147, 76)},
            // ZSTARATTACK
            {new WEAPON(6, ZSTARATTACK, 194, 195), new WEAPON(6, ZSTARATTACK, 194, 185),
                    new WEAPON(6, ZSTARATTACK, 194, 175), new WEAPON(6, ZSTARATTACK, 194, 165),
                    new WEAPON(6, ZSTARATTACK, 194, 155), new WEAPON(6, ZSTARATTACK, 194, 145),
                    new WEAPON(6, ZSTARATTACK, 194, 135), new WEAPON(6, ZSTARATTACK, 194, 125),
                    new WEAPON(6, ZSTARATTACK, 194, 115), new WEAPON(6, ZSTARATTACK, 194, 105),
                    new WEAPON(6, ZSTARATTACK, 194, 95), new WEAPON(1, ZSTARATTACK, 194, 85),
                    new WEAPON(1, 0, 147, 76)},
            // SWORD
            {new WEAPON(10, SWORDPULL, 58, 160), new WEAPON(10, SWORDPULL + 1, 81, 111),
                    new WEAPON(10, SWORDPULL + 2, 19, 88), new WEAPON(10, SWORDPULL + 3, 0, 93),
                    new WEAPON(10, SWORDPULL + 4, 104, 0), new WEAPON(10, SWORDPULL + 5, 169, 0),
                    new WEAPON(10, SWORDPULL + 6, 244, 38), new WEAPON(6, SWORDPULL + 7, 225, 121),
                    new WEAPON(1, SWORDPULL + 7, 225, 121), new WEAPON(1, SWORDPULL + 7, 225, 121),
                    new WEAPON(1, SWORDPULL + 7, 225, 121), new WEAPON(1, SWORDPULL + 7, 225, 121),
                    new WEAPON(1, 0, 147, 76)},
            // ZAXE
            {new WEAPON(8, ZAXEREADY, 0, 108), new WEAPON(8, ZAXEREADY + 1, 0, 58),
                    new WEAPON(8, ZAXEREADY + 2, 0, 57), new WEAPON(8, ZAXEREADY + 3, 0, 69),
                    new WEAPON(8, ZAXEREADY + 4, 0, 100), new WEAPON(8, ZAXEREADY + 5, 0, 9),
                    new WEAPON(8, ZAXEREADY + 6, 33, 0), new WEAPON(8, ZAXEREADY + 7, 61, 0),
                    new WEAPON(8, ZAXEREADY + 8, 73, 20), new WEAPON(8, ZAXEREADY + 9, 179, 117),
                    new WEAPON(8, 0, 182, 116), new WEAPON(1, 0, 200, 122), new WEAPON(1, 0, 147, 76)},
            // ZBOW
            {new WEAPON(12, ZBOWREADY, 0, 0), new WEAPON(12, ZBOWREADY + 1, 0, 20),
                    new WEAPON(12, ZBOWREADY + 2, 0, 46), new WEAPON(12, ZBOWREADY + 3, 0, 26),
                    new WEAPON(12, ZBOWREADY + 4, 0, 0), new WEAPON(12, ZBOWREADY + 5, 71, 0),
                    new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, ZBOWWALK, 75, 13),
                    new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, ZBOWWALK, 75, 13),
                    new WEAPON(1, 0, 147, 76)},
            // ZPIKE
            {new WEAPON(8, ZPIKEREADY, 0, 150), new WEAPON(8, ZPIKEREADY + 1, 0, 94),
                    new WEAPON(8, ZPIKEREADY + 2, 47, 45), new WEAPON(8, ZPIKEREADY + 3, 138, 62),
                    new WEAPON(8, ZPIKEREADY + 4, 194, 95), new WEAPON(8, ZPIKEREADY + 5, 59, 121),
                    new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52), new WEAPON(1, 0, 147, 76),
                    new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52), new WEAPON(1, 0, 147, 76),
                    new WEAPON(1, 0, 147, 76)},
            {new WEAPON(12, ZTWOHANDREADY, 167, 131), new WEAPON(12, ZTWOHANDREADY + 1, 71, 117),
                    new WEAPON(12, ZTWOHANDREADY, 0, 128), new WEAPON(12, ZTWOHANDREADY + 3, 0, 150),
                    new WEAPON(12, ZTWOHANDREADY + 4, 10, 74), new WEAPON(12, ZTWOHANDREADY + 5, 44, 81),
                    new WEAPON(12, ZTWOHANDREADY + 6, 0, 53), new WEAPON(12, ZTWOHANDREADY + 7, 112, 0),
                    new WEAPON(12, ZTWOHANDREADY + 8, 220, 0), new WEAPON(12, ZTWOHANDREADY + 9, 198, 84),
                    new WEAPON(12, ZTWOHANDREADY + 10, 186, 120), new WEAPON(12, ZTWOHANDREADY + 11, 188, 124),
                    new WEAPON(1, 0, 147, 76)},

            {new WEAPON(12, HALBERDDRAW, 183, 62), new WEAPON(12, HALBERDDRAW + 1, 166, 10),
                    new WEAPON(12, HALBERDDRAW + 2, 173, 29), new WEAPON(12, HALBERDDRAW + 3, 114, 35),
                    new WEAPON(1, HALBERDATTACK1, 245, 22), new WEAPON(1, HALBERDATTACK1, 245, 22),
                    new WEAPON(1, HALBERDATTACK1, 245, 22), new WEAPON(1, HALBERDATTACK1, 245, 22),
                    new WEAPON(1, HALBERDATTACK1, 245, 22), new WEAPON(1, HALBERDATTACK1, 245, 22),
                    new WEAPON(1, HALBERDATTACK1, 245, 22), new WEAPON(1, ZHALBERDATTACK, 245, 30),
                    new WEAPON(1, 0, 147, 76)}

    };
    public static final WEAPON[][] readyanimtics = {
            // FIST
            {new WEAPON(10, RFIST, 216, 180), new WEAPON(10, RFIST, 216, 170), new WEAPON(10, RFIST, 216, 160),
                    new WEAPON(10, RFIST, 216, 150), new WEAPON(10, RFIST, 216, 140), new WEAPON(10, RFIST, 216, 130),
                    new WEAPON(10, RFIST, 216, 124), new WEAPON(1, RFIST, 216, 124), new WEAPON(1, RFIST, 216, 124),
                    new WEAPON(1, RFIST, 216, 122), new WEAPON(1, RFIST, 216, 122), new WEAPON(1, RFIST, 216, 122),
                    new WEAPON(1, 0, 147, 76)},
            // KNIFE
            {new WEAPON(10, KNIFEREADY, 69, 171), new WEAPON(10, KNIFEREADY + 1, 11, 146),
                    new WEAPON(10, KNIFEREADY + 2, 25, 146), new WEAPON(10, KNIFEREADY + 3, 35, 158),
                    new WEAPON(10, KNIFEREADY + 4, 38, 158), new WEAPON(10, KNIFEREADY + 5, 16, 157),
                    new WEAPON(10, KNIFEREADY + 6, 37, 102), new WEAPON(10, KNIFEREADY + 7, 239, 63),
                    new WEAPON(10, KNIFEREADY + 8, 214, 85), new WEAPON(10, KNIFEREADY + 9, 206, 110),
                    new WEAPON(10, KNIFEREADY + 10, 217, 108), new WEAPON(10, KNIFEREADY + 11, 204, 95),
                    new WEAPON(1, 0, 147, 76)},
            // GOBSWORD
            {new WEAPON(12, GOBSWORDPULL, 79, 169), new WEAPON(12, GOBSWORDPULL + 1, 95, 115),
                    new WEAPON(12, GOBSWORDPULL + 2, 94, 93), new WEAPON(12, GOBSWORDPULL + 3, 156, 77),
                    new WEAPON(12, GOBSWORDPULL + 4, 218, 64), new WEAPON(12, GOBSWORDPULL + 5, 224, 57),
                    new WEAPON(8, GOBSWORDPULL + 6, 251, 54), new WEAPON(1, GOBSWORDPULL + 7, 243, 92),
                    new WEAPON(1, GOBSWORDPULL + 7, 243, 92), new WEAPON(1, GOBSWORDPULL + 7, 243, 92),
                    new WEAPON(1, GOBSWORDPULL + 7, 243, 92), new WEAPON(1, GOBSWORDPULL + 7, 243, 92),
                    new WEAPON(1, 0, 147, 76)},
            // MORNINGSTAR
            {new WEAPON(6, MORNINGSTAR, 193, 190), new WEAPON(6, MORNINGSTAR, 193, 180),
                    new WEAPON(6, MORNINGSTAR, 193, 170), new WEAPON(6, MORNINGSTAR, 193, 160),
                    new WEAPON(6, MORNINGSTAR, 193, 150), new WEAPON(6, MORNINGSTAR, 193, 140),
                    new WEAPON(6, MORNINGSTAR, 193, 130), new WEAPON(6, MORNINGSTAR, 193, 120),
                    new WEAPON(6, MORNINGSTAR, 193, 110), new WEAPON(6, MORNINGSTAR, 193, 100),
                    new WEAPON(6, MORNINGSTAR, 193, 90), new WEAPON(1, MORNINGSTAR, 193, 90),
                    new WEAPON(1, 0, 147, 76)},
            // SWORD
            {new WEAPON(10, SWORDPULL, 58, 160), new WEAPON(10, SWORDPULL + 1, 81, 111),
                    new WEAPON(10, SWORDPULL + 2, 19, 88), new WEAPON(10, SWORDPULL + 3, 0, 93),
                    new WEAPON(10, SWORDPULL + 4, 104, 0), new WEAPON(10, SWORDPULL + 5, 169, 0),
                    new WEAPON(10, SWORDPULL + 6, 244, 38), new WEAPON(6, SWORDPULL + 7, 225, 121),
                    new WEAPON(1, SWORDPULL + 7, 225, 121), new WEAPON(1, SWORDPULL + 7, 225, 121),
                    new WEAPON(1, SWORDPULL + 7, 225, 121), new WEAPON(1, SWORDPULL + 7, 225, 121),
                    new WEAPON(1, 0, 147, 76)},
            {new WEAPON(12, BIGAXEDRAW, 71, 108), new WEAPON(12, BIGAXEDRAW + 1, 17, 58),
                    new WEAPON(12, BIGAXEDRAW + 2, 0, 56), new WEAPON(12, BIGAXEDRAW + 3, 0, 71),
                    new WEAPON(12, BIGAXEDRAW + 4, 0, 102), new WEAPON(12, BIGAXEDRAW + 5, 0, 11),
                    new WEAPON(12, BIGAXEDRAW + 6, 33, 0), new WEAPON(12, BIGAXEDRAW + 7, 69, 0),
                    new WEAPON(12, BIGAXEDRAW + 8, 75, 20), new WEAPON(12, BIGAXEDRAW9, 150, 92),
                    new WEAPON(12, BIGAXEDRAW10, 182, 116), new WEAPON(1, 0, 200, 122), new WEAPON(1, 0, 147, 76)},
            // BOW
            {new WEAPON(12, BOWREADY, 0, 0), new WEAPON(12, BOWREADY + 1, 0, 20), new WEAPON(12, BOWREADY + 2, 0, 46),
                    new WEAPON(12, BOWREADY + 3, 0, 26), new WEAPON(12, BOWREADY + 4, 0, 0),
                    new WEAPON(12, BOWREADY + 5, 71, 0), new WEAPON(8, BOWREADYEND, 77, 23),
                    new WEAPON(1, BOWREADYEND, 77, 23), new WEAPON(1, BOWREADYEND, 77, 23),
                    new WEAPON(1, BOWREADYEND, 77, 23), new WEAPON(1, BOWREADYEND, 77, 23),
                    new WEAPON(1, BOWREADYEND, 77, 23), new WEAPON(1, 0, 147, 76)},
            {new WEAPON(8, PIKEDRAW, 0, 156), new WEAPON(8, PIKEDRAW + 1, 15, 98), new WEAPON(8, PIKEDRAW + 2, 83, 49),
                    new WEAPON(8, PIKEDRAW + 3, 144, 66), new WEAPON(8, PIKEDRAW + 4, 197, 99),
                    new WEAPON(8, PIKEDRAW + 5, 216, 131), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 147, 76)},
            {new WEAPON(12, EXCALDRAW, 167, 130), new WEAPON(12, EXCALDRAW + 1, 70, 117),
                    new WEAPON(12, EXCALDRAW + 2, 0, 128), new WEAPON(12, EXCALDRAW + 3, 0, 150),
                    new WEAPON(12, EXCALDRAW + 4, 4, 72), new WEAPON(12, EXCALDRAW + 5, 38, 81),
                    new WEAPON(12, EXCALDRAW + 6, 0, 44), new WEAPON(12, EXCALDRAW + 7, 112, 0),
                    new WEAPON(12, EXCALDRAW + 8, 224, 0), new WEAPON(12, EXCALDRAW + 9, 198, 84),
                    new WEAPON(12, EXCALDRAW + 10, 186, 120), new WEAPON(12, EXCALDRAW + 11, 188, 123),
                    new WEAPON(1, 0, 147, 76)},
            {new WEAPON(12, HALBERDDRAW, 183, 62), new WEAPON(12, HALBERDDRAW + 1, 166, 10),
                    new WEAPON(12, HALBERDDRAW + 2, 173, 29), new WEAPON(12, HALBERDDRAW + 3, 114, 35),
                    new WEAPON(1, HALBERDATTACK1, 245, 22), new WEAPON(1, HALBERDATTACK1, 245, 22),
                    new WEAPON(1, HALBERDATTACK1, 245, 22), new WEAPON(1, HALBERDATTACK1, 245, 22),
                    new WEAPON(1, HALBERDATTACK1, 245, 22), new WEAPON(1, HALBERDATTACK1, 245, 22),
                    new WEAPON(1, HALBERDATTACK1, 245, 22), new WEAPON(1, HALBERDATTACK1, 245, 22),
                    new WEAPON(1, 0, 147, 76)}};
    public static final WEAPON[][] weaponanimtics = {
            // FIST
            {new WEAPON(10, RFIST, 216, 120), new WEAPON(10, RFIST + 1, 166, 113), new WEAPON(10, RFIST + 2, 156, 129),
                    new WEAPON(10, RFIST + 3, 169, 151), new WEAPON(10, RFIST + 4, 153, 124),
                    new WEAPON(10, RFIST + 5, 224, 133), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // KNIFE
            {new WEAPON(8, KNIFEATTACK, 189, 52), new WEAPON(8, KNIFEATTACK + 1, 254, 68), new WEAPON(8, 0, 147, 76),
                    new WEAPON(8, 0, 80, 41), new WEAPON(8, KNIFEATTACK + 2, 254, 69),
                    new WEAPON(8, KNIFEATTACK + 3, 218, 80), new WEAPON(8, KNIFEATTACK + 4, 137, 83),
                    new WEAPON(8, KNIFEATTACK + 5, 136, 100), new WEAPON(8, KNIFEATTACK + 6, 126, 140),
                    new WEAPON(8, KNIFEATTACK + 5, 136, 100), new WEAPON(8, KNIFEATTACK + 4, 137, 83),
                    new WEAPON(8, KNIFEATTACK, 189, 52)},
            // GOBLINATTACK
            {new WEAPON(10, GOBSWORDATTACK, 243, 92), new WEAPON(10, GOBSWORDATTACK + 1, 255, 68),
                    new WEAPON(10, GOBSWORDATTACK + 2, 279, 65), new WEAPON(10, GOBSWORDATTACK + 3, 238, 55),
                    new WEAPON(10, GOBSWORDATTACK + 4, 153, 52), new WEAPON(10, GOBSWORDATTACK + 5, 129, 152),
                    new WEAPON(10, GOBSWORDATTACK + 6, 90, 184), new WEAPON(1, 0, 297, 169), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)},
            // MORNINGSTAR
            {new WEAPON(12, MORNINGSTAR, 193, 90), new WEAPON(12, MORNINGSTAR + 1, 102, 133),
                    new WEAPON(12, MORNINGSTAR + 2, 77, 164), new WEAPON(12, MORNINGSTAR + 3, 239, 86),
                    new WEAPON(12, 0, 299, 86), new WEAPON(12, 0, 107, 52), new WEAPON(12, MORNINGSTAR + 4, 197, 24),
                    new WEAPON(12, MORNINGSTAR + 5, 125, 124), new WEAPON(12, MORNINGSTAR + 6, 109, 191),
                    new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52), new WEAPON(1, 0, 147, 76)},
            // SWORD
            {new WEAPON(8, SWORDATTACK, 229, 123), new WEAPON(8, SWORDATTACK + 1, 221, 87),
                    new WEAPON(8, SWORDATTACK + 2, 193, 21), new WEAPON(8, SWORDATTACK + 3, 173, 0),
                    new WEAPON(8, SWORDATTACK + 4, 61, 0), new WEAPON(8, SWORDATTACK + 5, 33, 48),
                    new WEAPON(8, SWORDATTACK + 6, 126, 131), new WEAPON(8, SWORDATTACK + 7, 297, 164),
                    new WEAPON(3, 0, 147, 76), new WEAPON(3, 0, 80, 41), new WEAPON(3, 0, 107, 52),
                    new WEAPON(3, 0, 147, 76)},
            {new WEAPON(12, BIGAXEATTACK, 184, 123), new WEAPON(12, BIGAXEATTACK + 1, 223, 112),
                    new WEAPON(12, BIGAXEATTACK + 2, 63, 151), new WEAPON(12, BIGAXEATTACK + 3, 91, 133),
                    new WEAPON(12, BIGAXEATTACK + 4, 127, 138), new WEAPON(12, BIGAXEATTACK + 5, 106, 128),
                    new WEAPON(12, BIGAXEATTACK + 6, 117, 49), new WEAPON(12, BIGAXEATTACK + 7, 140, 0),
                    new WEAPON(12, BIGAXEATTACK + 8, 152, 47), new WEAPON(12, BIGAXEATTACK + 9, 166, 143),
                    new WEAPON(12, 0, 107, 52), new WEAPON(1, 0, 147, 76)},
            // BOW
            {new WEAPON(8, BOWWALK, 75, 13), new WEAPON(8, BOWWALK + 1, 90, 0), new WEAPON(8, BOWWALK + 2, 70, 0),
                    new WEAPON(8, BOWWALK + 3, 70, 0), new WEAPON(6, BOWWALK + 4, 70, 0),
                    new WEAPON(4, BOWWALK + 5, 70, 0), new WEAPON(1, 0, 126, 131), new WEAPON(1, 0, 297, 164),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            {new WEAPON(10, PIKEDRAW + 5, 216, 131), new WEAPON(10, 0, 80, 41), new WEAPON(10, 0, 107, 52),
                    new WEAPON(10, 0, 147, 76), new WEAPON(10, PIKEATTACK1, 0, 47),
                    new WEAPON(10, PIKEATTACK1 + 1, 0, 0), new WEAPON(10, PIKEATTACK1 + 2, 0, 0),
                    new WEAPON(10, PIKEATTACK1 + 3, 73, 0), new WEAPON(10, PIKEATTACK1 + 4, 130, 27),
                    new WEAPON(10, PIKEATTACK1 + 5, 138, 125), new WEAPON(12, 0, 80, 41), new WEAPON(1, 0, 107, 52)},
            {new WEAPON(8, EXCALATTACK1, 98, 133), new WEAPON(8, EXCALATTACK1 + 1, 123, 130),
                    new WEAPON(8, EXCALATTACK1 + 2, 125, 128), new WEAPON(8, EXCALATTACK1 + 3, 115, 82),
                    new WEAPON(8, EXCALATTACK1 + 4, 115, 6), new WEAPON(8, EXCALATTACK1 + 5, 178, 0),
                    new WEAPON(8, EXCALATTACK1 + 6, 155, 0), new WEAPON(8, EXCALATTACK1 + 7, 143, 0),
                    new WEAPON(8, EXCALATTACK1 + 8, 90, 91), new WEAPON(8, EXCALATTACK1 + 9, 30, 159),
                    new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52)},
            {new WEAPON(12, HALBERDATTACK1, 245, 22), new WEAPON(12, 0, 107, 52), new WEAPON(12, 0, 147, 76),
                    new WEAPON(12, HALBERDATTACK1 + 1, 249, 45), new WEAPON(12, HALBERDATTACK1 + 2, 161, 60),
                    new WEAPON(12, HALBERDATTACK1 + 3, 45, 88), new WEAPON(12, 0, 80, 41),
                    new WEAPON(12, HALBERDATTACK1 + 3, 45, 88), new WEAPON(12, HALBERDATTACK1 + 2, 161, 60),
                    new WEAPON(12, HALBERDATTACK1 + 1, 249, 45), new WEAPON(12, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)}};
    public static final WEAPON[][] zweaponanimtics = {
            // FIST
            {new WEAPON(10, RFIST, 216, 120), new WEAPON(10, RFIST + 1, 166, 113), new WEAPON(10, RFIST + 2, 156, 129),
                    new WEAPON(10, RFIST + 3, 169, 151), new WEAPON(10, RFIST + 4, 153, 124),
                    new WEAPON(10, RFIST + 5, 224, 133), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // ZKNIFE
            {new WEAPON(8, ZKNIFEATTACK, 189, 52), new WEAPON(8, ZKNIFEATTACK + 1, 254, 68),
                    new WEAPON(16, 0, 147, 76), new WEAPON(8, ZKNIFEATTACK + 1, 254, 68),
                    new WEAPON(8, ZKNIFEATTACK + 2, 218, 80), new WEAPON(8, ZKNIFEATTACK + 3, 137, 83),
                    new WEAPON(8, ZKNIFEATTACK + 4, 136, 100), new WEAPON(8, ZKNIFEATTACK + 5, 126, 140),
                    new WEAPON(8, ZKNIFEATTACK + 4, 136, 100), new WEAPON(8, ZKNIFEATTACK + 3, 137, 83),
                    new WEAPON(8, ZKNIFEATTACK + 2, 218, 80), new WEAPON(1, ZKNIFEATTACK, 189, 52)},
            // ZSHORTATTACK
            {new WEAPON(10, ZSHORTATTACK, 243, 68), new WEAPON(10, ZSHORTATTACK + 4, 255, 50),
                    new WEAPON(10, ZSHORTATTACK + 5, 279, 66), new WEAPON(10, ZSHORTATTACK + 6, 238, 52),
                    new WEAPON(10, ZSHORTATTACK + 7, 181, 49), new WEAPON(10, ZSHORTATTACK + 8, 129, 141),
                    new WEAPON(1, 0, 90, 184), new WEAPON(1, 0, 297, 169), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)},
            // ZSTARATTACK
            {new WEAPON(12, ZSTARATTACK, 193, 90), new WEAPON(12, ZSTARATTACK + 3, 102, 128),
                    new WEAPON(12, ZSTARATTACK + 4, 77, 159), new WEAPON(12, ZSTARATTACK + 5, 239, 79),
                    new WEAPON(12, 0, 299, 86), new WEAPON(12, 0, 107, 52), new WEAPON(12, ZSTARATTACK + 6, 175, 19),
                    new WEAPON(12, ZSTARATTACK + 7, 125, 124), new WEAPON(12, ZSTARATTACK + 8, 109, 187),
                    new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52), new WEAPON(1, 0, 147, 76)},
            // SWORD
            {new WEAPON(8, SWORDATTACK, 229, 123), new WEAPON(8, SWORDATTACK + 1, 221, 87),
                    new WEAPON(8, SWORDATTACK + 2, 193, 21), new WEAPON(8, SWORDATTACK + 3, 173, 0),
                    new WEAPON(8, SWORDATTACK + 4, 61, 0), new WEAPON(8, SWORDATTACK + 5, 33, 48),
                    new WEAPON(8, SWORDATTACK + 6, 126, 131), new WEAPON(8, SWORDATTACK + 7, 297, 164),
                    new WEAPON(3, 0, 147, 76), new WEAPON(3, 0, 80, 41), new WEAPON(3, 0, 107, 52),
                    new WEAPON(3, 0, 147, 76)},
            // ZAXEATTACK
            {new WEAPON(6, ZAXEATTACK, 179, 117), new WEAPON(6, ZAXEATTACK + 6, 217, 107),
                    new WEAPON(6, ZAXEATTACK + 7, 106, 146), new WEAPON(6, ZAXEATTACK + 8, 94, 128),
                    new WEAPON(6, ZAXEATTACK + 9, 123, 132), new WEAPON(6, ZAXEATTACK + 10, 102, 134),
                    new WEAPON(6, ZAXEATTACK + 11, 112, 45), new WEAPON(6, ZAXEATTACK + 12, 102, 0),
                    new WEAPON(6, ZAXEATTACK + 13, 68, 42), new WEAPON(6, ZAXEATTACK + 14, 42, 138),
                    new WEAPON(6, 0, 107, 52), new WEAPON(1, 0, 147, 76)},
            // ZBOW
            {new WEAPON(8, ZBOWWALK, 75, 13), new WEAPON(8, ZBOWATTACK, 90, 0), new WEAPON(8, ZBOWATTACK + 1, 70, 0),
                    new WEAPON(8, ZBOWATTACK + 2, 70, 0), new WEAPON(6, ZBOWATTACK + 3, 70, 0),
                    new WEAPON(4, ZBOWATTACK + 4, 70, 0), new WEAPON(1, ZBOWWALK, 75, 13),
                    new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, ZBOWWALK, 75, 13),
                    new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, 0, 147, 76)},
            // ZPIKE
            {new WEAPON(10, ZPIKEREADY + 5, 210, 127), new WEAPON(10, 0, 80, 41), new WEAPON(10, 0, 107, 52),
                    new WEAPON(10, 0, 147, 76), new WEAPON(10, ZPIKEATTACK, 0, 43),
                    new WEAPON(10, ZPIKEATTACK + 1, 0, 0), new WEAPON(10, ZPIKEATTACK + 2, 0, 0),
                    new WEAPON(10, ZPIKEATTACK + 3, 45, 0), new WEAPON(10, ZPIKEATTACK + 4, 51, 23),
                    new WEAPON(10, ZPIKEATTACK + 5, 59, 121), new WEAPON(12, 0, 80, 41), new WEAPON(1, 0, 107, 52)},
            {new WEAPON(8, ZTWOHANDATTACK, 98, 133), new WEAPON(8, ZTWOHANDATTACK + 6, 115, 130),
                    new WEAPON(8, ZTWOHANDATTACK + 7, 125, 128), new WEAPON(8, ZTWOHANDATTACK + 8, 116, 82),
                    new WEAPON(8, ZTWOHANDATTACK + 9, 117, 9), new WEAPON(8, ZTWOHANDATTACK + 10, 180, 0),
                    new WEAPON(8, ZTWOHANDATTACK + 11, 174, 0), new WEAPON(8, ZTWOHANDATTACK + 12, 166, 0),
                    new WEAPON(8, ZTWOHANDATTACK + 13, 125, 90), new WEAPON(8, ZTWOHANDATTACK + 14, 83, 166),
                    new WEAPON(1, 0, 30, 159), new WEAPON(1, 0, 80, 41)},
            {new WEAPON(12, ZHALBERDATTACK, 245, 30), new WEAPON(12, 0, 107, 52), new WEAPON(12, 0, 147, 76),
                    new WEAPON(12, HALBERDATTACK1 + 1, 249, 45), new WEAPON(12, HALBERDATTACK1 + 2, 161, 60),
                    new WEAPON(12, HALBERDATTACK1 + 3, 45, 88), new WEAPON(12, 0, 80, 41),
                    new WEAPON(12, HALBERDATTACK1 + 3, 45, 88), new WEAPON(12, HALBERDATTACK1 + 2, 161, 60),
                    new WEAPON(12, HALBERDATTACK1 + 1, 249, 45), new WEAPON(12, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)}};
    // SCOTT
    public static final WEAPON[][] zlefthandanimtics = {
            {new WEAPON(10, RFIST, 15, 121), new WEAPON(10, RFIST + 1, 17, 114), new WEAPON(10, RFIST + 2, 54, 131),
                    new WEAPON(10, RFIST + 3, 76, 152), new WEAPON(10, RFIST + 4, 31, 126),
                    new WEAPON(10, RFIST + 5, 26, 135), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // KNIFE
            {new WEAPON(8, KNIFEATTACK2, 0, 113), new WEAPON(8, KNIFEATTACK2 + 1, 44, 111),
                    new WEAPON(8, KNIFEATTACK2 + 2, 119, 137), new WEAPON(8, KNIFEATTACK2 + 3, 187, 159),
                    new WEAPON(16, 0, 136, 100), new WEAPON(8, KNIFEATTACK2 + 3, 187, 159),
                    new WEAPON(8, KNIFEATTACK2 + 2, 119, 137), new WEAPON(8, KNIFEATTACK2 + 1, 44, 111),
                    new WEAPON(8, KNIFEATTACK2, 0, 113), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // ZSHORTATTACK
            {new WEAPON(10, ZSHORTATTACK, 255, 50), new WEAPON(10, ZSHORTATTACK + 1, 279, 66),
                    new WEAPON(10, ZSHORTATTACK + 2, 238, 52), new WEAPON(10, ZSHORTATTACK + 3, 181, 49),
                    new WEAPON(10, ZSHORTATTACK + 4, 129, 141), new WEAPON(10, ZSHORTATTACK + 5, 70, 93),
                    new WEAPON(10, 0, 90, 184), new WEAPON(1, 0, 297, 169), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)},
            // ZSTARATTACK2
            {new WEAPON(12, ZSTARATTACK2, 38, 141), new WEAPON(12, ZSTARATTACK2 + 1, 0, 111),
                    new WEAPON(12, ZSTARATTACK2 + 2, 0, 91), new WEAPON(12, ZSTARATTACK2 + 3, 0, 47),
                    new WEAPON(12, 0, 0, 24), new WEAPON(1, 0, 0, 24), new WEAPON(1, 0, 0, 24), new WEAPON(1, 0, 0, 24),
                    new WEAPON(1, 0, 0, 24), new WEAPON(1, 0, 0, 24), new WEAPON(1, 0, 0, 24),
                    new WEAPON(1, 0, 0, 24)},
            // ZSHORTATTACK2
            {new WEAPON(10, ZSHORTATTACK2, 238, 99), new WEAPON(10, ZSHORTATTACK2 + 1, 202, 11),
                    new WEAPON(10, ZSHORTATTACK2 + 2, 182, 0), new WEAPON(10, ZSHORTATTACK2 + 3, 79, 13),
                    new WEAPON(10, ZSHORTATTACK2 + 4, 79, 13), new WEAPON(10, ZSHORTATTACK2 + 5, 119, 123),
                    new WEAPON(10, ZSHORTATTACK2 + 6, 295, 179), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)}};
    public static final WEAPON[][] weaponanimtics2 = {
            // FIST
            {new WEAPON(10, RFIST, 216, 120), new WEAPON(10, RFIST + 1, 166, 113), new WEAPON(10, RFIST + 2, 156, 129),
                    new WEAPON(10, RFIST + 3, 169, 151), new WEAPON(10, RFIST + 4, 153, 124),
                    new WEAPON(10, RFIST + 5, 224, 133), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // KNIFE
            {new WEAPON(8, KNIFEATTACK, 189, 52), new WEAPON(8, KNIFEATTACK + 1, 254, 68), new WEAPON(16, 0, 147, 76),
                    new WEAPON(8, KNIFEATTACK2, 206, 114), new WEAPON(8, KNIFEATTACK2 + 1, 107, 112),
                    new WEAPON(8, KNIFEATTACK2 + 2, 22, 138), new WEAPON(8, KNIFEATTACK2 + 3, 0, 161),
                    new WEAPON(16, 0, 136, 100), new WEAPON(8, KNIFEATTACK2 + 3, 0, 161),
                    new WEAPON(8, KNIFEATTACK2 + 2, 22, 138), new WEAPON(8, KNIFEATTACK2 + 1, 107, 112),
                    new WEAPON(8, KNIFEATTACK2, 206, 114)},
            // GOBLINATTACK
            {new WEAPON(10, GOBSWORDATTACK2, 236, 99), new WEAPON(10, GOBSWORDATTACK2 + 1, 202, 24),
                    new WEAPON(10, GOBSWORDATTACK2 + 2, 181, 0), new WEAPON(10, GOBSWORDATTACK2 + 3, 52, 12),
                    new WEAPON(10, GOBSWORDATTACK2 + 4, 72, 72), new WEAPON(10, GOBSWORDATTACK2 + 5, 134, 139),
                    new WEAPON(10, GOBSWORDATTACK2 + 6, 297, 169), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)},
            // MORNINGATTACK2
            {new WEAPON(12, MORNINGATTACK2, 85, 136), new WEAPON(12, MORNINGATTACK2 + 1, 34, 110),
                    new WEAPON(12, MORNINGATTACK2 + 2, 32, 91), new WEAPON(12, MORNINGATTACK2 + 3, 186, 47),
                    new WEAPON(12, MORNINGATTACK2 + 4, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)},
            // SWORD
            {new WEAPON(8, SWORDATTACK2 + 1, 195, 63), new WEAPON(8, SWORDATTACK2 + 2, 250, 54),
                    new WEAPON(8, SWORDATTACK2 + 3, 275, 37), new WEAPON(16, 0, 61, 0),
                    new WEAPON(8, SWORDATTACK2 + 4, 229, 66), new WEAPON(8, SWORDATTACK2 + 5, 185, 0),
                    new WEAPON(8, SWORDATTACK2 + 6, 158, 115), new WEAPON(8, SWORDATTACK2 + 7, 57, 163),
                    new WEAPON(1, 0, 57, 163), new WEAPON(1, 0, 57, 163), new WEAPON(1, 0, 57, 163),
                    new WEAPON(1, 0, 57, 163)},
            {new WEAPON(12, BIGAXEATTACK2, 200, 111), new WEAPON(12, BIGAXEATTACK2 + 1, 5, 136),
                    new WEAPON(12, BIGAXEATTACK2 + 2, 69, 162), new WEAPON(12, BIGAXEATTACK2 + 3, 147, 164),
                    new WEAPON(12, BIGAXEATTACK2 + 4, 76, 152), new WEAPON(12, BIGAXEATTACK2 + 5, 33, 95),
                    new WEAPON(12, BIGAXEATTACK2 + 6, 0, 91), new WEAPON(12, BIGAXEATTACK2 + 7, 0, 98),
                    new WEAPON(12, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // BOW
            {new WEAPON(8, BOWWALK, 75, 13), new WEAPON(8, BOWWALK + 1, 90, 0), new WEAPON(8, BOWWALK + 2, 70, 0),
                    new WEAPON(8, BOWWALK + 3, 70, 0), new WEAPON(6, BOWWALK + 4, 70, 0),
                    new WEAPON(4, BOWWALK + 5, 70, 0), new WEAPON(1, 0, 126, 131), new WEAPON(1, 0, 297, 164),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            {new WEAPON(10, PIKEATTACK2, 266, 147), new WEAPON(10, PIKEATTACK2 + 1, 182, 117),
                    new WEAPON(10, PIKEATTACK2 + 2, 123, 84), new WEAPON(10, PIKEATTACK2 + 3, 7, 48),
                    new WEAPON(10, PIKEATTACK2 + 4, 0, 83), new WEAPON(10, PIKEATTACK2 + 5, 0, 158),
                    new WEAPON(10, PIKEATTACK2 + 6, 25, 117), new WEAPON(10, PIKEATTACK2 + 7, 139, 93),
                    new WEAPON(10, PIKEATTACK2 + 8, 234, 75), new WEAPON(8, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            {new WEAPON(8, EXCALATTACK2, 0, 143), new WEAPON(8, EXCALATTACK2 + 1, 0, 103),
                    new WEAPON(8, EXCALATTACK2 + 2, 0, 70), new WEAPON(8, EXCALATTACK2 + 3, 48, 0),
                    new WEAPON(8, EXCALATTACK2 + 4, 67, 0), new WEAPON(8, EXCALATTACK2 + 5, 78, 21),
                    new WEAPON(8, EXCALATTACK2 + 6, 165, 107), new WEAPON(8, EXCALATTACK2 + 7, 260, 168),
                    new WEAPON(1, 0, 130, 27), new WEAPON(1, 0, 138, 125), new WEAPON(1, 0, 80, 41),
                    new WEAPON(1, 0, 107, 52)},
            {new WEAPON(12, HALBERDATTACK1, 245, 22), new WEAPON(12, HALBERDATTACK2, 114, 35),
                    new WEAPON(12, HALBERDATTACK2 + 1, 105, 87), new WEAPON(12, HALBERDATTACK2 + 2, 54, 107),
                    new WEAPON(12, HALBERDATTACK2 + 3, 48, 102), new WEAPON(1, HALBERDATTACK2 + 3, 48, 102),
                    new WEAPON(1, HALBERDATTACK2 + 3, 48, 102), new WEAPON(12, HALBERDATTACK2 + 2, 54, 107),
                    new WEAPON(12, HALBERDATTACK2 + 1, 105, 87), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)}};
    // SCOTT
    public static final WEAPON[][] zweaponanimtics2 = {
            // FIST
            {new WEAPON(10, RFIST, 216, 120), new WEAPON(10, RFIST + 1, 166, 113), new WEAPON(10, RFIST + 2, 156, 129),
                    new WEAPON(10, RFIST + 3, 169, 151), new WEAPON(10, RFIST + 4, 153, 124),
                    new WEAPON(10, RFIST + 5, 224, 133), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // ZKNIFE
            {new WEAPON(8, ZKNIFEATTACK, 189, 52), new WEAPON(8, ZKNIFEATTACK + 1, 254, 68),
                    new WEAPON(16, 0, 147, 76), new WEAPON(8, ZKNIFEATTACK2, 206, 114),
                    new WEAPON(8, ZKNIFEATTACK2 + 1, 107, 112), new WEAPON(8, ZKNIFEATTACK2 + 2, 22, 138),
                    new WEAPON(8, ZKNIFEATTACK2 + 3, 0, 161), new WEAPON(16, 0, 136, 100),
                    new WEAPON(8, KNIFEATTACK2 + 3, 0, 161), new WEAPON(8, ZKNIFEATTACK2 + 2, 22, 138),
                    new WEAPON(8, ZKNIFEATTACK2 + 1, 107, 112), new WEAPON(8, KNIFEATTACK2, 206, 114)},
            // ZSHORTATTACK2
            {new WEAPON(10, ZSHORTATTACK2, 238, 99), new WEAPON(10, ZSHORTATTACK2 + 1, 202, 11),
                    new WEAPON(10, ZSHORTATTACK2 + 2, 182, 0), new WEAPON(10, ZSHORTATTACK2 + 3, 79, 13),
                    new WEAPON(10, ZSHORTATTACK2 + 4, 40, 45), new WEAPON(10, ZSHORTATTACK2 + 5, 119, 123),
                    new WEAPON(10, ZSHORTATTACK2 + 6, 295, 179), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)},
            // ZSTARATTACK2
            {new WEAPON(12, ZSTARATTACK2, 44, 110), new WEAPON(12, ZSTARATTACK2 + 1, 26, 91),
                    new WEAPON(12, ZSTARATTACK2 + 2, 177, 38), new WEAPON(12, ZSTARATTACK2 + 3, 262, 11),
                    new WEAPON(12, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)},
            // SWORD
            {new WEAPON(8, SWORDATTACK2 + 1, 195, 63), new WEAPON(8, SWORDATTACK2 + 2, 250, 54),
                    new WEAPON(8, SWORDATTACK2 + 3, 275, 37), new WEAPON(16, 0, 61, 0),
                    new WEAPON(8, SWORDATTACK2 + 4, 229, 66), new WEAPON(8, SWORDATTACK2 + 5, 185, 0),
                    new WEAPON(8, SWORDATTACK2 + 6, 158, 115), new WEAPON(8, SWORDATTACK2 + 7, 57, 163),
                    new WEAPON(1, 0, 57, 163), new WEAPON(1, 0, 57, 163), new WEAPON(1, 0, 57, 163),
                    new WEAPON(1, 0, 57, 163)},
            // ZAXEATTACK2
            {new WEAPON(6, ZAXEATTACK2, 200, 111), new WEAPON(6, ZAXEATTACK2 + 1, 5, 136),
                    new WEAPON(6, ZAXEATTACK2 + 2, 69, 162), new WEAPON(6, ZAXEATTACK2 + 3, 147, 164),
                    new WEAPON(6, ZAXEATTACK2 + 4, 76, 152), new WEAPON(6, ZAXEATTACK2 + 5, 33, 95),
                    new WEAPON(6, ZAXEATTACK2 + 6, 0, 91), new WEAPON(6, ZAXEATTACK2 + 7, 0, 98),
                    new WEAPON(6, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            {new WEAPON(8, ZBOWWALK, 75, 13), new WEAPON(8, ZBOWATTACK, 90, 0), new WEAPON(8, ZBOWATTACK + 1, 70, 0),
                    new WEAPON(8, ZBOWATTACK + 2, 70, 0), new WEAPON(6, ZBOWATTACK + 3, 70, 0),
                    new WEAPON(4, ZBOWATTACK + 4, 70, 0), new WEAPON(1, ZBOWWALK, 75, 13),
                    new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, ZBOWWALK, 75, 13),
                    new WEAPON(1, ZBOWWALK, 75, 13), new WEAPON(1, 0, 147, 76)},

            // ZPIKEATTACK2
            {new WEAPON(10, ZPIKEATTACK2, 266, 147), new WEAPON(10, ZPIKEATTACK2 + 1, 182, 117),
                    new WEAPON(10, ZPIKEATTACK2 + 2, 123, 84), new WEAPON(10, ZPIKEATTACK2 + 3, 7, 48),
                    new WEAPON(10, ZPIKEATTACK2 + 4, 0, 83), new WEAPON(10, ZPIKEATTACK2 + 5, 0, 158),
                    new WEAPON(10, ZPIKEATTACK2 + 6, 25, 117), new WEAPON(10, ZPIKEATTACK2 + 7, 139, 93),
                    new WEAPON(10, ZPIKEATTACK2 + 8, 234, 75), new WEAPON(8, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // ZTWOHANDATTACK2
            {new WEAPON(8, ZTWOHANDATTACK2, 0, 143), new WEAPON(8, ZTWOHANDATTACK2 + 1, 0, 103),
                    new WEAPON(8, ZTWOHANDATTACK2 + 2, 0, 70), new WEAPON(8, ZTWOHANDATTACK2 + 3, 41, 0),
                    new WEAPON(8, ZTWOHANDATTACK2 + 4, 54, 0), new WEAPON(8, ZTWOHANDATTACK2 + 5, 166, 21),
                    new WEAPON(8, ZTWOHANDATTACK2 + 6, 242, 108), new WEAPON(8, 0, 260, 168), new WEAPON(1, 0, 130, 27),
                    new WEAPON(1, 0, 138, 125), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52)},
            // ZHALBERDATTACK2
            {new WEAPON(12, HALBERDATTACK1, 245, 22), new WEAPON(12, HALBERDATTACK2, 114, 35),
                    new WEAPON(12, HALBERDATTACK2 + 1, 105, 87), new WEAPON(12, HALBERDATTACK2 + 2, 54, 107),
                    new WEAPON(12, HALBERDATTACK2 + 3, 48, 102), new WEAPON(1, HALBERDATTACK2 + 3, 48, 102),
                    new WEAPON(1, HALBERDATTACK2 + 3, 48, 102), new WEAPON(12, HALBERDATTACK2 + 2, 54, 107),
                    new WEAPON(12, HALBERDATTACK2 + 1, 105, 87), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)}

    };
    public static final WEAPON[][] lefthandanimtics = {
            {new WEAPON(10, RFIST, 15, 121), new WEAPON(10, RFIST + 1, 17, 114), new WEAPON(10, RFIST + 2, 54, 131),
                    new WEAPON(10, RFIST + 3, 76, 152), new WEAPON(10, RFIST + 4, 31, 126),
                    new WEAPON(10, RFIST + 5, 26, 135), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // KNIFE
            {new WEAPON(8, KNIFEATTACK2, 0, 113), new WEAPON(8, KNIFEATTACK2 + 1, 44, 111),
                    new WEAPON(8, KNIFEATTACK2 + 2, 119, 137), new WEAPON(8, KNIFEATTACK2 + 3, 187, 159),
                    new WEAPON(16, 0, 136, 100), new WEAPON(8, KNIFEATTACK2 + 3, 187, 159),
                    new WEAPON(8, KNIFEATTACK2 + 2, 119, 137), new WEAPON(8, KNIFEATTACK2 + 1, 44, 111),
                    new WEAPON(8, KNIFEATTACK2, 0, 113), new WEAPON(1, 0, 80, 41), new WEAPON(1, 0, 107, 52),
                    new WEAPON(1, 0, 147, 76)},
            // GOBLINATTACK
            {new WEAPON(10, GOBSWORDATTACK, 243, 92), new WEAPON(10, GOBSWORDATTACK + 1, 255, 68),
                    new WEAPON(10, GOBSWORDATTACK + 2, 279, 65), new WEAPON(10, GOBSWORDATTACK + 3, 238, 55),
                    new WEAPON(10, GOBSWORDATTACK + 4, 153, 52), new WEAPON(10, GOBSWORDATTACK + 5, 129, 152),
                    new WEAPON(10, GOBSWORDATTACK + 6, 90, 184), new WEAPON(1, 0, 297, 169), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)},
            // MORNINGATTACK2
            {new WEAPON(12, MORNINGATTACK2, 38, 142), new WEAPON(12, MORNINGATTACK2 + 1, 0, 111),
                    new WEAPON(12, MORNINGATTACK2 + 2, 0, 91), new WEAPON(12, MORNINGATTACK2 + 3, 0, 47),
                    new WEAPON(12, MORNINGATTACK2 + 4, 0, 24), new WEAPON(1, 0, 0, 24), new WEAPON(1, 0, 0, 24),
                    new WEAPON(1, 0, 0, 24), new WEAPON(1, 0, 0, 24), new WEAPON(1, 0, 0, 24), new WEAPON(1, 0, 0, 24),
                    new WEAPON(1, 0, 0, 24)},
            // GOBLINATTACK2
            {new WEAPON(10, GOBSWORDATTACK2, 236, 99), new WEAPON(10, GOBSWORDATTACK2 + 1, 202, 24),
                    new WEAPON(10, GOBSWORDATTACK2 + 2, 181, 0), new WEAPON(10, GOBSWORDATTACK2 + 3, 52, 12),
                    new WEAPON(10, GOBSWORDATTACK2 + 4, 72, 72), new WEAPON(10, GOBSWORDATTACK2 + 5, 134, 139),
                    new WEAPON(10, GOBSWORDATTACK2 + 6, 297, 169), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24),
                    new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24), new WEAPON(1, 0, 275, 24)}};

    // EG 17 Oct 2017: Backport shield toggle
    public static int snakex, snakey;
    public static int enchantedsoundhandle = -1;
    private static int arrowcnt, throwpikecnt;

    public static boolean checkmedusadist(int i, int x, int y, int z, int lvl) {
        int attackdist = (game.WH2 ? 8192 : 1024) + (lvl << 9);
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return false;
        }

        return (klabs(x - spr.getX()) + klabs(y - spr.getY()) < attackdist)
                && (klabs((z >> 8) - ((spr.getZ() >> 8) - (engine.getTile(spr.getPicnum()).getHeight() >> 1))) <= 120);
    }

    public static void autoweaponchange(PLAYER plr, int dagun) {
        if (plr.getCurrweaponanim() > 0 || dagun == plr.getSelectedgun() || plr.getCurrweaponflip() > 0) {
            return;
        }

        plr.setSelectedgun(dagun);
        plr.setHasshot(0);
        plr.setCurrweaponfired(2); // drop weapon

        if (enchantedsoundhandle != -1) {
            SND_StopLoop(enchantedsoundhandle);
            enchantedsoundhandle = -1;
        }

        switch (plr.getSelectedgun()) {
            case 0:
            case 4:
            case 8:
            case 9:
                //			if (enchantedsoundhandle == -1 && plr.weapon[plr.selectedgun] == 3) {
//				enchantedsoundhandle = playsound(S_FIREWEAPONLOOP, 0, 0, -1);
//			}
                plr.setWeapondropgoal(40);
                plr.setWeapondrop(0);
                break;
            case 1:
                plr.setWeapondropgoal(100);
                plr.setWeapondrop(0);
                break;
            case 2:
                if (plr.getWeapon()[plr.getSelectedgun()] == 3) {
                    enchantedsoundhandle = playsound(S_FIREWEAPONLOOP, 0, 0, -1);
                }
                plr.setWeapondropgoal((game.WH2 ? 40 : 100));
                plr.setWeapondrop(0);
                break;
            case 3:
                if (plr.getWeapon()[plr.getSelectedgun()] == 3) {
                    enchantedsoundhandle = playsound(S_ENERGYWEAPONLOOP, 0, 0, -1);
                }
                plr.setWeapondropgoal(100);
                plr.setWeapondrop(0);
                break;
            case 5:
                if (plr.getWeapon()[plr.getSelectedgun()] == 3) {
                    enchantedsoundhandle = playsound(S_ENERGYWEAPONLOOP, 0, 0, -1);
                }
                plr.setWeapondropgoal(40);
                plr.setWeapondrop(0);
                break;
            case 6:
                if (plr.getWeapon()[plr.getSelectedgun()] == 3) {
                    enchantedsoundhandle = playsound(S_FIREWEAPONLOOP, 0, 0, -1);
                }
                plr.setWeapondropgoal(40);
                plr.setWeapondrop(0);
                if (plr.getAmmo()[6] < 0) {
                    plr.getAmmo()[6] = 0;
                }
                break;
            case 7:
                if (plr.getWeapon()[plr.getSelectedgun()] == 3) {
                    enchantedsoundhandle = playsound(S_ENERGYWEAPONLOOP, 0, 0, -1);
                }
                plr.setWeapondropgoal(40);
                plr.setWeapondrop(0);
                if (plr.getWeapon()[7] == 2) {
                    if (plr.getAmmo()[7] < 0) {
                        plr.getAmmo()[7] = 0;
                    }
                }
                break;
        }
    }

    public static void weaponchange(int snum) {
        PLAYER plr = player[snum];
        if (plr.getCurrweaponanim() == 0 && plr.getCurrweaponflip() == 0) {
            int bits = plr.getInput().bits;
            int key = ((bits & (15 << 8)) >> 8) - 1;
            if (key != -1 && key < 12) {
                if (key == 10 || key == 11) {
                    int k = plr.getCurrweapon();
                    key = (key == 10 ? -1 : 1);
                    while (k >= 0 && k < 10) {
                        k += key;

                        if (k == -1) {
                            k = 9;
                        } else if (k == 10) {
                            k = 0;
                        }

                        if (plr.getWeapon()[k] > 0) {
                            key = k;
                            break;
                        }
                    }
                }
                int gun = key;
                if (plr.getWeapon()[gun] > 0) {
                    if (gun != plr.getSelectedgun()) {
                        if (enchantedsoundhandle != -1) {
                            SND_StopLoop(enchantedsoundhandle);
                            enchantedsoundhandle = -1;
                        }
                    }

                    autoweaponchange(plr, gun);
                }
            }
        }
    }

    public static void plrfireweapon(PLAYER plr) {
        if (plr.getCurrweaponfired() == 4) {
            if (game.WH2) {
                plr.setCurrweapontics(wh2throwanimtics[plr.getCurrentorb()][0].daweapontics);
            } else {
                plr.setCurrweapontics(throwanimtics[plr.getCurrentorb()][0].daweapontics);
            }
            return;
        }

        if (plr.getAmmo()[plr.getSelectedgun()] <= 0) {
            if (plr.getCurrweapon() == 6) {
                for (int i = 0; i < MAXWEAPONS; i++) {
                    if (plr.getAmmo()[i] > 0 && plr.getWeapon()[i] == 1) {
                        plr.setSelectedgun(i);
                        plr.setHasshot(0);
                        plr.setCurrweaponfired(2); // drop weapon
                        plr.setWeapondropgoal(100);
                        plr.setWeapondrop(0);
                    }
                }
            }
            return;
        }

        boolean weaponuseless = false; // #GDX 04.06.2024
        plr.getAmmo()[plr.getSelectedgun()]--;

        if (game.WH2 && plr.getWeapon()[plr.getSelectedgun()] == 3) {
            if (plr.getAmmo()[plr.getSelectedgun()] == 0) {
                plr.getWeapon()[plr.getSelectedgun()] = plr.getPreenchantedweapon()[plr.getSelectedgun()];
                plr.getAmmo()[plr.getSelectedgun()] = plr.getPreenchantedammo()[plr.getSelectedgun()];
                if (enchantedsoundhandle != -1) {
                    SND_StopLoop(enchantedsoundhandle);
                    enchantedsoundhandle = -1;
                }
            }
        }

        if (plr.getAmmo()[plr.getSelectedgun()] <= 0 || plr.getAmmo()[plr.getSelectedgun()] == 10) {
            switch (plr.getSelectedgun()) {
                case 0: // fist
                    plr.getAmmo()[0] = 9999;
                    break;
                case 1: // knife
                    if (plr.getAmmo()[plr.getSelectedgun()] == 10) {
                        showmessage("Dagger is damaged", 360);
                    }
                    if (plr.getAmmo()[plr.getSelectedgun()] <= 0) {
                        plr.getAmmo()[1] = 0;
                        plr.getWeapon()[1] = 0;
                        showmessage("Dagger is Useless", 360);
                        weaponuseless = true;
                    }
                    break;
                case 2: // short sword
                    if (plr.getAmmo()[plr.getSelectedgun()] == 10) {
                        showmessage("Short Sword is damaged", 360);
                    }
                    if (plr.getAmmo()[plr.getSelectedgun()] <= 0) {
                        plr.getAmmo()[2] = 0;
                        plr.getWeapon()[2] = 0;
                        showmessage("Short Sword is Useless", 360);
                        weaponuseless = true;
                    }
                    break;
                case 3: // mace
                    if (plr.getAmmo()[plr.getSelectedgun()] == 10) {
                        showmessage("Morning Star is damaged", 360);
                    }
                    if (plr.getAmmo()[plr.getSelectedgun()] <= 0) {
                        plr.getAmmo()[3] = 0;
                        plr.getWeapon()[3] = 0;
                        showmessage("Morning Star is Useless", 360);
                        weaponuseless = true;
                    }
                    break;

                case 4: // sword
                    if (plr.getAmmo()[plr.getSelectedgun()] == 10) {
                        showmessage("Sword is damaged", 360);
                    }
                    if (plr.getAmmo()[plr.getSelectedgun()] <= 0) {
                        plr.getAmmo()[4] = 0;
                        plr.getWeapon()[4] = 0;
                        showmessage("Sword is Useless", 360);
                        weaponuseless = true;
                    }
                    break;
                case 5: // battle axe
                    if (plr.getAmmo()[plr.getSelectedgun()] == 10) {
                        showmessage("Battle axe is damaged", 360);
                    }
                    if (plr.getAmmo()[plr.getSelectedgun()] <= 0) {
                        plr.getAmmo()[5] = 0;
                        plr.getWeapon()[5] = 0;
                        showmessage("Battle axe is Useless", 360);
                        weaponuseless = true;
                    }
                    break;
                case 6: // bow
                    break;
                case 7: // pike
                    if (plr.getWeapon()[7] == 1) {
                        if (plr.getAmmo()[plr.getSelectedgun()] == 10) {
                            showmessage("Pike is damaged", 360);
                        }
                        if (plr.getAmmo()[plr.getSelectedgun()] <= 0) {
                            plr.getAmmo()[7] = 0;
                            plr.getWeapon()[7] = 0;
                            showmessage("Pike is Useless", 360);
                            weaponuseless = true;
                        }
                    }
                    if (plr.getWeapon()[7] == 2 && plr.getAmmo()[7] <= 0) {
                        plr.getWeapon()[7] = 1;
                        plr.getAmmo()[7] = 30;
                    }
                    break;
                case 8: // two handed sword
                    if (plr.getAmmo()[plr.getSelectedgun()] == 10) {
                        showmessage("Magic Sword is damaged", 360);
                    }
                    if (plr.getAmmo()[plr.getSelectedgun()] <= 0) {
                        plr.getAmmo()[8] = 0;
                        plr.getWeapon()[8] = 0;
                        showmessage("Magic Sword is Useless", 360);
                        weaponuseless = true;
                    }
                    break;
                case 9: // halberd
                    if (plr.getAmmo()[plr.getSelectedgun()] == 10) {
                        showmessage("Halberd is damaged", 360);
                    }
                    if (plr.getAmmo()[plr.getSelectedgun()] <= 0) {
                        plr.getAmmo()[9] = 0;
                        plr.getWeapon()[9] = 0;
                        showmessage("Halberd is Useless", 360);
                        weaponuseless = true;
                    }
                    break;
            }
        }


        if (weaponuseless) {
            for (int i = 0; i < MAXWEAPONS; i++) {
                if (plr.getWeapon()[i] > 0 && plr.getAmmo()[i] > 0) {
                    plr.setSelectedgun(i);
                    plr.setCurrweaponfired(3); // ready weapon
                    plr.setCurrweaponflip(0);
                    weaponuseless = false;
                }
            }
        } else {
            plr.setCurrweaponfired(1);
        }

        plr.setCurrweapon(plr.getSelectedgun());

        plr.setCurrweaponattackstyle(engine.krand() % 2);

        if (plr.getWeapon()[7] == 2 && plr.getCurrweapon() == 7) {
            plr.setCurrweaponattackstyle(0);
        } else if (game.WH2 && plr.getWeapon()[7] == 3 && plr.getCurrweapon() == 7) {
            plr.setCurrweaponattackstyle(0);
        }

        if (plr.getCurrweapon() == 9) {
            if (engine.krand() % 100 > 80) {
                plr.setCurrweaponattackstyle(0);
            } else {
                plr.setCurrweaponattackstyle(1);
            }
        }

        if (plr.getCurrweaponanim() > 11) {
            if (game.WH2) {
                if (plr.getWeapon()[plr.getCurrweapon()] == 1) {
                    plr.setCurrweapontics(weaponanimtics[plr.getCurrweapon()][0].daweapontics);
                } else {
                    plr.setCurrweapontics(zweaponanimtics[plr.getCurrweapon()][0].daweapontics);
                }
            } else {
                plr.setCurrweapontics(weaponanimtics[plr.getCurrweapon()][0].daweapontics);
            }
        }
    }

    public static void weaponsprocess(int snum) {
        PLAYER plr = player[snum];

        if (plr.getShadowtime() <= 0) {
            if (plr.getWeapon()[plr.getCurrweapon()] == 3) {
                if (enchantedsoundhandle == -1 && plr.getWeapon()[plr.getSelectedgun()] == 3) {
                    switch (plr.getSelectedgun()) {
                        case 2:
                        case 6:
                            enchantedsoundhandle = playsound(S_FIREWEAPONLOOP, 0, 0, -1);
                            break;
                        case 3:
                        case 5:
                        case 7:
                        case 8:
                            enchantedsoundhandle = playsound(S_ENERGYWEAPONLOOP, 0, 0, -1);
                            break;
                    }
                }
            }
        }

        if (plr.getCurrweapon() == 0 && plr.getDahand() == 0) {
            if (engine.krand() % 2 == 0) {
                plr.setDahand(1);
            } else {
                plr.setDahand(2);
            }
        }

        switch (plr.getCurrweaponfired()) {
            case 6:
                switch (plr.getCurrweapon()) {
                    case 1: // knife
                        if (plr.getCurrweaponframe() == KNIFEATTACK2 + 1) {
                            if ((plr.getCurrweaponanim() == 2 || plr.getCurrweaponanim() == 10) && plr.getCurrweapontics() == 8) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 3: // morning
                        if (plr.getCurrweaponframe() == MORNINGATTACK2 + 3) {
                            if (plr.getCurrweaponanim() == 3 && plr.getCurrweapontics() == 12) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                }

                if (plr.getCurrweaponframe() == RFIST + 5 || plr.getCurrweaponframe() == KNIFEATTACK + 6
                        || plr.getCurrweaponframe() == ZKNIFEATTACK + 5 // new
                        || plr.getCurrweaponframe() == MORNINGSTAR + 5 || plr.getCurrweaponframe() == SWORDATTACK + 7
                        || plr.getCurrweaponframe() == BOWWALK + 5 || plr.getCurrweaponframe() == ZBOWATTACK + 4
                        || plr.getCurrweaponframe() == KNIFEATTACK2 + 2 || plr.getCurrweaponframe() == ZKNIFEATTACK2 + 2
                        || plr.getCurrweaponframe() == SWORDATTACK2 + 6 || plr.getCurrweaponframe() == MORNINGATTACK2 + 3
                        || plr.getCurrweaponframe() == HALBERDATTACK1 + 3 || plr.getCurrweaponframe() == HALBERDATTACK2 + 3
                        || plr.getCurrweaponframe() == BIGAXEATTACK + 7 || plr.getCurrweaponframe() == BIGAXEATTACK2 + 6
                        || plr.getCurrweaponframe() == PIKEATTACK1 + 4 || plr.getCurrweaponframe() == PIKEATTACK2 + 4
                        || plr.getCurrweaponframe() == EXCALATTACK1 + 7 || plr.getCurrweaponframe() == EXCALATTACK2 + 5
                        || plr.getCurrweaponframe() == GOBSWORDATTACK2 + 4 || plr.getCurrweaponframe() == GOBSWORDATTACK + 4
                        || plr.getCurrweaponframe() == ZSHORTATTACK + 7 || plr.getCurrweaponframe() == ZSHORTATTACK2 + 4
                        || plr.getCurrweaponframe() == ZSTARATTACK + 7 || plr.getCurrweaponframe() == ZSTARATTACK2 + 3
                        || plr.getCurrweaponframe() == ZAXEATTACK + 12 || plr.getCurrweaponframe() == ZAXEATTACK2 + 6
                        || plr.getCurrweaponframe() == ZPIKEATTACK + 4 || plr.getCurrweaponframe() == ZPIKEATTACK2 + 4
                        || plr.getCurrweaponframe() == ZTWOHANDATTACK + 12 || plr.getCurrweaponframe() == ZTWOHANDATTACK2 + 5
                        || plr.getCurrweaponframe() == ZHALBERDATTACK + 4 || plr.getCurrweaponframe() == ZHALBERDATTACK2 + 3) {
                    swingdaweapon(plr);
                }

                plr.setCurrweapontics(plr.getCurrweapontics() - TICSPERFRAME);
                if (plr.getHelmettime() > 0) {
                    plr.setCurrweapontics(plr.getCurrweapontics() - 1);
                }

                if (plr.getCurrweapontics() < 0) {
                    plr.setCurrweaponanim(plr.getCurrweaponanim() + 1);

                    if (plr.getCurrweaponanim() > 11) {
                        plr.setCurrweaponanim(0);
                        plr.setCurrweaponfired(0);
                        plr.setCurrweaponflip(0);
                        plr.setCurrweapon(plr.getSelectedgun());
                        if (plr.getDahand() > 0) {
                            plr.setDahand(0);
                        }
                    }

                    if (game.WH2) {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            plr.setCurrweapontics(lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                            plr.setCurrweaponframe(lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        } else {
                            plr.setCurrweapontics(zlefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                            plr.setCurrweaponframe(zlefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);

                        }
                    } else {
                        plr.setCurrweapontics(lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                        plr.setCurrweaponframe(lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                    }
                } else {
                    if (game.WH2) {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            plr.setCurrweaponframe(lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        } else {
                            plr.setCurrweaponframe(zlefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        }
                    } else {
                        plr.setCurrweaponframe(lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                    }
                }

                if (plr.getCurrweapon() == 0 && plr.getCurrweaponframe() == 0) {
                    plr.setDahand(0);
                    plr.setCurrweaponanim(0);
                    plr.setCurrweaponfired(0);
                }

                if (plr.getSelectedgun() == 4 && plr.getCurrweaponframe() == 0) {
                    plr.setCurrweaponanim(0);
                    plr.setCurrweaponfired(0);
                    plr.setCurrweaponflip(0);
                    plr.setCurrweapon(plr.getSelectedgun());
                }
                break;
            case 1: // fire
                switch (plr.getCurrweapon()) {
                    case 0: // fist
                        if (plr.getCurrweaponframe() == RFIST + 5) {
                            if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 10) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 1: // knife
                        if (plr.getCurrweaponframe() == KNIFEATTACK + 6 || plr.getCurrweaponframe() == ZKNIFEATTACK + 5) {
                            if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 8) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        if (plr.getCurrweaponframe() == KNIFEATTACK2 + 2 || plr.getCurrweaponframe() == ZKNIFEATTACK2 + 2) {
                            if ((plr.getCurrweaponanim() == 5 || plr.getCurrweaponanim() == 9) && plr.getCurrweapontics() == 8) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 2: // shortsword
                        if (plr.getCurrweaponframe() == GOBSWORDATTACK + 4 || plr.getCurrweaponframe() == ZSHORTATTACK + 7) {
                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        if (plr.getCurrweaponframe() == GOBSWORDATTACK2 + 4 || plr.getCurrweaponframe() == ZSHORTATTACK2 + 4) {
                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 3: // morning
                        if (plr.getCurrweaponframe() == MORNINGSTAR + 5 || plr.getCurrweaponframe() == ZSTARATTACK + 7) {
                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        if (plr.getCurrweaponframe() == MORNINGATTACK2 + 3 || plr.getCurrweaponframe() == ZSTARATTACK2 + 3) {
                            if (plr.getCurrweaponanim() == 3 && plr.getCurrweapontics() == 12) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 4: // sword
                        if (plr.getCurrweaponframe() == SWORDATTACK + 7) {
                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        if (plr.getCurrweaponframe() == SWORDATTACK2 + 6) {
                            if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 8) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 5: // battleaxe
                        if (plr.getCurrweaponframe() == BIGAXEATTACK + 7 || plr.getCurrweaponframe() == ZAXEATTACK + 12) {
                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        if (plr.getCurrweaponframe() == BIGAXEATTACK2 + 6 || plr.getCurrweaponframe() == ZAXEATTACK2 + 6) {
                            if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 6: // bow
                        if (plr.getCurrweaponframe() == BOWWALK + 4) {
                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        if (plr.getCurrweaponframe() == ZBOWATTACK + 4) {
                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 7: // pike
                        if (plr.getCurrweaponframe() == PIKEATTACK1 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK + 4) {
                            if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 10) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        if (plr.getCurrweaponframe() == PIKEATTACK2 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK2 + 4) {
                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 8: // two handed sword
                        if (plr.getCurrweaponframe() == EXCALATTACK1 + 7 || plr.getCurrweaponframe() == ZTWOHANDATTACK + 12) {
                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        if (plr.getCurrweaponframe() == EXCALATTACK2 + 5 || plr.getCurrweaponframe() == ZTWOHANDATTACK2 + 5) {
                            if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 8) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                    case 9: // halberd
                        if (plr.getCurrweaponframe() == HALBERDATTACK1 + 3 || plr.getCurrweaponframe() == ZHALBERDATTACK + 4) {
                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        if (plr.getCurrweaponframe() == HALBERDATTACK2 + 3 || plr.getCurrweaponframe() == ZHALBERDATTACK2 + 3) {
                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 12) {
                                swingdasound(plr.getCurrweapon(), plr.getWeapon()[plr.getCurrweapon()] == 3);
                            }
                        }
                        break;
                }

                if (plr.getCurrweaponframe() == RFIST + 5 || plr.getCurrweaponframe() == KNIFEATTACK + 6
                        || plr.getCurrweaponframe() == ZKNIFEATTACK + 5 // new
                        || plr.getCurrweaponframe() == MORNINGSTAR + 5 || plr.getCurrweaponframe() == SWORDATTACK + 7
                        || plr.getCurrweaponframe() == BOWWALK + 5 || plr.getCurrweaponframe() == ZBOWATTACK + 4
                        || plr.getCurrweaponframe() == KNIFEATTACK2 + 2 || plr.getCurrweaponframe() == ZKNIFEATTACK2 + 2
                        || plr.getCurrweaponframe() == SWORDATTACK2 + 6 || plr.getCurrweaponframe() == MORNINGATTACK2 + 3
                        || plr.getCurrweaponframe() == HALBERDATTACK1 + 3 || plr.getCurrweaponframe() == HALBERDATTACK2 + 3
                        || plr.getCurrweaponframe() == BIGAXEATTACK + 7 || plr.getCurrweaponframe() == BIGAXEATTACK2 + 6
                        || plr.getCurrweaponframe() == PIKEATTACK1 + 4 || plr.getCurrweaponframe() == PIKEATTACK2 + 4
                        || plr.getCurrweaponframe() == EXCALATTACK1 + 7 || plr.getCurrweaponframe() == EXCALATTACK2 + 5
                        || plr.getCurrweaponframe() == GOBSWORDATTACK2 + 4 || plr.getCurrweaponframe() == GOBSWORDATTACK + 4
                        || plr.getCurrweaponframe() == ZSHORTATTACK + 7 || plr.getCurrweaponframe() == ZSHORTATTACK2 + 4
                        || plr.getCurrweaponframe() == ZSTARATTACK + 7 || plr.getCurrweaponframe() == ZSTARATTACK2 + 3
                        || plr.getCurrweaponframe() == ZAXEATTACK + 12 || plr.getCurrweaponframe() == ZAXEATTACK2 + 6
                        || plr.getCurrweaponframe() == ZPIKEATTACK + 4 || plr.getCurrweaponframe() == ZPIKEATTACK2 + 4
                        || plr.getCurrweaponframe() == ZTWOHANDATTACK + 12 || plr.getCurrweaponframe() == ZTWOHANDATTACK2 + 5
                        || plr.getCurrweaponframe() == ZHALBERDATTACK + 4 || plr.getCurrweaponframe() == ZHALBERDATTACK2 + 3) {
                    swingdaweapon(plr);
                }

                plr.setCurrweapontics(plr.getCurrweapontics() - TICSPERFRAME);
                if (plr.getHelmettime() > 0) {
                    plr.setCurrweapontics(plr.getCurrweapontics() - 1);
                }

                if (plr.getShieldpoints() <= 0) {
                    plr.setDroptheshield(true);
                }

                if ((plr.getCurrweaponframe() == SWORDATTACK + 7 || plr.getCurrweaponframe() == SWORDATTACK2 + 7)
                        && plr.getCurrweapontics() < 0 && plr.isDroptheshield()) {
                    if (engine.krand() % 100 > 50) {

                        if (game.WH2) {
                            if (plr.getAmmo()[1] > 0 && plr.getWeapon()[3] == 0) {
                                plr.setCurrweapon(1);
                                plr.setCurrweapontics(6);
                                plr.setCurrweaponanim(0);
                                plr.setCurrweaponfired(6);
                                plr.setHasshot(0);
                                plr.setCurrweaponflip(1);
                            }
                            if (plr.getAmmo()[3] > 0) {
                                plr.setCurrweapon(3);
                                plr.setCurrweapontics(6);
                                plr.setCurrweaponanim(0);
                                plr.setCurrweaponfired(6);
                                plr.setHasshot(0);
                                plr.setCurrweaponflip(1);
                            }
                        } else {
                            if (plr.getLvl() == 4 && plr.getAmmo()[1] > 0) {
                                plr.setCurrweapon(1);
                                plr.setCurrweapontics(6);
                                plr.setCurrweaponanim(0);
                                plr.setCurrweaponfired(6);
                                plr.setHasshot(0);
                                plr.setCurrweaponflip(1);
                            } else if (plr.getLvl() >= 5 && plr.getAmmo()[3] > 0) {
                                plr.setCurrweapon(3);
                                plr.setCurrweapontics(6);
                                plr.setCurrweaponanim(0);
                                plr.setCurrweaponfired(6);
                                plr.setHasshot(0);
                                plr.setCurrweaponflip(1);
                            }
                        }
                    }
                }
                if (plr.getCurrweapontics() < 0) {
                    plr.setCurrweaponanim(plr.getCurrweaponanim() + 1);
                    if (plr.getCurrweaponanim() > 11) {
                        plr.setCurrweaponanim(0);
                        plr.setCurrweaponfired(0);
                        if (plr.getDahand() > 0) {
                            plr.setDahand(0);
                        }
                    }
                    if (plr.getCurrweaponattackstyle() == 0) {
                        if (game.WH2) {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                plr.setCurrweapontics(weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                                plr.setCurrweaponframe(weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                            } else {
                                plr.setCurrweapontics(zweaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                                plr.setCurrweaponframe(zweaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                            }
                        } else {
                            plr.setCurrweapontics(weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                            plr.setCurrweaponframe(weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        }
                    } else {
                        if (game.WH2) {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                plr.setCurrweapontics(weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                                plr.setCurrweaponframe(weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                            } else {
                                plr.setCurrweapontics(zweaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                                plr.setCurrweaponframe(zweaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                            }
                        } else {
                            plr.setCurrweapontics(weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                            plr.setCurrweaponframe(weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        }
                    }
                } else {
                    if (game.WH2) {
                        if (plr.getCurrweaponattackstyle() == 0) {
                            // flip
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                plr.setCurrweaponframe(weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                            } else {
                                plr.setCurrweaponframe(zweaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                            }
                        } else {
                            // flip
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                plr.setCurrweaponframe(weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                            } else {
                                plr.setCurrweaponframe(zweaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                            }
                        }
                    } else {
                        if (plr.getCurrweaponattackstyle() == 0) {
                            // flip
                            plr.setCurrweaponframe(weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        } else {
                            // flip
                            plr.setCurrweaponframe(weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        }
                    }
                }

                if (plr.getCurrweapon() == 0 && plr.getCurrweaponframe() == 0) {
                    plr.setDahand(0);
                    plr.setCurrweaponanim(0);
                    plr.setCurrweaponfired(0);
                }
                break;

            case 0: // walking
                if (game.WH2) {
                    if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                        plr.setCurrweapontics(weaponanimtics[plr.getCurrweapon()][0].daweapontics);
                    } else {
                        plr.setCurrweapontics(zweaponanimtics[plr.getCurrweapon()][0].daweapontics);
                    }

                    if (plr.getCurrweapon() == 6 && plr.getAmmo()[6] <= 0) {
                        // wango
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1) {
                            plr.setCurrweaponframe(BOWREADYEND);
                        } else {
                            plr.setCurrweaponframe(ZBOWWALK);
                        }
                    } else {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            plr.setCurrweaponframe(weaponanimtics[plr.getCurrweapon()][0].daweaponframe);
                        } else {
                            plr.setCurrweaponframe(zweaponanimtics[plr.getCurrweapon()][0].daweaponframe);
                        }
                    }
                } else {
                    plr.setCurrweapontics(weaponanimtics[plr.getCurrweapon()][0].daweapontics);

                    if (plr.getCurrweapon() == 6 && plr.getAmmo()[6] <= 0) {
                        plr.setCurrweaponframe(BOWREADYEND);
                    } else {
                        plr.setCurrweaponframe(weaponanimtics[plr.getCurrweapon()][0].daweaponframe);
                    }
                }
                if ((plr.getInput().fvel | plr.getInput().svel) != 0) {
                    snakex = (EngineUtils.sin((lockclock << 4) & 2047) >> 12);
                    snakey = (EngineUtils.sin((engine.getTotalClock() << 4) & 2047) >> 12);
                }
                break;
            case 2: // unready
                if (plr.getCurrweapon() == 1) {
                    plr.setWeapondrop(plr.getWeapondrop() + (TICSPERFRAME << 1));
                } else {
                    plr.setWeapondrop(plr.getWeapondrop() + TICSPERFRAME);
                }
                if (plr.getWeapondrop() > plr.getWeapondropgoal()) {
                    plr.setCurrweaponfired(3);
//				weaponraise = 40;
                    plr.setCurrweapon(plr.getSelectedgun());
                }

                if (game.WH2) {
                    if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                        plr.setCurrweapontics(weaponanimtics[plr.getCurrweapon()][0].daweapontics);
                    } else {
                        plr.setCurrweapontics(zweaponanimtics[plr.getCurrweapon()][0].daweapontics);
                    }

                    if (plr.getCurrweapon() == 6 && plr.getAmmo()[6] <= 0) {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1) {
                            plr.setCurrweaponframe(BOWREADYEND);
                        } else
                        // currweaponframe=ZBOWREADYEND;
                        {
                            plr.setCurrweaponframe(ZBOWWALK);
                        }
                    } else {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            plr.setCurrweaponframe(weaponanimtics[plr.getCurrweapon()][0].daweaponframe);
                        } else {
                            plr.setCurrweaponframe(zweaponanimtics[plr.getCurrweapon()][0].daweaponframe);
                        }
                    }
                } else {
                    plr.setCurrweapontics(weaponanimtics[plr.getCurrweapon()][0].daweapontics);

                    if (plr.getCurrweapon() == 6 && plr.getAmmo()[6] <= 0) {
                        plr.setCurrweaponframe(BOWREADYEND);
                    } else {
                        plr.setCurrweaponframe(weaponanimtics[plr.getCurrweapon()][0].daweaponframe);
                    }
                }
                break;
            case 3: // ready
                plr.setCurrweapontics(plr.getCurrweapontics() - TICSPERFRAME);
                if (plr.getCurrweapontics() < 0) {
                    plr.setCurrweaponanim(plr.getCurrweaponanim() + 1);
                    if (plr.getCurrweaponanim() == 12) {
                        plr.setCurrweaponanim(0);
                        plr.setCurrweaponfired(0);

                        if (game.WH2) {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                plr.setCurrweaponframe(readyanimtics[plr.getCurrweapon()][11].daweaponframe);
                            } else {
                                plr.setCurrweaponframe(zreadyanimtics[plr.getCurrweapon()][11].daweaponframe);
                            }
                        } else {
                            plr.setCurrweaponframe(readyanimtics[plr.getCurrweapon()][11].daweaponframe);
                        }
                        break;
                    }
                    if (game.WH2) {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            plr.setCurrweapontics(readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                            plr.setCurrweaponframe(readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        } else {
                            plr.setCurrweapontics(zreadyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                            plr.setCurrweaponframe(zreadyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        }
                    } else {
                        plr.setCurrweapontics(readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweapontics);
                        plr.setCurrweaponframe(readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                    }
                } else {
                    if (game.WH2) {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            plr.setCurrweaponframe(readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);

                        } else {
                            plr.setCurrweaponframe(zreadyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                        }
                    } else {
                        plr.setCurrweaponframe(readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].daweaponframe);
                    }
                }
                break;
            case 5: // cock
                plr.setCurrweapontics(plr.getCurrweapontics() - (TICSPERFRAME));
                if (plr.getCurrweapontics() < 0) {
                    plr.setCurrweaponanim(plr.getCurrweaponanim() + 1);
                    if (plr.getCurrweaponanim() == 4) {
                        plr.setCurrweaponanim(0);
                        plr.setCurrweaponfired(0);
                        if (game.WH2) {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                plr.setCurrweaponframe(cockanimtics[3].daweaponframe);
                            } else {
                                if (plr.getWeapon()[plr.getCurrweapon()] == 3) {
                                    plr.setCurrweaponframe(zcockanimtics[4].daweaponframe);
                                } else {
                                    plr.setCurrweaponframe(cockanimtics[4].daweaponframe);

                                }
                            }

                        } else {
                            plr.setCurrweaponframe(cockanimtics[3].daweaponframe);
                        }
                        break;
                    }

                    if (game.WH2) {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            plr.setCurrweapontics(cockanimtics[plr.getCurrweaponanim()].daweapontics);
                            plr.setCurrweaponframe(cockanimtics[plr.getCurrweaponanim()].daweaponframe);
                        } else {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 3) {
                                plr.setCurrweapontics(zcockanimtics[plr.getCurrweaponanim()].daweapontics);
                                plr.setCurrweaponframe(zcockanimtics[plr.getCurrweaponanim()].daweaponframe);

                            } else {
                                plr.setCurrweapontics(cockanimtics[plr.getCurrweaponanim()].daweapontics);
                                plr.setCurrweaponframe(cockanimtics[plr.getCurrweaponanim()].daweaponframe);
                            }
                        }
                    } else {
                        plr.setCurrweapontics(cockanimtics[plr.getCurrweaponanim()].daweapontics);
                        plr.setCurrweaponframe(cockanimtics[plr.getCurrweaponanim()].daweaponframe);
                    }
                } else {
                    if (game.WH2) {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            plr.setCurrweaponframe(cockanimtics[plr.getCurrweaponanim()].daweaponframe);
                        } else {
                            plr.setCurrweaponframe(zcockanimtics[plr.getCurrweaponanim()].daweaponframe);
                        }
                    } else {
                        plr.setCurrweaponframe(cockanimtics[plr.getCurrweaponanim()].daweaponframe);
                    }
                }
                break;
            case 4: // throw the orb

                if (plr.getCurrweaponframe() == 0) {
                    castaorb(plr);
                }

                plr.setCurrweapontics(plr.getCurrweapontics() - (TICSPERFRAME));
                if (plr.getCurrweapontics() < 0) {
                    plr.setCurrweaponanim(plr.getCurrweaponanim() + 1);
                    if (plr.getCurrweaponanim() > 12) {
                        plr.setCurrweaponanim(0);
                        plr.setCurrweaponfired(0);
                        plr.setOrbshot(0);

                        if (game.WH2) {
                            plr.setCurrweaponframe(wh2throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].daweaponframe);
                        } else {
                            plr.setCurrweaponframe(throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].daweaponframe);
                        }
                        break;
                    }

                    if (game.WH2) {
                        plr.setCurrweapontics(wh2throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].daweapontics);
                        plr.setCurrweaponframe(wh2throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].daweaponframe);
                    } else {
                        plr.setCurrweapontics(throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].daweapontics);
                        plr.setCurrweaponframe(throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].daweaponframe);
                    }

                } else {
                    if (game.WH2) {
                        plr.setCurrweaponframe(wh2throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].daweaponframe);
                    } else {
                        plr.setCurrweaponframe(throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].daweaponframe);
                    }
                }
                break;
        }

        if (plr.getCurrweaponfired() != 4 && plr.getOrbammo()[plr.getCurrentorb()] <= 0) {
            spellswitch(plr, 1);
        }

        if (plr.getShieldpoints() > 0 && (plr.getCurrweaponfired() == 0 || plr.getCurrweaponfired() == 1) && plr.getSelectedgun() > 0
                && plr.getSelectedgun() < 5 && !plr.isDroptheshield()) {
            if (plr.getCurrweaponfired() == 1) {
                snakex = (EngineUtils.sin((lockclock << 4) & 2047) >> 12);
                snakey = (EngineUtils.sin((engine.getTotalClock() << 4) & 2047) >> 12);
                if (plr.isDroptheshield()) {
                    plr.setDropshieldcnt(plr.getDropshieldcnt() + (TICSPERFRAME << 1));
                    snakey += plr.getDropshieldcnt();
                }
            }

            if (plr.getDropshieldcnt() > 200) {
                plr.setDropshieldcnt(0);
                plr.setDroptheshield(true);
            }
        }
    }

    public static void madenoise(int val, int x, int y, int ignoreZ) {
        ListNode<Sprite> next;
        for (ListNode<Sprite> node = boardService.getStatNode(FACE); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            if ((klabs(x - spr.getX()) + klabs(y - spr.getY()) < (val * 4096))) {
                newstatus(node.getIndex(), FINDME);
            }
        }
    }

    public static void shootgun(PLAYER plr, float ang, int guntype) {
        int daang = (int) ang;
        if (plr.getHasshot() == 1) {
            return;
        }

        switch (guntype) {
            case 0: {
                boolean madeahit = false; // #GDX 04.06.2024 was global
                int daz2 = (int) (100 - plr.getHoriz()) * 2000;

                engine.hitscan(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), // Start position
                        EngineUtils.sin((daang + 2560) & 2047), // X vector of 3D ang
                        EngineUtils.sin((daang + 2048) & 2047), // Y vector of 3D ang
                        daz2, // Z vector of 3D ang
                        pHitInfo, CLIPMASK1);

                final int hitSpriteIndex = pHitInfo.hitsprite;
                if (hitSpriteIndex != -1) {
                    madeahit = true;
                }

                if (pHitInfo.hitwall != -1) {
                    if ((klabs(plr.getX() - pHitInfo.hitx) + klabs(plr.getY() - pHitInfo.hity) < 512)
                            && (klabs((plr.getZ() >> 8) - ((pHitInfo.hitz >> 8) - (64))) <= (512 >> 3))) {
                        madeahit = true;
                        switch (plr.getCurrweapon()) {
                            case 0: // fist
                                if (plr.getCurrweaponframe() == RFIST + 5) {
                                    if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 10) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                break;
                            case 1: // knife
                                if (plr.getCurrweaponframe() == KNIFEATTACK + 6) {
                                    if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 8) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                if (plr.getCurrweaponframe() == KNIFEATTACK2 + 2) {
                                    if (plr.getCurrweaponanim() == 5 || plr.getCurrweaponanim() == 9 && plr.getCurrweapontics() == 8) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                break;
                            case 2: // short sword
                                if (plr.getCurrweaponframe() == GOBSWORDATTACK + 4 || plr.getCurrweaponframe() == ZSHORTATTACK + 7) {
                                    if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                if (plr.getCurrweaponframe() == GOBSWORDATTACK + 4 || plr.getCurrweaponframe() == ZSHORTATTACK + 4) {
                                    if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                break;
                            case 3: // morning
                                if (plr.getCurrweaponframe() == MORNINGSTAR + 5 || plr.getCurrweaponframe() == ZSTARATTACK + 7) {
                                    if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                if (plr.getCurrweaponframe() == MORNINGATTACK2 + 3 || plr.getCurrweaponframe() == ZSTARATTACK + 3) {
                                    if (plr.getCurrweaponanim() == 3 && plr.getCurrweapontics() == 12) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                break;
                            case 4: // sword
                                if (plr.getCurrweaponframe() == SWORDATTACK + 7) {
                                    if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                        madenoise(2, plr.getX(), plr.getY(), plr.getZ());
                                    }
                                }
                                if (plr.getCurrweaponframe() == SWORDATTACK2 + 6) {
                                    if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 8) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                        madenoise(2, plr.getX(), plr.getY(), plr.getZ());
                                    }
                                }
                                break;
                            case 5: // battleaxe
                                if (plr.getCurrweaponframe() == BIGAXEATTACK + 7 || plr.getCurrweaponframe() == ZAXEATTACK + 12) {
                                    if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                if (plr.getCurrweaponframe() == BIGAXEATTACK2 + 6 || plr.getCurrweaponframe() == ZAXEATTACK2 + 6) {
                                    if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                break;
                            case 6: // bow
                                if (plr.getCurrweaponframe() == BOWWALK + 4) {
                                    if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                if (plr.getCurrweaponframe() == ZBOWATTACK + 4) {
                                    if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                break;
                            case 7: // pike
                                if (plr.getCurrweaponframe() == PIKEATTACK1 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK + 4) {
                                    if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 10) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                if (plr.getCurrweaponframe() == PIKEATTACK2 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK2 + 4) {
                                    if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                break;
                            case 8: // two handed sword
                                if (plr.getCurrweaponframe() == EXCALATTACK1 + 7 || plr.getCurrweaponframe() == ZTWOHANDATTACK + 12) {
                                    if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                if (plr.getCurrweaponframe() == EXCALATTACK2 + 5 || plr.getCurrweaponframe() == ZTWOHANDATTACK2 + 5) {
                                    if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 8) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                break;
                            case 9: // halberd
                                if (plr.getCurrweaponframe() == HALBERDATTACK1 + 3 || plr.getCurrweaponframe() == ZHALBERDATTACK + 4) {
                                    if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                if (plr.getCurrweaponframe() == HALBERDATTACK2 + 3 || plr.getCurrweaponframe() == ZHALBERDATTACK2 + 3) {
                                    if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 12) {
                                        swingdapunch(plr, plr.getCurrweapon());
                                    }
                                }
                                break;
                        }
                    }
                }

                if (checkweapondist(hitSpriteIndex, plr.getX(), plr.getY(), plr.getZ(), plr.getSelectedgun())) {
                    Sprite hitSprite = boardService.getSprite(hitSpriteIndex);
                    if (hitSprite == null) {
                        break;
                    }

                    madeahit = true;
                    switch (hitSprite.getDetail()) {
                        case DEMONTYPE:
                        case GONZOTYPE:
                        case KATIETYPE:
                        case KURTTYPE:
                        case NEWGUYTYPE:
                        case GRONTYPE:
                        case KOBOLDTYPE:
                        case DRAGONTYPE:
                        case DEVILTYPE:
                        case FREDTYPE:
                        case SKELETONTYPE:
                        case GOBLINTYPE:
                        case IMPTYPE:
                        case MINOTAURTYPE:
                        case SPIDERTYPE:
                        case SKULLYTYPE:
                        case FATWITCHTYPE:
                        case FISHTYPE:
                        case RATTYPE:
                        case WILLOWTYPE:
                        case GUARDIANTYPE:
                        case JUDYTYPE:
//                            if (netgame) {
//                                //netshootgun(hitSpriteIndex,currweapon);
//                            }

                            if (hitSprite.getStatnum() == DIE || hitSprite.getStatnum() == DEAD) //already dying
                            {
                                break;
                            }

                            if (game.WH2 && plr.getCurrweapon() == 3) {
                                if (plr.getWeapon()[plr.getCurrweapon()] == 3) {
                                    explosion(hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz, 4096);
                                }
                            }

                            if (plr.getInvisibletime() > 0) {
                                if (game.WH2) {
                                    if ((engine.krand() & 32) > 15) {
                                        plr.setInvisibletime(-1);
                                    }
                                } else {
                                    if ((engine.krand() & 1) != 0) {
                                        plr.setInvisibletime(-1);
                                    }
                                }
                            }

                            int k = 0;
                            switch (plr.getSelectedgun()) {
                                case 0: // fist
                                    if (game.WH2) {
                                        k = (engine.krand() % 5) + 1;
                                        break;
                                    }
                                    k = (engine.krand() & 5) + 1;
                                    break;
                                case 1: // dagger
                                    if (game.WH2) {
                                        k = (engine.krand() % 5) + 5;
                                        break;
                                    }

                                    if (plr.getCurrweaponattackstyle() == 0) {
                                        k = (engine.krand() & 5) + 10;
                                    } else {
                                        k = (engine.krand() & 3) + 5;
                                    }

                                    break;
                                case 2: // short sword
                                    if (game.WH2) {
                                        k = (engine.krand() % 10) + 5;
                                        break;
                                    }

                                    if (plr.getCurrweaponattackstyle() == 0) {
                                        k = (engine.krand() & 10) + 10;
                                    } else {
                                        k = (engine.krand() & 6) + 10;
                                    }
                                    break;
                                case 3: // morning star
                                    if (game.WH2) {
                                        k = (engine.krand() % 15) + 5;
                                        break;
                                    }

                                    if (plr.getCurrweaponattackstyle() == 0) {
                                        k = (engine.krand() & 8) + 10;
                                    } else {
                                        k = (engine.krand() & 8) + 15;
                                    }
                                    break;
                                case 4: // broad sword
                                    if (game.WH2) {
                                        k = (engine.krand() % 20) + 5;
                                        break;
                                    }

                                    if (plr.getCurrweaponattackstyle() == 0) {
                                        k = (engine.krand() & 5) + 20;
                                    } else {
                                        k = (engine.krand() & 5) + 15;
                                    }
                                    break;
                                case 5: // battle axe
                                    if (game.WH2) {
                                        k = (engine.krand() % 25) + 5;
                                        switch (hitSprite.getDetail()) {
                                            case GRONTYPE:
                                            case NEWGUYTYPE:
                                            case KURTTYPE:
                                            case GONZOTYPE:
                                                k += k >> 1;
                                                break;
                                        }
                                        break;
                                    }

                                    if (plr.getCurrweaponattackstyle() == 0) {
                                        k = (engine.krand() & 5) + 25;
                                    } else {
                                        k = (engine.krand() & 5) + 20;
                                    }
                                    break;
                                case 6: // bow
                                    if (game.WH2) {
                                        k = (engine.krand() % 30) + 5;
                                        break;
                                    }

                                    k = (engine.krand() & 15) + 5;
                                    break;
                                case 7: // pike axe
                                    if (game.WH2) {
                                        k = (engine.krand() % 35) + 5;
                                        break;
                                    }
                                    if (plr.getCurrweaponattackstyle() == 0) {
                                        k = (engine.krand() & 15) + 10;
                                    } else {
                                        k = (engine.krand() & 15) + 5;
                                    }
                                    break;
                                case 8: // two handed sword
                                    if (game.WH2) {
                                        k = (engine.krand() % 40) + 5;
                                        break;
                                    }
                                    if (plr.getCurrweaponattackstyle() == 0) {
                                        k = (engine.krand() & 15) + 45;
                                    } else {
                                        k = (engine.krand() & 15) + 40;
                                    }
                                    break;
                                case 9: // halberd
                                    if (game.WH2) {
                                        k = (engine.krand() % 45) + 5;
                                        break;
                                    }

                                    if (plr.getCurrweaponattackstyle() == 0) {
                                        k = (engine.krand() & 15) + 25;
                                    } else {
                                        k = (engine.krand() & 15) + 15;
                                    }
                                    break;

                            }

                            k += plr.getLvl();
                            if (game.WH2 && plr.getWeapon()[plr.getCurrweapon()] == 3) {
                                k <<= 1;
                            }

                            if (plr.getVampiretime() > 0) {
                                if (plr.getHealth() <= plr.getMaxhealth()) {
                                    addhealth(plr, (engine.krand() % 10) + 1);
                                }
                            }
                            if (plr.getHelmettime() > 0) {
                                k <<= 1;
                            }
                            if (plr.getStrongtime() > 0) {
                                k += k >> 1;

                                switch (plr.getCurrweapon()) {
                                    case 0: // fist
                                        if (plr.getCurrweaponframe() == RFIST + 5) {
                                            if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 1: // knife
                                        if (plr.getCurrweaponframe() == KNIFEATTACK + 6) {
                                            if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == KNIFEATTACK2 + 2) {
                                            if (plr.getCurrweaponanim() == 5 || plr.getCurrweaponanim() == 9 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 2: // short sword
                                        if (plr.getCurrweaponframe() == GOBSWORDATTACK + 4 || plr.getCurrweaponframe() == ZSHORTATTACK + 7) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == GOBSWORDATTACK2 + 4 || plr.getCurrweaponframe() == ZSHORTATTACK2 + 4) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 3: // morning
                                        if (plr.getCurrweaponframe() == MORNINGSTAR + 5 || plr.getCurrweaponframe() == ZSTARATTACK + 7) {
                                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == MORNINGATTACK2 + 3 || plr.getCurrweaponframe() == ZSTARATTACK2 + 3) {
                                            if (plr.getCurrweaponanim() == 3 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 4: // sword
                                        if (plr.getCurrweaponframe() == SWORDATTACK + 7) {
                                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == SWORDATTACK2 + 6) {
                                            if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 5: // battleaxe
                                        if (plr.getCurrweaponframe() == BIGAXEATTACK + 7 || plr.getCurrweaponframe() == ZAXEATTACK + 12) {
                                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == BIGAXEATTACK2 + 6 || plr.getCurrweaponframe() == ZAXEATTACK2 + 6) {
                                            if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 6: // bow
                                        if (plr.getCurrweaponframe() == BOWWALK + 4) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == ZBOWATTACK + 4) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 7: // pike
                                        if (plr.getCurrweaponframe() == PIKEATTACK1 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK + 4) {
                                            if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == PIKEATTACK2 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK2 + 4) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 8: // two handed sword
                                        if (plr.getCurrweaponframe() == EXCALATTACK1 + 7 || plr.getCurrweaponframe() == ZTWOHANDATTACK + 12) {
                                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == EXCALATTACK2 + 5 || plr.getCurrweaponframe() == ZTWOHANDATTACK2 + 5) {
                                            if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 9: // halberd
                                        if (plr.getCurrweaponframe() == HALBERDATTACK1 + 3 || plr.getCurrweaponframe() == ZHALBERDATTACK + 4) {
                                            if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == HALBERDATTACK2 + 3 || plr.getCurrweaponframe() == ZHALBERDATTACK2 + 3) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                }
                                hitSprite.setHitag(hitSprite.getHitag() - (k << 1));
                                if (game.WH2 && plr.getWeapon()[plr.getCurrweapon()] == 3 && plr.getCurrweapon() == 8
                                        && hitSprite.getPal() != 6) {
                                    if (hitSprite.getHitag() <= 0) {
                                        hitSprite.setHitag(1);
                                    }
                                    if (engine.krand() % 100 > 50) {
                                        medusa(plr, hitSpriteIndex);
                                    }
                                    break;
                                } else if (plr.getCurrweapon() != 0) {

                                    // JSA GORE1 you have strong time
                                    if (engine.krand() % 100 > 50) {
                                        if (hitSprite.getPicnum() == SKELETON
                                                || hitSprite.getPicnum() == SKELETONATTACK
                                                || hitSprite.getPicnum() == SKELETONDIE) {
                                            playsound_loc(S_SKELHIT1 + (engine.krand() % 2), hitSprite.getX(),
                                                    hitSprite.getY());
                                        }
                                    }

                                    // HERE
                                    switch (plr.getCurrweapon()) {
                                        case 0: // fist
                                            break;
                                        case 1: // knife
                                            if (plr.getCurrweaponframe() == KNIFEATTACK + 6) {
                                                if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == KNIFEATTACK2 + 2) {
                                                if (plr.getCurrweaponanim() == 5 || plr.getCurrweaponanim() == 9 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 2: // short sword
                                            if (plr.getCurrweaponframe() == GOBSWORDATTACK + 4
                                                    || plr.getCurrweaponframe() == ZSHORTATTACK + 7) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == GOBSWORDATTACK2 + 4
                                                    || plr.getCurrweaponframe() == ZSHORTATTACK + 4) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 3: // morning
                                            if (plr.getCurrweaponframe() == MORNINGSTAR + 5 || plr.getCurrweaponframe() == ZSTARATTACK + 7) {
                                                if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == MORNINGATTACK2 + 3
                                                    || plr.getCurrweaponframe() == ZSTARATTACK2 + 3) {
                                                if (plr.getCurrweaponanim() == 3 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 4: // sword
                                            if (plr.getCurrweaponframe() == SWORDATTACK + 7) {
                                                if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == SWORDATTACK2 + 6) {
                                                if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 8) {
                                                    break;
                                                }
                                            }
                                            chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz,
                                                    pHitInfo.hitsect, daang);
                                        case 5: // battleaxe
                                            if (plr.getCurrweaponframe() == BIGAXEATTACK + 7 || plr.getCurrweaponframe() == ZAXEATTACK + 12) {
                                                if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == BIGAXEATTACK2 + 6 || plr.getCurrweaponframe() == ZAXEATTACK2 + 6) {
                                                if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                                    break;
                                                }
                                            }
                                            chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz,
                                                    pHitInfo.hitsect, daang);
                                        case 6: // bow
                                            if (plr.getCurrweaponframe() == BOWWALK + 4) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == ZBOWATTACK + 4) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 7: // pike
                                            if (plr.getCurrweaponframe() == PIKEATTACK1 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK + 4) {
                                                if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 10) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == PIKEATTACK2 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK2 + 4) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                    break;
                                                }
                                            }
                                            chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz,
                                                    pHitInfo.hitsect, daang);
                                        case 8: // two handed sword
                                            if (plr.getCurrweaponframe() == EXCALATTACK1 + 7
                                                    || plr.getCurrweaponframe() == ZTWOHANDATTACK + 12) {
                                                if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == EXCALATTACK2 + 5
                                                    || plr.getCurrweaponframe() == ZTWOHANDATTACK2 + 5) {
                                                if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 8) {
                                                    break;
                                                }
                                            }
                                            chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz,
                                                    pHitInfo.hitsect, daang);
                                        case 9: // halberd
                                            if (plr.getCurrweaponframe() == HALBERDATTACK1 + 3
                                                    || plr.getCurrweaponframe() == ZHALBERDATTACK + 4) {
                                                if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == HALBERDATTACK2 + 3
                                                    || plr.getCurrweaponframe() == ZHALBERDATTACK2 + 3) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                    }
                                }

                            } else {
                                switch (plr.getCurrweapon()) {
                                    case 0: // fist
                                        if (plr.getCurrweaponframe() == RFIST + 5) {
                                            if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 1: // knife
                                        if (plr.getCurrweaponframe() == KNIFEATTACK + 6) {
                                            if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == KNIFEATTACK2 + 2) {
                                            if (plr.getCurrweaponanim() == 5 || plr.getCurrweaponanim() == 9 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 2: // SHORT SWORD
                                        if (plr.getCurrweaponframe() == GOBSWORDATTACK + 4 || plr.getCurrweaponframe() == ZSHORTATTACK + 7) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == GOBSWORDATTACK2 + 4 || plr.getCurrweaponframe() == ZSHORTATTACK2 + 4) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 3: // morning
                                        if (plr.getCurrweaponframe() == MORNINGSTAR + 5 || plr.getCurrweaponframe() == ZSTARATTACK + 7) {
                                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == MORNINGATTACK2 + 3 || plr.getCurrweaponframe() == ZSTARATTACK2 + 3) {
                                            if (plr.getCurrweaponanim() == 3 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 4: // sword
                                        if (plr.getCurrweaponframe() == SWORDATTACK + 7) {
                                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == SWORDATTACK2 + 6) {
                                            if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 5: // battleaxe
                                        if (plr.getCurrweaponframe() == BIGAXEATTACK + 7 || plr.getCurrweaponframe() == ZAXEATTACK + 12) {
                                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == BIGAXEATTACK2 + 6 || plr.getCurrweaponframe() == ZAXEATTACK2 + 6) {
                                            if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 6: // bow
                                        if (plr.getCurrweaponframe() == BOWWALK + 4) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == ZBOWATTACK + 4) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;

                                    case 7: // pike
                                        if (plr.getCurrweaponframe() == PIKEATTACK1 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK + 4) {
                                            if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == PIKEATTACK2 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK2 + 4) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 8: // two handed sword
                                        if (plr.getCurrweaponframe() == EXCALATTACK1 + 7 || plr.getCurrweaponframe() == ZTWOHANDATTACK + 12) {
                                            if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == EXCALATTACK2 + 5 || plr.getCurrweaponframe() == ZTWOHANDATTACK2 + 5) {
                                            if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 8) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                    case 9: // halberd
                                        if (plr.getCurrweaponframe() == HALBERDATTACK1 + 3 || plr.getCurrweaponframe() == ZHALBERDATTACK + 4) {
                                            if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        if (plr.getCurrweaponframe() == HALBERDATTACK2 + 3 || plr.getCurrweaponframe() == ZHALBERDATTACK2 + 3) {
                                            if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 12) {
                                                swingdacrunch(plr, plr.getCurrweapon());
                                            }
                                        }
                                        break;
                                }
                                hitSprite.setHitag(hitSprite.getHitag() - k);

                                if (game.WH2 && plr.getWeapon()[plr.getCurrweapon()] == 3 && plr.getCurrweapon() == 8
                                        && hitSprite.getPal() != 6) {
                                    if (hitSprite.getHitag() <= 0) {
                                        hitSprite.setHitag(1);
                                    }
                                    if (engine.krand() % 100 > 75) {
                                        medusa(plr, hitSpriteIndex);
                                    }
                                    break;
                                }

                                if (plr.getCurrweapon() != 0) {
                                    // JSA GORE normal
                                    if (engine.krand() % 100 > 50) {
                                        if (hitSprite.getPicnum() == SKELETON
                                                || hitSprite.getPicnum() == SKELETONATTACK
                                                || hitSprite.getPicnum() == SKELETONDIE) {
                                            playsound_loc(S_SKELHIT1 + (engine.krand() % 2), hitSprite.getX(),
                                                    hitSprite.getY());
                                        }
                                    }
                                    // HERE
                                    switch (plr.getCurrweapon()) {
                                        case 0: // fist
                                            break;
                                        case 1: // knife
                                            if (plr.getCurrweaponframe() == KNIFEATTACK + 6) {
                                                if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == KNIFEATTACK2 + 2) {
                                                if (plr.getCurrweaponanim() == 5 || plr.getCurrweaponanim() == 9 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 2: // short sword
                                            if (plr.getCurrweaponframe() == GOBSWORDATTACK + 4
                                                    || plr.getCurrweaponframe() == ZSHORTATTACK + 7) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == GOBSWORDATTACK2 + 4
                                                    || plr.getCurrweaponframe() == ZSHORTATTACK2 + 4) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 3: // morning
                                            if (plr.getCurrweaponframe() == MORNINGSTAR + 5 || plr.getCurrweaponframe() == ZSTARATTACK + 7) {
                                                if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == MORNINGATTACK2 + 3
                                                    || plr.getCurrweaponframe() == ZSTARATTACK2 + 3) {
                                                if (plr.getCurrweaponanim() == 3 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 4: // sword
                                            if (plr.getCurrweaponframe() == SWORDATTACK + 7) {
                                                if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == SWORDATTACK2 + 6) {
                                                if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 5: // battleaxe
                                            if (plr.getCurrweaponframe() == BIGAXEATTACK + 7 || plr.getCurrweaponframe() == ZAXEATTACK + 12) {
                                                if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == BIGAXEATTACK2 + 6 || plr.getCurrweaponframe() == ZAXEATTACK2 + 6) {
                                                if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 6: // bow
                                            if (plr.getCurrweaponframe() == BOWWALK + 4) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == ZBOWATTACK + 4) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 6) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 7: // pike
                                            if (plr.getCurrweaponframe() == PIKEATTACK1 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK + 4) {
                                                if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 10) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == PIKEATTACK2 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK2 + 4) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 8: // two handed sword
                                            if (plr.getCurrweaponframe() == EXCALATTACK1 + 7
                                                    || plr.getCurrweaponframe() == ZTWOHANDATTACK + 12) {
                                                if (plr.getCurrweaponanim() == 7 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == EXCALATTACK2 + 5
                                                    || plr.getCurrweaponframe() == ZTWOHANDATTACK2 + 5) {
                                                if (plr.getCurrweaponanim() == 5 && plr.getCurrweapontics() == 8) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                        case 9: // halberd
                                            if (plr.getCurrweaponframe() == HALBERDATTACK1 + 3
                                                    || plr.getCurrweaponframe() == ZHALBERDATTACK + 4) {
                                                if (plr.getCurrweaponanim() == 6 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            if (plr.getCurrweaponframe() == HALBERDATTACK2 + 3
                                                    || plr.getCurrweaponframe() == ZHALBERDATTACK2 + 3) {
                                                if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 12) {
                                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity,
                                                            pHitInfo.hitz, pHitInfo.hitsect, daang);
                                                }
                                            }
                                            break;
                                    }
                                }
                            }

                            if (netgame) {
                                break;
                            }

                            if (hitSprite.getHitag() <= 0) {
                                if (plr.getSelectedgun() > 1) {
                                    // JSA GORE on death ?
                                    // RAF ans:death
                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz,
                                            pHitInfo.hitsect, daang);
                                    if (hitSprite.getPicnum() == SKELETON
                                            || hitSprite.getPicnum() == SKELETONATTACK
                                            || hitSprite.getPicnum() == SKELETONDIE) {
                                        playsound_loc(S_SKELHIT1 + (engine.krand() % 2), hitSprite.getX(),
                                                hitSprite.getY());
                                    }
                                }
                                newstatus(hitSpriteIndex, DIE);
                            }
                            hitSprite.setAng((short) (plr.getAng() + ((engine.krand() & 32) - 64)));
                            if (hitSprite.getHitag() > 0) {
                                newstatus(hitSpriteIndex, PAIN);
                            }
                            break;
                    } // switch enemytype

                    switch (hitSprite.getDetail()) {
                        case GRONTYPE:
                        case KOBOLDTYPE:
                        case DRAGONTYPE:
                        case DEVILTYPE:
                        case FREDTYPE:
                        case SKELETONTYPE:
                        case GOBLINTYPE:
                        case IMPTYPE:
                        case MINOTAURTYPE:
                        case SKULLYTYPE:
                        case SPIDERTYPE:
                        case FATWITCHTYPE:
                        case JUDYTYPE:
                        case NEWGUYTYPE:
                        case GONZOTYPE:
                        case KURTTYPE:
                            if (hitSprite.getPal() == 6) {
                                // JSA_NEW
                                SND_Sound(S_SOCK1 + (engine.krand() % 4));
                                playsound_loc(S_FREEZEDIE, pHitInfo.hitx, pHitInfo.hity);
                                for (int k = 0; k < 32; k++) {
                                    icecubes(hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz,
                                            hitSpriteIndex);
                                }
                                addscore(plr, 100);
                                engine.deletesprite(hitSpriteIndex);
                            }
                            break;
                    } // switch frozen

                    switch (hitSprite.getPicnum()) {
                        case STAINGLASS1:
                        case STAINGLASS2:
                        case STAINGLASS3:
                        case STAINGLASS4:
                        case STAINGLASS5:
                        case STAINGLASS6:
                        case STAINGLASS7:
                        case STAINGLASS8:
                        case STAINGLASS9:
                            if (!game.WH2) {
                                break;
                            }
                        case BARREL:
                        case VASEA:
                        case VASEB:
                        case VASEC:

                            newstatus(hitSpriteIndex, BROKENVASE);
                            break;
                    } // switch
                } // if weapondist

                if (!madeahit) {
                    plr.getAmmo()[plr.getCurrweapon()]++;
                    madeahit = true;
                }
                break;
            }
            case 1: { //bow's arrow
                int daz2 = (int) (100 - plr.getHoriz()) * 2000;

                engine.hitscan(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), // Start position
                        EngineUtils.sin((daang + 2560) & 2047), // X vector of 3D ang
                        EngineUtils.sin((daang + 2048) & 2047), // Y vector of 3D ang
                        daz2, // Z vector of 3D ang
                        pHitInfo, CLIPMASK1);

                final int hitSpriteIndex = pHitInfo.hitsprite;
                if (pHitInfo.hitwall != -1 && hitSpriteIndex == -1) { // XXX WH2 sector lotag < 6 || > 8
                    if (game.WH2) {
                        arrowcnt = (arrowcnt + 1) % ARROWCOUNTLIMIT;
                        if (arrowsprite[arrowcnt] != -1) {
                            engine.deletesprite(arrowsprite[arrowcnt]);
                            arrowsprite[arrowcnt] = -1;
                        }
                    }

                    engine.neartag(pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz, pHitInfo.hitsect, (short) daang,
                            neartag, 1024, 3);

                    if (neartag.tagsector < 0) {
                        int j = engine.insertsprite(pHitInfo.hitsect, 0);
                        Sprite sprite = boardService.getSprite(j);
                        if (sprite != null) {
                            sprite.setX(pHitInfo.hitx);
                            sprite.setY(pHitInfo.hity);
                            sprite.setZ(pHitInfo.hitz + (8 << 8));
                            sprite.setCstat(17);// was16
                            sprite.setPicnum(WALLARROW);
                            sprite.setShade(0);
                            sprite.setPal(0);
                            sprite.setXrepeat(16);
                            sprite.setYrepeat(48);
                            sprite.setAng((short) (((daang) - 512 + (engine.krand() & 128 - 64)) & 2047));
                            sprite.setXvel(0);
                            sprite.setYvel(0);
                            sprite.setZvel(0);
                            sprite.setOwner(plr.getSprite().getOwner());
                            sprite.setLotag(32);
                            sprite.setHitag(0);
                            playsound_loc(S_ARROWHIT, sprite.getX(), sprite.getY());

                            if (game.WH2 && plr.getWeapon()[6] == 3 && plr.getCurrweapon() == 6) {
                                j = engine.insertsprite(pHitInfo.hitsect, FIRECHUNK);
                                Sprite sprite2 = boardService.getSprite(j);
                                if (sprite2 != null) {
                                    sprite2.setX(pHitInfo.hitx);
                                    sprite2.setY(pHitInfo.hity);
                                    sprite2.setZ(pHitInfo.hitz + (14 << 8));
                                    sprite2.setCstat(0);
                                    sprite2.setPicnum(ARROWFLAME);
                                    sprite2.setShade(0);
                                    sprite2.setPal(0);
                                    sprite2.setXrepeat(64);
                                    sprite2.setYrepeat(64);
                                    sprite2.setAng(0);
                                    sprite2.setXvel(0);
                                    sprite2.setYvel(0);
                                    sprite2.setZvel(0);
                                    sprite2.setOwner(0);
                                    sprite2.setLotag(1200);
                                    sprite2.setHitag(0);
                                }
                            }
                        }
                    }

//                    if (netgame) {
//					netshootgun(-1,5);
//                    }
                }
                if (pHitInfo.hitwall != -1 && hitSpriteIndex != -1) {
                    int j = engine.insertsprite(pHitInfo.hitsect, FX);
                    Sprite sprite = boardService.getSprite(j);
                    if (sprite != null) {
                        sprite.setX(pHitInfo.hitx);
                        sprite.setY(pHitInfo.hity);
                        sprite.setZ(pHitInfo.hitz + (8 << 8));
                        sprite.setCstat(2);
                        sprite.setPicnum(PLASMA);
                        sprite.setShade(-32);
                        sprite.setPal(0);
                        sprite.setXrepeat(32);
                        sprite.setYrepeat(32);
                        sprite.setAng((short) daang);
                        sprite.setXvel(0);
                        sprite.setYvel(0);
                        sprite.setZvel(0);
                        sprite.setOwner(plr.getSprite().getOwner());
                        sprite.setLotag(32);
                        sprite.setHitag(0);
                        movesprite(j, ((EngineUtils.sin((sprite.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                                ((EngineUtils.sin(sprite.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
                    }
                }
                Sprite hitSprite = boardService.getSprite(hitSpriteIndex);
                if (hitSprite != null && (hitSprite.getStatnum() < MAXSTATUS)) {
                    switch (hitSprite.getDetail()) {
                        case KURTTYPE:
                        case KATIETYPE:
                        case NEWGUYTYPE:
                        case GONZOTYPE:
                        case GRONTYPE:
                        case KOBOLDTYPE:
                        case DRAGONTYPE:
                        case DEMONTYPE:
                        case DEVILTYPE:
                        case FREDTYPE:
                        case SKELETONTYPE:
                        case GOBLINTYPE:
                        case IMPTYPE:
                        case MINOTAURTYPE:
                        case SPIDERTYPE:
                        case SKULLYTYPE:
                        case FATWITCHTYPE:
                        case FISHTYPE:
                        case RATTYPE:
                        case WILLOWTYPE:
                        case GUARDIANTYPE:
                        case JUDYTYPE:
                            if (netgame) {
//						netshootgun(hitSpriteIndex,currweapon);
                                break;
                            }
                            if (game.WH2) {
                                hitSprite.setHitag(hitSprite.getHitag() - ((engine.krand() & 30) + 15));
                            } else {
                                hitSprite.setHitag(hitSprite.getHitag() - ((engine.krand() & 15) + 15));
                            }

                            if (hitSprite.getHitag() <= 0) {
                                newstatus(hitSpriteIndex, DIE);
                                if (hitSprite.getPicnum() == RAT) {
                                    chunksofmeat(plr, hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz,
                                            pHitInfo.hitsect, daang);
                                }
                            } else {
                                hitSprite.setAng((short) (EngineUtils.getAngle(plr.getX() - hitSprite.getX(),
                                        plr.getY() - hitSprite.getY()) & 2047));
                                newstatus(hitSpriteIndex, PAIN);
                            }
                            break;
                    }

                    switch (hitSprite.getDetail()) {
                        // SHATTER FROZEN CRITTER
                        case GRONTYPE:
                        case KOBOLDTYPE:
                        case DRAGONTYPE:
                        case DEVILTYPE:
                        case FREDTYPE:
                        case SKELETONTYPE:
                        case GOBLINTYPE:
                        case IMPTYPE:
                        case MINOTAURTYPE:
                        case SKULLYTYPE:
                        case SPIDERTYPE:
                        case FATWITCHTYPE:
                        case JUDYTYPE:
                        case NEWGUYTYPE:
                        case GONZOTYPE:
                        case KURTTYPE:
                            if (hitSprite.getPal() == 6) {
                                // JSA_NEW
                                SND_Sound(S_SOCK1 + (engine.krand() % 4));
                                playsound_loc(S_FREEZEDIE, pHitInfo.hitx, pHitInfo.hity);
                                for (int k = 0; k < 32; k++) {
                                    icecubes(hitSpriteIndex, pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz,
                                            hitSpriteIndex);
                                }
                                engine.deletesprite(hitSpriteIndex);
                            }
                    } // switch frozen

                    switch (hitSprite.getPicnum()) {
                        case STAINGLASS1:
                        case STAINGLASS2:
                        case STAINGLASS3:
                        case STAINGLASS4:
                        case STAINGLASS5:
                        case STAINGLASS6:
                        case STAINGLASS7:
                        case STAINGLASS8:
                        case STAINGLASS9:
                            if (!game.WH2) {
                                break;
                            }

                        case BARREL:
                        case VASEA:
                        case VASEB:
                        case VASEC:
                            newstatus(hitSpriteIndex, BROKENVASE);
                            break;
                    } // switch
                }
                break;
            }
            case 6: { // MEDUSA
                for (int i = 0; i < MAXSPRITES; i++) {
                    // cansee
                    if (i != plr.getSpritenum()) {
                        Sprite spr = boardService.getSprite(i);
                        if (spr != null) {
                            switch (spr.getDetail()) {
                                case FREDTYPE:
                                case KOBOLDTYPE:
                                case GOBLINTYPE:
                                case IMPTYPE:
                                case MINOTAURTYPE:
                                case SPIDERTYPE:
                                case SKELETONTYPE:
                                case GRONTYPE:
                                case GONZOTYPE:
                                case KURTTYPE:
                                case NEWGUYTYPE:
                                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                                            spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                                        // distance check
                                        if (checkmedusadist(i, plr.getX(), plr.getY(), plr.getZ(), plr.getLvl())) {
                                            medusa(plr, i);
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
                break;
            }
            case 7: { // KNOCKSPELL
                int daz2 = (int) (100 - plr.getHoriz()) * 2000;

                engine.hitscan(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), // Start position
                        EngineUtils.sin((daang + 2560) & 2047), // X vector of 3D ang
                        EngineUtils.sin((daang + 2048) & 2047), // Y vector of 3D ang
                        daz2, // Z vector of 3D ang
                        pHitInfo, CLIPMASK1);

                final int hitSpriteIndex = pHitInfo.hitsprite;
                if (pHitInfo.hitsect == -1 && hitSpriteIndex == -1 || pHitInfo.hitwall != -1) {

                    engine.neartag(pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz, pHitInfo.hitsect, (short) daang,
                            neartag, 1024, 3);

                    Sector nearSec = boardService.getSector(neartag.tagsector);
                    if (nearSec != null) {
                        if (nearSec.getLotag() >= 60 && nearSec.getLotag() <= 69) {
                            nearSec.setLotag(6);
                            nearSec.setHitag(0);
                        }
                        if (nearSec.getLotag() >= 70 && nearSec.getLotag() <= 79) {
                            nearSec.setLotag(7);
                            nearSec.setHitag(0);
                        }
                        operatesector(plr, neartag.tagsector);
                    }

                }
                break;
            }
            case 10: { // throw a pike axe
                if (plr.getCurrweaponframe() == PIKEATTACK1 + 4 || plr.getCurrweaponframe() == ZPIKEATTACK + 4) {
                    if (plr.getCurrweaponanim() == 8 && plr.getCurrweapontics() == 10) {
//                        if (netgame) {
//                            netshootgun(-1,15);
//                        }

                        if (game.WH2) {
                            throwpikecnt = (throwpikecnt + 1) % THROWPIKELIMIT;
                            if (throwpikesprite[throwpikecnt] != -1) {
                                engine.deletesprite(throwpikesprite[throwpikecnt]);
                                throwpikesprite[throwpikecnt] = -1;
                            }

                            if (plr.getWeapon()[plr.getCurrweapon()] == 3) {
                                int j = engine.insertsprite(plr.getSector(), MISSILE);
                                Sprite sprite = boardService.getSprite(j);
                                if (sprite != null) {
                                    throwpikesprite[throwpikecnt] = (short) j;
                                    sprite.setX(plr.getX());
                                    sprite.setY(plr.getY());
                                    sprite.setZ(plr.getZ() + (24 << 8));
                                    sprite.setCstat(21);
                                    sprite.setPicnum(THROWPIKE);
                                    sprite.setAng((short) (((daang + 2048 + 96) - 512) & 2047));
                                    sprite.setXrepeat(24);
                                    sprite.setYrepeat(24);
                                    sprite.setClipdist(32);
                                    sprite.setExtra((short) daang);
                                    sprite.setShade(-15);
                                    sprite.setXvel((short) ((engine.krand() & 256) - 128));
                                    sprite.setYvel((short) ((engine.krand() & 256) - 128));
                                    if (plr.getShootgunzvel() != 0) {
                                        sprite.setZvel((short) plr.getShootgunzvel());
                                        plr.setShootgunzvel(0);
                                    } else {
                                        sprite.setZvel((short) ((100 - (int) plr.getHoriz()) << 4));
                                    }
                                    sprite.setOwner(plr.getSprite().getOwner());
                                    sprite.setLotag(1024);
                                    sprite.setHitag(0);
                                    sprite.setPal(0);
                                    movesprite(j, ((EngineUtils.sin((sprite.getExtra() + 512) & 2047)) * TICSPERFRAME) << 3,
                                            ((EngineUtils.sin(sprite.getExtra() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
                                    engine.setsprite(j, sprite.getX(), sprite.getY(), sprite.getZ());
                                }
                            } else {
                                int j = engine.insertsprite(plr.getSector(), MISSILE);
                                Sprite sprite = boardService.getSprite(j);
                                if (sprite != null) {
                                    throwpikesprite[throwpikecnt] = (short) j;
                                    sprite.setX(plr.getX());
                                    sprite.setY(plr.getY());
                                    sprite.setZ(plr.getZ() + (24 << 8));
                                    sprite.setCstat(21);
                                    sprite.setPicnum(THROWPIKE);
                                    sprite.setAng((short) ((((int) plr.getAng() + 2048 + 96) - 512) & 2047));
                                    sprite.setXrepeat(24);
                                    sprite.setYrepeat(24);
                                    sprite.setClipdist(32);
                                    sprite.setExtra((short) plr.getAng());
                                    sprite.setShade(-15);
                                    sprite.setXvel((short) ((engine.krand() & 256) - 128));
                                    sprite.setYvel((short) ((engine.krand() & 256) - 128));
                                    sprite.setZvel((short) ((100 - (int) plr.getHoriz()) << 4));
                                    sprite.setOwner(plr.getSprite().getOwner());
                                    sprite.setLotag(1024);
                                    sprite.setHitag(0);
                                    sprite.setPal(0);
                                }
                            }
                        } else {
                            int j = engine.insertsprite(plr.getSector(), MISSILE);
                            Sprite sprite = boardService.getSprite(j);
                            if (sprite != null) {
                                sprite.setX(plr.getX());
                                sprite.setY(plr.getY());
                                sprite.setZ(plr.getZ() + (16 << 8));

                                // sprite.cstat=17;
                                sprite.setCstat(21);

                                sprite.setPicnum(THROWPIKE);
                                sprite.setAng((short) BClampAngle((plr.getAng() + 96) - 512));
                                sprite.setXrepeat(24);
                                sprite.setYrepeat(24);
                                sprite.setClipdist(24);

                                sprite.setExtra((short) plr.getAng());
                                sprite.setShade(-15);
                                sprite.setXvel((short) ((engine.krand() & 256) - 128));
                                sprite.setYvel((short) ((engine.krand() & 256) - 128));
                                // sprite.zvel=((engine.krand()&256)-128);
                                sprite.setZvel((short) ((int) (100 - plr.getHoriz()) << 4));
                                sprite.setOwner(plr.getSprite().getOwner());
                                sprite.setLotag(1024);
                                sprite.setHitag(0);
                                sprite.setPal(0);
                            }
                        }
                    }
                }

                if (plr.getCurrweaponframe() == PIKEATTACK2 + 4) {
                    if (plr.getCurrweaponanim() == 4 && plr.getCurrweapontics() == 10) {

                        if (game.WH2) {
                            throwpikecnt = (throwpikecnt + 1) % THROWPIKELIMIT;
                            if (throwpikesprite[throwpikecnt] != -1) {
                                engine.deletesprite(throwpikesprite[throwpikecnt]);
                                throwpikesprite[throwpikecnt] = -1;
                            }

                            if (plr.getWeapon()[plr.getCurrweapon()] == 3) {
                                int j = engine.insertsprite(plr.getSector(), MISSILE);
                                Sprite sprite = boardService.getSprite(j);
                                if (sprite != null) {
                                    throwpikesprite[throwpikecnt] = (short) j;
                                    sprite.setX(plr.getX());
                                    sprite.setY(plr.getY());
                                    sprite.setZ(plr.getZ() + (24 << 8));
                                    sprite.setCstat(21);
                                    sprite.setPicnum(THROWPIKE);
                                    sprite.setAng((short) daang);

                                    sprite.setXrepeat(24);
                                    sprite.setYrepeat(24);
                                    sprite.setClipdist(32);
                                    sprite.setExtra((short) daang);

                                    sprite.setShade(-15);
                                    sprite.setXvel((short) ((engine.krand() & 256) - 128));
                                    sprite.setYvel((short) ((engine.krand() & 256) - 128));
                                    sprite.setZvel((short) ((100 - (int) plr.getHoriz()) << 4));
                                    sprite.setOwner(plr.getSprite().getOwner());
                                    sprite.setLotag(1024);
                                    sprite.setHitag(0);
                                    sprite.setPal(0);
                                    movesprite(j, ((EngineUtils.sin((sprite.getExtra() + 512) & 2047)) * TICSPERFRAME) << 3,
                                            ((EngineUtils.sin(sprite.getExtra() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
                                    engine.setsprite(j, sprite.getX(), sprite.getY(), sprite.getZ());
                                }
                            } else {
                                int j = engine.insertsprite(plr.getSector(), MISSILE);
                                Sprite sprite = boardService.getSprite(j);
                                if (sprite != null) {
                                    throwpikesprite[throwpikecnt] = (short) j;
                                    sprite.setX(plr.getX());
                                    sprite.setY(plr.getY());
                                    sprite.setZ(plr.getZ() + (24 << 8));
                                    sprite.setCstat(21);
                                    sprite.setPicnum(THROWPIKE);
                                    sprite.setAng((short) ((((int) plr.getAng() + 2048 + 96) - 512) & 2047));
                                    sprite.setXrepeat(24);
                                    sprite.setYrepeat(24);
                                    sprite.setClipdist(32);
                                    sprite.setExtra((short) plr.getAng());
                                    sprite.setShade(-15);
                                    sprite.setXvel((short) ((engine.krand() & 256) - 128));
                                    sprite.setYvel((short) ((engine.krand() & 256) - 128));
                                    sprite.setZvel((short) ((100 - (int) plr.getHoriz()) << 4));
                                    sprite.setOwner(plr.getSprite().getOwner());
                                    sprite.setLotag(1024);
                                    sprite.setHitag(0);
                                    sprite.setPal(0);
                                }
                            }
                        } else {
                            int j = engine.insertsprite(plr.getSector(), MISSILE);
                            Sprite sprite = boardService.getSprite(j);
                            if (sprite != null) {
                                sprite.setX(plr.getX());
                                sprite.setY(plr.getY());
                                sprite.setZ(plr.getZ());

                                sprite.setCstat(21);

                                sprite.setPicnum(THROWPIKE);
                                sprite.setAng((short) BClampAngle((plr.getAng()) - 512));
                                sprite.setXrepeat(24);
                                sprite.setYrepeat(24);
                                sprite.setClipdist(24);

                                sprite.setExtra((short) plr.getAng());
                                sprite.setShade(-15);
                                sprite.setXvel((short) ((engine.krand() & 256) - 128));
                                sprite.setYvel((short) ((engine.krand() & 256) - 128));
                                sprite.setZvel((short) ((engine.krand() & 256) - 128));
                                sprite.setOwner(plr.getSprite().getOwner());
                                sprite.setLotag(1024);
                                sprite.setHitag(0);
                                sprite.setPal(0);
                            }
                        }

                    }
                }
                break;
            }
            case 2: { // parabolic trajectory

//                if (netgame) {
//						netshootgun(-1,12);
//                }

                int j = engine.insertsprite(plr.getSector(), MISSILE);
                Sprite sprite = boardService.getSprite(j);
                if (sprite != null) {
                    sprite.setX(plr.getX());
                    sprite.setY(plr.getY());
                    sprite.setZ(plr.getZ() + (8 << 8) + ((engine.krand() & 10) << 8));
                    sprite.setCstat(0);
                    sprite.setPicnum(PLASMA);
                    sprite.setShade(-32);
                    sprite.setPal(0);
                    sprite.setXrepeat(16);
                    sprite.setYrepeat(16);
                    sprite.setAng((short) daang);
                    sprite.setXvel((short) (EngineUtils.sin((daang + 2560) & 2047) >> 5));
                    sprite.setYvel((short) (EngineUtils.sin((daang) & 2047) >> 5));

                    if (plr.getShootgunzvel() != 0) {
                        sprite.setZvel((short) plr.getShootgunzvel());
                        plr.setShootgunzvel(0);
                    } else {
                        sprite.setZvel((short) ((int) (100 - plr.getHoriz()) << 4));
                    }

                    sprite.setOwner(plr.getSprite().getOwner());
                    sprite.setLotag(256);
                    sprite.setHitag(0);
                    sprite.setClipdist(48);

                    movesprite(j, ((EngineUtils.sin((sprite.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                            ((EngineUtils.sin(sprite.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
                    engine.setsprite(j, sprite.getX(), sprite.getY(), sprite.getZ());
                }
                break;
            }
            case 3: {

//                if (netgame) {
//						netshootgun(-1,13);
//                }

                int j = engine.insertsprite(plr.getSector(), MISSILE);
                Sprite sprite = boardService.getSprite(j);
                if (sprite != null) {
                    sprite.setX(plr.getX());
                    sprite.setY(plr.getY());
                    sprite.setZ(plr.getZ() + (8 << 8));
                    sprite.setCstat(0); // Hitscan does not hit other bullets
                    sprite.setPicnum(MONSTERBALL);
                    sprite.setShade(-32);
                    sprite.setPal(0);
                    sprite.setXrepeat(64);
                    sprite.setYrepeat(64);
                    sprite.setAng((short) plr.getAng());
                    sprite.setXvel((short) (EngineUtils.sin((daang + 2560) & 2047) >> 7));
                    sprite.setYvel((short) (EngineUtils.sin((daang) & 2047) >> 7));

                    if (plr.getShootgunzvel() != 0) {
                        sprite.setZvel((short) plr.getShootgunzvel());
                        plr.setShootgunzvel(0);
                    } else {
                        sprite.setZvel((short) ((int) (100 - plr.getHoriz()) << 4));
                    }

                    sprite.setOwner(plr.getSprite().getOwner());
                    sprite.setLotag(256);
                    sprite.setHitag(0);
                    sprite.setClipdist(64);

                    // dax=(EngineUtils.sin((sprite.ang+512)&2047)>>6);
                    // day=(EngineUtils.sin(sprite.ang)>>6);

                    movesprite(j, ((EngineUtils.sin((sprite.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                            ((EngineUtils.sin(sprite.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
                    engine.setsprite(j, sprite.getX(), sprite.getY(), sprite.getZ());
                }
                break;
            }
            case 4: {

//                if (netgame) {
//						netshootgun(-1,14);
//                }

                for (int j1 = 0; j1 < MAXSPRITES; j1++) {
                    Sprite sprite = boardService.getSprite(j1);
                    if (sprite != null) {
                        switch (sprite.getDetail()) {
                            case DEMONTYPE:
                            case NEWGUYTYPE:
                            case KURTTYPE:
                            case GONZOTYPE:
                            case IMPTYPE:
                                if (!game.WH2) {
                                    break;
                                }

                            case SPIDERTYPE:
                            case KOBOLDTYPE:
                            case DEVILTYPE:
                            case GOBLINTYPE:
                            case MINOTAURTYPE:
                            case SKELETONTYPE:
                            case GRONTYPE:
                            case DRAGONTYPE:
                            case GUARDIANTYPE:
                            case FATWITCHTYPE:
                            case SKULLYTYPE:
                            case JUDYTYPE:
                            case WILLOWTYPE:
                                if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), sprite.getX(), sprite.getY(),
                                        sprite.getZ() - (engine.getTile(sprite.getPicnum()).getHeight() << 7), sprite.getSectnum())) {
                                    if ((game.WH2 && sprite.getOwner() != plr.getSprite().getOwner())
                                            || checkmedusadist(j1, plr.getX(), plr.getY(), plr.getZ(), 12)) {
                                        nukespell(plr, j1);
                                    }
                                }
                                break;
                        }
                    }
                }
                break;
            }
        }
    }

    public static boolean checkweapondist(int i, int x, int y, int z, int guntype) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return false;
        }
        int length = 1024;

        if (guntype != 0) {
            switch (guntype) {
                case 2:
                case 3:
                case 4:
                case 5:
                    length = 1536;
                    break;
                case 6:
                case 8:
                case 9:
                    length = 2048;
                    break;
                case 1:
                case 7:
                    break;
            }
        }

        return (klabs(x - spr.getX()) + klabs(y - spr.getY()) < length)
                && (klabs((z >> 8) - ((spr.getZ() >> 8) - (engine.getTile(spr.getPicnum()).getHeight() >> 1))) <= (length >> 3));
    }

    public static void swingdapunch(PLAYER plr, int daweapon) {
        switch (daweapon) {
            case 0:// hands
                SND_Sound(S_SOCK4);
                SND_Sound(S_PLRPAIN1 + (engine.krand() % 2));
                addhealth(plr, -1);
                startredflash(10);
                break;
            case 1: // knife
            case 2: // mace
            case 4: // sword
            case 5:
            case 6:
            case 7:
            case 8:
                SND_Sound(S_WALLHIT1);
                break;
            case 3: // arrow
                break;
        }
    }

    public static void swingdaweapon(PLAYER plr) {
        float daang = plr.getAng();

        if (plr.getCurrweaponframe() == BOWWALK + 5 && plr.getAmmo()[6] > 0) {
            plr.setCurrweaponfired(5);
            plr.setCurrweaponanim(checkCodeVersion(game.getScreen(), 101) ? 1 : 0); // #GDX 12.07.2024 was 0 (useless line?)
        }

        if (plr.getCurrweaponframe() == BOWWALK + 5 && plr.getAmmo()[6] <= 0) {
            plr.setCurrweaponfired(0);
            plr.setCurrweaponanim(0);
            return;
        }

        if (!game.WH2) {
            if (plr.getCurrweaponframe() == PIKEATTACK1 + 4
                    // || plr.currweaponframe == PIKEATTACK2+4
                    && plr.getWeapon()[7] == 2 && plr.getAmmo()[7] > 0) {
                shootgun(plr, daang, 10);
                playsound_loc(S_THROWPIKE, plr.getX(), plr.getY());
                plr.setHasshot(1);
                return;
            }
        } else {
            if (plr.getCurrweaponframe() == BOWWALK + 5 && plr.getAmmo()[6] > 0) {
                plr.setCurrweaponfired(5);
                plr.setCurrweaponanim(checkCodeVersion(game.getScreen(), 101) ? 1 : 0); // #GDX 12.07.2024 was 0
            } else if (plr.getCurrweaponframe() == ZBOWATTACK + 4 && plr.getAmmo()[6] > 0) {
                plr.setCurrweaponfired(5);
                plr.setCurrweaponanim(checkCodeVersion(game.getScreen(), 101) ? 1 : 0);
            }

            if (plr.getCurrweaponframe() == BOWWALK + 5 && plr.getAmmo()[6] <= 0) {
                plr.setCurrweaponfired(0);
                plr.setCurrweaponanim(0);
                return;
            } else if (plr.getCurrweaponframe() == ZBOWATTACK + 4 && plr.getAmmo()[6] <= 0) {
                plr.setCurrweaponfired(0);
                plr.setCurrweaponanim(0);
                return;
            }

            if (plr.getCurrweaponframe() == PIKEATTACK1 + 4 && plr.getWeapon()[7] == 2 && plr.getAmmo()[7] > 0) {
                shootgun(plr, daang, 10);
                playsound_loc(S_GENTHROW, plr.getX(), plr.getY());
                plr.setHasshot(1);
                return;
            } else if (plr.getCurrweaponframe() == ZPIKEATTACK + 4 && plr.getWeapon()[7] == 3 && plr.getAmmo()[7] > 0) {
                lockon(plr, 3, 10);
                playsound_loc(S_GENTHROW, plr.getX(), plr.getY());
                plr.setHasshot(1);
                return;
            }
        }

        switch (plr.getSelectedgun()) {
            case 0: // fist & close combat weapons
            case 1: // knife
            case 2: // shortsword
            case 4: // sword
            case 7: // pike
            case 8: // two handed
            case 9: // halberd
                shootgun(plr, daang, 0);
                plr.setHasshot(1);
                break;
            case 3: // morningstar
                shootgun(plr, daang, 0);
                if (game.WH2 && plr.getWeapon()[plr.getSelectedgun()] == 3) {
                    lockon(plr, 1, 3);
                }

                plr.setHasshot(1);
                break;
            case 5: // battleaxe
                if (game.WH2 && enchantedsoundhandle != -1) {
                    SND_Sound(S_ENERGYSWING);
                }

                shootgun(plr, daang, 0);
                plr.setHasshot(1);
                break;
            case 6: // bow
                if (game.WH2 && enchantedsoundhandle != -1) {
                    SND_Sound(S_FIREBALL);
                    SND_Sound(S_PLRWEAPON3);
                }
                shootgun(plr, daang, 1);
                plr.setHasshot(1);
                break;
        }
    }

    public static void swingdacrunch(PLAYER plr, int daweapon) {

        switch (daweapon) {
            case 0: // fist
            case 3: // morningstar
                playsound_loc(S_SOCK1 + (engine.krand() % 4), plr.getX(), plr.getY());
                break;
            case 1: // dagger
                if ((engine.krand() % 2) != 0) {
                    playsound_loc(S_GORE1 + (engine.krand() % 4), plr.getX(), plr.getY());
                }
                break;
            case 2: // short sword
                playsound_loc(S_SWORD2 + (engine.krand() % 3), plr.getX(), plr.getY());
                break;
            case 4: // broad sword
                playsound_loc(S_SWORD1 + (engine.krand() % 3), plr.getX(), plr.getY());
                break;
            case 5: // battle axe
            case 7: // pike
            case 9: // halberd
                if ((engine.krand() % 2) != 0) {
                    playsound_loc(S_SOCK1 + (engine.krand() % 4), plr.getX(), plr.getY());
                } else {
                    playsound_loc(S_SWORD1 + (engine.krand() % 3), plr.getX(), plr.getY());
                }
                break;
            case 6: // bow

                break;
            case 8: // two handed sword
                playsound_loc(S_SWORD1 + (engine.krand() % 2), plr.getX(), plr.getY());
                break;
        }
    }

    public static void swingdasound(int daweapon, boolean enchanted) {

        switch (daweapon) {
            case 0: // fist
                SND_Sound(S_PLRWEAPON0);
                break;
            case 1: // knife
                SND_Sound(S_PLRWEAPON1);
                break;
            case 2: // short sword
                if (game.WH2 && enchanted) {
                    SND_Sound(S_FIRESWING);
                } else {
                    SND_Sound(S_PLRWEAPON4);
                }
                break;
            case 3: // mace
                if (game.WH2 && enchanted) {
                    SND_Sound(S_FIRESWING);
                    SND_Sound(S_FIREBALL);
                } else {
                    SND_Sound(S_PLRWEAPON2);
                }
                break;
            case 4: //
            case 5: // sword
            case 8:
            case 9:
                SND_Sound(S_PLRWEAPON4);
                break;
            case 6: // bow
                SND_Sound(S_PLRWEAPON3);
                break;
            case 7: //
                if (game.WH2 && enchanted) {
                    SND_Sound(S_ENERGYSWING);
                } else {
                    SND_Sound(S_PLRWEAPON4);
                }
                break;
        }
    }

}
