package ru.m210projects.Witchaven.AI;

public class Enemy {

    public EnemyInfo info;

    public AIState chase;
    public AIState resurrect;
    public AIState nuked;
    public AIState frozen;
    public AIState pain;
    public AIState face;
    public AIState attack;
    public AIState flee;
    public AIState cast;
    public AIState die;
    public AIState skirmish;
    public AIState stand;
    public AIState search;

}
