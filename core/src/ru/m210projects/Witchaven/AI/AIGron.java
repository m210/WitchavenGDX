package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Items.THROWHALBERDTYPE;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WH1Names.GRONCHAR;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.chunksofmeat;
import static ru.m210projects.Witchaven.WHSND.*;

public class AIGron {

    public static void create() {
        enemy[GRONTYPE] = new Enemy();
        enemy[GRONTYPE].info = new EnemyInfo(game.WH2 ? 35 : 30, game.WH2 ? 35 : 30, -1, 120, 0, 64, false, 300, 0) {
            @Override
            public int getAttackDist(Sprite spr) {
                int out = attackdist;
                int pic = spr.getPicnum();

                if (pic == GRONHAL || pic == GRONHALATTACK) {
                    out = 1024 + 512;
                } else if (pic == GRONMU || pic == GRONMUATTACK) {
                    out = 2048;
                } else if (pic == GRONSW || pic == GRONSWATTACK) {
                    out = 1024 + 256;
                }

                return out;
            }

            @Override
            public short getHealth(Sprite spr) {
                if (game.WH2) {
                    if (spr.getPicnum() == GRONHAL) {
                        return adjusthp(65);
                    }
                    if (spr.getPicnum() == GRONMU) {
                        return adjusthp(70);
                    }
                }
                return adjusthp(health);
            }
        };
        enemy[GRONTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                if (spr.getPicnum() == GRONSW) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                            spr.getSectnum()) && plr.getInvisibletime() < 0) {
                        if (checkdist(plr, i)) {
                            if (plr.getShadowtime() > 0) {
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                newstatus(i, FLEE);
                            } else {
                                newstatus(i, ATTACK);
                            }
                        } else if (engine.krand() % 63 > 60) {
                            spr.setAng((short) (((engine.krand() & 128 - 256) + spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                        } else {
                            int movestat = aimove(i);
                            if ((movestat & HIT_TYPE_MASK) == HIT_FLOOR) {
                                spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                                newstatus(i, FLEE);
                                return;
                            }

                            if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                                if ((movestat & HIT_INDEX_MASK) != plr.getSpritenum()) {
                                    short daang = (short) ((spr.getAng() - 256) & 2047);
                                    spr.setAng(daang);
                                    if (plr.getShadowtime() > 0) {
                                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                        newstatus(i, FLEE);
                                    } else {
                                        newstatus(i, SKIRMISH);
                                    }
                                } else {
                                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                    newstatus(i, SKIRMISH);
                                }
                            }
                        }
                    } else {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    }
                } else {
                    if (engine.krand() % 63 == 0) {
                        if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                                spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum()))// && invisibletime < 0)
                        {
                            newstatus(i, ATTACK);
                        }
                    } else {
                        checksight(plr, i);
                        if (!checkdist(plr, i)) {
                            if ((aimove(i) & HIT_TYPE_MASK) == HIT_FLOOR) {
                                spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                                newstatus(i, FLEE);
                                return;
                            }
                        } else {
                            if (engine.krand() % 8 == 0) // NEW
                            {
                                newstatus(i, ATTACK); // NEW
                            } else { // NEW
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                                newstatus(i, FLEE); // NEW
                            }
                        }
                    }
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                if (osec.getLotag() == KILLSECTOR) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                sec = boardService.getSector(spr.getSectnum());

                if (sec != null && ((zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == LAVA
                        || sec.getFloorpicnum() == LAVA1 || sec.getFloorpicnum() == ANILAVA))) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                checkexpl(plr, i);
            }
        };

        enemy[GRONTYPE].resurrect = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                    switch (engine.krand() % 3) {
                        case 0:
                            spr.setPicnum(GRONHAL);
                            spr.setHitag(adjusthp(120));
                            spr.setExtra(3);
                            break;
                        case 1:
                            spr.setPicnum(GRONSW);
                            spr.setHitag(adjusthp(120));
                            spr.setExtra(0);
                            break;
                        case 2:
                            spr.setPicnum(GRONMU);
                            spr.setHitag(adjusthp(120));
                            spr.setExtra(2);
                            break;
                    }
                    spr.setLotag(100);
                    spr.setCstat(spr.getCstat() | 1);
                }
            }
        };

        enemy[GRONTYPE].skirmish = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    newstatus(i, FACE);
                }
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                if (checksector6(i)) {
                    return;
                }

                checkexpl(plr, i);
            }
        };

        enemy[GRONTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, false);
                if (!checksector6(i)) {
                    checkexpl(plr, i);
                }
            }
        };

        enemy[GRONTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                if (game.WH2) {
                    chunksofmeat(plr, i, spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(), spr.getAng());
                    trailingsmoke(i, false);
                    newstatus(i, DIE);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(24);
                    if (spr.getPicnum() == GRONCHAR + 4) {
                        trailingsmoke(i, false);
                        engine.deletesprite(i);
                    }
                }
            }
        };

        enemy[GRONTYPE].frozen = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPal(0);
                    if (spr.getPicnum() == GRONHALDIE) {
                        spr.setPicnum(GRONHAL);
                    } else if (spr.getPicnum() == GRONSWDIE) {
                        spr.setPicnum(GRONSW);
                    } else if (spr.getPicnum() == GRONMUDIE) {
                        spr.setPicnum(GRONMU);
                    }
                    newstatus(i, FACE);
                }
            }
        };

        enemy[GRONTYPE].pain = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    if (spr.getPicnum() == GRONHALPAIN) {
                        spr.setPicnum(GRONHAL);
                    } else if (spr.getPicnum() == GRONSWPAIN) {
                        spr.setPicnum(GRONSW);
                    } else if (spr.getPicnum() == GRONMUPAIN) {
                        spr.setPicnum(GRONMU);
                    }

                    spr.setAng((short) plr.getAng());
                    newstatus(i, FLEE);
                }

                aimove(i);
                processfluid(i, zr_florhit, false);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checkexpl(plr, i);
            }
        };

        enemy[GRONTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }


                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    spr.setAng((short) (EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()) & 2047));

                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) {
                        newstatus(i, FLEE);
                    }
                }

                if (checkdist(plr, i)) {
                    newstatus(i, ATTACK);
                }

                checkexpl(plr, i);
            }
        };

        enemy[GRONTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                if (spr.getPicnum() == GRONSWATTACK) {
                    engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                    spr.setZ(zr_florz);

                    switch (checkfluid(i, zr_florhit)) {
                        case TYPELAVA:
                            spr.setHitag(spr.getHitag() - 1);
                            if (spr.getHitag() < 0) {
                                newstatus(i, DIE);
                            }
                        case TYPEWATER:
                            spr.setZ(spr.getZ() + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                            break;
                    }

                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                    if (spr.getLotag() == 31) {
                        if (checksight(plr, i)) {
                            if (checkdist(plr, i)) {
                                spr.setAng((short) checksight_ang);
                                attack(plr, i);
                            }
                        }
                    } else if (spr.getLotag() < 0) {
                        if (plr.getShadowtime() > 0) {
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                            newstatus(i, FLEE);
                        } else {
                            newstatus(i, CHASE);
                        }
                    }
                    spr.setLotag(spr.getLotag() - TICSPERFRAME);
                } else {
                    spr.setLotag(spr.getLotag() - TICSPERFRAME);
                    if (spr.getLotag() < 0) {
                        if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                                spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                            newstatus(i, CAST);
                        } else {
                            newstatus(i, CHASE);
                        }
                    } else {
                        spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    }
                }

                checksector6(i);
            }
        };

        enemy[GRONTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        newstatus(i, FACE);
                    }
                }
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checkexpl(plr, i);
            }
        };

        enemy[GRONTYPE].cast = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    if (spr.getPicnum() == GRONHALATTACK) {
                        spr.setExtra(spr.getExtra() - 1);
                        playsound_loc(S_THROWPIKE, spr.getX(), spr.getY());
                        throwhalberd(i);
                        newstatus(i, CHASE);
                    } else if (spr.getPicnum() == GRONMUATTACK) {
                        spr.setExtra(spr.getExtra() - 1);
                        playsound_loc(S_SPELL2, spr.getX(), spr.getY());
                        castspell(plr, i);
                        newstatus(i, CHASE);
                    }
                }

                checksector6(i);
            }
        };

        enemy[GRONTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getPicnum() == GRONSWDIE || spr.getPicnum() == GRONHALDIE || spr.getPicnum() == GRONMUDIE) {
                    if (spr.getLotag() < 0) {
                        spr.setPicnum(GRONDIE);
                        spr.setLotag(20);
                    } else {
                        return;
                    }
                }

                if (spr.getLotag() <= 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(20);

                    if (spr.getPicnum() == GRONDEAD) {
                        if (difficulty == 4) {
                            newstatus(i, RESURECT);
                        } else {
                            kills++;
                            newstatus(i, DEAD);
                        }
                    }
                }
            }
        };
    }

    public static void checkexpl(PLAYER ignoredPlr, int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        ListNode<Sprite> sectNode = boardService.getSectNode(spr.getSectnum());
        while (sectNode != null) {
            ListNode<Sprite> next = sectNode.getNext();
            Sprite secSpr = sectNode.get();
            long dx = klabs(spr.getX() - secSpr.getX()); // x distance to sprite
            long dy = klabs(spr.getY() - secSpr.getY()); // y distance to sprite
            long dz = klabs((spr.getZ() >> 8) - (secSpr.getZ() >> 8)); // z distance to sprite
            long dh = engine.getTile(secSpr.getPicnum()).getHeight() >> 1; // height of sprite
            if (dx + dy < PICKDISTANCE && dz - dh <= getPickHeight()) {
                if (secSpr.getPicnum() == EXPLO2
                        || secSpr.getPicnum() == MONSTERBALL) {
                    spr.setHitag(spr.getHitag() - (TICSPERFRAME << 2));
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }
            }
            sectNode = next;
        }
    }

    public static void throwhalberd(int s) {
        Sprite spr = boardService.getSprite(s);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), JAVLIN);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ() - (40 << 8));

        spr2.setCstat(17);

        spr2.setPicnum(THROWHALBERD);
        spr2.setDetail(THROWHALBERDTYPE);
        spr2.setAng((short) (((spr.getAng() + 2048) - 512) & 2047));
        spr2.setXrepeat(8);
        spr2.setYrepeat(16);
        spr2.setClipdist(32);

        spr2.setExtra(spr.getAng());
        spr2.setShade(-15);
        spr2.setXvel((short) ((engine.krand() & 256) - 128));
        spr2.setYvel((short) ((engine.krand() & 256) - 128));
        spr2.setZvel((short) ((engine.krand() & 256) - 128));
        spr2.setOwner((short) s);
        spr2.setLotag(0);
        spr2.setHitag(0);
        spr2.setPal(0);

        spr2.setCstat(0);
        int daz = (((spr2.getZvel()) * TICSPERFRAME) >> 3);
        movesprite(j, ((EngineUtils.sin((spr2.getExtra() + 512) & 2047)) * TICSPERFRAME) << 7,
                ((EngineUtils.sin(spr2.getExtra() & 2047)) * TICSPERFRAME) << 7, daz, 4 << 8, 4 << 8, 1);
        spr2.setCstat(21);
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        if (spr.getPicnum() == GRONSW && spr.getPal() == 10) {
            engine.deletesprite(i);
        }

        spr.setDetail(GRONTYPE);
        enemy[GRONTYPE].info.set(spr);
        engine.changespritestat(i, FACE);

        if (spr.getPicnum() == GRONHAL) {
            spr.setExtra(4);
        } else if (spr.getPicnum() == GRONSW) {
            spr.setExtra(0);
        } else if (spr.getPicnum() == GRONMU) {
            spr.setExtra(2);
        }
    }
}
