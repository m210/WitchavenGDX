package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WH1Names.KOBOLDCHAR;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.chunksofmeat;
import static ru.m210projects.Witchaven.WHSND.S_KSNARL1;
import static ru.m210projects.Witchaven.WHSND.playsound_loc;

public class AIKobold {

    public static void create() {
        enemy[KOBOLDTYPE] = new Enemy();
        enemy[KOBOLDTYPE].info = new EnemyInfo(game.WH2 ? 60 : 54, game.WH2 ? 60 : 54, 1024, 120, 0, 64, false, 20, 0);
        enemy[KOBOLDTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                if ((engine.krand() % 100) > 98) {
                    playsound_loc(S_KSNARL1 + (engine.krand() % 4), spr.getX(), spr.getY());
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum()) && plr.getInvisibletime() < 0) {
                    if (checkdist(plr, i)) {
                        if (plr.getShadowtime() > 0) {
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                        } else {
                            newstatus(i, ATTACK);
                        }
                    } else if (engine.krand() % 63 > 60) {
                        spr.setAng((short) (((engine.krand() & 128 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        int movestat = aimove(i);
                        if ((movestat & HIT_TYPE_MASK) == HIT_FLOOR) {
                            spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                            return;
                        }

                        if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                            if ((movestat & HIT_INDEX_MASK) != plr.getSpritenum()) {
                                short daang = (short) ((spr.getAng() - 256) & 2047);
                                spr.setAng(daang);
                                if (plr.getShadowtime() > 0) {
                                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                    newstatus(i, FLEE);
                                } else {
                                    newstatus(i, SKIRMISH);
                                }
                            } else {
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                newstatus(i, SKIRMISH);
                            }
                        }
                    }
                } else {
                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                    newstatus(i, FLEE);
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                if (osec.getLotag() == KILLSECTOR) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                sec = boardService.getSector(spr.getSectnum());

                if (sec != null && (zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == LAVA
                        || sec.getFloorpicnum() == LAVA1 || sec.getFloorpicnum() == ANILAVA)) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                checkexpl(plr, i);
            }
        };

        enemy[KOBOLDTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() <= 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(20);

                    if (spr.getPicnum() == KOBOLDDEAD) {
                        if (difficulty == 4) {
                            newstatus(i, RESURECT);
                        } else {
                            kills++;
                            newstatus(i, DEAD);
                        }
                    }
                }
            }
        };

        enemy[KOBOLDTYPE].pain = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(KOBOLD);
                    spr.setAng((short) plr.getAng());
                    newstatus(i, FLEE);
                }

                aimove(i);
                processfluid(i, zr_florhit, false);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checkexpl(plr, i);
            }
        };

        enemy[KOBOLDTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }


                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    spr.setAng((short) (EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()) & 2047));

                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) {
                        newstatus(i, FLEE);
                    }
                }

                if (checkdist(plr, i)) {
                    newstatus(i, ATTACK);
                }

                checkexpl(plr, i);
            }
        };

        enemy[KOBOLDTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        newstatus(i, FACE);
                    }
                }
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checkexpl(plr, i);
            }
        };

        enemy[KOBOLDTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                switch (checkfluid(i, zr_florhit)) {
                    case TYPELAVA:
                    case TYPEWATER:
                        spr.setZ(spr.getZ() + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                        break;
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                if (spr.getLotag() == 34) {
                    if (checksight(plr, i)) {
                        if (checkdist(plr, i)) {
                            spr.setAng((short) checksight_ang);
                            attack(plr, i);
                        }
                    }
                } else if (spr.getLotag() < 0) {
                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                        newstatus(i, FLEE);
                    } else {
                        newstatus(i, CHASE);
                    }
                }
                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                checksector6(i);
            }
        };

        enemy[KOBOLDTYPE].resurrect = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                    spr.setPicnum(KOBOLD);
                    spr.setHitag(adjusthp(30));
                    spr.setLotag(100);
                    spr.setCstat(spr.getCstat() | 1);
                }
            }
        };

        enemy[KOBOLDTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                if ((engine.krand() % 100) > 98) {
                    playsound_loc(S_KSNARL1 + (engine.krand() % 4), spr.getX(), spr.getY());
                }
                aisearch(plr, i, false);
                if (!checksector6(i)) {
                    checkexpl(plr, i);
                }
            }
        };

        enemy[KOBOLDTYPE].frozen = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPal(0);
                    spr.setPicnum(KOBOLD);
                    newstatus(i, FACE);
                }
            }
        };

        enemy[KOBOLDTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                if (game.WH2) {
                    chunksofmeat(plr, i, spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(), spr.getAng());
                    trailingsmoke(i, false);
                    newstatus(i, DIE);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(24);
                    if (spr.getPicnum() == KOBOLDCHAR + 4) {
                        trailingsmoke(i, false);
                        engine.deletesprite(i);
                    }
                }
            }
        };

        enemy[KOBOLDTYPE].skirmish = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                if (checksector6(i)) {
                    return;
                }

                checkexpl(plr, i);
            }
        };
    }

    public static void checkexpl(PLAYER ignoredPlr, int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        ListNode<Sprite> sectNode = boardService.getSectNode(spr.getSectnum());
        while (sectNode != null) {
            ListNode<Sprite> next = sectNode.getNext();
            Sprite secSpr = sectNode.get();
            long dx = klabs(spr.getX() - secSpr.getX()); // x distance to sprite
            long dy = klabs(spr.getY() - secSpr.getY()); // y distance to sprite
            long dz = klabs((spr.getZ() >> 8) - (secSpr.getZ() >> 8)); // z distance to sprite
            long dh = engine.getTile(secSpr.getPicnum()).getHeight() >> 1; // height of sprite
            if (dx + dy < PICKDISTANCE && dz - dh <= getPickHeight()) {
                if (secSpr.getPicnum() == EXPLO2
                        || secSpr.getPicnum() == SMOKEFX
                        || secSpr.getPicnum() == MONSTERBALL) {
                    spr.setHitag(spr.getHitag() - (TICSPERFRAME << 2));
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }
            }
            sectNode = next;
        }
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(KOBOLDTYPE);
        enemy[KOBOLDTYPE].info.set(spr);
        engine.changespritestat(i, FACE);

        if (spr.getPal() == 8) {
            spr.setHitag(adjusthp(40));
        } else if (spr.getPal() == 7) {
            spr.setHitag(adjusthp(60));
        }
    }
}
