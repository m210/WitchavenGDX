package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.addhealth;
import static ru.m210projects.Witchaven.WHSND.*;

public class AIGoblin {

    public static final AIState goblinChill = new AIState() {
        @Override
        public void process(PLAYER plr, int i) {
            Sprite spr = boardService.getSprite(i);
            if (spr == null) {
                engine.deletesprite(i);
                return;
            }

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                spr.setPicnum(spr.getPicnum() + 1);
                spr.setLotag(18);
                if (spr.getPicnum() == GOBLINSURPRISE + 5) {
                    spr.setPicnum(GOBLIN);
                    newstatus(i, FACE);
                }
            }
        }
    };
    public static final AIState goblinWar = new AIState() {
        @Override
        public void process(PLAYER plr, int i) {
            Sprite spr = boardService.getSprite(i);
            if (spr == null) {
                engine.deletesprite(i);
                return;
            }

            if (spr.getLotag() > 256) {
                spr.setLotag(100);
                spr.setExtra(0);
            }

            switch (spr.getExtra()) {
                case 0: // find new target
                    long olddist = 1024 << 4;
                    boolean found = false;
                    for (int k = 0; k < MAXSPRITES; k++) {
                        Sprite spr2 = boardService.getSprite(k);
                        if (spr2 == null) {
                            continue;
                        }

                        if (spr2.getPicnum() == GOBLIN && spr.getPal() != spr2.getPal() && spr.getHitag() == spr2.getHitag()) {
                            long dist = klabs(spr.getX() - spr2.getX()) + klabs(spr.getY() - spr2.getY());
                            if (dist < olddist) {
                                found = true;
                                olddist = dist;
                                spr.setOwner(k);
                                spr.setAng(EngineUtils.getAngle(spr2.getX() - spr.getX(), spr2.getY() - spr.getY()));
                                spr.setExtra(1);
                            }
                        }
                    }
                    if (!found) {
                        if (spr.getPal() == 5)
                            spr.setHitag(adjusthp(35));
                        else if (spr.getPal() == 4)
                            spr.setHitag(adjusthp(25));
                        else
                            spr.setHitag(adjusthp(15));
                        if (plr.getShadowtime() > 0) {
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                            newstatus(i, FLEE);
                        } else
                            newstatus(i, FACE);
                    }
                    break;
                case 1: {// chase
                    final int k = spr.getOwner();
                    Sprite ownerSpr = boardService.getSprite(k);
                    if (ownerSpr == null) {
                        break;
                    }

                    int movehit = aimove(i);
                    if (movehit == 0)
                        spr.setAng(EngineUtils.getAngle(ownerSpr.getX() - spr.getX(), ownerSpr.getY() - spr.getY()));
                    else if ((movehit & HIT_TYPE_MASK) == HIT_WALL) {
                        spr.setExtra(3);
                        spr.setAng((short) ((spr.getAng() + (engine.krand() & 256 - 128)) & 2047));
                        spr.setLotag(60);
                    } else if ((movehit & HIT_TYPE_MASK) == HIT_SPRITE) {
                        int sprnum = movehit & HIT_INDEX_MASK;
                        if (sprnum != k) {
                            spr.setExtra(3);
                            spr.setAng((short) ((spr.getAng() + (engine.krand() & 256 - 128)) & 2047));
                            spr.setLotag(60);
                        } else
                            spr.setAng(EngineUtils.getAngle(ownerSpr.getX() - spr.getX(), ownerSpr.getY() - spr.getY()));
                    }

                    processfluid(i, zr_florhit, false);

                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                    if (checkdist(i, ownerSpr.getX(), ownerSpr.getY(), ownerSpr.getZ())) {
                        spr.setExtra(2);
                    } else
                        spr.setPicnum(GOBLIN);

                    if (checksector6(i))
                        return;

                    break; }
                case 2: { // attack
                    final int k = spr.getOwner();
                    Sprite ownerSpr = boardService.getSprite(k);
                    if (ownerSpr == null) {
                        break;
                    }

                    if (checkdist(i, ownerSpr.getX(), ownerSpr.getY(), ownerSpr.getZ())) {
                        if ((engine.krand() & 1) != 0) {
                            // goblins are fighting
                            // JSA_DEMO
                            if (engine.krand() % 10 > 6)
                                playsound_loc(S_GENSWING, spr.getX(), spr.getY());
                            if (engine.krand() % 10 > 6)
                                playsound_loc(S_SWORD1 + (engine.krand() % 6), spr.getX(), spr.getY());

                            if (checkdist(plr, i))
                                addhealth(plr, -(engine.krand() & 5));

                            if (engine.krand() % 100 > 90) { // if k is dead
                                spr.setExtra(0); // needs to
                                spr.setPicnum(GOBLIN);
                                ownerSpr.setExtra(4);
                                ownerSpr.setPicnum(GOBLINDIE);
                                ownerSpr.setLotag(20);
                                ownerSpr.setHitag(0);
                                newstatus(k, DIE);
                            } else { // i attack k flee
                                spr.setExtra(0);
                                ownerSpr.setExtra(3);
                                ownerSpr.setAng((short) ((spr.getAng() + (engine.krand() & 256 - 128)) & 2047));
                                ownerSpr.setLotag(60);
                            }
                        }
                    } else {
                        spr.setExtra(1);
                    }

                    processfluid(i, zr_florhit, false);

                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                    if (checksector6(i))
                        return;

                    break; }
                case 3: // flee
                    spr.setLotag(spr.getLotag() - TICSPERFRAME);

                    if (aimove(i) != 0)
                        spr.setAng((short) (engine.krand() & 2047));
                    processfluid(i, zr_florhit, false);

                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                    if (spr.getLotag() < 0) {
                        spr.setLotag(0);
                        spr.setExtra(0);
                    }

                    if (checksector6(i))
                        return;

                    break;
                case 4: // pain
                    spr.setPicnum(GOBLINDIE);
                    break;
                case 5: // cast
                    break;
            }

            checkexpl(plr, i);
        }
    };

    public static void create() {
        enemy[GOBLINTYPE] = new Enemy();
        enemy[GOBLINTYPE].info = new EnemyInfo(36, 36, 1024, 120, 0, 64, false, 15, 0) {
            @Override
            public short getHealth(Sprite spr) {
                if (spr.getPal() == 5)
                    return adjusthp(35);
                else if (spr.getPal() == 4)
                    return adjusthp(25);

                return adjusthp(health);
            }
        };
        enemy[GOBLINTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0)
                    spr.setLotag(250);

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }

                if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum()) && plr.getInvisibletime() < 0) {
                    if (checkdist(plr, i)) {
                        if (plr.getShadowtime() > 0) {
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                        } else
                            newstatus(i, ATTACK);
                    } else if (engine.krand() % 63 > 60) {
                        spr.setAng((short) (((engine.krand() & 128 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        int movestat = aimove(i);
                        if ((movestat & HIT_TYPE_MASK) == HIT_FLOOR) {
                            spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                            return;
                        }

                        if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                            if ((movestat & HIT_INDEX_MASK) != plr.getSpritenum()) {
                                short daang = (short) ((spr.getAng() - 256) & 2047);
                                spr.setAng(daang);
                                if (plr.getShadowtime() > 0) {
                                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                    newstatus(i, FLEE);
                                } else
                                    newstatus(i, SKIRMISH);
                            } else {
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                newstatus(i, SKIRMISH);
                            }
                        }
                    }
                } else {
                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                    newstatus(i, FLEE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10))
                    warpsprite(i);

                if (checksector6(i))
                    return;

                processfluid(i, zr_florhit, false);

                if (osec.getLotag() == KILLSECTOR) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0)
                        newstatus(i, DIE);
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == LAVA
                        || sec.getFloorpicnum() == LAVA1 || sec.getFloorpicnum() == ANILAVA)) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0)
                        newstatus(i, DIE);
                }

                checkexpl(plr, i);
            }
        };

        enemy[GOBLINTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() <= 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(20);

                    if (spr.getPicnum() == GOBLINDEAD) {
                        if (difficulty == 4)
                            newstatus(i, RESURECT);
                        else {
                            kills++;
                            newstatus(i, DEAD);
                        }
                    }
                }
            }
        };

        enemy[GOBLINTYPE].pain = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(GOBLIN);
                    spr.setAng((short) plr.getAng());
                    newstatus(i, FLEE);
                }

                aimove(i);
                processfluid(i, zr_florhit, false);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checkexpl(plr, i);
            }
        };

        enemy[GOBLINTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    spr.setPicnum(GOBLIN);
                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) newstatus(i, FLEE);
                }

                if (checkdist(plr, i))
                    newstatus(i, ATTACK);

                checkexpl(plr, i);
            }
        };

        enemy[GOBLINTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        newstatus(i, FACE);
                    }
                }
                if (spr.getLotag() < 0)
                    newstatus(i, FACE);

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10))
                    warpsprite(i);

                if (checksector6(i))
                    return;

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checkexpl(plr, i);
            }
        };

        enemy[GOBLINTYPE].stand = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                if (EngineUtils.sin((spr.getAng() + 2560) & 2047) * (plr.getX() - spr.getX())
                        + EngineUtils.sin((spr.getAng() + 2048) & 2047) * (plr.getY() - spr.getY()) >= 0)
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                            spr.getSectnum()) && plr.getInvisibletime() < 0) {
                        if (spr.getPicnum() == GOBLINCHILL) {
                            spr.setPicnum(GOBLINSURPRISE);
                            playsound_loc(S_GOBPAIN1 + (engine.krand() % 2), spr.getX(), spr.getY());
                            newstatus(i, CHILL);
                        } else {
                            spr.setPicnum(GOBLIN);
                            if (plr.getShadowtime() > 0) {
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                                newstatus(i, FLEE);
                            } else
                                newstatus(i, CHASE);
                        }
                    }

                checksector6(i);
            }
        };

        enemy[GOBLINTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                switch (checkfluid(i, zr_florhit)) {
                    case TYPELAVA:
                        spr.setHitag(spr.getHitag() - 1);
                        if (spr.getHitag() < 0)
                            newstatus(i, DIE);
                    case TYPEWATER:
                        spr.setZ(spr.getZ() + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                        break;
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                if (spr.getLotag() == 31) {
                    if (checksight(plr, i))
                        if (checkdist(plr, i)) {
                            spr.setAng((short) checksight_ang);
                            attack(plr, i);
                        }
                } else if (spr.getLotag() < 0) {
                    spr.setPicnum(GOBLIN);
                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                        newstatus(i, FLEE);
                    } else
                        newstatus(i, CHASE);
                }
                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                checksector6(i);
            }
        };

        enemy[GOBLINTYPE].resurrect = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                    spr.setPicnum(GOBLIN);
                    spr.setHitag(adjusthp(35));
                    spr.setLotag(100);
                    spr.setCstat(spr.getCstat() | 1);
                }
            }
        };

        enemy[GOBLINTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, false);
                if (!checksector6(i))
                    checkexpl(plr, i);
            }
        };

        enemy[GOBLINTYPE].frozen = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPal(0);
                    spr.setPicnum(GOBLIN);
                    newstatus(i, FACE);
                }
            }
        };

        enemy[GOBLINTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(24);
                    if (spr.getPicnum() == GOBLINCHAR + 4) {
                        trailingsmoke(i, false);
                        engine.deletesprite(i);
                    }
                }
            }
        };

        enemy[GOBLINTYPE].skirmish = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() < 0)
                    newstatus(i, FACE);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }

                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10))
                    warpsprite(i);

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                if (checksector6(i))
                    return;

                checkexpl(plr, i);
            }
        };
    }

    public static void goblinWarProcess(PLAYER plr) {
        ListNode<Sprite> node = boardService.getStatNode(WAR);
        while (node != null) {
            ListNode<Sprite> next = node.getNext();
            Sprite spr = node.get();
            if (spr.getDetail() == GOBLINTYPE) {
                goblinWar.process(plr, node.getIndex());
            }
            node = next;
        }
    }

    public static void checkexpl(PLAYER ignoredPlr, int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        ListNode<Sprite> sectNode = boardService.getSectNode(spr.getSectnum());
        while (sectNode != null) {
            ListNode<Sprite> next = sectNode.getNext();
            Sprite secSprite = sectNode.get();
            long dx = klabs(spr.getX() - secSprite.getX()); // x distance to sprite
            long dy = klabs(spr.getY() - secSprite.getY()); // y distance to sprite
            long dz = klabs((spr.getZ() >> 8) - (secSprite.getZ() >> 8)); // z distance to sprite
            long dh = engine.getTile(secSprite.getPicnum()).getHeight() >> 1; // height of sprite
            if (dx + dy < PICKDISTANCE && dz - dh <= getPickHeight()) {
                if (secSprite.getPicnum() == EXPLO2
                        || secSprite.getPicnum() == SMOKEFX
                        || secSprite.getPicnum() == MONSTERBALL) {
                    spr.setHitag(spr.getHitag() - (TICSPERFRAME << 2));
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }
            }
            sectNode = next;
        }
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(GOBLINTYPE);

        if (spr.getHitag() < 90 || spr.getHitag() > 99)
            enemy[GOBLINTYPE].info.set(spr);
        else {
            short ohitag = spr.getHitag();
            enemy[GOBLINTYPE].info.set(spr);
            if (spr.getPal() != 0)
                spr.setXrepeat(30);
            spr.setExtra(0);
            spr.setOwner(0);
            spr.setHitag(ohitag);
            return;
        }

        if (spr.getPicnum() == GOBLINCHILL) {
            engine.changespritestat(i, STAND);
            spr.setLotag(30);
            if (engine.krand() % 100 > 50)
                spr.setExtra(1);
            return;
        }

        engine.changespritestat(i, FACE);
        if (engine.krand() % 100 > 50)
            spr.setExtra(1);
    }
}
