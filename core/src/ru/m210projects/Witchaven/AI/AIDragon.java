package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.Names.MONSTERBALL;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHSND.*;

public class AIDragon {

    public static final AIState dragonAttack2 = new AIState() {
        @Override
        public void process(PLAYER plr, int i) {
            Sprite spr = boardService.getSprite(i);
            if (spr == null) {
                engine.deletesprite(i);
                return;
            }

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum()) && plr.getInvisibletime() < 0) {
                    newstatus(i, CAST);
                } else {
                    newstatus(i, CHASE);
                }
                return;
            } else {
                spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
            }

            checksector6(i);
        }
    };

    public static void create() {
        enemy[DRAGONTYPE] = new Enemy();
        enemy[DRAGONTYPE].info = new EnemyInfo(54, 54, 512, 120, 0, 128, false, 900, 0) {
            @Override
            public int getAttackDist(Sprite spr) {
                int out = attackdist;
                if (spr.getPicnum() > DRAGONATTACK + 2 && spr.getPicnum() < DRAGONATTACK + 17 || spr.getPicnum() > DRAGONATTACK2 && spr.getPicnum() < DRAGONATTACK2 + 5) {
                    out *= 4;
                }
                return out;
            }
        };
        enemy[DRAGONTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }

//				int speed = 10;
                if ((engine.krand() % 16) == 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum()) && plr.getInvisibletime() < 0) {
                        if (plr.getZ() < spr.getZ()) {
                            newstatus(i, ATTACK2);
                        } else {
                            newstatus(i, ATTACK);
                        }
                    }
                    return;
                } else {
                    int dax = (EngineUtils.sin((spr.getAng() + 512) & 2047) * TICSPERFRAME) << 3;
                    int day = (EngineUtils.sin(spr.getAng() & 2047) * TICSPERFRAME) << 3;
//					checkspeed(i, speed);
                    checksight(plr, i);
                    if (!checkdist(plr, i)) {
//						checkmove(i, checksight_x, checksight_y);
                        checkmove(i, dax, day);
                    } else {
                        if (plr.getInvisibletime() < 0) {
                            if (engine.krand() % 8 == 0) { // NEW
                                if (plr.getZ() < spr.getZ()) {
                                    newstatus(i, ATTACK2);
                                } else {
                                    newstatus(i, ATTACK);
                                }
                            } else { // NEW
                                newstatus(i, FACE); // NEW
                            }
                        }
                    }
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                if (osec.getLotag() == KILLSECTOR) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[DRAGONTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                short osectnum = spr.getSectnum();

                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_SECTOR && movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        newstatus(i, FACE);
                    }
                }
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[DRAGONTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() <= 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(20);
                    if (spr.getPicnum() == DRAGONDEAD) {
                        if (difficulty == 4) {
                            newstatus(i, RESURECT);
                        } else {
                            kills++;
                            newstatus(i, DEAD);
                        }
                    }
                }
            }
        };

        enemy[DRAGONTYPE].cast = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(12);
                }

                switch (spr.getPicnum()) {
                    case DRAGONATTACK + 17:
                    case DRAGONATTACK + 4:
                        if ((engine.krand() % 2) != 0) {
                            playsound_loc(S_FLAME1, spr.getX(), spr.getY());
                        } else {
                            playsound_loc(S_FIREBALL, spr.getX(), spr.getY());
                        }

                        firebreath(plr, i, 1, 2, LOW);
                        break;
                    case DRAGONATTACK + 18:
                    case DRAGONATTACK + 5:
                        if ((engine.krand() % 2) != 0) {
                            playsound_loc(S_FLAME1, spr.getX(), spr.getY());
                        } else {
                            playsound_loc(S_FIREBALL, spr.getX(), spr.getY());
                        }

                        firebreath(plr, i, 2, 1, LOW);
                        break;
                    case DRAGONATTACK + 19:
                    case DRAGONATTACK + 6:
                        if ((engine.krand() % 2) != 0) {
                            playsound_loc(S_FLAME1, spr.getX(), spr.getY());
                        } else {
                            playsound_loc(S_FIREBALL, spr.getX(), spr.getY());
                        }

                        firebreath(plr, i, 4, 0, LOW);
                        break;
                    case DRAGONATTACK + 20:
                    case DRAGONATTACK + 7:
                        firebreath(plr, i, 2, -1, LOW);
                        break;
                    case DRAGONATTACK + 21:
                    case DRAGONATTACK + 8:
                        firebreath(plr, i, 1, -2, LOW);
                        break;

                    case DRAGONATTACK2 + 2:
                        if ((engine.krand() % 2) != 0) {
                            playsound_loc(S_FLAME1, spr.getX(), spr.getY());
                        } else {
                            playsound_loc(S_FIREBALL, spr.getX(), spr.getY());
                        }

                        firebreath(plr, i, 1, -1, HIGH);
                        break;
                    case DRAGONATTACK2 + 3:
                        firebreath(plr, i, 2, 0, HIGH);
                        break;

                    case DRAGONATTACK2 + 5:
                        spr.setPicnum(DRAGON);
                        newstatus(i, CHASE);
                        break;
                    case DRAGONATTACK + 22:
                        spr.setPicnum(DRAGONATTACK);
                        newstatus(i, CHASE);
                        break;
                }

                checksector6(i);
            }
        };

        enemy[DRAGONTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                        newstatus(i, CAST);
                    } else {
                        newstatus(i, CHASE);
                    }
                } else {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                }
            }
        };

        enemy[DRAGONTYPE].resurrect = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                    spr.setPicnum(DRAGON);
                    spr.setHitag(adjusthp(900));
                    spr.setLotag(100);
                    spr.setCstat(spr.getCstat() | 1);
                }
            }
        };

        enemy[DRAGONTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, true);
                checksector6(i);
            }
        };

        enemy[DRAGONTYPE].frozen = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPal(0);
                    spr.setPicnum(DRAGON);
                    newstatus(i, FACE);
                }
            }
        };

        enemy[DRAGONTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(24);
                    if (spr.getPicnum() == DRAGONCHAR + 4) {
                        trailingsmoke(i, false);
                        engine.deletesprite(i);
                    }
                }
            }
        };

        enemy[DRAGONTYPE].pain = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                aimove(i);
                processfluid(i, zr_florhit, false);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[DRAGONTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }


                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) {
                        newstatus(i, FLEE);
                    }
                }

                if (checkdist(plr, i)) {
                    newstatus(i, ATTACK);
                }
            }
        };
    }

    public static void dragonProcess(PLAYER plr) {
        ListNode<Sprite> next;
        for (ListNode<Sprite> node = boardService.getStatNode(ATTACK2); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            if (spr.getDetail() == DRAGON) {
                dragonAttack2.process(plr, node.getIndex());
            }
        }
    }

    public static void firebreath(PLAYER plr, final int i, int a, int b, int c) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return;
        }

        for (int k = 0; k <= a; k++) {
            int j = engine.insertsprite(spr.getSectnum(), MISSILE);
            Sprite spr2 = boardService.getSprite(j);
            if (spr2 == null) {
                return;
            }

            spr2.setX(spr.getX());
            spr2.setY(spr.getY());
            if (c == LOW) {
                spr2.setZ(sec.getFloorz() - (32 << 8));
            } else {
                spr2.setZ(sec.getFloorz() - (engine.getTile(spr.getPicnum()).getHeight() << 7));
            }
            spr2.setCstat(0);
            spr2.setPicnum(MONSTERBALL);
            spr2.setShade(-15);
            spr2.setXrepeat(128);
            spr2.setYrepeat(128);
            spr2.setAng((short) ((((EngineUtils.getAngle(plr.getX() - spr2.getX(), plr.getY() - spr2.getY()) + (engine.krand() & 15) - 8) + 2048) + ((b * 22) + (k * 10))) & 2047));
            spr2.setXvel((short) (EngineUtils.sin((spr2.getAng() + 2560) & 2047) >> 6));
            spr2.setYvel((short) (EngineUtils.sin((spr2.getAng() + 2048) & 2047) >> 6));
            long discrim = EngineUtils.sqrt((plr.getX() - spr2.getX()) * (plr.getX() - spr2.getX()) + (plr.getY() - spr2.getY()) * (plr.getY() - spr2.getY()));
            if (discrim == 0) {
                discrim = 1;
            }
            if (c == HIGH) {
                spr2.setZvel((short) (((plr.getZ() + (32 << 8) - spr2.getZ()) << 7) / discrim));
            } else {
                spr2.setZvel((short) ((((plr.getZ() + (8 << 8)) - spr2.getZ()) << 7) / discrim));// NEW
            }

            spr2.setOwner((short) i);
            spr2.setClipdist(16);
            spr2.setLotag(512);
            spr2.setHitag(0);
        }
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(DRAGONTYPE);
        engine.changespritestat(i, FACE);
        enemy[DRAGONTYPE].info.set(spr);
    }

}
