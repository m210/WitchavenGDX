package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.Names.FATSPANK;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHSND.S_WITCHTHROW;
import static ru.m210projects.Witchaven.WHSND.playsound_loc;

public class AIFatwitch {

    public static void create() {
        enemy[FATWITCHTYPE] = new Enemy();
        enemy[FATWITCHTYPE].info = new EnemyInfo(32, 32, 2048, 120, 0, 64, false, 280, 0);
        enemy[FATWITCHTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }

                if (engine.krand() % 63 == 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                            spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum()))// && invisibletime < 0)
                    {
                        newstatus(i, ATTACK);
                    }
                } else {
                    checksight(plr, i);
                    if (!checkdist(i, plr.getX(), plr.getY(), plr.getZ())) {
                        int movestat = aimove(i);
                        if ((movestat & HIT_TYPE_MASK) == HIT_FLOOR) {
                            spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                            return;
                        }
                    } else {
                        if (engine.krand() % 8 == 0) // NEW
                        {
                            newstatus(i, ATTACK); // NEW
                        } else { // NEW
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                            newstatus(i, FLEE); // NEW
                        }
                    }
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                if (osec.getLotag() == KILLSECTOR) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[FATWITCHTYPE].resurrect = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                    spr.setPicnum(FATWITCH);
                    spr.setHitag(adjusthp(90));
                    spr.setLotag(100);
                    spr.setCstat(spr.getCstat() | 1);
                }
            }
        };

        enemy[FATWITCHTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, false);
                checksector6(i);
            }
        };

        enemy[FATWITCHTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(24);
                    if (spr.getPicnum() == FATWITCHCHAR + 4) {
                        trailingsmoke(i, false);
                        engine.deletesprite(i);
                    }
                }
            }
        };

        enemy[FATWITCHTYPE].pain = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(FATWITCH);
                    spr.setAng((short) plr.getAng());
                    newstatus(i, FLEE);
                }

                aimove(i);
                processfluid(i, zr_florhit, false);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[FATWITCHTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) {
                        newstatus(i, FLEE);
                    }
                }

                if (checkdist(i, plr.getX(), plr.getY(), plr.getZ())) {
                    newstatus(i, ATTACK);
                }
            }
        };

        enemy[FATWITCHTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                switch (checkfluid(i, zr_florhit)) {
                    case TYPELAVA:
                    case TYPEWATER:
                        spr.setZ(spr.getZ() + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                        break;
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                            spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                        newstatus(i, CAST);
                    } else {
                        newstatus(i, CHASE);
                    }
                } else {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                }
            }
        };

        enemy[FATWITCHTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        newstatus(i, FACE);
                    }
                }
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[FATWITCHTYPE].cast = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(12);
                }

                if (spr.getPicnum() == FATWITCHATTACK + 3) {
                    spr.setPicnum(FATWITCH);
                    throwspank(plr, i);
                    newstatus(i, CHASE);
                }
                checksector6(i);
            }
        };

        enemy[FATWITCHTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() <= 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(20);

                    if (spr.getPicnum() == FATWITCHDEAD) {
                        if (difficulty == 4) {
                            newstatus(i, RESURECT);
                        } else {
                            kills++;
                            newstatus(i, DEAD);
                        }
                    }
                }
            }
        };
    }

    public static void throwspank(PLAYER plr, int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }
        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), MISSILE);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }
        playsound_loc(S_WITCHTHROW, spr.getX(), spr.getY());

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(sec.getFloorz() - ((engine.getTile(spr.getPicnum()).getHeight() >> 1) << 8));
        spr2.setCstat(0); // Hitscan does not hit other bullets
        spr2.setPicnum(FATSPANK);
        spr2.setShade(-15);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
        spr2.setAng((short) (((EngineUtils.getAngle(plr.getX() - spr2.getX(), plr.getY() - spr2.getY()) + (engine.krand() & 15)
                - 8) + 2048) & 2047));
        spr2.setXvel((short) (EngineUtils.sin((spr2.getAng() + 2560) & 2047) >> 6));
        spr2.setYvel((short) (EngineUtils.sin((spr2.getAng() + 2048) & 2047) >> 6));
        long discrim = EngineUtils.sqrt((plr.getX() - spr2.getX()) * (plr.getX() - spr2.getX()) + (plr.getY() - spr2.getY()) * (plr.getY() - spr2.getY()));
        if (discrim == 0) {
            discrim = 1;
        }
        spr2.setZvel((short) (((plr.getZ() + (48 << 8) - spr2.getZ()) << 7) / discrim));
        spr2.setOwner((short) i);
        spr2.setClipdist(16);
        spr2.setLotag(512);
        spr2.setHitag(0);
        spr2.setPal(0);
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(FATWITCHTYPE);
        engine.changespritestat(i, FACE);
        enemy[FATWITCHTYPE].info.set(spr);
        if (spr.getPal() == 7) {
            spr.setHitag(adjusthp(290));
        }
        if (engine.krand() % 100 > 50) {
            spr.setExtra(1);
        }

    }
}
