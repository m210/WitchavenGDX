package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.chunksofmeat;
import static ru.m210projects.Witchaven.WHSND.*;

public class AINewguy {

    public static void create() {
        engine.getTile(NEWGUYDIE).setFlags(0);
        engine.getTile(NEWGUYDIE + 3).setFlags(0);
        enemy[NEWGUYTYPE] = new Enemy();
        enemy[NEWGUYTYPE].info = new EnemyInfo(35, 35, 1024 + 256, 120, 0, 48, false, 90, 0) {
            @Override
            public int getAttackDist(Sprite spr) {
                int out;
                switch (spr.getPicnum()) {
                    case NEWGUY:
                    case NEWGUYMACE:
                    case NEWGUYCAST:
                    case NEWGUYBOW:
                        if (spr.getExtra() > 10) {
                            out = 2048 << 1;
                        } else {
                            out = 1024 + 256;
                        }
                        break;
                    case NEWGUYPUNCH:
                        out = 1024 + 256;
                        break;
                    default:
                        out = 512;
                        break;
                }

                return out;
            }

            @Override
            public short getHealth(Sprite spr) {
                switch (spr.getPicnum()) {
                    case NEWGUYSTAND:
                    case NEWGUYKNEE:
                        return adjusthp(50);
                    case NEWGUYCAST:
                    case NEWGUYBOW:
                        return adjusthp(85);
                    case NEWGUYMACE:
                        return adjusthp(45);
                    case NEWGUYPUNCH:
                        return adjusthp(15);
                }

                return adjusthp(health);
            }
        };
        enemy[NEWGUYTYPE].stand = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                if (EngineUtils.sin((spr.getAng() + 512) & 2047) * (plr.getX() - spr.getX())
                        + EngineUtils.sin(spr.getAng() & 2047) * (plr.getY() - spr.getY()) >= 0) {
                    if (engine.cansee(spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum(), plr.getX(), plr.getY(),
                            plr.getZ(), plr.getSector()) && plr.getInvisibletime() < 0) {
                        if (plr.getShadowtime() > 0) {
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                        } else {
                            newstatus(i, CHASE);
                        }
                    }
                }
            }
        };

        enemy[NEWGUYTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum()) && plr.getInvisibletime() < 0) {
                    if (checkdist(plr, i)) {
                        if (plr.getShadowtime() > 0) {
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                        } else {
                            newstatus(i, ATTACK);
                        }
                    } else if (engine.krand() % 63 > 60) {
                        spr.setAng((short) (((engine.krand() & 128 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        int movestat = aimove(i);
                        if ((movestat & HIT_TYPE_MASK) == HIT_FLOOR) {
                            spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                            return;
                        }

                        if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                            if ((movestat & HIT_INDEX_MASK) != plr.getSpritenum()) {
                                short daang = (short) ((spr.getAng() - 256) & 2047);
                                spr.setAng(daang);
                                if (plr.getShadowtime() > 0) {
                                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                    newstatus(i, FLEE);
                                } else {
                                    newstatus(i, SKIRMISH);
                                }
                            } else {
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                newstatus(i, SKIRMISH);
                            }
                        }
                    }
                } else {
                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                    newstatus(i, FLEE);
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);
                Sector sec = boardService.getSector(spr.getSectnum());

                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                if (osec.getLotag() == KILLSECTOR) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                sec = boardService.getSector(spr.getSectnum());

                if (sec != null && (zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == LAVA
                        || sec.getFloorpicnum() == LAVA1 || sec.getFloorpicnum() == ANILAVA)) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }
            }
        };

        enemy[NEWGUYTYPE].resurrect = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                    int j = engine.krand() % 4;
                    switch (j) {
                        case 0:
                            spr.setExtra(30);
                            spr.setHitag(adjusthp(85));
                            break;
                        case 1:
                            spr.setExtra(20);
                            spr.setHitag(adjusthp(85));
                            break;
                        case 2:
                            spr.setExtra(10);
                            spr.setHitag(adjusthp(45));
                            break;
                        case 3:
                            spr.setExtra(0);
                            spr.setHitag(adjusthp(15));
                            break;
                    }
                    spr.setXrepeat(35);
                    spr.setYrepeat(35);
                    spr.setPicnum(NEWGUY);
                }
            }
        };

        enemy[NEWGUYTYPE].skirmish = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    newstatus(i, FACE);
                }
                Sector sec = boardService.getSector(spr.getSectnum());

                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checksector6(i);
            }
        };

        enemy[NEWGUYTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, false);
                checksector6(i);
            }
        };

        enemy[NEWGUYTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                chunksofmeat(plr, i, spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(), spr.getAng());
                trailingsmoke(i, false);
                newstatus(i, DIE);
            }
        };

        enemy[NEWGUYTYPE].pain = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(NEWGUY);
                    spr.setAng((short) plr.getAng());
                    newstatus(i, FLEE);
                }

                aimove(i);
                processfluid(i, zr_florhit, false);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[NEWGUYTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) {
                        newstatus(i, FLEE);
                    }
                }

                if (checkdist(plr, i)) {
                    newstatus(i, ATTACK);
                }
            }
        };

        enemy[NEWGUYTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        if (plr.getInvisibletime() < 0) {
                            spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        }
                        newstatus(i, FACE);
                    }
                }

                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());

                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[NEWGUYTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                switch (checkfluid(i, zr_florhit)) {
                    case TYPELAVA:
                    case TYPEWATER:
                        spr.setZ(spr.getZ() + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                        break;
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                switch (spr.getPicnum()) {
                    case NEWGUYCAST:
                    case NEWGUYBOW:
                        spr.setLotag(spr.getLotag() - TICSPERFRAME);
                        if (spr.getLotag() < 0) {
                            if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                                    spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                                newstatus(i, CAST);
                            } else {
                                newstatus(i, CHASE);
                            }
                        } else {
                            spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        }
                        break;
                    case NEWGUYMACE:
                    case NEWGUYPUNCH:
                        if (spr.getLotag() == 31) {
                            if (checksight(plr, i)) {
                                if (checkdist(plr, i)) {
                                    spr.setAng((short) checksight_ang);
                                    attack(plr, i);
                                }
                            }
                        } else if (spr.getLotag() < 0) {
                            if (plr.getShadowtime() > 0) {
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                                newstatus(i, FLEE);
                            } else {
                                newstatus(i, CHASE);
                            }
                        }
                        spr.setLotag(spr.getLotag() - TICSPERFRAME);

                        checksector6(i);
                        break;
                }
            }
        };

        enemy[NEWGUYTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() <= 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(20);

                    if (spr.getPicnum() == NEWGUYDEAD) {
                        if (difficulty == 4) {
                            newstatus(i, RESURECT);
                        } else {
                            kills++;
                            newstatus(i, DEAD);
                        }
                    }
                }
            }
        };

        enemy[NEWGUYTYPE].cast = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(12);
                }

                if (spr.getPicnum() == NEWGUYCAST + 2) {
                    spr.setExtra(spr.getExtra() - 1);
                    spr.setPicnum(NEWGUY);
                    playsound_loc(S_WISP, spr.getX(), spr.getY());
                    skullycastspell(plr, i);
                    newstatus(i, CHASE);
                }
                if (spr.getPicnum() == NEWGUYBOW + 2) {
                    spr.setExtra(spr.getExtra() - 1);
                    spr.setPicnum(NEWGUY);
                    playsound_loc(S_PLRWEAPON3, spr.getX(), spr.getY());
                    newguyarrow(i, plr);
                    newstatus(i, CHASE);
                }
                checksector6(i);
            }
        };
    }

    public static void newguyarrow(int s, PLAYER plr) {
        Sprite spr = boardService.getSprite(s);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), JAVLIN);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }


        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ() - (40 << 8));

        spr2.setCstat(21);

        spr2.setPicnum(WALLARROW);
        spr2.setAng((short) (((spr.getAng() + 2048 + 96) - 512) & 2047));
        spr2.setXrepeat(24);
        spr2.setYrepeat(24);
        spr2.setClipdist(32);

        spr2.setExtra(spr.getAng());
        spr2.setShade(-15);
        spr2.setXvel((short) ((engine.krand() & 256) - 128));
        spr2.setYvel((short) ((engine.krand() & 256) - 128));

        spr2.setZvel((short) (((plr.getZ() + (8 << 8) - spr.getZ()) << 7) / EngineUtils.sqrt((plr.getX() - spr.getX()) * (plr.getX() - spr.getX()) + (plr.getY() - spr.getY()) * (plr.getY() - spr.getY()))));

        spr2.setZvel(spr2.getZvel() + ((engine.krand() % 256) - 128));

        spr2.setOwner(s);
        spr2.setLotag(1024);
        spr2.setHitag(0);
        spr2.setPal(0);
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(NEWGUYTYPE);

        enemy[NEWGUYTYPE].info.set(spr);

        switch (spr.getPicnum()) {
            case NEWGUYSTAND:
            case NEWGUYKNEE:
                engine.changespritestat(i, STAND);
                if (spr.getPicnum() == NEWGUYSTAND) {
                    spr.setExtra(20);
                } else {
                    spr.setExtra(30);
                }
                break;
            case NEWGUYCAST:
            case NEWGUYBOW:
            case NEWGUYMACE:
            case NEWGUYPUNCH:
            case NEWGUY:
                switch (spr.getPicnum()) {
                    case NEWGUYCAST:
                        spr.setExtra(30);
                        break;
                    case NEWGUYBOW:
                        spr.setExtra(20);
                        break;
                    case NEWGUYMACE:
                        spr.setExtra(10);
                        break;
                    case NEWGUYPUNCH:
                        spr.setExtra(0);
                        break;
                }
                engine.changespritestat(i, FACE);
                spr.setPicnum(NEWGUY);
        }
    }
}
