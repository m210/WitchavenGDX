package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WH2Names.DEMON;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.newstatus;
import static ru.m210projects.Witchaven.WHOBJ.trailingsmoke;
import static ru.m210projects.Witchaven.WHPLR.chunksofmeat;

public class AIDemon {

    public static void create() {
        enemy[DEMONTYPE] = new Enemy();
        enemy[DEMONTYPE].info = new EnemyInfo(38, 41, 4096 + 2048, 120, 0, 64, true, 300, 0);
        enemy[DEMONTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, final int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                if (plr.getZ() < spr.getZ()) {
                    spr.setZ(spr.getZ() - (TICSPERFRAME << 8));
                }
                if (plr.getZ() > spr.getZ()) {
                    spr.setZ(spr.getZ() + (TICSPERFRAME << 8));
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }

                if (engine.krand() % 63 == 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                            spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum()) && plr.getInvisibletime() < 0) {
                        newstatus(i, ATTACK);
                    }
                    return;
                } else {
                    if (lockclock % 100 > 70) {
                        trailingsmoke(i, true);
                    }

                    int dax = ((EngineUtils.sin((spr.getAng() + 512) & 2047) * TICSPERFRAME) << 2);
                    int day = ((EngineUtils.sin(spr.getAng() & 2047) * TICSPERFRAME) << 2);
                    checksight(plr, i);


                    if (!checkdist(plr, i)) {
                        checkmove(i, dax, day);
                    } else {
                        if (plr.getInvisibletime() < 0) {
                            if (engine.krand() % 8 == 0) // NEW
                            {
                                newstatus(i, ATTACK); // NEW
                            } else { // NEW
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                                newstatus(i, CHASE); // NEW
                            }
                        }
                    }
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                checksector6(i);

                processfluid(i, zr_florhit, true);

                if (osec.getLotag() == KILLSECTOR && spr.getZ() + (8 << 8) >= osec.getFloorz()) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                sec = boardService.getSector(spr.getSectnum());

                if (sec != null && ((zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == LAVA
                        || sec.getFloorpicnum() == LAVA1 || sec.getFloorpicnum() == ANILAVA))) {
                    if (spr.getZ() + (8 << 8) >= osec.getFloorz()) {
                        spr.setHitag(spr.getHitag() - 1);
                        if (spr.getHitag() < 0) {
                            newstatus(i, DIE);
                        }
                    }
                }
            }
        };

        enemy[DEMONTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, true);
                checksector6(i);
            }
        };

        enemy[DEMONTYPE].pain = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(DEMON);
                    spr.setAng((short) plr.getAng());
                    newstatus(i, FLEE);
                }

                aifly(i);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[DEMONTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    spr.setAng((short) (EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()) & 2047));
                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) {
                        newstatus(i, FLEE);
                    }
                }

                if (checkdist(plr, i)) {
                    newstatus(i, ATTACK);
                }
            }
        };

        enemy[DEMONTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                if (plr.getZ() < spr.getZ()) {
                    spr.setZ(spr.getZ() - (TICSPERFRAME << 8));
                }
                if (plr.getZ() > spr.getZ()) {
                    spr.setZ(spr.getZ() + (TICSPERFRAME << 8));
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                            spr.getSectnum())) {
                        newstatus(i, CAST);
                    } else {
                        newstatus(i, CHASE);
                    }
                } else {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                }
            }
        };

        enemy[DEMONTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                short osectnum = spr.getSectnum();

                int movestat = aifly(i);

                if (movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        newstatus(i, FACE);
                    }
                }
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, true);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[DEMONTYPE].cast = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (plr.getZ() < spr.getZ()) {
                    spr.setZ(spr.getZ() - (TICSPERFRAME << 8));
                }
                if (plr.getZ() > spr.getZ()) {
                    spr.setZ(spr.getZ() + (TICSPERFRAME << 8));
                }

                if (spr.getLotag() < 0) {
                    castspell(plr, i);
                    newstatus(i, CHASE);
                }
            }
        };

        enemy[DEMONTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                chunksofmeat(plr, i, spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(), spr.getAng());
                trailingsmoke(i, false);
                newstatus(i, DIE);
            }
        };
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(DEMONTYPE);
        engine.changespritestat(i, FACE);
        enemy[DEMONTYPE].info.set(spr);
    }
}
