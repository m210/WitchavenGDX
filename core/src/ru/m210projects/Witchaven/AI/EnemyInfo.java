package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Witchaven.WHOBJ.adjusthp;

public class EnemyInfo {

    public final short sizx;
    public final short sizy;
    public final int attackdamage;
    public final int attackheight;
    public final int clipdist;
    public final boolean fly;
    public final int score;
    protected final int attackdist;
    protected final short health;

    public EnemyInfo(int sizx, int sizy, int dist, int height, int damage, int clipdist, boolean fly, int health, int score) {
        this.sizx = (short) sizx;
        this.sizy = (short) sizy;
        this.attackdist = dist;
        this.attackheight = height;
        this.attackdamage = damage;
        this.clipdist = clipdist;
        this.fly = fly;
        this.health = (short) health;
        this.score = score;
    }

    public short getHealth(Sprite spr) {
        return adjusthp(health);
    }

    public int getAttackDist(Sprite spr) {
        return attackdist;
    }

    public void set(Sprite spr) {
        spr.setClipdist(clipdist);
        spr.setHitag(getHealth(spr));
        if (sizx != -1) {
            spr.setXrepeat(sizx);
        }
        if (sizy != -1) {
            spr.setYrepeat(sizy);
        }
        spr.setLotag(100);

        int tflag = 0;
        if ((spr.getCstat() & 514) != 0) {
            tflag = spr.getCstat() & 514;
        }

        spr.setCstat((short) (0x101 | tflag));
    }
}
