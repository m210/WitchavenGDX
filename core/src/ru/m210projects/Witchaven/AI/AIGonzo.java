package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Screens.DemoScreen.checkCodeVersion;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHFX.shards;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.chunksofmeat;
import static ru.m210projects.Witchaven.WHPLR.getPlayerHeight;
import static ru.m210projects.Witchaven.WHSND.S_GENTHROW;
import static ru.m210projects.Witchaven.WHSND.playsound_loc;

public class AIGonzo {

    public static void gonzoProcess(PLAYER plr) {
        ListNode<Sprite> next;
        for (ListNode<Sprite> node = boardService.getStatNode(LAND); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                spr.setLotag(12);
                spr.setPicnum(spr.getPicnum() + 1);
            }

            switch (spr.getPicnum()) {
                case GONZOHMJUMPEND:
                case GONZOSHJUMPEND:
                    spr.setPicnum(GONZOGSH);
                    spr.setDetail(GONZOTYPE);
                    enemy[GONZOTYPE].info.set(spr);
                    spr.setHitag(adjusthp(100));
                    newstatus(node.getIndex(), FACE);
                    break;
            }
        }

        int movestat;
        for (ListNode<Sprite> node = boardService.getStatNode(AMBUSH); node != null; node = next) {
            next = node.getNext();
            Sprite spr = node.get();
            int i = node.getIndex();
            switch (spr.getExtra()) {
                case 1: // forward
                    spr.setZvel(spr.getZvel() + (TICSPERFRAME << 3));

                    movestat = movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                            ((EngineUtils.sin(spr.getAng())) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 0);

                    spr.setLotag(spr.getLotag() - TICSPERFRAME);

                    if (zr_florz <= spr.getZ() && spr.getLotag() < 0) {
                        spr.setZ(zr_florz);
                        engine.changespritestat(i, LAND);
                    }

                    if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) { // Bullet hit a sprite
                        int k = (movestat & HIT_INDEX_MASK);
                        for (int j = 0; j < 15; j++) {
                            shards(k, 1);
                        }
                        damageactor(plr, movestat, i);
                    }

                    break;
                case 2: // fall
                    spr.setZvel(spr.getZvel() + (TICSPERFRAME << 4));

                    movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 1,
                            ((EngineUtils.sin(spr.getAng())) * TICSPERFRAME) << 1, spr.getZvel(), 4 << 8, 4 << 8, 0);

                    spr.setLotag(spr.getLotag() - TICSPERFRAME);

                    if (zr_florz <= spr.getZ() && spr.getLotag() < 0) {
                        spr.setZ(zr_florz);
                        engine.changespritestat(i, LAND);
                    }

                    break;
                case 3: // jumpup

                    spr.setZvel(spr.getZvel() - (TICSPERFRAME << 4));

                     movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                            ((EngineUtils.sin(spr.getAng())) * TICSPERFRAME) << 3, spr.getZvel(), 4 << 8, 4 << 8, 0);

                    spr.setLotag(spr.getLotag() - TICSPERFRAME);

                    engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                    if (spr.getLotag() < 0) {
                        spr.setExtra(2);
                        spr.setLotag(20);
                    }

                    break;
            }
        }
    }

    public static void create() {
        enemy[GONZOTYPE] = new Enemy();
        enemy[GONZOTYPE].info = new EnemyInfo(35, 35, 1024 + 256, 120, 0, 48, false, 50, 0) {
            @Override
            public int getAttackDist(Sprite spr) {
                int out = attackdist;
                switch (spr.getPicnum()) {
                    case KURTAT:
                    case GONZOCSW:
                    case GONZOCSWAT:
                        if (spr.getExtra() > 10) {
                            out = 2048 << 1;
                        }
                        break;
                }

                return out;
            }

            @Override
            public short getHealth(Sprite spr) {
                switch (spr.getPicnum()) {
                    case KURTAT:
                        return 10;
                    case KURTPUNCH:
                        return adjusthp(15);
                    case GONZOGSW:
                        return adjusthp(100);
                    case GONZOGHM:
                        return adjusthp(40);
                }

                return adjusthp(health);
            }
        };
        enemy[GONZOTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, final int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                switch (spr.getPicnum()) {
                    case GONZOGHM:
                    case GONZOGSH:
                        if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                                spr.getSectnum()) && plr.getInvisibletime() < 0) {

                            int tmpvar = checkCodeVersion(game.getScreen(), 101) ? 1 : 0;

                            if (checkdist(plr, i)) {
                                if (plr.getShadowtime() > 0) {
                                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                    newstatus(i, FLEE);
                                } else {
                                    newstatus(i, ATTACK);
                                }
                                break;
                            }
                            else if ((engine.krand() & tmpvar) == 1) {
                                spr.setAng((short) (((engine.krand() & 128 - 256) + spr.getAng() + 1024) & 2047));
                                newstatus(i, FLEE);
                            }

                            if (engine.krand() % 63 > 60) {
                                spr.setAng((short) (((engine.krand() & 128 - 256) + spr.getAng() + 1024) & 2047));
                                newstatus(i, FLEE);
                                break;
                            }

                            int dax = spr.getX(); // Back up old x&y if stepping off cliff
                            int day = spr.getY();
                            int daz = spr.getZ();

                            int movestat = aimove(i);
                            if ((movestat & HIT_TYPE_MASK) == HIT_FLOOR) {
                                spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                                newstatus(i, FLEE);
                                return;
                            }

                            Sector plrSec = boardService.getSector(plr.getSector());
                            if (plrSec == null) {
                                return;
                            }

                            if (zr_florz > spr.getZ() + (48 << 8)) {
                                spr.setX(dax);
                                spr.setY(day);
                                spr.setZ(daz);
                                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                                movestat = 1;

                                if (engine.krand() % 100 > 80 && plrSec.getLotag() == 25) {
                                    newstatus(i, AMBUSH);
                                    spr.setZ(spr.getZ() - (getPlayerHeight() << 6));
                                    spr.setLotag(60);
                                    spr.setExtra(1);
                                    spr.setPicnum(GONZOHMJUMP);
                                    return;

                                }

                            }

                            if ((movestat & HIT_TYPE_MASK) == HIT_WALL && plrSec.getLotag() == 25) {
                                newstatus(i, AMBUSH);
                                spr.setZ(spr.getZ() - (getPlayerHeight() << 6));
                                spr.setLotag(90);
                                spr.setExtra(3);
                                spr.setPicnum(GONZOHMJUMP);
                                return;
                            }

                            if (movestat != 0) {
                                if ((movestat & HIT_INDEX_MASK) != plr.getSpritenum()) {
                                    int daang;
                                    if ((engine.krand() & tmpvar) == 1) {
                                        daang = (spr.getAng() + 256) & 2047;
                                    } else {
                                        daang = (spr.getAng() - 256) & 2047;
                                    }
                                    spr.setAng((short) daang);
                                    if (plr.getShadowtime() > 0) {
                                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                        newstatus(i, FLEE);
                                    } else {
                                        newstatus(i, SKIRMISH);
                                    }
                                } else {
                                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                    newstatus(i, SKIRMISH);
                                }
                            }
                            break;
                        } else {
                            if (!patrolprocess(plr, i)) {
                                newstatus(i, FLEE);
                            }
                        }
                        break;
                    case GONZOCSW:
                    case GONZOGSW:
                        if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                                spr.getSectnum()) && plr.getInvisibletime() < 0) {
                            if (checkdist(plr, i)) {
                                if (plr.getShadowtime() > 0) {
                                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                    newstatus(i, FLEE);
                                } else {
                                    newstatus(i, ATTACK);
                                }
                            } else if (engine.krand() % 63 > 60) {
                                spr.setAng((short) (((engine.krand() & 128 - 256) + spr.getAng() + 1024) & 2047));
                                newstatus(i, FLEE);
                            } else {
                                int movestat = aimove(i);
                                if ((movestat & HIT_TYPE_MASK) == HIT_FLOOR) {
                                    spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                                    newstatus(i, FLEE);
                                    return;
                                }

                                if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                                    if ((movestat & HIT_INDEX_MASK) != plr.getSpritenum()) {
                                        short daang = (short) ((spr.getAng() - 256) & 2047);
                                        spr.setAng(daang);
                                        if (plr.getShadowtime() > 0) {
                                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                            newstatus(i, FLEE);
                                        } else {
                                            newstatus(i, SKIRMISH);
                                        }
                                    } else {
                                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                        newstatus(i, SKIRMISH);
                                    }
                                }
                            }
                        } else {
                            if (!patrolprocess(plr, i)) {
                                newstatus(i, FLEE);
                            }
                        }

                        engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                        spr.setZ(zr_florz);

                        Sector sec = boardService.getSector(spr.getSectnum());
                        if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                            warpsprite(i);
                        }

                        if (checksector6(i)) {
                            return;
                        }

                        processfluid(i, zr_florhit, false);

                        if (osec.getLotag() == KILLSECTOR) {
                            spr.setHitag(spr.getHitag() - 1);
                            if (spr.getHitag() < 0) {
                                newstatus(i, DIE);
                            }
                        }

                        engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                        sec = boardService.getSector(spr.getSectnum());

                        if (sec != null && ((zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == LAVA
                                || sec.getFloorpicnum() == LAVA1 || sec.getFloorpicnum() == ANILAVA))) {
                            spr.setHitag(spr.getHitag() - 1);
                            if (spr.getHitag() < 0) {
                                newstatus(i, DIE);
                            }
                        }
                }

                checkexpl(plr, i);
            }
        };

        enemy[GONZOTYPE].resurrect = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                    switch (spr.getPicnum()) {
                        case GONZOCSWDEAD:
                            spr.setPicnum(GONZOCSW);
                            spr.setHitag(adjusthp(50));
                            break;
                        case GONZOGSWDEAD:
                            spr.setPicnum(GONZOGSW);
                            spr.setHitag(adjusthp(100));
                            break;
                        case GONZOGHMDEAD:
                            spr.setPicnum(GONZOGHM);
                            spr.setHitag(adjusthp(40));
                            break;
                        case GONZOGSHDEAD:
                            spr.setPicnum(GONZOGSH);
                            spr.setHitag(adjusthp(50));
                            break;
                    }
                    spr.setLotag(100);
                    spr.setCstat(spr.getCstat() | 1);
                }
            }
        };

        enemy[GONZOTYPE].skirmish = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }

                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                if (checksector6(i)) {
                    return;
                }

                checkexpl(plr, i);
            }
        };

        enemy[GONZOTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, false);
                if (!checksector6(i)) {
                    checkexpl(plr, i);
                }
            }
        };

        enemy[GONZOTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }
                chunksofmeat(plr, i, spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(), spr.getAng());
                trailingsmoke(i, false);
                newstatus(i, DIE);
            }
        };

        enemy[GONZOTYPE].frozen = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPal(0);
                    switch (spr.getPicnum()) {
                        case GONZOCSWPAIN:
                            spr.setPicnum(GONZOCSW);
                            break;
                        case GONZOGSWPAIN:
                            spr.setPicnum(GONZOGSW);
                            break;
                        case GONZOGHMPAIN:
                            spr.setPicnum(GONZOGHM);
                            break;
                        case GONZOGSHPAIN:
                            spr.setPicnum(GONZOGSH);
                            break;
                    }
                    newstatus(i, FACE);
                }
            }
        };

        enemy[GONZOTYPE].pain = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    switch (spr.getPicnum()) {
                        case GONZOCSWPAIN:
                            spr.setPicnum(GONZOCSW);
                            break;
                        case GONZOGSWPAIN:
                            spr.setPicnum(GONZOGSW);
                            break;
                        case GONZOGHMPAIN:
                            spr.setPicnum(GONZOGHM);
                            break;
                        case GONZOGSHPAIN:
                            spr.setPicnum(GONZOGSH);
                            break;
                    }
                    spr.setAng((short) plr.getAng());
                    newstatus(i, FLEE);
                }

                aimove(i);
                processfluid(i, zr_florhit, false);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checkexpl(plr, i);
            }
        };

        enemy[GONZOTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }


                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    spr.setAng((short) (EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()) & 2047));

                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) {
                        newstatus(i, FLEE);
                    }
                }

                if (plr.getInvisibletime() < 0 && checkdist(plr, i)) {
                    newstatus(i, ATTACK);
                }

                checkexpl(plr, i);
            }
        };

        enemy[GONZOTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                switch (checkfluid(i, zr_florhit)) {
                    case TYPELAVA:
                    case TYPEWATER:
                        spr.setZ(spr.getZ() + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                        break;
                }

                switch (spr.getPicnum()) {
                    // WANGO
                    case KURTREADY:
                        spr.setLotag(spr.getLotag() - TICSPERFRAME);
                        if (spr.getLotag() < 0) {
                            spr.setPicnum(spr.getPicnum() + 1);
                            spr.setLotag(24);
                        }
                        break;
                    case KURTREADY + 1:
                        spr.setLotag(spr.getLotag() - TICSPERFRAME);
                        if (spr.getLotag() < 0) {
                            spr.setPicnum(KURTAT);
                            spr.setLotag(64);
                        }
                        break;
                    case KURTAT:
                    case KURTPUNCH:
                        if (spr.getLotag() == 46) {
                            if (checksight(plr, i)) {
                                if (checkdist(plr, i)) {
                                    spr.setAng((short) checksight_ang);
                                    attack(plr, i);
                                }
                            }
                        } else if (spr.getLotag() < 0) {
                            if (plr.getShadowtime() > 0) {
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                                newstatus(i, FLEE);
                            } else {
                                newstatus(i, CHASE);
                            }
                        }
                        spr.setLotag(spr.getLotag() - TICSPERFRAME);
                        break;
                    case GONZOCSWAT:
                        spr.setLotag(spr.getLotag() - TICSPERFRAME);
                        if (spr.getLotag() < 0) {
                            if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                                    spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                                newstatus(i, CAST);
                            } else {
                                newstatus(i, CHASE);
                            }
                        } else {
                            spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        }
                        break;
                    case GONZOGSWAT:
                    case GONZOGHMAT:
                    case GONZOGSHAT:
                        if (spr.getLotag() == 31) {
                            if (checksight(plr, i)) {
                                if (checkdist(plr, i)) {
                                    spr.setAng((short) checksight_ang);
                                    attack(plr, i);
                                }
                            }
                        } else if (spr.getLotag() < 0) {
                            if (plr.getShadowtime() > 0) {
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                                newstatus(i, FLEE);
                            } else {
                                newstatus(i, CHASE);
                            }
                        }
                        spr.setLotag(spr.getLotag() - TICSPERFRAME);
                        break;
                }

                checksector6(i);
            }
        };

        enemy[GONZOTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }

                int movestat = aimove(i);

                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        if (plr.getInvisibletime() < 0) {
                            spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                            newstatus(i, FACE);
                        }
                    }
                }
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checkexpl(plr, i);
            }
        };

        enemy[GONZOTYPE].cast = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(12);
                }

                if (spr.getPicnum() == GONZOCSWAT) {
                    spr.setExtra(spr.getExtra() - 1);
                    playsound_loc(S_GENTHROW, spr.getX(), spr.getY());
                    gonzopike(i, plr);
                    newstatus(i, CHASE);
                }
            }
        };

        enemy[GONZOTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() <= 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(20);

                    switch (spr.getPicnum()) {
                        case GONZOBSHDEAD:
                        case GONZOCSWDEAD:
                        case GONZOGSWDEAD:
                        case GONZOGHMDEAD:
                        case GONZOGSHDEAD:
                            if (difficulty == 4) {
                                newstatus(i, RESURECT);
                            } else {
                                kills++;
                                newstatus(i, DEAD);
                            }
                            break;
                    }
                }
            }
        };
    }

    public static int searchpatrol(Sprite spr) {
        int mindist = 0x7fffffff;
        int target = -1;
        ListNode<Sprite> next;
        for (ListNode<Sprite> node = boardService.getStatNode(APATROLPOINT); node != null; node = next) {
            next = node.getNext();
            Sprite tspr = node.get();
            int dist = klabs(tspr.getX() - spr.getX()) + klabs(tspr.getY() - spr.getY());
            if (dist < mindist) {
                mindist = dist;
                target = node.getIndex();
            }
        }

        return target;
    }

    public static boolean patrolprocess(PLAYER ignoredPlr, int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return false;
        }

        int target = searchpatrol(spr);
        Sprite tspr = boardService.getSprite(target);
        if (tspr != null) {
            if (engine.cansee(tspr.getX(), tspr.getY(), tspr.getZ(), tspr.getSectnum(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                    spr.getSectnum())) {
                spr.setAng(EngineUtils.getAngle(tspr.getX() - spr.getX(), tspr.getY() - spr.getY()));
                newstatus(i, PATROL);
            }
        }

        return target != -1;
    }

    public static void gonzopike(int s, PLAYER plr) {
        Sprite spr = boardService.getSprite(s);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), JAVLIN);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }


        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ() - (40 << 8));

        spr2.setCstat(21);
        spr2.setPicnum(THROWPIKE);
        spr2.setAng((((spr.getAng() + 2048 + 96) - 512) & 2047));
        spr2.setXrepeat(24);
        spr2.setYrepeat(24);
        spr2.setClipdist(32);

        spr2.setExtra(spr.getAng());
        spr2.setShade(-15);
        spr2.setXvel(((engine.krand() & 256) - 128));
        spr2.setYvel(((engine.krand() & 256) - 128));

        spr2.setZvel((((plr.getZ() + (8 << 8) - spr.getZ()) << 7) / EngineUtils.sqrt((plr.getX() - spr.getX()) * (plr.getX() - spr.getX()) + (plr.getY() - spr.getY()) * (plr.getY() - spr.getY()))));

        spr2.setZvel(spr2.getZvel() + ((engine.krand() % 256) - 128));

        spr2.setOwner(s);
        spr2.setLotag(1024);
        spr2.setHitag(0);
        spr2.setPal(0);
    }

    public static void checkexpl(PLAYER ignoredPlr, int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        ListNode<Sprite> sectNode = boardService.getSectNode(spr.getSectnum());
        while (sectNode != null) {
            Sprite sectSpr = sectNode.get();
            ListNode<Sprite> next = sectNode.getNext();
            long dx = klabs(spr.getX() - sectSpr.getX()); // x distance to sprite
            long dy = klabs(spr.getY() - sectSpr.getY()); // y distance to sprite
            long dz = klabs((spr.getZ() >> 8) - (sectSpr.getZ() >> 8)); // z distance to sprite
            long dh = engine.getTile(sectSpr.getPicnum()).getHeight() >> 1; // height of sprite
            if (dx + dy < PICKDISTANCE && dz - dh <= getPickHeight()) {
                if (sectSpr.getPicnum() == EXPLO2
                        || sectSpr.getPicnum() == MONSTERBALL) {
                    spr.setHitag(spr.getHitag() - (TICSPERFRAME << 2));
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }
            }
            sectNode = next;
        }
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(GONZOTYPE);
        enemy[GONZOTYPE].info.set(spr);
        engine.changespritestat(i, FACE);

        switch (spr.getPicnum()) {
            case KURTAT:
                spr.setPicnum(GONZOCSW);
                break;
            case KURTPUNCH:
                spr.setExtra(0);
                spr.setPicnum(GONZOCSW);
                break;
            case GONZOCSW:
                spr.setExtra(20);
                break;
            case GONZOGSW:
            case GONZOGHM:
            case GONZOGSH:
                spr.setClipdist(32);
                spr.setExtra(0);
                break;
        }
    }

    public static void deaddude(int sn) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), DEAD);
        Sprite spr2 =  boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        spr2.setZ(spr.getZ());
        spr2.setCstat(0);
        spr2.setPicnum(GONZOBSHDEAD);
        spr2.setShade(sec.getFloorshade());
        spr2.setPal(0);
        spr2.setXrepeat(spr.getXrepeat());
        spr2.setYrepeat(spr.getYrepeat());
        spr2.setOwner(0);
        spr2.setLotag(0);
        spr2.setHitag(0);
    }
}
