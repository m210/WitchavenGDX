package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.newstatus;
import static ru.m210projects.Witchaven.WHOBJ.trailingsmoke;
import static ru.m210projects.Witchaven.WHPLR.chunksofmeat;
import static ru.m210projects.Witchaven.WHSND.S_FIREBALL;
import static ru.m210projects.Witchaven.WHSND.playsound_loc;
import static ru.m210projects.Witchaven.WHScreen.showmessage;

public class AIWillow {

    public static final AIState willowDrain = new AIState() {
        @Override
        public void process(PLAYER plr, int i) {
            Sprite spr = boardService.getSprite(i);
            if (spr == null) {
                engine.deletesprite(i);
                return;
            }

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                playsound_loc(S_FIREBALL, spr.getX(), spr.getY());
                int oldz = spr.getZ();
                spr.setZ(spr.getZ() + 6144);
                castspell(plr, i);
                spr.setZ(oldz);
                newstatus(i, CHASE);
            }
        }
    };

    public static void create() {
        enemy[WILLOWTYPE] = new Enemy();
        enemy[WILLOWTYPE].info = new EnemyInfo(32, 32, 512, 120, 0, 64, true, game.WH2 ? 5 : 400, 0);
        enemy[WILLOWTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                if (engine.krand() % 63 == 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                            spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum()) && plr.getInvisibletime() < 0) {
                        newstatus(i, ATTACK);
                    }
                    return;
                } else {
                    //spr.z = boardService.getSector(spr.sectnum).floorz - (32 << 8);
                    int dax = (EngineUtils.sin((spr.getAng() + 512) & 2047) * TICSPERFRAME) << 3;
                    int day = (EngineUtils.sin(spr.getAng() & 2047) * TICSPERFRAME) << 3;
                    checksight(plr, i);

                    if (!checkdist(plr, i)) {
                        checkmove(i, dax, day);
                    } else {
                        if (engine.krand() % 8 == 0) // NEW
                        {
                            newstatus(i, ATTACK); // NEW
                        } else { // NEW
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                            newstatus(i, CHASE); // NEW
                        }
                    }
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (spr.getZ() > zr_florz) {
                    spr.setZ(zr_florz);
                }
                if (spr.getZ() < zr_ceilz - (32 << 8)) {
                    spr.setZ(zr_ceilz - (32 << 8));
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, true);

                if (osec.getLotag() == KILLSECTOR) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                sec = boardService.getSector(spr.getSectnum());

                if (sec != null && (zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == LAVA
                        || sec.getFloorpicnum() == LAVA1 || sec.getFloorpicnum() == ANILAVA)) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }
            }
        };

        enemy[WILLOWTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (plr.getZ() < spr.getZ()) {
                    spr.setZ(spr.getZ() - (TICSPERFRAME << 8));
                }

                if (plr.getZ() > spr.getZ()) {
                    spr.setZ(spr.getZ() + (TICSPERFRAME << 8));
                }

                if (spr.getLotag() < 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                            spr.getSectnum())) {
                        if (checkdist(plr, i)) {
                            if (plr.getShockme() < 0) {
                                if ((engine.krand() & 1) != 0) {
                                    plr.setShockme(120);
                                    if (!game.WH2) {
                                        plr.setLvl(plr.getLvl() - 1);
                                        switch (plr.getLvl()) {
                                            case 1:
                                                plr.setScore(0);
                                                plr.setMaxhealth(100);
                                                break;
                                            case 2:
                                                plr.setScore(2350);
                                                plr.setMaxhealth(120);
                                                break;
                                            case 3:
                                                plr.setScore(4550);
                                                plr.setMaxhealth(140);
                                                break;
                                            case 4:
                                                plr.setScore(9300);
                                                plr.setMaxhealth(160);
                                                break;
                                            case 5:
                                                plr.setScore(18400);
                                                plr.setMaxhealth(180);
                                                break;
                                            case 6:
                                                plr.setScore(36700);
                                                plr.setMaxhealth(200);
                                                break;
                                            case 7:
                                                plr.setScore(75400);
                                                plr.setMaxhealth(200);
                                                break;
                                        }
                                        if (plr.getLvl() < 1) {
                                            plr.setLvl(1);
                                            plr.setHealth(-1);
                                        }
                                        showmessage("Level Drained", 360);
                                    } else {
                                        showmessage("Shocked", 360);
                                    }

                                }
                            }
                        } else {
                            newstatus(i, DRAIN);
                        }
                    } else {
                        newstatus(i, CHASE);
                    }
                }

                int floorz = engine.getflorzofslope(spr.getSectnum(), spr.getX(), spr.getY()) - (16 << 8);
                if (spr.getZ() > floorz) {
                    spr.setZ(floorz);
                }
            }
        };

        enemy[WILLOWTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }


                if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum()) && plr.getInvisibletime() < 0) {
                    spr.setAng((short) (EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()) & 2047));

                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else {
                        newstatus(i, FLEE);
                    }
                }

                if (checkdist(plr, i)) {
                    newstatus(i, ATTACK);
                }
            }
        };

        enemy[WILLOWTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, true);
                checksector6(i);
            }
        };

        enemy[WILLOWTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                int movestat = aifly(i);

                if (movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        newstatus(i, FACE);
                    }
                }
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, true);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[WILLOWTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() <= 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(20);
                    if (spr.getPicnum() == WILLOWEXPLO || spr.getPicnum() == WILLOWEXPLO + 1
                            || spr.getPicnum() == WILLOWEXPLO + 2) {
                        spr.setYrepeat(spr.getYrepeat() << 1);
                        spr.setXrepeat(spr.getYrepeat());
                    }

                    if (spr.getPicnum() == WILLOWEXPLO + 2) {
                        if (difficulty == 4) {
                            newstatus(i, RESURECT);
                        } else {
                            kills++;
                            newstatus(i, DEAD);
                        }
                    }
                }
            }
        };

        enemy[WILLOWTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                if (game.WH2) {
                    chunksofmeat(plr, i, spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(), spr.getAng());
                    trailingsmoke(i, false);
                    newstatus(i, DIE);
                }
            }
        };
    }

    public static void willowProcess(PLAYER plr) {
        ListNode<Sprite> next;
        for (ListNode<Sprite> node = boardService.getStatNode(DRAIN); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = boardService.getSprite(i);
            if (spr == null) {
                engine.deletesprite(i);
                return;
            }

            if (spr.getDetail() == WILLOWTYPE) {
                willowDrain.process(plr, i);
            }
        }
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(WILLOWTYPE);
        enemy[WILLOWTYPE].info.set(spr);
        spr.setCstat(spr.getCstat() | 128);
        spr.setZ(spr.getZ() - (engine.getTile(WILLOW).getHeight() << 8));
        engine.changespritestat(i, FACE);
    }
}
