package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.newstatus;

public class AIRat {

    public static void create() {
        enemy[RATTYPE] = new Enemy();
        enemy[RATTYPE].info = new EnemyInfo(32, 32, 512, 120, 0, 32, false, 0, 0) {
            @Override
            public short getHealth(Sprite spr) {
                return 10;
            }
        };
        enemy[RATTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                newstatus(i, FLEE);
            }
        };

        enemy[RATTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                newstatus(i, FLEE);
            }
        };

        enemy[RATTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                spr.setOwner(plr.getSprite().getOwner());
                newstatus(i, FLEE);
            }
        };

        enemy[RATTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                engine.deletesprite(i);
            }
        };

        enemy[RATTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }

                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) == HIT_FLOOR) {
                    spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                    return;
                }

                if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                    Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                    if (wal != null) {
                        Wall wal2 = wal.getWall2();
                        int wallang = (short) ((EngineUtils.getAngle(wal2.getX() - wal.getX(), wal2.getY() - wal.getY()) + 512) & 2047);
                        spr.setAng((short) (engine.krand() & 512 - 256 + wallang));
                    }
                }

                if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                    Sprite sp = boardService.getSprite(movestat & HIT_INDEX_MASK);
                    if (sp != null) {
                        spr.setOwner((short) (movestat & HIT_INDEX_MASK));
                        spr.setAng(EngineUtils.getAngle(sp.getX() - spr.getX(), sp.getY() - spr.getY()));
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                    }
                }

                if (klabs(plr.getX() - spr.getX()) <= 1024 && klabs(plr.getY() - spr.getY()) <= 1024) {
                    spr.setOwner(plr.getSprite().getOwner());
                    newstatus(i, FACE);
                }
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }


                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

//				switch (checkfluid(i, zr_florhit)) {
//				case TYPELAVA:
//				case TYPEWATER:
//					spr.z += engine.getTile(spr.picnum).getHeight() << 5;
//					break;
//				}

                sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == LAVA
                        || sec.getFloorpicnum() == LAVA2
                        || sec.getFloorpicnum() == LAVA1 || sec.getFloorpicnum() == ANILAVA)) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(RATTYPE);
        enemy[RATTYPE].info.set(spr);
        engine.changespritestat(i, FACE);

        spr.setShade(12);
        spr.setPal(5);
    }
}
