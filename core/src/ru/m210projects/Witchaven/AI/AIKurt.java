package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.chunksofmeat;

public class AIKurt {

    public static void create() {
        enemy[KURTTYPE] = new Enemy();
        enemy[KURTTYPE].info = new EnemyInfo(35, 35, 1024 + 256, 120, 0, 48, false, 50, 0) {
            @Override
            public int getAttackDist(Sprite spr) {
                int out = attackdist;
                switch (spr.getPicnum()) {
                    case KURTAT:
                    case GONZOCSW:
                    case GONZOCSWAT:
                        if (spr.getExtra() > 10) {
                            out = 2048 << 1;
                        }
                        break;
                }

                return out;
            }
        };
        enemy[KURTTYPE].stand = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                if (EngineUtils.sin((spr.getAng() + 512) & 2047) * (plr.getX() - spr.getX())
                        + EngineUtils.sin(spr.getAng() & 2047) * (plr.getY() - spr.getY()) >= 0) {
                    if (engine.cansee(spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum(), plr.getX(), plr.getY(),
                            plr.getZ(), plr.getSector()) && plr.getInvisibletime() < 0) {
                        if (plr.getShadowtime() > 0) {
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                        } else {
                            newstatus(i, CHASE);
                        }
                    }
                }
            }
        };

        enemy[KURTTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                chunksofmeat(plr, i, spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(), spr.getAng());
                trailingsmoke(i, false);
                newstatus(i, DIE);
            }
        };

        enemy[KURTTYPE].chase = enemy[GONZOTYPE].chase;

        enemy[KURTTYPE].resurrect = enemy[GONZOTYPE].resurrect;

        enemy[KURTTYPE].skirmish = enemy[GONZOTYPE].skirmish;

        enemy[KURTTYPE].search = enemy[GONZOTYPE].search;

        enemy[KURTTYPE].frozen = enemy[GONZOTYPE].frozen;

        enemy[KURTTYPE].pain = enemy[GONZOTYPE].pain;

        enemy[KURTTYPE].face = enemy[GONZOTYPE].face;

        enemy[KURTTYPE].attack = enemy[GONZOTYPE].attack;

        enemy[KURTTYPE].cast = enemy[GONZOTYPE].cast;

        enemy[KURTTYPE].flee = enemy[GONZOTYPE].flee;

        enemy[KURTTYPE].die = enemy[GONZOTYPE].die;
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(KURTTYPE);
        enemy[KURTTYPE].info.set(spr);
        engine.changespritestat(i, STAND);

        switch (spr.getPicnum()) {
            case KURTSTAND:
                spr.setExtra(20);
                break;
            case KURTKNEE:
                spr.setExtra(10);
                break;
        }
    }
}
