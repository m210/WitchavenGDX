package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WH2Names.GONZOGSH;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.S_JUDY1;
import static ru.m210projects.Witchaven.WHSND.playsound_loc;

public class AIJudy {

    public static void judyOperate(PLAYER plr) {
        ListNode<Sprite> next;
        for (ListNode<Sprite> node = boardService.getStatNode(WITCHSIT); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            spr.setAng((short) (EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()) & 2047));
            if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                    spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(12);
                    if (spr.getPicnum() == JUDYSIT + 4) {
                        spr.setPicnum(JUDY);
                        newstatus(i, FACE);
                    }
                }
            }
        }
    }

    public static void create() {
        enemy[JUDYTYPE] = new Enemy();
        enemy[JUDYTYPE].info = new EnemyInfo(32, 32, 2048, 120, 0, 64, false, 500, 0);
        enemy[JUDYTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                if (mapon < 24) {
                    spr.setExtra(spr.getExtra() - TICSPERFRAME);
                    if (spr.getExtra() < 0) {
                        for (int j = 0; j < 8; j++) {
                            trailingsmoke(i, true);
                        }
                        engine.deletesprite(i);
                        return;
                    }
                }

                if (engine.krand() % 63 == 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                            spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum()))// && invisibletime < 0)
                    {
                        newstatus(i, ATTACK);
                    }
                } else {
                    checksight(plr, i);
                    if (!checkdist(i, plr.getX(), plr.getY(), plr.getZ())) {
                        int movestat = aimove(i);
                        if ((movestat & HIT_TYPE_MASK) == HIT_FLOOR) {
                            spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                            return;
                        }
                    } else {
                        if (engine.krand() % 8 == 0) // NEW
                        {
                            newstatus(i, ATTACK); // NEW
                        } else { // NEW
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                            newstatus(i, FLEE); // NEW
                        }
                    }
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                if (osec.getLotag() == KILLSECTOR) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[JUDYTYPE].resurrect = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                    spr.setPicnum(JUDY);
                    spr.setHitag(adjusthp(200));
                    spr.setLotag(100);
                    spr.setCstat(spr.getCstat() | 1);
                }
            }
        };

        enemy[JUDYTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, false);
                checksector6(i);
            }
        };

        enemy[JUDYTYPE].nuked = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(24);
                    if (spr.getPicnum() == JUDYCHAR + 4) {
                        trailingsmoke(i, false);
                        engine.deletesprite(i);
                    }
                }
            }
        };

        enemy[JUDYTYPE].pain = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(JUDY);
                    spr.setAng((short) plr.getAng());
                    newstatus(i, FLEE);
                }

                aimove(i);
                processfluid(i, zr_florhit, false);
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[JUDYTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));

                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) {
                        newstatus(i, FLEE);
                    }
                }

                if (checkdist(i, plr.getX(), plr.getY(), plr.getZ())) {
                    newstatus(i, ATTACK);
                }
            }
        };

        enemy[JUDYTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                spr.setZ(zr_florz);

                switch (checkfluid(i, zr_florhit)) {
                    case TYPELAVA:
                    case TYPEWATER:
                        spr.setZ(spr.getZ() + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                        break;
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                spr.setExtra(spr.getExtra() - TICSPERFRAME);
                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                            spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                        newstatus(i, CAST);
                    } else {
                        newstatus(i, CHASE);
                    }
                } else {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                }
            }
        };

        enemy[JUDYTYPE].flee = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                
                int movestat = aimove(i);
                if ((movestat & HIT_TYPE_MASK) != HIT_FLOOR && movestat != 0) {
                    if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                        Wall wal = boardService.getWall(movestat & HIT_INDEX_MASK);
                        if (wal != null) {
                            int nx = -(wal.getWall2().getY() - wal.getY()) >> 4;
                            int ny = (wal.getWall2().getX() - wal.getX()) >> 4;
                            spr.setAng(EngineUtils.getAngle(nx, ny));
                        }
                    } else {
                        spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                        newstatus(i, FACE);
                    }
                }
                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && (spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
            }
        };

        enemy[JUDYTYPE].cast = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(12);
                }

                if (spr.getPicnum() == JUDYATTACK1 + 3) {
                    spr.setPicnum(JUDYATTACK1);
                    playsound_loc(S_JUDY1 + engine.krand() % 4, spr.getX(), spr.getY());
                    if (engine.krand() % 100 > 70) {
                        castspell(plr, i);
                    } else {
                        if (engine.krand() % 100 > 40) {
                            // raise the dead

                            ListNode<Sprite> node = boardService.getStatNode(DEAD);
                            while (node != null) {
                                ListNode<Sprite> next = node.getNext();
                                node.get().setLotag((short) ((engine.krand() % 120) + 120));
                                kills--;
                                newstatus(node.getIndex(), RESURECT);
                                node = next;
                            }
                        } else {
                            if (engine.krand() % 100 > 50) {
                                // curse
                                for (int j = 1; j < 9; j++) {
                                    plr.getAmmo()[j] = 3;
                                }
                            } else {
                                int j = engine.krand() % 5;
                                switch (j) {
                                    case 0:// SPAWN WILLOW
                                        spawnabaddy(i, WILLOW);
                                        break;
                                    case 1:// SPAWN 10 SPIDERS
                                        for (j = 0; j < 4; j++) {
                                            spawnabaddy(i, SPIDER);
                                        }
                                        break;
                                    case 2:// SPAWN 2 GRONSW
                                        for (j = 0; j < 2; j++) {
                                            spawnabaddy(i, GRONSW);
                                        }
                                        break;
                                    case 3:// SPAWN SKELETONS
                                        for (j = 0; j < 4; j++) {
                                            spawnabaddy(i, SKELETON);
                                        }
                                        break;
                                    case 4:
                                        castspell(plr, i);
                                        break;
                                }
                            }
                        }
                    }
                    newstatus(i, CHASE);
                } else if (spr.getPicnum() == JUDYATTACK2 + 8) {
                    spr.setPicnum(JUDYATTACK2);
                    playsound_loc(S_JUDY1 + engine.krand() % 4, spr.getX(), spr.getY());
                    if (engine.krand() % 100 > 50) {
                        skullycastspell(plr, i);
                    } else {
                        if (engine.krand() % 100 > 70) {
                            if (engine.krand() % 100 > 50) {
                                plr.setHealth(0);
                                addhealth(plr, 1);
                            } else {
                                addarmor(plr, -(plr.getArmor()));
                                plr.setArmortype(0);
                            }
                        } else {
                            int j = engine.krand() % 5;
                            switch (j) {
                                case 0:// SPAWN WILLOW
                                    spawnabaddy(i, WILLOW);
                                    break;
                                case 1:// SPAWN 6 SPIDERS
                                    for (j = 0; j < 4; j++) {
                                        spawnabaddy(i, SPIDER);
                                    }
                                    break;
                                case 2:// SPAWN 2 GRONSW
                                    for (j = 0; j < 2; j++) {
                                        spawnabaddy(i, GRONSW);
                                    }
                                    break;
                                case 3:// SPAWN SKELETONS
                                    for (j = 0; j < 4; j++) {
                                        spawnabaddy(i, SKELETON);
                                    }
                                    break;
                                case 4:
                                    castspell(plr, i);
                                    break;
                            }
                        }

                    }
                    newstatus(i, CHASE);
                }
                checksector6(i);
            }
        };

        enemy[JUDYTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() <= 0) {
                    spr.setPicnum(spr.getPicnum() + 1);
                    spr.setLotag(20);

                    if (spr.getPicnum() == JUDYDEAD) {
                        if (difficulty == 4) {
                            newstatus(i, RESURECT);
                        } else {
                            kills++;
                            newstatus(i, DEAD);
                        }
                    }
                }
            }
        };
    }

    public static void spawnabaddy(int i, int monster) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), FACE);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX() + (engine.krand() & 2048) - 1024);
        spr2.setY(spr.getY() + (engine.krand() & 2048) - 1024);
        spr2.setZ(spr.getZ());

        spr2.setPal(0);
        spr2.setShade(0);
        spr2.setCstat(0);

        if (monster == WILLOW) {
            AIWillow.premap(j);
        } else if (monster == SPIDER) {
            AISpider.premap(j);
        } else if (monster == GRONSW) {
            AIGron.premap(j);
        } else if (monster == SKELETON) {
            AISkeleton.premap(j);
        } else if (monster == GONZOGSH) {
            AIGonzo.premap(j);
        }

        spr2.setPicnum((short) monster);
        killcnt++;

        engine.setsprite(j, spr2.getX(), spr2.getY(), spr2.getZ());
        game.pInt.setsprinterpolate(j, spr2);
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(JUDYTYPE);

        enemy[JUDYTYPE].info.set(spr);

        if (mapon > 24) {
            spr.setHitag(adjusthp(700));
        }

        if (spr.getPicnum() == JUDYSIT) {
            engine.changespritestat(i, WITCHSIT);
            spr.setExtra(1200);
        } else {
            engine.changespritestat(i, FACE);
        }
    }
}
