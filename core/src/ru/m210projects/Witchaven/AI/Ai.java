package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Witchaven.AI.AIDragon.dragonProcess;
import static ru.m210projects.Witchaven.AI.AIGoblin.goblinChill;
import static ru.m210projects.Witchaven.AI.AIGoblin.goblinWarProcess;
import static ru.m210projects.Witchaven.AI.AIGonzo.gonzoProcess;
import static ru.m210projects.Witchaven.AI.AIJudy.judyOperate;
import static ru.m210projects.Witchaven.AI.AISkeleton.skeletonChill;
import static ru.m210projects.Witchaven.AI.AIWillow.willowProcess;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Items.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WH2MUS.attacktheme;
import static ru.m210projects.Witchaven.WH2MUS.startsong;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHFX.makemonstersplash;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.showmessage;
import static ru.m210projects.Witchaven.WHScreen.startredflash;
import static ru.m210projects.Witchaven.WHTAG.operatesector;

public class Ai {

    public static final int DEMONTYPE = 1; // ok
    public static final int DEVILTYPE = 2; // nuked ok, frozen no
    public static final int DRAGONTYPE = 3; // wh1
    public static final int FATWITCHTYPE = 4; // wh1
    public static final int FISHTYPE = 5;
    public static final int FREDTYPE = 6;
    public static final int GOBLINTYPE = 7; // wh1
    public static final int GONZOTYPE = 8; // freeze nuke ok
    public static final int GRONTYPE = 9; // ok
    public static final int GUARDIANTYPE = 10; // nuke ok
    public static final int IMPTYPE = 11; // freeze nuke ok
    public static final int JUDYTYPE = 12; // wh1
    public static final int KATIETYPE = 13; // ok
    public static final int KOBOLDTYPE = 14; // freeze nuke ok
    public static final int KURTTYPE = 15;
    public static final int MINOTAURTYPE = 16; // freeze nuke ok
    public static final int NEWGUYTYPE = 17; // freeze nuke ok
    public static final int RATTYPE = 18;
    public static final int SKELETONTYPE = 19; // freezee nuke ok
    public static final int SKULLYTYPE = 20; // wh1
    public static final int SPIDERTYPE = 21; // wh1
    public static final int WILLOWTYPE = 22; //nuke ok
    public static final int MAXTYPES = 23;

    public static final Enemy[] enemy = new Enemy[MAXTYPES];

    public static final int TYPENONE = 0;
    public static final int TYPEWATER = 1;
    public static final int TYPELAVA = 2;
    public static int checksight_ang = 0;

    static {
        if (game.WH2) {
            AIImp.create();
        } else {
            AIGoblin.create();
        }
        AIDevil.create();
        AISkeleton.create();
        AIDragon.create();
        AIKobold.create();
        AIGuardian.create();
        AIWillow.create();
        AIRat.create();
        AIFred.create();
        AIFish.create();
        AISpider.create();
        AIMinotaur.create();
        AIGron.create();
        AIFatwitch.create();
        AISkully.create();
        AIJudy.create();
        AIDemon.create();
        AIKatie.create();
        AINewguy.create();
        AIGonzo.create();
        AIKurt.create(); // kurt must be initialized after gonzo
    }

    public static void aiInit() {
        for (int i = 0; i < MAXSPRITES; i++) {
            Sprite spr = boardService.getSprite(i);
            if (spr == null || spr.getStatnum() >= MAXSTATUS) {
                continue;
            }

            int pic = spr.getPicnum();
            switch (spr.getPicnum()) {
                default:
                    if (pic == SKELETON || pic == HANGMAN) {
                        AISkeleton.premap(i);
                        killcnt++;
                    } else if (pic == GUARDIAN) {
                        AIGuardian.premap(i);
                        killcnt++;
                    } else if (pic == WILLOW) {
                        AIWillow.premap(i);
                        killcnt++;
                    } else if (pic == RAT) {
                        AIRat.premap(i);
                    } else if (pic == FISH) {
                        AIFish.premap(i);
                    } else if (pic == GRONHAL || pic == GRONMU || pic == GRONSW) {
                        AIGron.premap(i);
                        killcnt++;
                    }
                    break;
                case GOBLIN: // IMP
                case GOBLINSTAND:
                case GOBLINCHILL:
                    killcnt++;
                    if (game.WH2 && spr.getPicnum() == IMP) {
                        AIImp.premap(i);
                        break;
                    }

                    if (!game.WH2) {
                        AIGoblin.premap(i);
                    }
                    break;
                case DEVIL:
                case DEVILSTAND:
                    if (spr.getPal() != 8) {
                        AIDevil.premap(i);
                        killcnt++;
                    }
                    break;
                case DRAGON:
                    AIDragon.premap(i);
                    killcnt++;
                    break;
                case KOBOLD:
                    AIKobold.premap(i);
                    killcnt++;
                    break;
                case FRED:
                case FREDSTAND:
                    AIFred.premap(i);
                    killcnt++;
                    break;
                case SPIDER:
                    AISpider.premap(i);
                    killcnt++;
                    break;
                case MINOTAUR:
                    AIMinotaur.premap(i);
                    killcnt++;
                    break;
                case FATWITCH:
                    AIFatwitch.premap(i);
                    killcnt++;
                    break;
                case SKULLY:
                    AISkully.premap(i);
                    killcnt++;
                    break;
                case JUDY:
                case JUDYSIT:
                    AIJudy.premap(i);
                    killcnt++;
                    break;
                case DEMON:
                    AIDemon.premap(i);
                    killcnt++;
                    break;
                case KATIE:
                    AIKatie.premap(i);
                    killcnt++;
                    break;
                case KURTSTAND:
                case KURTKNEE:
                    AIKurt.premap(i);
                    killcnt++;
                    break;
                case NEWGUYSTAND:
                case NEWGUYKNEE:
                case NEWGUYCAST:
                case NEWGUYBOW:
                case NEWGUYMACE:
                case NEWGUYPUNCH:
                case NEWGUY:
                    AINewguy.premap(i);
                    killcnt++;
                    break;
                case KURTAT:
                case KURTPUNCH:
                case GONZOCSW:
                case GONZOGSW:
                case GONZOGHM:
                case GONZOGSH:
                    AIGonzo.premap(i);
                    killcnt++;
                    break;
            }
        }
    }

    public static void aiProcess() {
        ListNode<Sprite> next;
        PLAYER plr = player[0];

        judyOperate(plr);
        gonzoProcess(plr);
        goblinWarProcess(plr);
        dragonProcess(plr);
        willowProcess(plr);

        for (ListNode<Sprite> node = boardService.getStatNode(PATROL); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();

            Sprite spr = node.get();
            int movestat = movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                    ((EngineUtils.sin(spr.getAng())) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
            if (zr_florz > spr.getZ() + (48 << 8)) {
                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                movestat = 1;
            } else {
                spr.setZ(zr_florz);
            }
            ListNode<Sprite> sectNode = boardService.getSectNode(spr.getSectnum());
            while (sectNode != null) {
                ListNode<Sprite> nextSect = sectNode.getNext();
                Sprite tspr = sectNode.get();
                if (tspr.getPicnum() == PATROLPOINT) {
                    long dx = klabs(spr.getX() - tspr.getX()); // x distance to sprite
                    long dy = klabs(spr.getY() - tspr.getY()); // y distance to sprite
                    long dz = klabs((spr.getZ() >> 8) - (tspr.getZ() >> 8)); // z distance to sprite
                    int dh = engine.getTile(tspr.getPicnum()).getHeight() >> 4; // height of sprite
                    if (dx + dy < PICKDISTANCE && dz - dh <= getPickHeight()) {
                        spr.setAng(tspr.getAng());
                    }
                }
                sectNode = nextSect;
            }
            if (EngineUtils.sin((spr.getAng() + 2560) & 2047) * (plr.getX() - spr.getX())
                    + EngineUtils.sin((spr.getAng() + 2048) & 2047) * (plr.getY() - spr.getY()) >= 0) {
                if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum())) {
                    newstatus(i, CHASE);
                }
            } else if (movestat != 0) {
                if ((movestat & HIT_TYPE_MASK) == HIT_WALL) { // hit a wall
                    actoruse(i);
                }
                newstatus(i, FINDME);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(CHASE); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();
            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].chase != null) {
                enemy[spr.getDetail()].chase.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(RESURECT); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();
            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].resurrect != null) {
                enemy[spr.getDetail()].resurrect.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(FINDME); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();
            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].search != null) {
                enemy[spr.getDetail()].search.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(NUKED); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            if (spr.getPicnum() == ZFIRE) {
                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() <= 0) {
                    engine.deletesprite(i);
                }
            } else {
                if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].nuked != null) {
                    enemy[spr.getDetail()].nuked.process(plr, i);
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(FROZEN); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();
            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].frozen != null) {
                enemy[spr.getDetail()].frozen.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(PAIN); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].pain != null) {
                enemy[spr.getDetail()].pain.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(FACE); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].face != null) {
                enemy[spr.getDetail()].face.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(ATTACK); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            if (game.WH2 && attacktheme == 0) {
                attacktheme = 1;
                startsong((engine.rand() % 2) + 2);
            }

            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].attack != null) {
                enemy[spr.getDetail()].attack.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(FLEE); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].flee != null) {
                enemy[spr.getDetail()].flee.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(CAST); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].cast != null) {
                enemy[spr.getDetail()].cast.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(DIE); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].die != null) {
                enemy[spr.getDetail()].die.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(SKIRMISH); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].skirmish != null) {
                enemy[spr.getDetail()].skirmish.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STAND); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            if (enemy[spr.getDetail()] != null && enemy[spr.getDetail()].stand != null) {
                enemy[spr.getDetail()].stand.process(plr, i);
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(CHILL); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();
            switch (spr.getDetail()) {
                case GOBLINTYPE:
                    goblinChill.process(plr, i);
                    break;
                case SKELETONTYPE:
                    skeletonChill.process(plr, i);
                    break;
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(DEAD); node != null; node = next) {
            next = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();

            engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
            switch (checkfluid(i, zr_florhit)) {
                case TYPELAVA:
                case TYPEWATER:
                    spr.setZ(zr_florz + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                    break;
            }
        }
    }

    public static int aimove(final int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return 0;
        }

        int ox = spr.getX();
        int oy = spr.getY();
        int oz = spr.getZ();
//		short osect = spr.sectnum;

        int movestate = movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, CLIFFCLIP);

        if (((zr_florz - oz) >> 4) > engine.getTile(spr.getPicnum()).getHeight() + spr.getYrepeat() << 2
                || (movestate & HIT_TYPE_MASK) == HIT_WALL) {
            engine.setsprite(i, ox, oy, oz);

            if ((movestate & HIT_TYPE_MASK) != HIT_WALL) {
                if (game.WH2) {
                    spr.setZ(spr.getZ() + WH2GRAVITYCONSTANT);
                } else {
                    spr.setZ(spr.getZ() + GRAVITYCONSTANT);
                }
                return HIT_SECTOR | zr_florhit;
            }
        }

        spr.setZ(zr_florz);

        return movestate;
    }

    public static int aifly(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return 0;
        }

        int movestate = movesprite(i, ((EngineUtils.sin((spr.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                ((EngineUtils.sin(spr.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, CLIFFCLIP);

        spr.setZ(spr.getZ() - (TICSPERFRAME << 8));
        short ocs = spr.getCstat();
        spr.setCstat(0);
        engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
        spr.setCstat(ocs);
        if (spr.getZ() > zr_florz) {
            spr.setZ(zr_florz);
        }
        if (spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7) < zr_ceilz) {
            spr.setZ(zr_ceilz + (engine.getTile(spr.getPicnum()).getHeight() << 7));
        }

        return movestate;
    }

    public static void aisearch(PLAYER plr, int i, boolean fly) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        spr.setLotag(spr.getLotag() - TICSPERFRAME);

        int osectnum = spr.getSectnum();

        int movestat;
        if (fly) {
            movestat = aifly(i);
        } else {
            movestat = aimove(i);
        }

        if (checkdist(plr, i)) {
            if (plr.getShadowtime() > 0) {
                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                newstatus(i, FLEE);
            } else {
                newstatus(i, ATTACK);
            }
            return;
        }

        if (movestat != 0) {
            if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                    spr.getSectnum()) && spr.getLotag() < 0) {
                spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                newstatus(i, FLEE);
                return;
            }
            if (spr.getLotag() < 0) {
                if (engine.krand() % 100 > 50) {
                    spr.setAng((short) ((spr.getAng() + 512) & 2047));
                } else {
                    spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                }

                spr.setLotag(30);
            } else {
                spr.setAng(spr.getAng() + ((TICSPERFRAME << 4) & 2047));
            }
        }

        if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                spr.getSectnum()) && movestat == 0 && spr.getLotag() < 0) {
            newstatus(i, FACE);
            return;
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec != null && (spr.getSectnum() != osectnum) && sec.getLotag() == 10) {
            warpsprite(i);
        }

        processfluid(i, zr_florhit, fly);

        engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
    }

    public static boolean checksector6(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return false;
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return false;
        }

        if (sec.getFloorz() - (32 << 8) < sec.getCeilingz()) {
            if (sec.getLotag() == 6) {
                newstatus(i, DIE);
            } else {
                engine.deletesprite(i);
                return true;
            }
        }

        return false;
    }

    public static int checkfluid(int i, int zr_florhit) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return 0;
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return 0;
        }

        if ((zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == WATER
                /* || boardService.getSector(spr.sectnum).floorpicnum == LAVA2 */ || sec.getFloorpicnum() == LAVA
                || sec.getFloorpicnum() == SLIME || sec.getFloorpicnum() == FLOORMIRROR
                /*
                 * || boardService.getSector(spr.sectnum).floorpicnum == LAVA1 ||
                 * boardService.getSector(spr.sectnum).floorpicnum == ANILAVA
                 */)) {
            if (sec.getFloorpicnum() == WATER || sec.getFloorpicnum() == SLIME
                    || sec.getFloorpicnum() == FLOORMIRROR) {
                return TYPEWATER;
            } else {
                return TYPELAVA;
            }
        }

        return TYPENONE;
    }

    public static void processfluid(final int i, int zr_florhit, boolean fly) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        switch (checkfluid(i, zr_florhit)) {
            case TYPELAVA:
                if (!fly) {
                    spr.setZ(spr.getZ() + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                    trailingsmoke(i, true);
                    makemonstersplash(LAVASPLASH, i);
                }
                break;
            case TYPEWATER:
                if (!fly) {
                    spr.setZ(spr.getZ() + (engine.getTile(spr.getPicnum()).getHeight() << 5));
                    if (engine.krand() % 100 > 60) {
                        makemonstersplash(SPLASHAROO, i);
                    }
                }
                break;
        }
    }

    public static void castspell(PLAYER plr, final int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), MISSILE);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }


        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        if (game.WH2 || spr.getPicnum() == SPAWNFIREBALL) {
            spr2.setZ(spr.getZ() - ((engine.getTile(spr.getPicnum()).getHeight() >> 1) << 8));
        } else {
            spr2.setZ(engine.getflorzofslope(spr.getSectnum(), spr.getX(), spr.getY()) - ((engine.getTile(spr.getPicnum()).getHeight() >> 1) << 8));
        }
        spr2.setCstat(0); // Hitscan does not hit other bullets
        spr2.setPicnum(MONSTERBALL);
        spr2.setShade(-15);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
        if (spr.getPicnum() == SPAWNFIREBALL) {
            spr2.setAng((short) ((EngineUtils.getAngle(plr.getX() - spr2.getX(), plr.getY() - spr2.getY()) + 2048) & 2047));
        } else {
            spr2.setAng((short) (((EngineUtils.getAngle(plr.getX() - spr2.getX(), plr.getY() - spr2.getY()) + (engine.krand() & 15)
                    - 8) + 2048) & 2047));
        }
        spr2.setXvel((short) (EngineUtils.sin((spr2.getAng() + 2560) & 2047) >> 6));
        spr2.setYvel((short) (EngineUtils.sin((spr2.getAng() + 2048) & 2047) >> 6));

        int discrim = EngineUtils.sqrt((plr.getX() - spr2.getX()) * (plr.getX() - spr2.getX()) + (plr.getY() - spr2.getY()) * (plr.getY() - spr2.getY()));
        if (discrim == 0) {
            discrim = 1;
        }
        if (game.WH2) {
            spr2.setZvel((short) (((plr.getZ() + (8 << 8) - spr2.getZ()) << 7) / discrim));
        } else {
            spr2.setZvel((short) (((plr.getZ() + (48 << 8) - spr2.getZ()) << 7) / discrim));
        }

        spr2.setOwner((short) i);
        spr2.setClipdist(16);
        spr2.setLotag(512);
        spr2.setHitag(0);

        game.pInt.setsprinterpolate(j, spr2);
    }

    public static void skullycastspell(PLAYER plr, int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = engine.insertsprite(spr.getSectnum(), MISSILE);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        spr2.setX(spr.getX());
        spr2.setY(spr.getY());
        if (spr.getPicnum() == SPAWNFIREBALL) {
            spr2.setZ(spr.getZ() - ((engine.getTile(spr.getPicnum()).getHeight() >> 1) << 8));
        } else {
            spr2.setZ(engine.getflorzofslope(spr.getSectnum(), spr.getX(), spr.getY()) - ((engine.getTile(spr.getPicnum()).getHeight() >> 1) << 8));
        }
        spr2.setCstat(0); // Hitscan does not hit other bullets
        spr2.setPicnum(PLASMA);
        spr2.setShade(-15);
        spr2.setXrepeat(64);
        spr2.setYrepeat(64);
        if (spr.getPicnum() == SPAWNFIREBALL) {
            spr2.setAng((short) ((EngineUtils.getAngle(plr.getX() - spr2.getX(), plr.getY() - spr2.getY()) + 2048) & 2047));
        } else {
            spr2.setAng((short) (((EngineUtils.getAngle(plr.getX() - spr2.getX(), plr.getY() - spr2.getY()) + (engine.krand() & 15)
                    - 8) + 2048) & 2047));
        }
        spr2.setXvel((short) (EngineUtils.sin((spr2.getAng() + 2560) & 2047) >> 6));
        spr2.setYvel((short) (EngineUtils.sin((spr2.getAng() + 2048) & 2047) >> 6));

        long discrim = EngineUtils.sqrt((plr.getX() - spr2.getX()) * (plr.getX() - spr2.getX()) + (plr.getY() - spr2.getY()) * (plr.getY() - spr2.getY()));
        if (discrim == 0) {
            discrim = 1;
        }
        spr2.setZvel((short) (((plr.getZ() + (48 << 8) - spr2.getZ()) << 7) / discrim));

        spr2.setOwner((short) i);
        spr2.setClipdist(16);
        spr2.setLotag(512);
        spr2.setHitag(0);
        spr2.setPal(7);

        game.pInt.setsprinterpolate(j, spr2);
    }

    public static void attack(PLAYER plr, final int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int s = 0;
        if (plr.getInvincibletime() > 0 || plr.isGodMode()) {
            return;
        }

        if (plr.getTreasure()[TADAMANTINERING] == 1 && (engine.krand() & 1) != 0) {
            return;
        }

        if (!plr.isDroptheshield() && plr.getShieldpoints() > 0 && plr.getSelectedgun() > 0 && plr.getSelectedgun() < 5) {
            int a = EngineUtils.getAngle(spr.getX() - plr.getX(), spr.getY() - plr.getY());
            if ((a < plr.getAng() && plr.getAng() - a < 128) || (a > plr.getAng() && (((short) plr.getAng() + a) & 2047) < 128)) {
                if (engine.krand() % 100 > 80) {
                    playsound_loc(S_SWORD1 + engine.krand() % 3, plr.getX(), plr.getY());
                    return;
                } else {
                    s = engine.krand() % 50;
                    plr.setShieldpoints(plr.getShieldpoints() - s);
                    if (engine.krand() % 100 > 50) {
                        playsound_loc(S_SWORD1 + engine.krand() % 3, plr.getX(), plr.getY());
                        return;
                    }
                }
            }
            if (plr.getShieldpoints() <= 0) {
                showmessage("Shield useless", 360);
            }
        }

        int k = 5;
        if (!game.WH2) {
            k = engine.krand() % 100;
            if (k > (plr.getArmortype() << 3)) {
                k = 15;
            } else {
                k = 5;
            }
        }

        switch (spr.getDetail()) {
            case SPIDER:
                k = 5;
                break;
            case FISHTYPE:
            case RATTYPE:
                k = 3;
                break;
            case SKELETONTYPE:
                playsound_loc(S_RIP1 + (engine.krand() % 3), spr.getX(), spr.getY());
                if ((engine.krand() % 2) != 0) {
                    playsound_loc(S_GORE1 + (engine.krand() % 4), spr.getX(), spr.getY());
                }
                if ((engine.krand() % 2) != 0) {
                    playsound_loc(S_BREATH1 + (engine.krand() % 6), spr.getX(), spr.getY());
                }

                if (game.WH2) {
                    k = (engine.krand() % 5) + 5;
                } else {
                    k >>= 2;
                }
                break;
            case KATIETYPE: // damage 5 - 50
                playsound_loc(S_DEMONTHROW, spr.getX(), spr.getY());
                k = (engine.krand() % 45) + 5;
                break;

            case DEVILTYPE:
                playsound_loc(S_DEMONTHROW, spr.getX(), spr.getY());
                if (!game.WH2) {
                    k >>= 2;
                }
                break;

            case KOBOLDTYPE:
                playsound_loc(S_GENSWING, spr.getX(), spr.getY());
                if ((engine.krand() % 10) > 4) {
                    playsound_loc(S_KOBOLDHIT, plr.getX(), plr.getY());
                    playsound_loc(S_BREATH1 + (engine.krand() % 6), plr.getX(), plr.getY());
                }
                if (game.WH2) {
                    k = (engine.krand() % 5) + 5;
                } else {
                    k >>= 2;
                }
                break;
            case FREDTYPE:

                /* Sounds for Fred (currently copied from Goblin) */
                playsound_loc(S_GENSWING, spr.getX(), spr.getY());
                if (engine.rand() % 10 > 4) { // playsound
                    playsound_loc(S_SWORD1 + (engine.rand() % 6), spr.getX(), spr.getY());
                }

                k >>= 3;
                break;
            case IMPTYPE:
                if (!game.WH2) {
                    break;
                }
                playsound_loc(S_RIP1 + (engine.krand() % 3), spr.getX(), spr.getY());
                if ((engine.krand() % 2) != 0) {
                    playsound_loc(S_GORE1 + (engine.krand() % 4), spr.getX(), spr.getY());
                }
                if ((engine.krand() % 2) != 0) {
                    playsound_loc(S_BREATH1 + (engine.krand() % 6), spr.getX(), spr.getY());
                }

                k = (engine.krand() % 5) + 5;
                if (k > 8) {
                    plr.setPoisoned(1);
                }
                break;
            case GOBLINTYPE:
                if (game.WH2) {
                    break;
                }

                playsound_loc(S_GENSWING, spr.getX(), spr.getY());
                if ((engine.krand() % 10) > 4) {
                    playsound_loc(S_SWORD1 + (engine.krand() % 6), spr.getX(), spr.getY());
                }
                k >>= 2;
                break;
            case NEWGUYTYPE:
                if (spr.getPicnum() == NEWGUYMACE) { // damage 5 - 20
                    playsound_loc(S_PLRWEAPON2, spr.getX(), spr.getY());
                    if (engine.krand() % 10 > 4) {
                        playsound_loc(S_KOBOLDHIT, plr.getX(), plr.getY());
                        playsound_loc(S_BREATH1 + (engine.krand() % 6), plr.getX(), plr.getY());
                    }
                    k = (engine.krand() % 15) + 5;
                    break;
                }
            case KURTTYPE:
            case GONZOTYPE:
                playsound_loc(S_GENSWING, spr.getX(), spr.getY());
                if (spr.getPicnum() == GONZOCSWAT || spr.getPicnum() == GONZOGSWAT) { // damage 5 - 15
                    if (engine.krand() % 10 > 6) {
                        playsound_loc(S_SWORD1 + (engine.krand() % 6), spr.getX(), spr.getY());
                    }
                    k = (engine.krand() % 15) + 5;
                } else if (spr.getPicnum() == GONZOGHMAT) { // damage 5 - 15
                    if (engine.krand() % 10 > 6) {
                        playsound_loc(S_SWORD1 + (engine.krand() % 6), spr.getX(), spr.getY());
                    }
                    k = (engine.krand() % 10) + 5;
                } else if (spr.getPicnum() == GONZOGSHAT) { // damage 5 - 20
                    if (engine.krand() % 10 > 3) {
                        playsound_loc(S_SWORD1 + (engine.krand() % 6), spr.getX(), spr.getY());
                    }
                    k = (engine.krand() % 15) + 5;
                } else if (spr.getPicnum() == KURTAT) { // damage 5 - 15
                    playsound_loc(S_GENSWING, spr.getX(), spr.getY());
                    if (engine.krand() % 10 > 3) {
                        playsound_loc(S_SWORD1 + (engine.krand() % 6), spr.getX(), spr.getY());
                    }
                    k = (engine.krand() % 10) + 5;
                } else {
                    playsound_loc(S_GENSWING, spr.getX(), spr.getY());
                    if (engine.krand() % 10 > 4) {
                        playsound_loc(S_SOCK1 + (engine.krand() % 4), plr.getX(), plr.getY());
                        playsound_loc(S_BREATH1 + (engine.krand() % 6), plr.getX(), plr.getY());
                    }
                    k = (engine.krand() % 4) + 1;
                }
                break;

            case GRONTYPE:
                if (spr.getPicnum() != GRONSWATTACK) {
                    break;
                }

                if (game.WH2) {
                    k = (engine.krand() % 20) + 5;
                    if (spr.getShade() > 30) {
                        k += engine.krand() % 10;
                    }
                } else {
                    if (spr.getShade() > 30) {
                        k >>= 1;
                    }
                }
                playsound_loc(S_GENSWING, spr.getX(), spr.getY());
                if ((engine.krand() % 10) > 3) {
                    playsound_loc(S_SWORD1 + (engine.krand() % 6), spr.getX(), spr.getY());
                }

                break;
            case MINOTAURTYPE:
                playsound_loc(S_GENSWING, spr.getX(), spr.getY());
                if (engine.krand() % 10 > 4) {
                    playsound_loc(S_SWORD1 + (engine.krand() % 6), spr.getX(), spr.getY());
                }
                if (game.WH2) {
                    k = (engine.krand() % 25) + 5;
                }
                break;
        }

        if (plr.getShieldpoints() > 0) {
            if (s > k) {
                k = 0;
            } else {
                k -= s;
            }
        }

        int a;
        switch (plr.getArmortype()) {
            case 0: // none
                addhealth(plr, -k);
                break;
            case 1: // leather
                a = engine.krand() % 5;
                if (a > k) {
                    k = 0;
                } else {
                    k -= a;
                }
                addarmor(plr, -a);
                addhealth(plr, -k);
                break;
            case 2: // chain
                a = engine.krand() % 10;
                if (a > k) {
                    k = 0;
                } else {
                    k -= a;
                }
                addarmor(plr, -a);
                addhealth(plr, -k);
                break;
            case 3: // plate
                a = engine.krand() % 20;
                if (a > k) {
                    k = 0;
                } else {
                    k -= a;
                }
                addarmor(plr, -a);
                addhealth(plr, -k);
                break;
        }

        startredflash(3 * k);

        if (k == 0) {
            k = 1;
        }

        k = engine.krand() % k;

        plr.setDamage_angvel(plr.getDamage_angvel() + (k << 3));
        plr.setDamage_svel(plr.getDamage_svel() + (k << 3));
        plr.setDamage_vel(plr.getDamage_vel() - (k << 3));

        plr.setHvel(plr.getHvel() + (k << 2));
    }

    public static void checkmove(final int i, int dax, int day) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int movestat = movesprite(i, dax, day, 0, 4 << 8, 4 << 8, CLIFFCLIP);
        if (movestat != 0) {
            spr.setAng((spr.getAng() + TICSPERFRAME) & 2047);
        }
    }

    public static boolean checkdist(PLAYER plr, int i) {
        if (plr.getInvisibletime() > 0 || plr.getHealth() <= 0) {
            return false;
        }

        return checkdist(i, plr.getX(), plr.getY(), plr.getZ());
    }

    public static boolean checkdist(final int i, int x, int y, int z) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return false;
        }

        int attackdist = 512;
        int attackheight = 120;
        if (spr.getDetail() > 0) {
            attackdist = enemy[spr.getDetail()].info.getAttackDist(spr);
            attackheight = enemy[spr.getDetail()].info.attackheight;
        }

        switch (spr.getPicnum()) {
            case LFIRE:
            case SFIRE:
                attackdist = 1024;
                break;
        }

        return (klabs(x - spr.getX()) + klabs(y - spr.getY()) < attackdist)
                && (klabs((z >> 8) - ((spr.getZ() >> 8) - (engine.getTile(spr.getPicnum()).getHeight() >> 1))) <= attackheight);
    }

    public static boolean checksight(PLAYER plr, final int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return false;
        }

        if (plr.getInvisibletime() > 0) {
            checksight_ang = ((engine.krand() & 512) - 256) & 2047;
            return false;
        }

        if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(),
                spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum()) && plr.getInvisibletime() < 0) {
            checksight_ang = (EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()) & 2047);
            if (((spr.getAng() + 2048 - checksight_ang) & 2047) < 1024) {
                spr.setAng((short) ((spr.getAng() + 2048 - (TICSPERFRAME << 1)) & 2047));
            } else {
                spr.setAng((short) ((spr.getAng() + (TICSPERFRAME << 1)) & 2047));
            }

            return true;
        } else {
            checksight_ang = 0;
        }

        return false;
    }

    public static void monsterweapon(final int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        if (spr.getShade() > 20) {
            return;
        }

        if (spr.getPicnum() == SKELETONDEAD || spr.getPicnum() == KOBOLDDEAD) {
            return;
        }

        if ((engine.krand() % 100) < 75) {
            return;
        }

        final int j = engine.insertsprite(spr.getSectnum(), (short) 0);
        Sprite weap = boardService.getSprite(j);
        if (weap == null) {
            return;
        }

        weap.setX(spr.getX());
        weap.setY(spr.getY());
        weap.setZ(spr.getZ() - (24 << 8));
        weap.setShade(-15);
        weap.setCstat(0);
        weap.setPal(0);

        int type = (engine.krand() % 4);
        weap.setPicnum((short) (FLASKBLUE + type));
        weap.setDetail((short) (FLASKBLUETYPE + type));
        weap.setXrepeat(64);
        weap.setYrepeat(64);

        switch (spr.getPicnum()) {
            case NEWGUYDEAD:
                weap.setXrepeat(25);
                weap.setYrepeat(20);
                if (weap.getExtra() < 20) {
                    weap.setPicnum(WEAPON2);
                    weap.setDetail(WEAPON2TYPE);
                } else {
                    weap.setPicnum(QUIVER);
                    weap.setDetail(QUIVERTYPE);
                }

                weap.setPal(0);
                break;

            case MINOTAURDEAD:
                weap.setXrepeat(25);
                weap.setYrepeat(20);
                if (!game.WH2) {
                    if (engine.krand() % 100 > 50) {
                        weap.setPicnum(WEAPON4);
                        weap.setDetail(WEAPON4TYPE);
                    } else {
                        weap.setXrepeat(20);
                        weap.setYrepeat(15);
                        weap.setPicnum(WEAPON6);
                        weap.setDetail(WEAPON6TYPE);
                    }
                } else {
                    weap.setPicnum(WEAPON4);
                    weap.setDetail(WEAPON4TYPE);
                }
                break;

            case GONZOBSHDEAD:
                weap.setPicnum(GONZOBSHIELD);
                weap.setDetail(GONZOSHIELDTYPE);
                weap.setXrepeat(12);
                weap.setYrepeat(12);
                break;

            case GONZOCSWDEAD:
                if (weap.getExtra() > 10) {
                    weap.setPicnum(WEAPON6);
                    weap.setDetail(WEAPON6TYPE);
                    weap.setXrepeat(25);
                    weap.setYrepeat(20);
                } else if (weap.getExtra() > 0) {
                    weap.setPicnum(GOBWEAPON);
                    weap.setDetail(GOBWEAPONTYPE);
                    weap.setXrepeat(25);
                    weap.setYrepeat(20);
                } else {
                    weap.setPicnum(WEAPON1);
                    weap.setDetail(WEAPON1TYPE);
                    weap.setXrepeat(25);
                    weap.setYrepeat(20);
                }
                break;
            case GONZOCSHDEAD:
                weap.setPicnum(GONZOCSHIELD);
                weap.setDetail(GONZOSHIELDTYPE);
                weap.setXrepeat(12);
                weap.setYrepeat(12);
                break;

            case GONZOGSWDEAD:
                weap.setPicnum(WEAPON8);
                weap.setDetail(WEAPON8TYPE);
                weap.setXrepeat(25);
                weap.setYrepeat(20);
                break;
            case GONZOGHMDEAD:
                weap.setPicnum(PLATEARMOR);
                weap.setDetail(PLATEARMORTYPE);
                weap.setXrepeat(26);
                weap.setYrepeat(26);
                break;
            case GONZOGSHDEAD:
                weap.setPicnum(GONZOGSHIELD);
                weap.setDetail(GONZOSHIELDTYPE);
                weap.setXrepeat(12);
                weap.setYrepeat(12);
                break;
            case GOBLINDEAD:
                weap.setXrepeat(16);
                weap.setYrepeat(16);
                weap.setPicnum(GOBWEAPON);
                weap.setDetail(GOBWEAPONTYPE);
                break;
            default:
                if (spr.getPicnum() == GRONDEAD) {
                    if (netgame) {
                        weap.setX(spr.getX());
                        weap.setY(spr.getY());
                        weap.setZ(spr.getZ() - (24 << 8));
                        weap.setShade(-15);
                        weap.setCstat(0);
                        weap.setXrepeat(25);
                        weap.setYrepeat(20);
                        int k = engine.krand() % 4;
                        switch (k) {
                            case 0:
                                weap.setPicnum(WEAPON3);
                                weap.setDetail(WEAPON3TYPE);
                                weap.setXrepeat(25);
                                weap.setYrepeat(20);
                                break;
                            case 1:
                                weap.setPicnum(WEAPON5);
                                weap.setDetail(WEAPON5TYPE);
                                weap.setXrepeat(25);
                                weap.setYrepeat(20);
                                break;
                            case 2:
                                weap.setPicnum(WEAPON6);
                                weap.setDetail(WEAPON6TYPE);
                                weap.setXrepeat(20);
                                weap.setYrepeat(15);
                                break;
                            case 3:
                                weap.setPicnum(SHIELD);
                                weap.setDetail(SHIELDTYPE);
                                weap.setXrepeat(32);
                                weap.setYrepeat(32);
                                break;
                        }
                    } else {
                        switch (spr.getPal()) { // #GDX 09.06.2024 was weap.pal (that always == 0)
                            case 0:
                            default:
                                weap.setPicnum(WEAPON3);
                                weap.setDetail(WEAPON3TYPE);
                                weap.setXrepeat(25);
                                weap.setYrepeat(20);
                                break;
                            case 10:
                                weap.setPicnum(WEAPON5);
                                weap.setDetail(WEAPON5TYPE);
                                weap.setXrepeat(25);
                                weap.setYrepeat(20);
                                break;
                            case 11:
                                weap.setPicnum(WEAPON6);
                                weap.setDetail(WEAPON6TYPE);
                                weap.setXrepeat(20);
                                weap.setYrepeat(15);
                                break;
                            case 12:
                                weap.setPicnum(SHIELD);
                                weap.setDetail(SHIELDTYPE);
                                weap.setXrepeat(32);
                                weap.setYrepeat(32);
                                break;
                        }
                    }
                    weap.setPal(0);
                    break;
                }
                treasurescnt++;
                break;
        }
    }

    public static PLAYER aiGetPlayerTarget(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return null;
        }

        Sprite owner = boardService.getSprite(spr.getOwner());
        if (owner != null) {
            int playernum = owner.getOwner();
            if (playernum >= 4096) {
                return player[playernum - 4096];
            }
        }

        return null;
    }

    public static void actoruse(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        engine.neartag(spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(), spr.getAng(), neartag, 1024, 3);
        Sector sec = boardService.getSector(neartag.tagsector);
        if (sec != null) {
            if (sec.getHitag() == 0) {
                if (sec.getFloorz() != sec.getCeilingz()) {
                    operatesector(player[pyrn], neartag.tagsector);
                }
            }
        }
    }
}
