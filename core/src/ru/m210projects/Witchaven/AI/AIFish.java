package ru.m210projects.Witchaven.AI;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WHFX.warpsprite;
import static ru.m210projects.Witchaven.WHOBJ.newstatus;

public class AIFish {

    public static void create() {
        enemy[FISHTYPE] = new Enemy();
        enemy[FISHTYPE].info = new EnemyInfo(1, 1, 512, 120, 0, 32, false, 10, 0);
        enemy[FISHTYPE].chase = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);
                if (spr.getLotag() < 0) {
                    spr.setLotag(250);
                }

                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }

                if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum()) && plr.getInvisibletime() < 0) {
                    if (checkdist(plr, i)) {
                        if (plr.getShadowtime() > 0) {
                            spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                            newstatus(i, FLEE);
                        } else {
                            newstatus(i, ATTACK);
                        }
                    } else if (engine.krand() % 63 > 60) {
                        spr.setAng((short) (((engine.krand() & 128 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        int movestat = aimove(i);

                        if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                            if ((movestat & HIT_INDEX_MASK) != plr.getSpritenum()) {
                                short daang = (short) ((spr.getAng() - 256) & 2047);
                                spr.setAng(daang);
                                if (plr.getShadowtime() > 0) {
                                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                    newstatus(i, FLEE);
                                } else {
                                    newstatus(i, SKIRMISH);
                                }
                            } else {
                                spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                                newstatus(i, SKIRMISH);
                            }
                        }
                    }
                } else {
                    spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                    newstatus(i, FLEE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                if (checksector6(i)) {
                    return;
                }

                processfluid(i, zr_florhit, false);

                if (osec.getLotag() == KILLSECTOR) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());
                sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((zr_florhit & HIT_TYPE_MASK) == HIT_SECTOR && (sec.getFloorpicnum() == LAVA
                        || sec.getFloorpicnum() == LAVA1 || sec.getFloorpicnum() == ANILAVA)) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }
            }
        };

        enemy[FISHTYPE].die = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                engine.deletesprite(i);
            }
        };

        enemy[FISHTYPE].attack = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                spr.setZ(sec.getFloorz());

                if (checkfluid(i, zr_florhit) == TYPELAVA) {
                    spr.setHitag(spr.getHitag() - 1);
                    if (spr.getHitag() < 0) {
                        newstatus(i, DIE);
                    }
                }

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                if (spr.getLotag() >= 64) {
                    if (checksight(plr, i)) {
                        if (checkdist(plr, i)) {
                            spr.setAng((short) checksight_ang);
                            attack(plr, i);
                        }
                    }
                } else if (spr.getLotag() < 0) {
                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047)); // NEW
                        newstatus(i, FLEE);
                    } else {
                        newstatus(i, CHASE);
                    }
                }
                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                checksector6(i);
            }
        };

        enemy[FISHTYPE].skirmish = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }

                spr.setLotag(spr.getLotag() - TICSPERFRAME);

                if (spr.getLotag() < 0) {
                    newstatus(i, FACE);
                }
                final int osectnum = spr.getSectnum();
                Sector osec = boardService.getSector(osectnum);
                if (osec == null) {
                    return;
                }
                if (aimove(i) != 0) {
                    spr.setAng(EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()));
                    newstatus(i, FACE);
                }

                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    return;
                }

                if ((spr.getSectnum() != osectnum) && (sec.getLotag() == 10)) {
                    warpsprite(i);
                }

                processfluid(i, zr_florhit, false);

                engine.setsprite(i, spr.getX(), spr.getY(), spr.getZ());

                checksector6(i);
            }
        };

        enemy[FISHTYPE].search = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                aisearch(plr, i, false);
                checksector6(i);
            }
        };

        enemy[FISHTYPE].face = new AIState() {
            @Override
            public void process(PLAYER plr, int i) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    engine.deletesprite(i);
                    return;
                }


                boolean cansee = engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                        spr.getSectnum());

                if (cansee && plr.getInvisibletime() < 0) {
                    spr.setAng((short) (EngineUtils.getAngle(plr.getX() - spr.getX(), plr.getY() - spr.getY()) & 2047));

                    if (plr.getShadowtime() > 0) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng() + 1024) & 2047));
                        newstatus(i, FLEE);
                    } else {
                        spr.setOwner(plr.getSpritenum());
                        newstatus(i, CHASE);
                    }
                } else { // get off the wall
                    if (spr.getOwner() == plr.getSpritenum()) {
                        spr.setAng((short) (((engine.krand() & 512 - 256) + spr.getAng()) & 2047));
                        newstatus(i, FINDME);
                    } else if (cansee) {
                        newstatus(i, FLEE);
                    }
                }

                if (checkdist(plr, i)) {
                    newstatus(i, ATTACK);
                }
            }
        };
    }

    public static void premap(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            engine.deletesprite(i);
            return;
        }

        spr.setDetail(FISHTYPE);
        engine.changespritestat(i, FACE);
        enemy[FISHTYPE].info.set(spr);
    }
}
