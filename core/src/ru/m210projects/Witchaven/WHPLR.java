package ru.m210projects.Witchaven;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Witchaven.Types.PLAYER;
import ru.m210projects.Witchaven.Types.PLOCATION;

import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Potions.usapotion;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHOBJ.movesprite;
import static ru.m210projects.Witchaven.WHOBJ.newstatus;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.*;
import static ru.m210projects.Witchaven.WHTAG.operatesector;
import static ru.m210projects.Witchaven.WHTAG.operatesprite;
import static ru.m210projects.Witchaven.Weapons.shootgun;
import static ru.m210projects.Witchaven.Weapons.spikeanimtics;

public class WHPLR {

    public static final PLAYER[] player = new PLAYER[MAXPLAYERS];
    public static final PLOCATION[] gPrevPlayerLoc = new PLOCATION[MAXPLAYERS];

    public static final short[] monsterangle = new short[MAXSPRITESONSCREEN];
    public static final short[] monsterlist = new short[MAXSPRITESONSCREEN];

    public static boolean justteleported;

//    public static int victor = 0;
    public static int autohoriz = 0; // XXX NOT FOR MULTIPLAYER

    public static int pyrn;
    public static int mapon;

    public static void viewBackupPlayerLoc(int nPlayer) {
        Sprite pSprite = boardService.getSprite(player[nPlayer].getSpritenum());
        if (pSprite == null) {
            return;
        }

        PLOCATION pPLocation = gPrevPlayerLoc[nPlayer];
        pPLocation.x = pSprite.getX();
        pPLocation.y = pSprite.getY();
        pPLocation.z = player[nPlayer].getZ();
        pPLocation.ang = player[nPlayer].getAng();
        pPLocation.horiz = player[nPlayer].getHoriz() + player[nPlayer].getJumphoriz();
    }

    public static void playerdead(PLAYER plr) {
        if (plr.isDead()) {
            return;
        }

        if (plr.getPotion()[0] > 0 && plr.getSpiked() == 0) {
            int i = plr.getCurrentpotion();
            plr.setCurrentpotion(0);
            usapotion(plr);
            plr.setCurrentpotion(i);
            return;
        }

        plr.setCurrspikeframe(0);

        if (plr.getSpiked() == 1) {
            plr.setSpiketics(spikeanimtics[0].daweapontics);
            playsound_loc(S_GORE1, plr.getX(), plr.getY());
            SND_Sound(S_HEARTBEAT);
        }

        SND_Sound(S_PLRDIE1);

//	 	netsendmove();
        plr.setDead(true);
    }

    public static void initplayersprite(PLAYER plr) {
        if (difficulty > 1) {
            plr.setCurrweapon(1);
            plr.setSelectedgun(1);
        } else {
            plr.setCurrweapon(4);
            plr.setSelectedgun(4);
        }

        plr.setFallz(0);
        plr.setDahand(0); // #GDX 04.06.2024
        plr.setShootgunzvel(0);
        plr.setIhaveflag(0);
        plr.setJustplayed(0);
        plr.setLopoint(0);
        plr.setWalktoggle(0);
        plr.setRunningtime(0);
        plr.setOldhoriz(0);
        plr.setDamage_vel(0);
        plr.setDamage_svel(0);
        plr.setDamage_angvel(0);

        plr.setDropshieldcnt(0);
        plr.setDroptheshield(false);
        plr.setWeapondrop(0);
        plr.setWeapondropgoal(0);

        plr.setCurrentpotion(0);
        plr.setHelmettime(-1);
        plr.setShadowtime(-1);
        plr.setNightglowtime(-1);
        plr.setStrongtime(-1);
        plr.setInvisibletime(-1);
        plr.setManatime(-1);
        plr.setCurrentorb(0);
        plr.setCurrweaponfired(3);
        plr.setCurrweaponanim(0);
        plr.setCurrweaponattackstyle(0);
        plr.setCurrweaponflip(0);

        plr.setVampiretime(0);
        plr.setShieldpoints(0);
        plr.setShieldtype(0);
        plr.setDead(false);
        plr.setSpiked(0);
        plr.setShockme(-1);
        plr.setPoisoned(0);
        plr.setPoisontime(-1);

        plr.setOldsector(plr.getSector());
        plr.setHoriz(100);
        plr.setHeight(getPlayerHeight());
        Sector sec = boardService.getSector(plr.getSector());
        if (sec != null) {
            plr.setZ(sec.getFloorz() - (plr.getHeight() << 8));
        }

        plr.setSpritenum(engine.insertsprite(plr.getSector(), (short) 0));

        plr.setOnsomething(1);

        Sprite plrSprite = plr.getSprite();
        plrSprite.setX(plr.getX());
        plrSprite.setY(plr.getY());
        plrSprite.setZ(plr.getZ() + (plr.getHeight() << 8));
        plrSprite.setCstat(1 + 256);
        plrSprite.setPicnum(game.WH2 ? GRONSW : FRED);
        plrSprite.setShade(0);
        plrSprite.setXrepeat(36);
        plrSprite.setYrepeat(36);
        plrSprite.setAng((short) plr.getAng());
        plrSprite.setXvel(0);
        plrSprite.setYvel(0);
        plrSprite.setZvel(0);
        plrSprite.setOwner((short) (4096 + myconnectindex));
        plrSprite.setLotag(0);
        plrSprite.setHitag(0);
        plrSprite.setPal((short) (game.WH2 ? 10 : 1));
        if (game.WH2) {
            plrSprite.setClipdist(48);
        }

        plr.setSelectedgun(0);

        if (game.WH2) {
            for (int i = 0; i <= 9; i++) {
                if (i < 5) {
                    plr.getAmmo()[i] = 40;
                    plr.getWeapon()[i] = 1;
                } else {
                    plr.getAmmo()[i] = 0;
                    plr.getWeapon()[i] = 0;
                }
                if (i < 8) {
                    plr.getOrb()[i] = 0;
                    plr.getOrbammo()[i] = 0;
                }
            }

            if (difficulty > 1) {
                plr.getWeapon()[0] = plr.getWeapon()[1] = 1;
                plr.getAmmo()[0] = 32000;
                plr.getAmmo()[1] = 45;
            }

        } else {

            if (difficulty > 1) {
                for (int i = 0; i <= 9; i++) {
                    plr.getAmmo()[i] = 0;
                    plr.getWeapon()[i] = 0;
                    if (i < 8) {
                        plr.getOrb()[i] = 0;
                        plr.getOrbammo()[i] = 0;
                    }
                }
                plr.getWeapon()[0] = plr.getWeapon()[1] = 1;
                plr.getAmmo()[0] = 32000;
                plr.getAmmo()[1] = 45;
            } else {
                for (int i = 0; i <= 9; i++) {
                    plr.getAmmo()[i] = 0;
                    plr.getWeapon()[i] = 0;
                    if (i < 5) {
                        plr.getAmmo()[i] = 40;
                        plr.getWeapon()[i] = 1;
                    }
                    if (i < 8) {
                        plr.getOrb()[i] = 0;
                        plr.getOrbammo()[i] = 0;
                    }
                }
            }
        }

        Arrays.fill(plr.getPotion(), 0);
        for (int i = 0; i < MAXTREASURES; i++) {
            plr.getTreasure()[i] = 0;
        }

        plr.setLvl(1);
        plr.setScore(0);
        plr.setHealth(100);
        plr.setMaxhealth(100);
        plr.setArmor(0);
        plr.setArmortype(0);
        plr.setCurrentorb(0);
        plr.setCurrentpotion(0);

        if (difficulty > 1) {
            plr.setCurrweapon(1);
            plr.setSelectedgun(1);
        } else {
            plr.setCurrweapon(4);
            plr.setSelectedgun(4);
        }

        if (game.WH2) {
            plr.getPotion()[0] = 3;
            plr.getPotion()[3] = 1;
            plr.setCurrweapon(4);
            plr.setSelectedgun(4);
        }

        plr.setCurrweaponfired(3);
        plr.setCurrweaponflip(0);

        Arrays.fill(plr.getOrbactive(), -1);

        lockclock = engine.getTotalClock();
        playertorch = 0;

        plr.setSpellbookflip(0);

        plr.setManatime(-1);
        plr.setInvincibletime(-1);
        plr.setHasshot(0);
        plr.setOrbshot(0);
        displaytime = -1;
        plr.setShadowtime(-1);
        plr.setHelmettime(-1);
        plr.setNightglowtime(-1);
        plr.setStrongtime(-1);
        plr.setInvisibletime(-1);
    }

    public static void updateviewmap(PLAYER plr) {
        Sector sec = boardService.getSector(plr.getSector());
        if (sec != null) {
            int wallid = sec.getWallptr();
            show2dsector.setBit(plr.getSector());

            for (int j = sec.getWallnum(); j > 0; j--) {
                Wall wal = boardService.getWall(wallid++);
                if (wal == null) {
                    continue;
                }

                sec = boardService.getSector(wal.getNextsector());
                if (sec == null) {
                    continue;
                }

                if ((wal.getCstat() & 0x0071) != 0) {
                    continue;
                }

                Wall nwall = boardService.getWall(wal.getNextwall());
                if (nwall != null && (nwall.getCstat() & 0x0071) != 0) {
                    continue;
                }

                if (sec.getCeilingz() >= sec.getFloorz()) {
                    continue;
                }
                show2dsector.setBit(wal.getNextsector());
            }
        }
    }

    public static void plruse(PLAYER plr) {
        engine.neartag(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), (short) plr.getAng(), neartag, 1024, 3);

        Sector nearSec = boardService.getSector(neartag.tagsector);
        if (nearSec != null) {
            if (nearSec.getHitag() == 0) {
                operatesector(plr, neartag.tagsector);
            } else {
                short daang = (short) plr.getAng();
                int daz2 = (int) (100 - plr.getHoriz()) * 2000;
                engine.hitscan(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), // Start position
                        EngineUtils.cos((daang + 2048) & 2047), // X vector of 3D ang
                        EngineUtils.sin((daang + 2048) & 2047), // Y vector of 3D ang
                        daz2, // Z vector of 3D ang
                        pHitInfo, CLIPMASK1);

                Wall hitWall = boardService.getWall(pHitInfo.hitwall);
                if (hitWall != null) {
                    if ((klabs(plr.getX() - pHitInfo.hitx) + klabs(plr.getY() - pHitInfo.hity) < 512)
                            && (klabs((plr.getZ() >> 8) - ((pHitInfo.hitz >> 8) - (64))) <= (512 >> 3))) {
                        int pic = hitWall.getPicnum();
                        if (pic == PENTADOOR1 || pic == PENTADOOR2 || (pic >= PENTADOOR3 && pic <= PENTADOOR7)) {
                            showmessage("find door trigger", 360);
                        }
                    }
                }
                playsound_loc(S_PUSH1 + (engine.krand() % 2), plr.getX(), plr.getY());
            }
        }

        Sprite nearSpr = boardService.getSprite(neartag.tagsprite);
        if (nearSpr != null) {
            if (nearSpr.getLotag() == 1) {
                if (nearSpr.getPicnum() == PULLCHAIN1 || nearSpr.getPicnum() == SKULLPULLCHAIN1) {
                    nearSpr.setLotag(0);
                    newstatus(neartag.tagsprite, PULLTHECHAIN);
                } else if (nearSpr.getPicnum() == LEVERUP) {
                    nearSpr.setLotag(0);
                    newstatus(neartag.tagsprite, ANIMLEVERUP);
                }
                for (int i = 0; i < boardService.getSectorCount(); i++) {
                    Sector sec = boardService.getSector(i);
                    if (sec != null && sec.getHitag() == nearSpr.getHitag()) {
                        operatesector(plr, i);
                    }
                }
            } else {
                operatesprite(plr, neartag.tagsprite);
            }
        }
    }

    public static void chunksofmeat(PLAYER plr, final int hitsprite, int hitx, int hity, int hitz, int hitsect, int ignoredAng) {
        if (!cfg.gGameGore) {
            return;
        }

        Sprite hitSpr = boardService.getSprite(hitsprite);
        if (hitSpr == null) {
            return;
        }

        if (hitSpr.getPicnum() == JUDY || hitSpr.getPicnum() == JUDYATTACK1
                || hitSpr.getPicnum() == JUDYATTACK2) {
            return;
        }

        final int zgore = getZGore(plr, hitSpr);

        if (hitSpr.getPicnum() == WILLOW || hitSpr.getPicnum() == WILLOWEXPLO
                || hitSpr.getPicnum() == WILLOWEXPLO + 1 || hitSpr.getPicnum() == WILLOWEXPLO + 2
                || hitSpr.getPicnum() == GUARDIAN || hitSpr.getPicnum() == GUARDIANATTACK
                || hitSpr.getPicnum() == DEMON) {
            return;
        }

        if (hitSpr.getPicnum() == SKELETON || hitSpr.getPicnum() == SKELETONATTACK
                || hitSpr.getPicnum() == SKELETONDIE) {
            playsound_loc(S_SKELHIT1 + (engine.krand() % 2), hitSpr.getX(), hitSpr.getY());
        } else {
            if (engine.krand() % 100 > 60) {
                playsound_loc(S_GORE1 + (engine.krand() % 4), hitSpr.getX(), hitSpr.getY());
            }
        }

        if (hitSpr.getStatnum() < MAXSTATUS) {
            int chunk = REDCHUNKSTART;
            for (int k = 0; k < zgore; k++) {
                int newchunk = 0;

                final int j = engine.insertsprite(hitsect, CHUNKOMEAT);
                Sprite spr2 = boardService.getSprite(j);
                if (spr2 == null) {
                    return;
                }
                spr2.setX(hitx);
                spr2.setY(hity);
                spr2.setZ(hitz);
                spr2.setCstat(0);

                if (engine.krand() % 100 > 50) {
                    switch (hitSpr.getDetail()) {
                        case GRONTYPE:
                        case DEVILTYPE:
                        case SKULLYTYPE:
                        case FATWITCHTYPE:
                        case JUDYTYPE:
                            chunk = REDCHUNKSTART + (engine.krand() % 8);
                            break;
                        case KOBOLDTYPE:
                            if (hitSpr.getPal() == 0) {
                                chunk = BROWNCHUNKSTART + (engine.krand() % 8);
                            }
                            if (hitSpr.getPal() == 4) {
                                chunk = GREENCHUNKSTART + (engine.krand() % 8);
                            }
                            if (hitSpr.getPal() == 7) {
                                chunk = REDCHUNKSTART + (engine.krand() % 8);
                            }
                            break;
                        case DRAGONTYPE:
                            chunk = GREENCHUNKSTART + (engine.krand() % 8);
                            break;
                        case FREDTYPE:
                            chunk = BROWNCHUNKSTART + (engine.krand() % 8);
                            break;
                        case GOBLINTYPE:
                        case IMPTYPE:
                            if (game.WH2 && (hitSpr.getPicnum() == IMP || hitSpr.getPicnum() == IMPATTACK)) {
                                if (hitSpr.getPal() == 0) {
                                    chunk = GREENCHUNKSTART + (engine.krand() % 8);
                                }
                            } else {
                                if (hitSpr.getPal() == 0) {
                                    chunk = GREENCHUNKSTART + (engine.krand() % 8);
                                }
                                if (hitSpr.getPal() == 4) {
                                    chunk = BROWNCHUNKSTART + (engine.krand() % 8);
                                }
                                if (hitSpr.getPal() == 5) {
                                    chunk = TANCHUNKSTART + (engine.krand() % 8);
                                }
                            }
                            break;
                        case MINOTAURTYPE:
                            chunk = TANCHUNKSTART + (engine.krand() % 8);
                            break;
                        case SPIDERTYPE:
                            chunk = GREYCHUNKSTART + (engine.krand() % 8);
                            break;
                    }
                } else {
                    newchunk = 1;
                    if (!game.WH2) {
                        chunk = NEWCHUNK + (engine.krand() % 9);
                    } else {
                        chunk = REDCHUNKSTART + (engine.krand() % 8);
                    }
                }

                if (hitSpr.getDetail() == SKELETONTYPE) {
                    chunk = BONECHUNK1 + (engine.krand() % 9);
                }

                if (plr.getWeapon()[2] == 3 && plr.getCurrweapon() == 2) {
                    spr2.setPicnum(ARROWFLAME);
                } else {
                    spr2.setPicnum(chunk); // = REDCHUNKSTART + (rand() % 8);
                }

                spr2.setShade(-16);
                spr2.setXrepeat(64);
                spr2.setYrepeat(64);
                spr2.setClipdist(16);
                spr2.setAng((short) (((engine.krand() & 1023) - 1024) & 2047));
                spr2.setXvel((short) ((engine.krand() & 1023) - 512));
                spr2.setYvel((short) ((engine.krand() & 1023) - 512));
                spr2.setZvel((short) ((engine.krand() & 1023) - 512));
                if (newchunk == 1) {
                    spr2.setZvel(spr2.getZvel() << 1);
                }
                spr2.setOwner(plr.getSprite().getOwner());
                spr2.setLotag(512);
                spr2.setHitag(0);
                spr2.setPal(0);
                movesprite(j, ((EngineUtils.sin((spr2.getAng() + 512) & 2047)) * TICSPERFRAME) << 3,
                        ((EngineUtils.sin(spr2.getAng() & 2047)) * TICSPERFRAME) << 3, 0, 4 << 8, 4 << 8, 0);
            }
        }

    }

    private static int getZGore(PLAYER plr, Sprite hitSpr) {
        int zgore = 0;
        switch (plr.getSelectedgun()) {
            case 1:
            case 2:
            case 6:
                zgore = 1;
                break;
            case 3:
            case 4:
            case 7:
                zgore = 2;
                break;
            case 5:
            case 8:
            case 9:
                zgore = 3;
                break;
        }

        if (hitSpr.getStatnum() == NUKED) {
            zgore = 32;
        }

        if (hitSpr.getPicnum() == RAT) {
            zgore = 1;
        }
        return zgore;
    }

    public static void addhealth(PLAYER plr, int hp) {
        if (plr.isGodMode() && hp < 0) {
            return;
        }

        plr.setHealth(plr.getHealth() + hp);

        if (plr.getHealth() < 0) {
            plr.setHealth(0);
        }
    }

    public static void addarmor(PLAYER plr, int arm) {
        plr.setArmor(plr.getArmor() + arm);

        if (plr.getArmor() < 0) {
            plr.setArmor(0);
            plr.setArmortype(0);
        }
    }

    public static void addscore(PLAYER plr, int score) {
        if (plr == null) {
            return;
        }

        plr.setScore(plr.getScore() + score);
        expgained += score;

        goesupalevel(plr);
    }

    public static void goesupalevel(PLAYER plr) {
        if (game.WH2) {
            goesupalevel2(plr);
        } else {
            goesupalevel1(plr);
        }
    }

    public static void goesupalevel2(PLAYER plr) {
        switch (plr.getLvl()) {
            case 0:
            case 1:
                if (plr.getScore() > 9999) {
                    showmessage("thou art a warrior", 360);
                    plr.setLvl(2);
                    plr.setMaxhealth(120);
                }
                break;
            case 2:
                if (plr.getScore() > 19999) {
                    showmessage("thou art a swordsman", 360);
                    plr.setLvl(3);
                    plr.setMaxhealth(140);
                }
                break;
            case 3:
                if (plr.getScore() > 29999) {
                    showmessage("thou art a hero", 360);
                    plr.setLvl(4);
                    plr.setMaxhealth(160);
                }
                break;
            case 4:
                if (plr.getScore() > 39999) {
                    showmessage("thou art a champion", 360);
                    plr.setLvl(5);
                    plr.setMaxhealth(180);
                }
                break;
            case 5:
                if (plr.getScore() > 49999) {
                    showmessage("thou art a superhero", 360);
                    plr.setLvl(6);
                    plr.setMaxhealth(200);
                }
                break;
            case 6:
                if (plr.getScore() > 59999) {
                    showmessage("thou art a lord", 360);
                    plr.setLvl(7);
                }
        }
    }

    public static void goesupalevel1(PLAYER plr) {
        if (plr.getScore() > 2250 && plr.getScore() < 4499 && plr.getLvl() < 2) {
            showmessage("thou art 2nd level", 360);
            plr.setLvl(2);
            plr.setMaxhealth(120);
        } else if (plr.getScore() > 4500 && plr.getScore() < 8999 && plr.getLvl() < 3) {
            showmessage("thou art 3rd level", 360);
            plr.setLvl(3);
            plr.setMaxhealth(140);
        } else if (plr.getScore() > 9000 && plr.getScore() < 17999 && plr.getLvl() < 4) {
            showmessage("thou art 4th level", 360);
            plr.setLvl(4);
            plr.setMaxhealth(160);
        } else if (plr.getScore() > 18000 && plr.getScore() < 35999 && plr.getLvl() < 5) {
            showmessage("thou art 5th level", 360);
            plr.setLvl(5);
            plr.setMaxhealth(180);
        } else if (plr.getScore() > 36000 && plr.getScore() < 74999 && plr.getLvl() < 6) {
            showmessage("thou art 6th level", 360);
            plr.setLvl(6);
            plr.setMaxhealth(200);
        } else if (plr.getScore() > 75000 && plr.getScore() < 179999 && plr.getLvl() < 7) {
            showmessage("thou art 7th level", 360);
            plr.setLvl(7);
        } else if (plr.getScore() > 180000 && plr.getScore() < 279999 && plr.getLvl() < 8) {
            showmessage("thou art 8th level", 360);
            plr.setLvl(8);
        } else if (plr.getScore() > 280000 && plr.getScore() < 379999 && plr.getLvl() < 9) {
            showmessage("thou art hero", 360);
            plr.setLvl(9);
        }
    }

    public static void lockon(PLAYER plr, int numshots, int shootguntype) {
        int n = 0;

        for (int i = 0; i < MAXSPRITES; i++) {
            Sprite spr = boardService.getSprite(i);
            if (spr == null || spr.getStatnum() == MAXSTATUS) {
                continue;
            }

            if (i != plr.getSpritenum()) {
                if (spr.getHitag() < 0) {
                    continue; // dead
                }

                switch (spr.getDetail()) {
                    case KOBOLDTYPE:
                    case DEVILTYPE:
                    case IMPTYPE:
                    case MINOTAURTYPE:
                    case SKELETONTYPE:
                    case GRONTYPE:
                    case DEMONTYPE:
                    case GUARDIANTYPE:
                    case WILLOWTYPE:
                    case NEWGUYTYPE:
                    case KURTTYPE:
                    case GONZOTYPE:
                    case KATIETYPE:

                    case FREDTYPE:
                    case GOBLINTYPE:
                    case SPIDERTYPE:
                        int dx = spr.getX() - plr.getX();
                        int dy = spr.getY() - plr.getY();
                        int nAngle = EngineUtils.getAngle(dx, dy);
                        int losAngle = ((1024 + nAngle - (int) plr.getAng()) & 2047) - 1024;
                        if (klabs(losAngle) > 768) {
                            continue;
                        }

                        if (engine.cansee(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7),
                                spr.getSectnum())) {
                            monsterangle[n] = (short) EngineUtils.getAngle(spr.getX() - plr.getX(), spr.getY() - plr.getY());
                            monsterlist[n] = (short) i;
                            n++;
                        }

                        break;
                }
            }
        }

        int daang = (int) (plr.getAng() - ((numshots * (128 / numshots)) >> 1));
        for (int k = 0, s = 0; k < numshots; k++) {
            if (n > 0) {
                Sprite spr = boardService.getSprite(monsterlist[s]);
                if (spr != null) {
                    daang = monsterangle[s];
                    plr.setShootgunzvel(((spr.getZ() - (48 << 8) - plr.getZ()) << 8)
                            / EngineUtils.sqrt((spr.getX() - plr.getX()) * (spr.getX() - plr.getX()) + (spr.getY() - plr.getY()) * (spr.getY() - plr.getY())));
                }
                s = ((s + 1) % n);
            } else {
                daang += (128 / numshots);
            }
            shootgun(plr, daang, shootguntype);
        }
    }

    public static int getPlayerHeight() {
        return game.WH2 ? WH2PLAYERHEIGHT : PLAYERHEIGHT;
    }
}
