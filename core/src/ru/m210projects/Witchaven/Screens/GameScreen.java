package ru.m210projects.Witchaven.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.ScreenAdapters.GameAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Witchaven.Factory.WHControl;
import ru.m210projects.Witchaven.Factory.WHMenuHandler;
import ru.m210projects.Witchaven.Factory.WHRenderer;
import ru.m210projects.Witchaven.Main;
import ru.m210projects.Witchaven.Menu.MenuInterfaceSet;
import ru.m210projects.Witchaven.Types.EpisodeInfo;
import ru.m210projects.Witchaven.Types.PLAYER;
import ru.m210projects.Witchaven.Types.PLOCATION;
import ru.m210projects.Witchaven.WHSND;
import ru.m210projects.Witchaven.Whmap;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Witchaven.Animate.doanimations;
import static ru.m210projects.Witchaven.Config.WhKeys;
import static ru.m210projects.Witchaven.Factory.WHControl.*;
import static ru.m210projects.Witchaven.Factory.WHMenuHandler.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Menu.WHMenuUserContent.checkEpisodeResources;
import static ru.m210projects.Witchaven.Menu.WHMenuUserContent.resetEpisodeResources;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Potions.potionchange;
import static ru.m210projects.Witchaven.Potions.usapotion;
import static ru.m210projects.Witchaven.Spellbooks.*;
import static ru.m210projects.Witchaven.WH1Names.FREDDEAD;
import static ru.m210projects.Witchaven.WH1Names.SPIDER;
import static ru.m210projects.Witchaven.WH2MUS.attacktheme;
import static ru.m210projects.Witchaven.WH2MUS.startsong;
import static ru.m210projects.Witchaven.WHANI.animateobjs;
import static ru.m210projects.Witchaven.WHFX.*;
import static ru.m210projects.Witchaven.WHOBJ.*;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.*;
import static ru.m210projects.Witchaven.WHTAG.animatetags;
import static ru.m210projects.Witchaven.WHTAG.dodelayitems;
import static ru.m210projects.Witchaven.Weapons.*;
import static ru.m210projects.Witchaven.Whldsv.*;
import static ru.m210projects.Witchaven.Whmap.loadnewlevel;
import static ru.m210projects.Witchaven.Whmap.nextlevel;

public class GameScreen extends GameAdapter {

    public int gNameShowTime;

    private final Main game;

    public GameScreen(Main game) {
        super(game, Main.gLoadScreen);
        this.game = game;
        for (int i = 0; i < MAXPLAYERS; i++) {
            player[i] = new PLAYER();
            gPrevPlayerLoc[i] = new PLOCATION();
        }
    }

    @Override
    public void ProcessFrame(BuildNet net) {
        for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
            player[i].getInput().Copy(net.gFifoInput[net.gNetFifoTail & 0xFF][i]);
        }
        net.gNetFifoTail++;

        if (game.gPaused || !DemoScreen.isDemoPlaying() && !DemoScreen.isDemoScreen(this) && (game.menu.gShowMenu || Console.out.isShowing())) {
            // if(!game.menu.isOpened(game.menu.mMenus[LASTSAVE]))
            return;
        }

        gDemoScreen.onRecord();

        int TICSPERFRAME = engine.getTimer().getFrameTicks(); // TODO: Temporaly code to reset interpolation in LegacyTimer
        lockclock += TICSPERFRAME;

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            player[i].setOldsector(player[i].getSector());
        }

        PLAYER plr = player[pyrn];
        viewBackupPlayerLoc(pyrn);

        processinput(pyrn);
        updateviewmap(plr);
        updatepaletteshifts();

        processobjs(plr);
        animateobjs(plr);
        animatetags(pyrn);
        doanimations();
        dodelayitems(TICSPERFRAME);
        dofx();
        speelbookprocess(plr);
        timerprocess(plr);
        weaponsprocess(pyrn);
        whtime();

        updatesounds();

        if (followmode) {
            followa += (int) followang;

            followx += (followvel * EngineUtils.sin((512 + 2048 - followa) & 2047)) >> 10;
            followy += (followvel * EngineUtils.sin((512 + 1024 - 512 - followa) & 2047)) >> 10;

            followx += (followsvel * EngineUtils.sin((512 + 1024 - 512 - followa) & 2047)) >> 10;
            followy -= (followsvel * EngineUtils.sin((512 + 2048 - followa) & 2047)) >> 10;
        }
    }

    private void whtime() {
        fortieth++;
        if (fortieth == 40) {
            fortieth = 0;
            seconds++;
        }
        if (seconds == 60) {
            minutes++;
            seconds = 0;
        }
        if (minutes == 60) {
            hours++;
            minutes = 0;
        }
    }

    @Override
    public void DrawWorld(float smooth) {
        drawscreen(pyrn, (int) smooth);
        if (!game.gPaused && game.WH2 && songptr.handle != null && !songptr.handle.isPlaying()) {
            startsong(engine.rand() % 2);
            attacktheme = 0;
        }
    }

    @Override
    public void DrawHud(float smooth) {
        WHRenderer renderer = game.getRenderer();
        if (pMenu.gShowMenu && !(pMenu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            return;
        }

        if (!player[pyrn].isDead()) {
            drawweapons(pyrn);
        }
        if (player[pyrn].getSpiked() == 1) {
            spikeheart(player[pyrn]);
        }
        if (scarytime >= 0) {
            drawscary();
        }

        drawInterface(player[pyrn]);

        if (game.isCurrentScreen(gGameScreen) && engine.getTotalClock() < gNameShowTime) {
            Transparent transp = Transparent.None;
            if (engine.getTotalClock() > gNameShowTime - 20) {
                transp = Transparent.Bit1;
            }
            if (engine.getTotalClock() > gNameShowTime - 10) {
                transp = Transparent.Bit2;
            }

            if (cfg.showMapInfo && !game.menu.gShowMenu) {
                if (gCurrentEpisode != null && gCurrentEpisode.getMap(mapon) != null) {
                    game.getFont(1).drawTextScaled(renderer, 160, 114, gCurrentEpisode.getMap(mapon).title, 1.0f, 0, 0, TextAlign.Center, transp, ConvertType.Normal, true);
                } else if (boardfilename != null) {
                    game.getFont(1).drawTextScaled(renderer, 160, 114, boardfilename.getName(), 1.0f, 0, 0, TextAlign.Center, transp, ConvertType.Normal, true);
                }
            }
        }

        WHITE_DAC.setIntensive(whitecount);
        RED_DAC.setIntensive(redcount);
        GREEN_DAC.setIntensive(greencount);
        BLUE_DAC.setIntensive(bluecount);

        renderer.scrSetDac();
    }

    protected boolean gameKeyDownCommon(GameKey gameKey, boolean inGame) {
        // the super has console button handling
        if (super.gameKeyDown(gameKey)) {
            return true;
        }

        WHMenuHandler menu = game.menu;
        if (GameKeys.Menu_Toggle.equals(gameKey)) {
            if (inGame) {
                menu.mOpen(menu.mMenus[GAME], -1);
            } else {
                menu.mOpen(menu.mMenus[MAIN], -1);
            }
            return true;
        }

        if (WhKeys.Show_Loadmenu.equals(gameKey)) {
            if (numplayers > 1 /*|| mFakeMultiplayer*/) {
                return false;
            }
            menu.mOpen(menu.mMenus[LOADGAME], -1);
            return true;
        }

        if (WhKeys.Show_Sounds.equals(gameKey)) {
            menu.mOpen(menu.mMenus[AUDIOSET], -1);
            return true;
        }

        if (WhKeys.Show_Options.equals(gameKey)) {
            menu.mOpen(menu.mMenus[OPTIONS], -1);
            return true;
        }

        if (WhKeys.Quit.equals(gameKey)) {
            menu.mOpen(menu.mMenus[QUIT], -1);
            return true;
        }

        if (WhKeys.Gamma.equals(gameKey)) {
            menu.mOpen(menu.mMenus[COLORCORR], -1);
            return true;
        }

        if (WhKeys.Make_Screenshot.equals(gameKey)) {
            makeScreenshot();
            return true;
        }

        return false;
    }

    protected void makeScreenshot() {
        Renderer renderer = game.getRenderer();
        String name;
        if (mUserFlag == UserFlag.UserMap) {
            name = "scr-" + FileUtils.getFullName(boardfilename.getName()) + "-xxxx.png";
        } else {
            name = "scr-map" + mapon + "-xxxx.png";
        }

        String filename = renderer.screencapture(game.getUserDirectory(), name);
        if (filename != null) {
            showmessage(filename + " saved", 240);
        } else {
            showmessage("Screenshot not saved. Access denied!", 240);
        }
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        if (gameKeyDownCommon(gameKey, true)) {
            return true;
        }
        
        WHMenuHandler menu = game.menu;

        // non mappable function keys
        if (WhKeys.Show_Savemenu.equals(gameKey)) {
            if (game.nNetMode == NetMode.Single) {
                gGameScreen.capture(160, 100);
                menu.mOpen(menu.mMenus[SAVEGAME], -1);
                return true;
            }
        }  else if (WhKeys.Quicksave.equals(gameKey)) {
            quicksave();
            return true;
        } else if (WhKeys.Quickload.equals(gameKey)) {
            quickload();
            return true;
        }

        return false;
    }

    @Override
    public void PostFrame(BuildNet net) {
        if (gQuickSaving) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(),"[quicksave_" + quickslot + "]", "quicksav" + quickslot + ".sav");
                quickslot ^= 1;
                gQuickSaving = false;
            } else {
                gGameScreen.capture(160, 100);
            }
        }

        if (gAutosaveRequest) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(),"[autosave]", "autosave.sav");
                gAutosaveRequest = false;
            } else {
                gGameScreen.capture(160, 100);
            }
        }
    }

    @Override
    protected boolean prepareboard(Entry map) {
        sndStopMusic();
        game.nNetMode = NetMode.Single;
        gNameShowTime = 500;
        followmode = false;
        ((WHControl) game.getProcessor()).reset();
        if (Whmap.prepareboard(this, map) == null) {
            game.show();
            return false;
        }

        return true;
    }

    public void processinput(int num) {

        int goalz, lohit, loz, tics, xvect, yvect;

        int oldposx, oldposy;
        int dist;
        int feetoffground;
        int onsprite = -1;

        PLAYER plr = player[num];

        int bits = plr.getInput().bits;

        if ((bits & (CtrlJump)) == 0) {
            plr.setKeytoggle(false);
        }

        boolean onground;

        if (plr.getHealth() <= 0) {
            playerdead(plr);
            if (plr.isDead()) {
                if (plr.getHoriz() < 100 + (YDIM >> 1)) {
                    plr.setHoriz(plr.getHoriz() + (TICSPERFRAME << 1));
                }

                if (!DemoScreen.isDemoPlaying()) {

                    if (lastload != null && lastload.exists()) {
                        showmessage("Press \"USE\" to load last saved game or press \"ENTER\" to restart level", 320);

                        if ((bits & CtrlOpen) != 0) {
                            game.changeScreen(gLoadScreen);
                            gLoadScreen.init(() -> {
                                if (!loadgame(lastload)) {
                                    game.GameMessage("Can't load game!");
                                }
                            });
                            return;
                        }
                    } else {
                        showmessage("Press \"ENTER\" to restart level", 320);
                    }

                    if (Gdx.input.isKeyPressed(Keys.ENTER)) {
                        if (mUserFlag == UserFlag.UserMap) {
                            loadboard(boardfilename, null);
                            return;
                        }

                        // restart
                        initplayersprite(plr);
                        justteleported = true;
                        lockclock = engine.getTotalClock();
                        if (!netgame) {
                            loadnewlevel(mapon);
                        }
                        zoom = 256;
                        displaytime = 0;
                    }
                }
            }
            return;
        }

        tics = TICSPERFRAME;

        plr.getInput().fvel += (tics * plr.getDamage_vel()) << 14;
        plr.getInput().svel += (tics * plr.getDamage_svel()) << 14;
        plr.getInput().angvel += plr.getDamage_angvel();


        if (plr.getDamage_vel() != 0) {
            plr.setDamage_vel(BClipHigh(plr.getDamage_vel() + (tics << 3), 0));
        }
        if (plr.getDamage_svel() != 0) {
            plr.setDamage_svel(BClipLow(plr.getDamage_svel() - (tics << 3), 0));
        }
        if (plr.getDamage_angvel() != 0) {
            plr.setDamage_angvel(BClipLow(plr.getDamage_angvel() - (tics << 1), 0));
        }

        plr.getSprite().setCstat(plr.getSprite().getCstat() ^ 1);
        pEngine.getzrange(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), 128, CLIPMASK0);

        loz = zr_florz;
        lohit = zr_florhit;

        plr.getSprite().setCstat(plr.getSprite().getCstat() ^ 1);

        if ((lohit & HIT_TYPE_MASK) == HIT_SPRITE) {
            int sprIndex = lohit & HIT_INDEX_MASK;
            Sprite spr = boardService.getSprite(sprIndex);
            if (spr != null && (spr.getZ() - plr.getZ()) <= (getPlayerHeight() << 8)) {
                onsprite = sprIndex;
            }
        }

        Sector psec = boardService.getSector(plr.getSector());
        feetoffground = (psec != null) ? (psec.getFloorz() - plr.getZ()) : 0;

        plr.setHoriz(BClipRange(plr.getHoriz() + plr.getInput().horiz, -(YDIM >> 1), 100 + (YDIM >> 1)));

        if ((bits & CtrlAim_Down) != 0) {
            if (plr.getHoriz() > 100 - (YDIM >> 1)) {
                plr.setHoriz(plr.getHoriz() - (TICSPERFRAME << 1));
                autohoriz = 0;
            }
        } else if ((bits & CtrlAim_Up) != 0) {
            if (plr.getHoriz() < 100 + (YDIM >> 1)) {
                plr.setHoriz(plr.getHoriz() + (TICSPERFRAME << 1));
            }
            autohoriz = 0;
        }

        if ((bits & CtrlEndflying) != 0) {
            plr.getOrbactive()[5] = -1;
        }

        if ((bits & CtrlWeapon_Fire) != 0 && plr.getHasshot() == 0) { // Les 07/27/95
            if (plr.getCurrweaponfired() == 0) {
                plrfireweapon(plr);
            }
        }

        // cast
        if ((bits & CtrlCastspell) != 0 && plr.getOrbshot() == 0 && plr.getCurrweaponflip() == 0 && plr.getCurrweaponanim() == 0) {
            if (plr.getOrb()[plr.getCurrentorb()] == 1 && plr.getCurrweapon() == 0) {
                if (lvlspellcheck(plr)) {
                    plr.setOrbshot(1);
                    activatedaorb(plr);
                }
            }
            if (plr.getCurrweapon() != 0) {
                autoweaponchange(plr, 0);
            }
        }

        if ((bits & CtrlInventory_Use) != 0) {
            if (plr.getPotion()[plr.getCurrentpotion()] > 0) {
                usapotion(plr);
            }
        }

        if ((bits & CtrlOpen) != 0) {
            if (!netgame) {
                plruse(plr);
            }
        }

        if ((bits & CtrlTurnAround) != 0) {
            if (plr.getTurnAround() == 0) {
                plr.setTurnAround((short) -1024);
            }
        }

        if (plr.getTurnAround() < 0) {
            plr.setTurnAround((short) BClipHigh(plr.getTurnAround() + 64, 0));
            plr.setAng(BClampAngle(plr.getAng() + 64));
        }

        psec = boardService.getSector(plr.getSector());
        if (psec != null && ((psec.getFloorpicnum() != LAVA || psec.getFloorpicnum() != SLIME
                || psec.getFloorpicnum() != WATER || psec.getFloorpicnum() != HEALTHWATER
                || psec.getFloorpicnum() != ANILAVA || psec.getFloorpicnum() != LAVA1
                || psec.getFloorpicnum() != LAVA2) && feetoffground <= (32 << 8))) {
            plr.getInput().fvel /= 3;
            plr.getInput().svel /= 3;
        }

        if ((psec != null && (psec.getFloorpicnum() == LAVA || psec.getFloorpicnum() == SLIME
                || psec.getFloorpicnum() == WATER || psec.getFloorpicnum() == HEALTHWATER
                || psec.getFloorpicnum() == ANILAVA || psec.getFloorpicnum() == LAVA1
                || psec.getFloorpicnum() == LAVA2)) && plr.getOrbactive()[5] < 0 // loz
                && plr.getZ() >= psec.getFloorz() - (plr.getHeight() << 8) - (8 << 8)) {
            goalz = loz - (32 << 8);
            switch (psec.getFloorpicnum()) {
                case ANILAVA:
                case LAVA:
                case LAVA1:
                case LAVA2:
                    if (plr.getTreasure()[TYELLOWSCEPTER] == 1) {
                        goalz = loz - (getPlayerHeight() << 8);
                        break;
                    } else {
                        plr.getInput().fvel -= plr.getInput().fvel >> 3;
                        plr.getInput().svel -= plr.getInput().svel >> 3;
                    }

                    if (plr.getInvincibletime() > 0 || plr.getManatime() > 0 || plr.isGodMode()) {
                        break;
                    } else {
                        if (lavasnd == -1) {
                            lavasnd = playsound(S_FIRELOOP1, 0, 0, -1);
                        }
                        addhealth(plr, -1);
                        startredflash(10);
                    }
                    break;
                case WATER:
                    if (plr.getTreasure()[TBLUESCEPTER] == 1) {
                        goalz = loz - (getPlayerHeight() << 8);
                    } else {
                        plr.getInput().fvel -= plr.getInput().fvel >> 3;
                        plr.getInput().svel -= plr.getInput().svel >> 3;
                    }
                    break;
                case HEALTHWATER:
                    if (plr.getHealth() < plr.getMaxhealth()) {
                        addhealth(plr, 1);
                        startblueflash(5);
                    }
                    break;
            }
        } else if (plr.getOrbactive()[5] > 0) {
            goalz = plr.getZ() - (plr.getHeight() << 8);
            plr.setHvel(0);
        } else {
            goalz = loz - (plr.getHeight() << 8);
        }

        if (plr.getOrbactive()[5] < 0 && (bits & CtrlJump) != 0 && !plr.isKeytoggle()) {
            if (plr.getOnsomething() != 0) {
                if (game.WH2) {
                    plr.setHvel(plr.getHvel() - WH2JUMPVEL);
                } else {
                    plr.setHvel(plr.getHvel() - JUMPVEL);
                }

                plr.setOnsomething(0);
                plr.setKeytoggle(true);
            }
        }

        if ((bits & CtrlCrouch) != 0) {
            if (psec != null && goalz < ((psec.getFloorz()) - (plr.getHeight() >> 3))) {
                if (game.WH2) {
                    goalz += (48 << 8);
                } else {
                    goalz += (24 << 8);
                }
            }
        }

        onground = plr.getOnsomething() != 0;
        int vel = (klabs(plr.getInput().fvel) + klabs(plr.getInput().svel)) >> 16;
        if (vel < 16) {
            vel = 0;
        }
        if ((bits & CtrlFlyup) != 0 || (bits & CtrlJump) != 0) {
            dophysics(plr, goalz, 1, vel);
        } else if ((bits & CtrlFlydown) != 0 || (bits & CtrlCrouch) != 0) {
            dophysics(plr, goalz, -1, vel);
        } else {
            dophysics(plr, goalz, 0, vel);
        }

        if (!onground && plr.getOnsomething() != 0) {
            if (plr.getFallz() > 32768) {
                if ((pEngine.krand() % 2) != 0) {
                    playsound_loc(S_PLRPAIN1 + (pEngine.krand() % 2), plr.getX(), plr.getY());
                } else {
                    playsound_loc(S_PUSH1 + (pEngine.krand() % 2), plr.getX(), plr.getY());
                }

                addhealth(plr, -(plr.getFallz() >> 13));
                plr.setFallz(0);// wango
            } else if (plr.getFallz() > 8192) {
                playsound_loc(S_BREATH1 + (pEngine.krand() % 2), plr.getX(), plr.getY());
            }
        }

        if (plr.getIhaveflag() > 0) { // WHNET
            plr.getInput().fvel -= plr.getInput().fvel >> 2;
            plr.getInput().svel -= plr.getInput().svel >> 2;
        }

        if (plr.getInput().fvel != 0 || plr.getInput().svel != 0) {

            xvect = plr.getInput().fvel;
            yvect = plr.getInput().svel;

            oldposx = plr.getX();
            oldposy = plr.getY();

            if (plr.isNoclip()) {
                plr.setX(plr.getX() + (xvect >> 14));
                plr.setY(plr.getY() + (yvect >> 14));
                int sect = engine.updatesector(plr.getX(), plr.getY(), plr.getSector());
                if (sect != -1) {
                    plr.setSector(sect);
                }
            } else {
                pEngine.clipmove(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), xvect, yvect, 128, 4 << 8, 4 << 8, CLIPMASK0);
                if (clipmove_sectnum != -1) {
                    plr.setX(clipmove_x);
                    plr.setY(clipmove_y);
                    plr.setZ(clipmove_z);
                    plr.setSector(clipmove_sectnum);
                }

                pEngine.pushmove(plr.getX(), plr.getY(), plr.getZ(), plr.getSector(), 128, 4 << 8, 4 << 8, CLIPMASK0);
                if (pushmove_sectnum != -1) {
                    plr.setX(pushmove_x);
                    plr.setY(pushmove_y);
                    plr.setZ(pushmove_z);
                    plr.setSector(pushmove_sectnum);
                }
            }

            // JSA BLORB

            psec = boardService.getSector(plr.getSector());
            if (plr.getSector() != plr.getOldsector()) {
                if (psec != null && lavasnd != -1) {
                    switch (psec.getFloorpicnum()) {
                        case ANILAVA:
                        case LAVA:
                        case LAVA1:
                        case LAVA2:
                            break;
                        default:
                            stopsound(lavasnd);
                            lavasnd = -1;
                            break;
                    }
                }
                sectorsounds();
            }

            // walking on sprite
            plr.setHoriz(plr.getHoriz() - plr.getOldhoriz());

            dist = EngineUtils.sqrt((plr.getX() - oldposx) * (plr.getX() - oldposx) + (plr.getY() - oldposy) * (plr.getY() - oldposy));

            if (klabs(plr.getInput().svel | plr.getInput().fvel) > (game.WH2 ? WH2NORMALKEYMOVE : NORMALKEYMOVE)) {
                dist >>= 2;
            }

            if (dist > 0 && feetoffground <= (plr.getHeight() << 8) || onsprite != -1) {
                plr.setOldhoriz(((dist * EngineUtils.sin((lockclock << 5) & 2047)) >> 19) >> 2);
                plr.setHoriz(plr.getHoriz() + plr.getOldhoriz());
            } else {
                plr.setOldhoriz(0);
            }

            plr.setHoriz(BClipRange(plr.getHoriz(), -(YDIM >> 1), 100 + (YDIM >> 1)));

            Sprite onsp = boardService.getSprite(onsprite);
            if (onsp != null && dist > 50 && plr.getLopoint() == 1 && plr.getJustplayed() == 0) {

                switch (onsp.getPicnum()) {
                    case WALLARROW:
                    case OPENCHEST:
                    case GIFTBOX:
                        if (plr.getWalktoggle() != 0) {
                            playsound_loc(S_WOOD1, (plr.getX() + 3000), plr.getY());
                        } else {
                            playsound_loc(S_WOOD1, plr.getX(), (plr.getY() + 3000));
                        }
                        plr.setWalktoggle(plr.getWalktoggle() ^ 1);
                        plr.setJustplayed(1);
                        break;
                    case WOODPLANK: // wood planks
                        if (plr.getWalktoggle() != 0) {
                            playsound_loc(S_SOFTCHAINWALK, (plr.getX() + 3000), plr.getY());
                        } else {
                            playsound_loc(S_SOFTCHAINWALK, plr.getX(), (plr.getY() + 3000));
                        }
                        plr.setWalktoggle(plr.getWalktoggle() ^ 1);
                        plr.setJustplayed(1);

                        break;
                    case SQUAREGRATE: // square grating
                    case SQUAREGRATE + 1:
                        if (plr.getWalktoggle() != 0) {
                            playsound_loc(S_LOUDCHAINWALK, (plr.getX() + 3000), plr.getY());
                        } else {
                            playsound_loc(S_LOUDCHAINWALK, plr.getX(), (plr.getY() + 3000));
                        }
                        plr.setWalktoggle(plr.getWalktoggle() ^ 1);
                        plr.setJustplayed(1);
                        break;
                    case SPACEPLANK: // spaced planks
                        if (plr.getWalktoggle() != 0) {
                            playsound_loc(S_SOFTCREAKWALK, (plr.getX() + 3000), plr.getY());
                        } else {
                            playsound_loc(S_SOFTCREAKWALK, plr.getX(), (plr.getY() + 3000));
                        }
                        plr.setWalktoggle(plr.getWalktoggle() ^ 1);
                        plr.setJustplayed(1);
                        break;
                    case SPIDER:
                        // STOMP
                        playsound_loc(S_DEADSTEP, onsp.getX(), onsp.getY());
                        plr.setJustplayed(1);
                        newstatus(onsprite, DIE);
                        break;

                    case FREDDEAD:
                    case 1980:
                    case 1981:
                    case 1984:
                    case 1979:
                    case 1957:
                    case 1955:
                    case 1953:
                    case 1952:
                    case 1941:
                    case 1940:
                        playsound_loc(S_DEADSTEP, plr.getX(), plr.getY());
                        plr.setJustplayed(1);

                        break;

                    default:
                        break;
                }

                if (onsp.getPicnum() == RAT) {
                    playsound_loc(S_RATS1 + (pEngine.krand() % 2), onsp.getX(), onsp.getY());
                    plr.setJustplayed(1);
                    pEngine.deletesprite(onsprite);
                }
            }

            if (plr.getLopoint() == 0 && plr.getOldhoriz() == -2 && plr.getJustplayed() == 0) {
                plr.setLopoint(1);
            }

            if (plr.getLopoint() == 1 && plr.getOldhoriz() != -2 && plr.getJustplayed() == 1) {
                plr.setLopoint(0);
                plr.setJustplayed(0);
            }

            vel = (klabs(plr.getInput().fvel) + klabs(plr.getInput().svel)) >> 16;
            if (vel > 40 && dist > 10) {
                plr.setRunningtime(plr.getRunningtime() + TICSPERFRAME);
            } else {
                plr.setRunningtime(plr.getRunningtime() - TICSPERFRAME);
            }

            if (plr.getRunningtime() < -360) {
                plr.setRunningtime(0);
            }

            if (plr.getRunningtime() > 360) {
                SND_Sound(S_PLRPAIN1);
                plr.setRunningtime(0);
            }
        }
        if (plr.getInput().angvel != 0) {
            plr.setAng(plr.getAng() + plr.getInput().angvel * TICSPERFRAME / 16.0f);
            plr.setAng(BClampAngle(plr.getAng()));
        }

        game.pInt.setsprinterpolate(plr.getSpritenum(), plr.getSprite());
        pEngine.setsprite(plr.getSpritenum(), plr.getX(), plr.getY(), plr.getZ() + (plr.getHeight() << 8));
        plr.getSprite().setAng((short) plr.getAng());

        if (plr.getSector() >= 0 && engine.getceilzofslope(plr.getSector(), plr.getX(), plr.getY()) > engine.getflorzofslope(plr.getSector(), plr.getX(), plr.getY()) - (8 << 8)) {
            addhealth(plr, -10);
        }

        if ((bits & CtrlAim_Center) != 0) {
            autohoriz = 1;
        }

        if (autohoriz == 1) {
            autothehoriz(plr);
        }
        if (plr.getCurrweaponfired() != 1 && plr.getCurrweaponfired() != 6) {
            plr.setHasshot(0);
        }
        weaponchange(num);
        bookprocess(num);
        potionchange(num);
    }

    public void autothehoriz(PLAYER plr) {
        if (plr.getHoriz() < 100) {
            plr.setHoriz(BClipHigh(plr.getHoriz() + (TICSPERFRAME << 2), 100));
        }
        if (plr.getHoriz() > 100) {
            plr.setHoriz(BClipLow(plr.getHoriz() - (TICSPERFRAME << 2), 100));
        }
        if (plr.getHoriz() == 100) {
            autohoriz = 0;
        }
    }

    public void dophysics(PLAYER plr, int goalz, int flyupdn, int v) {
        if (plr.getOrbactive()[5] > 0) {
            if (v > 0) {
                if (plr.getHoriz() > 125) {
                    plr.setHvel(plr.getHvel() - (TICSPERFRAME << 8));
                } else if (plr.getHoriz() < 75) {
                    plr.setHvel(plr.getHvel() + (TICSPERFRAME << 8));
                }
            }
            if (flyupdn > 0) {
                plr.setHvel(plr.getHvel() - (TICSPERFRAME << 7));
            }
            if (flyupdn < 0) {
                plr.setHvel(plr.getHvel() + (TICSPERFRAME << 7));
            }
            plr.setHvel(plr.getHvel() + (EngineUtils.sin((lockclock << 4) & 2047) >> 6));
            plr.setFallz(0);

        } else if (plr.getZ() < goalz) {
            if (game.WH2) {
                plr.setHvel(plr.getHvel() + (TICSPERFRAME * WH2GRAVITYCONSTANT));
            } else {
                plr.setHvel(plr.getHvel() + GRAVITYCONSTANT);
            }
            plr.setOnsomething(plr.getOnsomething() & ~(GROUNDBIT | PLATFORMBIT));
            plr.setFallz(plr.getFallz() + plr.getHvel());
        } else if (plr.getZ() > goalz) {
            plr.setHvel(plr.getHvel() - ((plr.getZ() - goalz) >> 6));
            plr.setOnsomething(plr.getOnsomething() | GROUNDBIT);
            plr.setFallz(0);
        } else {
            plr.setFallz(0);
        }

        plr.setZ(plr.getZ() + plr.getHvel());
        if (plr.getHvel() > 0 && plr.getZ() > goalz) {
            plr.setHvel(plr.getHvel() >> 2);
        } else if (plr.getOnsomething() != 0) {
            if (plr.getHvel() < 0 && plr.getZ() < goalz) {
                plr.setHvel(0);
                plr.setZ(goalz);
            }
        }

        if (plr.getSector() != -1) {
            if (plr.getZ() - (plr.getHeight() >> 2) < engine.getceilzofslope(plr.getSector(), plr.getX(), plr.getY())) {
                plr.setZ(engine.getceilzofslope(plr.getSector(), plr.getX(), plr.getY()) + (plr.getHeight() >> 2));
                plr.setHvel(0);
            } else {
                if (plr.getOrbactive()[5] > 0) {
                    if (plr.getZ() + (plr.getHeight() << 7) > engine.getflorzofslope(plr.getSector(), plr.getX(), plr.getY())) {
                        plr.setZ(engine.getflorzofslope(plr.getSector(), plr.getX(), plr.getY()) - (plr.getHeight() << 7));
                        plr.setHvel(0);
                    }
                } else {
                    if (plr.getZ() + (plr.getHeight() >> 4) > engine.getflorzofslope(plr.getSector(), plr.getX(), plr.getY())) {
                        plr.setZ(engine.getflorzofslope(plr.getSector(), plr.getX(), plr.getY()) - (plr.getHeight() >> 4));
                        plr.setHvel(0);
                    }
                }
            }
        }
        plr.setJumphoriz(-(plr.getHvel() >> 8));
    }

    @Override
    public void sndHandlePause(boolean pause) {
        WHSND.sndHandlePause(pause);
    }

    public void newgame(Object item, int nLevel, int skills) {
        gDemoScreen.onStopRecord();

        pNet.ready2send = false;
        game.nNetMode = NetMode.Single;

        mapon = nLevel;
        difficulty = skills;
        justteleported = false;
        nextlevel = false;

        UserFlag flag = UserFlag.None;
        if (item instanceof EpisodeInfo) {
            EpisodeInfo game = (EpisodeInfo) item;

            if (!game.equals(gOriginalEpisode)) {
                if (!checkEpisodeResources(game)) {
                    return;
                }

                flag = UserFlag.Addon;
                Console.out.println("Start user episode: " + game.Title);
            } else {
                resetEpisodeResources(game);
            }

            if (gCurrentEpisode != null && gCurrentEpisode.getMap(mapon) != null) {
                boardfilename = Main.game.cache.getEntry(gCurrentEpisode.getMap(mapon).path, true);
            }
        } else if (item instanceof FileEntry) {
            flag = UserFlag.UserMap;
            boardfilename = (Entry) item;
            mapon = 0;
            resetEpisodeResources(null);
            Console.out.println("Start user map: " + ((FileEntry) item).getName());
        }
        mUserFlag = flag;

        sndStopMusic();
        if (mUserFlag == UserFlag.UserMap) {
            loadboard(boardfilename, null).setTitle("Loading " + boardfilename);
        } else if (gCurrentEpisode != null && boardfilename != null) {
            loadboard(boardfilename, null);
        } else {
            game.show(); //can't restart the level
        }

        game.menu.mClose();
    }

}
