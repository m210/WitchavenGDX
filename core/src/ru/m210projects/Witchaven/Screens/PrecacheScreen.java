package ru.m210projects.Witchaven.Screens;

import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Witchaven.Main;

import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Build.Engine.MAXSTATUS;
import static ru.m210projects.Witchaven.Main.boardService;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Potions.MAXPOTIONS;
import static ru.m210projects.Witchaven.Spellbooks.sspellbookanim;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.Weapons.spikeanimtics;

public class PrecacheScreen extends DefaultPrecacheScreen {

    public PrecacheScreen(Runnable toLoad, PrecacheListener listener) {
        super(Main.game, toLoad, listener);

        addQueue("Preload floor and ceiling tiles...", () -> {
            Sector[] sectors = boardService.getBoard().getSectors();
            for (Sector sec : sectors) {
                addTile(sec.getFloorpicnum());
                addTile(sec.getCeilingpicnum());
            }
            doprecache(0);
        });

        addQueue("Preload wall tiles...", () -> {
            for (Wall wall : boardService.getBoard().getWalls()) {
                addTile(wall.getPicnum());
                if (wall.getOverpicnum() >= 0) {
                    addTile(wall.getOverpicnum());
                }
            }
            doprecache(0);
        });

        addQueue("Preload sprite tiles...", () -> {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr != null && spr.getStatnum() < MAXSTATUS) {
                    cachespritenum(spr);
                }
            }

            addTile(BAT);
            addTile(GOBLINATTACK);
            addTile(MINOTAURATTACK);
            addTile(KOBOLDATTACK);
            addTile(FREDATTACK);
            addTile(DEVILATTACK);

            doprecache(1);
        });

        addQueue("Preload hud tiles...", () -> {
            for (int i = KNIFEREADY; i <= BIGAXEDRAW10; i++) //hud weapons
            {
                addTile(i);
            }
            for (int i = THEFONT; i < CRYSTALSTAFF; i++) //small font
            {
                addTile(i);
            }

            addTile(SSTATUSBAR);
            for (int i = 0; i < 8; i++) {
                addTile(sspellbookanim[i][0].daweaponframe);
            }

            addTile(ANNIHILATE);
            addTile(HELMET);
            addTile(SSCOREBACKPIC);
            addTile(SHEALTHBACK);

            for (int i = 0; i < 4; i++) {
                addTile(SKEYBLANK + i);
                addTile(SCARY + i);
            }

            addTile(SPOTIONBACKPIC);
            for (int i = 0; i < MAXPOTIONS; i++) {
                addTile(SPOTIONARROW + i);
                addTile(SFLASKBLUE + i);
            }
            addTile(SFLASKBLACK);
            for (int i = 0; i < 5; i++) {
                addTile(spikeanimtics[i].daweaponframe);
            }

            doprecache(1);
        });
    }

    @Override
    protected void draw(String title, int index) {
        Renderer renderer = game.getRenderer();
        renderer.clearview(77);
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, MAINMENU, -128,
                0, 2 | 8 | 64);

        game.getFont(1).drawTextScaled(renderer, 160, 100, "Loading", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
        game.getFont(1).drawTextScaled(renderer, 160, 114, "please wait...", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);

        game.getFont(1).drawTextScaled(renderer, 160, 130, title, 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

    private void cachespritenum(Sprite sp) {
        int maxc = 1;
        if (sp.getPicnum() == RAT || sp.getPicnum() == GUARDIAN) {
            maxc = 15;
        }
        if (sp.getPicnum() == HANGMAN) {
            maxc = 40;
        }

        if (sp.getPicnum() == GRONHAL || sp.getPicnum() == GRONMU || sp.getPicnum() == GRONSW) {
            maxc = 19;
        }

        switch (sp.getPicnum()) {
            case GOBLINSTAND:
            case GOBLIN:
                maxc = 21;
                break;
            case KOBOLD:
                maxc = 24;
                break;
            case DEVILSTAND:
            case DEVIL:
                maxc = 25;
                break;
            case DRAGON:
                maxc = 11;
                break;

            case SPIDER:
                maxc = 39;
                break;
            case MINOTAUR:
                maxc = 35;
                break;
            case FATWITCH:
                maxc = 19;
                break;
            case SKULLY:
                maxc = 20;
                break;
            case JUDYSIT:
            case JUDY:
                maxc = 18;
                break;
        }
        for (int j = sp.getPicnum(); j < (sp.getPicnum() + maxc); j++) {
            addTile(j);
        }
    }

}
