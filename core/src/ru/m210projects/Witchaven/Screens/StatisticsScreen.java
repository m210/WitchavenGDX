package ru.m210projects.Witchaven.Screens;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Names.MAINMENU;
import static ru.m210projects.Witchaven.WHPLR.mapon;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.resetflash;

public class StatisticsScreen extends CutsceneScreen {

    private boolean inited = false;
    private final String[] ratings = {"poor", "average", "good", "perfect"};
    private int bonus, rating;

    public StatisticsScreen(BuildGame game) {
        super(game);
    }

    public void show(PLAYER plr, Runnable callback) {
        SND_Sound(S_CHAINDOOR1);
        inited = init("stairs.smk");

        if (kills > killcnt) {
            kills = killcnt;
        }
        int killp = (kills * 100) / (killcnt + 1);
        if (treasuresfound > treasurescnt) {
            treasuresfound = treasurescnt;
        }
        int treap = (treasuresfound * 100) / (treasurescnt + 1);
        rating = (killp + treap) / 2;
        if (rating >= 95) {
            rating = 3;
        } else if (rating >= 70) {
            rating = 2;
        } else if (rating >= 40) {
            rating = 1;
        } else {
            rating = 0;
        }
        bonus = rating * 500;
        plr.setScore(plr.getScore() + bonus);

        game.changeScreen(this.setSkipping(callback));
    }

    @Override
    public void show() {
        SND_CheckLoops();
        resetflash();

        if (game.pMenu.gShowMenu) {
            game.pMenu.mClose();
        }

        LastMS = engine.getCurrentTimeMillis();
        engine.getTimer().reset();
    }


    @Override
    public void draw(float delta) {
        Renderer renderer = game.getRenderer();
        if (inited) {
            if (!play()) {
                frame = 0;
            }
        } else {
            renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, MAINMENU, 24, 0, 2 | 8 | 64);
        }

        if (gCurrentEpisode != null && gCurrentEpisode.getMap(mapon) != null) {
            game.getFont(1).drawTextScaled(renderer, 10, 13, gCurrentEpisode.getMap(mapon).title, 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
        }
        game.getFont(1).drawTextScaled(renderer, 10, 31, "Level conquered", 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

        game.getFont(1).drawTextScaled(renderer, 10, 64, "Enemies killed", 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
        game.getFont(1).drawTextScaled(renderer, 160 + 48 + 14, 64, kills + " of " + killcnt, 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

        game.getFont(1).drawTextScaled(renderer, 10, 64 + 18, "Treasures found", 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
        game.getFont(1).drawTextScaled(renderer, 160 + 48 + 14, 64 + 18, treasuresfound + " of " + treasurescnt, 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

        game.getFont(1).drawTextScaled(renderer, 10, 64 + 2 * 18, "Experience gained", 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
        game.getFont(1).drawTextScaled(renderer, 160 + 48 + 14, 64 + 2 * 18, "" + (expgained + bonus), 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

        game.getFont(1).drawTextScaled(renderer, 10, 64 + 3 * 18, "Rating", 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
        game.getFont(1).drawTextScaled(renderer, 160 + 48 + 14, 64 + 3 * 18, ratings[rating], 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

        game.getFont(1).drawTextScaled(renderer, 10, 64 + 4 * 18, "Bonus", 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
        game.getFont(1).drawTextScaled(renderer, 160 + 48 + 14, 64 + 4 * 18, "" + bonus, 1, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

    }

}
