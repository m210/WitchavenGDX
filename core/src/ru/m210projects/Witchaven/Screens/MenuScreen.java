package ru.m210projects.Witchaven.Screens;

import ru.m210projects.Build.Pattern.ScreenAdapters.MenuAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Witchaven.Factory.WHMenuHandler;
import ru.m210projects.Witchaven.Main;

import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Witchaven.Factory.WHMenuHandler.MAIN;
import static ru.m210projects.Witchaven.Factory.WHMenuHandler.QUIT;
import static ru.m210projects.Witchaven.Main.gDemoScreen;
import static ru.m210projects.Witchaven.Names.MAINMENU;
import static ru.m210projects.Witchaven.Names.TITLEPIC;
import static ru.m210projects.Witchaven.WH1Names.SVGAMENU;
import static ru.m210projects.Witchaven.WH1Names.SVGAMENU2;
import static ru.m210projects.Witchaven.WH2Names.VMAINBLANK;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.resetflash;

public class MenuScreen extends MenuAdapter {

    private final WHMenuHandler menu;
    private final Main game;

    public MenuScreen(Main game) {
        super(game, game.menu.mMenus[MAIN]);
        this.menu = game.menu;
        this.game = game;
    }

    @Override
    public void draw(float delta) {
        Renderer renderer = game.getRenderer();
        if (game.WH2) {
            int xdim = renderer.getWidth();
            int ydim = renderer.getHeight();
            int mainHeight = renderer.getTile(VMAINBLANK).getHeight();
            if (mainHeight > 0) { // #GDX 11.10.2024 height should be zero?
                int scale = divscale(ydim, mainHeight, 16);
                renderer.rotatesprite(xdim << 15, ydim << 15, scale, 0, VMAINBLANK, 0, 0, 8);
            }
        } else {
            renderer.rotatesprite(0, 0, 32768, 0, SVGAMENU, 0, 0, 2 + 8 + 16);
            renderer.rotatesprite(0, 120 << 16, 32768, 0, SVGAMENU2, 0, 0, 2 + 8 + 16);

            if (menu.isOpened(menu.mMenus[MAIN]) || menu.isOpened(menu.mMenus[QUIT])) {
                renderer.rotatesprite(1 << 16, 1 << 16, 32768, 0, MAINMENU, 0, 0, 2 + 8 + 16);
            } else {
                renderer.rotatesprite(1 << 16, 1 << 16, 32768, 0, TITLEPIC, 0, 0, 2 + 8 + 16);
            }
        }
    }

    @Override
    public void show() {
        gDemoScreen.onStopRecord();
        SND_CheckLoops();
        SND_MenuMusic();
        resetflash();
    }

    @Override
    public void pause() {
        sndStopMusic();

    }

    @Override
    public void resume() {
        SND_MenuMusic();
    }

}
