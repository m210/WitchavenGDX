package ru.m210projects.Witchaven.Screens;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.LoadingAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.Witchaven.Names.MAINMENU;
import static ru.m210projects.Witchaven.WHSND.SND_CheckLoops;
import static ru.m210projects.Witchaven.WHScreen.resetflash;

public class LoadingScreen extends LoadingAdapter {

    public LoadingScreen(BuildGame game) {
        super(game);
    }

    @Override
    public void draw(String title, float delta) {
        Renderer renderer = game.getRenderer();
        renderer.clearview(77);
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, MAINMENU, -128,
                0, 2 | 8 | 64);

        game.getFont(1).drawTextScaled(renderer, 160, 100, "Loading", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
        game.getFont(1).drawTextScaled(renderer, 160, 114, "please wait...", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

    @Override
    public void show() {
        super.show();
        resetflash();
        SND_CheckLoops();
    }
}
