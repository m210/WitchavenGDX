package ru.m210projects.Witchaven.Screens;

import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.MenuItems.MenuItem;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;

import static ru.m210projects.Witchaven.Names.MAINMENU;

public class WHMessageScreen extends MessageScreen {

    public WHMessageScreen(BuildGame game, String header, String message, MessageType type) {
        super(game, header, message, game.getFont(0), game.getFont(1), type);

        for (MenuItem item : messageItems) {
            item.y += 40;
        }

        for (MenuItem item : variantItems) {
            item.y += 30;
        }
    }

    @Override
    public void drawBackground(Renderer renderer) {
        renderer.clearview(77);
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, MAINMENU, -128,
                0, 2 | 8 | 64);
    }
}
