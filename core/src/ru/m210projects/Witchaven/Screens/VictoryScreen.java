package ru.m210projects.Witchaven.Screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.input.InputListener;

import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.resetflash;

public class VictoryScreen extends ScreenAdapter implements InputListener {

    public int state;

    @Override
    public void show() {
        state = 0;
        SND_CheckLoops();
        sndStopMusic();

        SND_Sound(S_PICKUPFLAG);
        resetflash();

        game.getProcessor().resetPollingStates();
    }

    @Override
    public void render(float delta) {
        if (game.getProcessor().isKeyJustPressed(Input.Keys.ANY_KEY)) {
            anyKeyPressed();
        }

        Renderer renderer = game.getRenderer();
        renderer.clearview(0);
        switch (state) {
            case 0:
                renderer.rotatesprite(0, 0, 65536, 0, VICTORYA, 0, 0, 2 | 8 | 16);
                break;
            case 1:
                renderer.rotatesprite(0, 0, 65536, 0, VICTORYB, 0, 0, 2 | 8 | 16);
                break;
            case 2:
                renderer.rotatesprite(0, 0, 65536, 0, VICTORYC, 0, 0, 2 | 8 | 16);
                break;
            case 3:
                game.changeScreen(gMenuScreen);
                break;
        }

        engine.nextpage(delta);
    }

    public void anyKeyPressed() {
        game.getProcessor().prepareNext();

        state++;
        switch (state) {
            case 1:
                SND_Sound(S_DROPFLAG);
                break;
            case 2:
                SND_Sound(S_WISP2);
                break;
        }
    }
}
