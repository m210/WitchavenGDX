package ru.m210projects.Witchaven.Screens;

import com.badlogic.gdx.Screen;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Witchaven.Main;
import ru.m210projects.Witchaven.Types.CompareService.CompareService;
import ru.m210projects.Witchaven.Types.DemoFile;
import ru.m210projects.Witchaven.Types.DemoRecorder;
import ru.m210projects.Witchaven.Types.EpisodeInfo;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.WHPLR.player;
import static ru.m210projects.Witchaven.Whldsv.lastload;

public class DemoScreen extends GameScreen {

    public static final String header = "WHDEM";
    public static DemoFile demfile;
    public final Map<Group, List<Entry>> demofiles = new HashMap<>();
    public int nDemonum = -1;
    protected Entry lastDemoEntry = DUMMY_ENTRY;

    private int ototalclock;

    public DemoScreen(Main game) {
        super(game);
    }

    public static boolean isDemoPlaying() {
        return recstat == DEMOSTAT_PLAYING;
    }

    public static boolean isDemoScreen(Screen screen) {
        return screen == gDemoScreen;
    }

    public static boolean checkCodeVersion(Screen screen, int version) {
        return screen == gGameScreen || isDemoPlaying() && demfile != null && demfile.version >= version;
    }

    @Override
    public void show() {
        lastload = null;
    }

    public boolean showDemo(Entry entry) {
        onStopRecord();

        demfile = null;
        try (InputStream is = entry.getInputStream()) {
            demfile = new DemoFile(is);
        } catch (Exception e) {
            Console.out.println("Can't play the demo file: " + entry.getName(), OsdColor.RED);
            return false;
        }

        if (numplayers > 1)
            game.pNet.NetDisconnect(myconnectindex);

        recstat = DEMOSTAT_PLAYING;
        lastDemoEntry = entry;

        EpisodeInfo episodeInfo = gOriginalEpisode;
        if (demfile.addon != null) {
            episodeInfo = demfile.addon;
        }

//         CompareService.prepare(java.nio.file.Paths.get("D:\\DemosData\\" + (game.WH2 ? "WH2\\" : "WH1\\") +  entry.getName() + ".jdm"), CompareService.Type.Read);
        this.newgame(episodeInfo, demfile.map, demfile.skill);

        Console.out.println("Playing demo " + entry.getName());

        return true;
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        return gameKeyDownCommon(gameKey, false);
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void pause() {
        ototalclock = engine.getTimer().getTotalClock();
        super.pause();
    }

    public void onPrepareboard(GameScreen screen) {
        if (screen != this && isDemoPlaying()) {
            gDemoScreen.onStopPlaying();
        }

        if (demfile != null && demfile.version >= GDXBYTEVERSION && isDemoPlaying()) {
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                demfile.playerData.applyTo(player[i]);
            }
        }

        if (screen != this && isRecordEnabled()) {
            m_recstat = DEMOSTAT_NULL;
//            WHEngine.randomStack = "";
//            CompareService.prepare(Paths.get("D:\\DemosData\\WH\\" + "Demo" + ".jdm"), CompareService.Type.Write);
//
            int a, b, c, d, democount = 0;
            do {
                a = ((democount / 1000) % 10);
                b = ((democount / 100) % 10);
                c = ((democount / 10) % 10);
                d = (democount % 10);

                String fn = "demo" + a + b + c + d + ".dmo";
                if (!game.getCache().getGameDirectory().getEntry(fn).exists()) {
                    try {
                        Path path = game.getCache().getGameDirectory().getPath().resolve(fn);
                        rec = new DemoRecorder(new FileOutputStream(path.toFile()), path, GDXCURRENTBYTEVERSION);
                        Console.out.println("Start recording to " + fn);
                        recstat = DEMOSTAT_RECORD;
                    } catch (Exception e) {
                        Console.out.println("Can't start demo record: " + e, OsdColor.RED);
                    }
                    break;
                }

                democount++;
            } while (democount < 9999);
        }
    }

    public boolean isRecordEnabled() {
        return m_recstat == DEMOSTAT_RECORD;
    }

    public void onStopPlaying() {
        demfile = null;
        recstat = DEMOSTAT_NULL;
    }

    public void onStopRecord() {
        if (rec == null) {
            return;
        }

        CompareService.close();
        rec.close();
        rec = null;
        recstat = DEMOSTAT_NULL;
    }

    public void onRecord() {
        if (rec != null) {
            rec.record();
        }
    }

    public void onLoad() {
        onStopRecord();
        demfile = null;
        recstat = DEMOSTAT_NULL;
    }

    @Override
    protected void startboard(final Runnable startboard) {
        game.doPrecache(() -> {
            startboard.run(); //call faketimehandler
            pNet.ResetTimers(); //reset ototalclock
            lockclock = 0;
            pNet.ready2send = false;
        });
    }

    @Override
    public void render(float delta) {
        if (numplayers > 1)
            pNet.GetPackets();

        DemoRender();

        float smoothratio = 65536;
        if (!game.gPaused) {
            smoothratio = pEngine.getTimer().getsmoothratio(delta);
            if (smoothratio < 0 || smoothratio > 0x10000) {
                smoothratio = BClipRange(smoothratio, 0, 0x10000);
            }
        }

        game.pInt.dointerpolations(smoothratio);
        DrawWorld(smoothratio);

        DrawHud(smoothratio);
        game.pInt.restoreinterpolations();

        if (pMenu.gShowMenu)
            pMenu.mDrawMenu();

        PostFrame(pNet);
        pEngine.nextpage(delta);
    }

    private void DemoRender() {
        pNet.ready2send = false;

        if (!game.isCurrentScreen(this))
            return;

        if (!game.gPaused && demfile != null) {
            while (engine.getTotalClock() >= (lockclock + TICSPERFRAME)) {
                CompareService.update(demfile.rcnt);
                for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
                    pNet.gFifoInput[pNet.gNetFifoHead[j] & 0xFF][j].Copy(demfile.recsync[demfile.rcnt][j]);
                    pNet.gNetFifoHead[j]++;
                    demfile.reccnt--;
                }

                if (demfile.reccnt <= 0) {
                    demfile = null;

                    Group group = lastDemoEntry.getParent();
                    if (!showDemo(group)) {
                        game.changeScreen(gMenuScreen);
                    }
                    return;
                }

                demfile.rcnt++;
                game.pInt.clearinterpolations();

                ProcessFrame(pNet);
            }
        } else {
            engine.getTimer().setTotalClock(ototalclock);
        }
    }

    public boolean showDemo(Group group) {
        List<Entry> list = checkDemoEntry(group);

        switch (cfg.gDemoSeq) {
            case 0: //OFF
                return false;
            case 1: //Consistently
                if (nDemonum < (list.size() - 1)) nDemonum++;
                else {
                    nDemonum = 0;
                    // throw new RuntimeException("Done");
                }
                break;
            case 2: //Accidentally
                int nextnum = nDemonum;
                if (list.size() > 1) {
                    while (nextnum == nDemonum) {
                        nextnum = (int) (Math.random() * (list.size()));
                    }
                }
                nDemonum = BClipRange(nextnum, 0, list.size() - 1); // #GDX 28.12.2024
                break;
        }

        if (!list.isEmpty()) {
            boolean result = showDemo(list.get(nDemonum));
            if (!result) {
                list.remove(nDemonum);
                return showDemo(group);
            }

            return true;
        }

        return false;
    }

    public boolean isDemoFile(Entry file) {
        if (file.exists()) {
            if (file.isExtension("dmo")) {
                try (InputStream is = file.getInputStream()) {
                    String header = StreamUtils.readString(is, DemoScreen.header.length());
                    StreamUtils.skip(is, 4); // rcnt
                    int version = StreamUtils.readUnsignedByte(is);
                    if (header.equals(DemoScreen.header) && version >= GDXBYTEVERSION) {
                        return true;
                    }
                } catch (Exception ignore) {
                }
            }
        }
        return false;
    }

    public List<Entry> checkDemoEntry(Group group) {
        if (demofiles.containsKey(group)) {
            return demofiles.get(group);
        }

        nDemonum = -1;
        List<Entry> demos = group.stream()
                .filter(this::isDemoFile)
                .sorted(Entry::compareTo)
                .collect(Collectors.toList());

        if (demos.isEmpty()) { //try to find it in mainGrp
            int k;
            int which_demo = 1;
            do {
                k = which_demo;

                char[] d = "demo_.dmo".toCharArray();
                if (which_demo == 10) {
                    d[4] = 'x';
                } else {
                    d[4] = (char) ('0' + which_demo);
                }
                String name = String.copyValueOf(d);
                Entry entry = game.getCache().getEntry(name, true);
                if (isDemoFile(entry)) {
                    demos.add(entry);
                } else {
                    break;
                }
                which_demo++;
            } while (k != which_demo);
        }

        demofiles.put(group, demos);
        Console.out.println("There are " + demos.size() + " demo(s) in the loop", OsdColor.YELLOW);

        return demos;
    }
}
