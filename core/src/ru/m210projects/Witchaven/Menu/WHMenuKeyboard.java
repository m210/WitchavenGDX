package ru.m210projects.Witchaven.Menu;

import ru.m210projects.Build.input.keymap.Keymap;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuKeyboard;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;

import static com.badlogic.gdx.Input.Keys.*;

public class WHMenuKeyboard extends MenuKeyboard {

    public WHMenuKeyboard(BuildGame app) {
        super(app, 46, 55, 240, 8, app.getFont(1));

        mList.pal_left = 7;
        mText.font = app.getFont(0);
        mText2.font = app.getFont(0);
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return new WHTitle(text, 90, 0);
    }

    @Override
    public String keyNames(int keycode) {
        if (keycode == 0) {
            return "None";
        }

        switch (keycode) {
            case COMMA:
                return "Comma";
            case PERIOD:
                return "Period";
            case MINUS:
                return "Minus";
            case EQUALS:
                return "Equals";
            case LEFT_BRACKET:
                return "L_bracket";
            case RIGHT_BRACKET:
                return "R_bracket";
            case BACKSLASH:
                return "Backslash";
            case SEMICOLON:
                return "Semicolon";
            case APOSTROPHE:
                return "Apostrophe";
            case SLASH:
                return "Slash";
            case AT:
                return "At";
            case COLON:
                return "Colon";
            default:
                return Keymap.toString(keycode);
        }
    }
}
