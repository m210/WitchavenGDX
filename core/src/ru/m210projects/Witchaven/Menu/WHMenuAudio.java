package ru.m210projects.Witchaven.Menu;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuAudio;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Witchaven.Main;
import ru.m210projects.Witchaven.Main.UserFlag;

import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.WHPLR.mapon;
import static ru.m210projects.Witchaven.WHSND.*;

public class WHMenuAudio extends MenuAudio {

    public WHMenuAudio(Main app) {
        super(app, 10, 50, 300, app.getFont(0).getSize() - 2, 8, app.getFont(0));

        AudioListener listener = new AudioListener() {
            @Override
            public void PreDrvChange() {
                MusicOff();
                SoundOff();
            }

            @Override
            public void PostDrvChange() {
                try {
                    sndInit();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                MusicOn();
            }

            @Override
            public void SoundOff() {
                stopallsounds();
            }

            @Override
            public void MusicOn() {
                if (app.isCurrentScreen(gGameScreen) || app.isCurrentScreen(gDemoScreen)) {
                    if (mUserFlag != UserFlag.UserMap) {
                        startmusic(mapon - 1);
                    }
                }
            }

            @Override
            public void MusicOff() {
                sndStopMusic();
            }
        };
        this.setListener(listener);

        if (!app.WH2) {
            sSoundDrv.listFont = app.getFont(1);
            sMusicDrv.listFont = app.getFont(1);

            sSound.sliderNumbers = app.getFont(1);
            sVoices.sliderNumbers = app.getFont(1);
            sMusic.sliderNumbers = app.getFont(1);

            sResampler.listFont = app.getFont(1);
            sMusicType.listFont = app.getFont(1);

            sSoundSwitch.switchFont = app.getFont(1);
            sMusicSwitch.switchFont = app.getFont(1);
        }
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return new WHTitle(text, 90, 0);
    }

    @Override
    protected char[][] getMusicTypeList() {
        char[][] list = new char[2][];
        list[0] = "Midi".toCharArray();
        list[1] = "External".toCharArray();
        return list;
    }
}
