package ru.m210projects.Witchaven.Menu;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuControls;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Witchaven.Main;

public class WHMenuControls extends MenuControls {

    public WHMenuControls(BuildGame app) {
        super(app, 50, 90, app.getFont(0).getSize(), 8, app.getFont(0), 2, 3);
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return new WHTitle(text, 90, 0);
    }

    @Override
    public BuildMenu getMouseMenu(BuildGame app) {
        return new WHMenuMouse((Main) app);
    }

    @Override
    public BuildMenu getJoystickMenu(BuildGame app) {
        return new WHMenuJoystick((Main) app);
    }

    @Override
    public BuildMenu getKeyboardMenu(BuildGame app) {
        return new WHMenuKeyboard(app);
    }

    @Override
    public void mResetDefault(GameConfig cfg, MenuHandler menu) {
        cfg.resetInput(false);

        menu.mMenuBack();
    }

    @Override
    public void mResetClassic(GameConfig cfg, MenuHandler menu) {
        cfg.resetInput(true);
        menu.mMenuBack();
    }

    @Override
    protected BuildMenu getResetDefaultMenu(final BuildGame app, Font style, int posy, int pal) {
        BuildMenu menu = new BuildMenu(app.pMenu);

        MenuText QuitQuestion = new MenuText("Do you really", style, 160, posy, 1);
        MenuText QuitQuestion2 = new MenuText("want to reset keys?", style, 160, posy += 10, 1);
        MenuVariants QuitVariants = new MenuVariants(app.pEngine, "[Y/N]", style, 160, posy += 20) {
            @Override
            public void positive(MenuHandler menu) {
                mResetDefault(app.pCfg, menu);
            }
        };

        menu.addItem(QuitQuestion, false);
        menu.addItem(QuitQuestion2, false);
        menu.addItem(QuitVariants, true);

        return menu;
    }

    @Override
    protected BuildMenu getResetClassicMenu(final BuildGame app, Font style, int posy, int pal) {
        BuildMenu menu = new BuildMenu(app.pMenu);

        MenuText QuitQuestion = new MenuText("Do you really want", style, 160, posy, 1);
        MenuText QuitQuestion2 = new MenuText("reset to classic?", style, 160, posy += 10, 1);
        MenuVariants QuitVariants = new MenuVariants(app.pEngine, "[Y/N]", style, 160, posy += 20) {
            @Override
            public void positive(MenuHandler menu) {
                mResetClassic(app.pCfg, menu);
            }
        };

        menu.addItem(QuitQuestion, false);
        menu.addItem(QuitQuestion2, false);
        menu.addItem(QuitVariants, true);

        return menu;
    }

}
