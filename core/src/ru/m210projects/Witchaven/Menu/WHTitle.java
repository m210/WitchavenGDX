package ru.m210projects.Witchaven.Menu;

import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.Build.Gameutils.coordsConvertXScaled;
import static ru.m210projects.Witchaven.Globals.WHLOGO;
import static ru.m210projects.Witchaven.Main.game;

public class WHTitle extends MenuTitle {

    public WHTitle(Object text, int x, int y) {
        super(game.pEngine, text, game.WH2 ? game.getFont(0) : game.getFont(1), x, y, WHLOGO);
    }

    @Override
    public void draw(MenuHandler handler) {
        if (text != null) {
            Renderer renderer = game.getRenderer();
            int tx = x + 64;
            int ty = y + 24;
            int xdim = renderer.getWidth();
            int ydim = renderer.getHeight();

            renderer.rotatesprite((tx - 25) << 16, ty + 15 << 16, 65536, 0, 585, 0, 0, 2 | 4 | 8, 0, 0, coordsConvertXScaled(tx, ConvertType.Normal), ydim - 1);
            renderer.rotatesprite((tx + 25) << 16, ty + 15 << 16, 65536, 0, 585, 0, 0, 2 | 4 | 8, coordsConvertXScaled(tx, ConvertType.Normal), 0, xdim - 1, ydim - 1);

            font.drawTextScaled(renderer, tx, ty + 10, text, 1.0f, 0, game.WH2 ? 0 : 9, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

            renderer.rotatesprite(x << 16, y << 16, 16384, 0, nTile, 0, 0, 2 | 8 | 16);
        }
    }


}
