package ru.m210projects.Witchaven.Menu;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Witchaven.Main;

import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;

public class MenuGameSetup extends BuildMenu {

    public MenuGameSetup(Main app) {
        super(app.pMenu);
        int pos = 0;
        WHTitle mLogo = new WHTitle("Game Setup", 90, pos);

        MenuSwitch sAutoload = new MenuSwitch("Autoload folder", app.getFont(0), 46, pos += 70, 240, cfg.isAutoloadFolder(), (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.setAutoloadFolder(sw.value);
        }, "Enabled", "Disabled");
        if (!app.WH2) {
            sAutoload.switchFont = app.getFont(1);
        }

        MenuSwitch sGore = new MenuSwitch("Gore mode:", app.getFont(0), 46, pos += 15, 240, cfg.gGameGore, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.gGameGore = sw.value;
        }, null, null);
        if (!app.WH2) {
            sGore.switchFont = app.getFont(1);
        }

        MenuSwitch sShowCutscenes = new MenuSwitch("Cutscenes:", app.getFont(0), 46, pos += 15, 240, cfg.showCutscenes,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.showCutscenes = sw.value;
                }, null, null);
        if (!app.WH2) {
            sShowCutscenes.switchFont = app.getFont(1);
        }

        MenuConteiner mPlayingDemo = new MenuConteiner("Demos playback:", app.getFont(0), 46, pos += 15, 240, null, 0,
                (handler, pItem) -> {
                    MenuConteiner item = (MenuConteiner) pItem;
                    cfg.gDemoSeq = item.num;
                }) {
            @Override
            public void open() {
                if (this.list == null) {
                    this.list = new char[3][];
                    this.list[0] = "Off".toCharArray();
                    this.list[1] = "In order".toCharArray();
                    this.list[2] = "Randomly".toCharArray();
                }
                num = cfg.gDemoSeq;
            }
        };
        if (!app.WH2) {
            mPlayingDemo.listFont = app.getFont(1);
        }

        MenuSwitch sRecord = new MenuSwitch("Record demo:", app.getFont(0), 46, pos += 15, 240, m_recstat == DEMOSTAT_RECORD,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    m_recstat = sw.value ? DEMOSTAT_RECORD : DEMOSTAT_NULL;
                }, null, null) {

            @Override
            public void open() {
                value = gDemoScreen.isRecordEnabled();
                mCheckEnableItem(!app.isCurrentScreen(gGameScreen));
            }
        };
        if (!app.WH2) {
            sRecord.switchFont = app.getFont(1);
        }

        MenuSwitch mTimer = new MenuSwitch("Game loop timer:", app.getFont(0), 46, pos + 15, 240, cfg.isLegacyTimer(),
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.setLegacyTimer(sw.value);
                    engine.inittimer(cfg.isLegacyTimer(),  TIMERRATE, TICSPERFRAME);
                }, "Legacy", "Gdx") {
        };

        if (!app.WH2) {
            mTimer.switchFont = app.getFont(1);
        }


        addItem(mLogo, false);
        addItem(sAutoload, true);
        addItem(sGore, false);
        addItem(sShowCutscenes, false);
        addItem(mPlayingDemo, false);
        addItem(sRecord, false);
        addItem(mTimer, false);
    }
}
