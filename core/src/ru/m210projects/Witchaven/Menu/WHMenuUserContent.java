package ru.m210projects.Witchaven.Menu;

import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuFileBrowser;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.filehandle.grp.GrpFile;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Witchaven.Main;
import ru.m210projects.Witchaven.Types.EpisodeEntry;
import ru.m210projects.Witchaven.Types.EpisodeInfo;
import ru.m210projects.Witchaven.Types.EpisodeManager;

import java.util.List;

import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.HIGHEST;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_DIRECTORY;
import static ru.m210projects.Witchaven.Globals.gCurrentEpisode;
import static ru.m210projects.Witchaven.Globals.gOriginalEpisode;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.MAINMENU;

public class WHMenuUserContent extends BuildMenu {

    public static final EpisodeManager episodeManager = new EpisodeManager();
    public static boolean usecustomarts;
    private static GrpFile usergroup;
    public int skills;

    public WHMenuUserContent(final Main app) {
        super(app.pMenu);
        MenuTitle title = new WHTitle("User content", 90, 0);

        int width = 240;
        MenuFileBrowser list = new MenuFileBrowser(app, app.WH2 ? app.getFont(0) : app.getFont(1), app.getFont(0), app.WH2 ? app.getFont(0) : app.getFont(1), 40, 55, width, 1, app.WH2 ? 8 : 10, MAINMENU) {
            @Override
            public void init() {
                registerExtension("map", 0, 0);
                registerExtension("zip", 5, 1);
                registerExtension("pk3", 5, 1);
                registerClass(EpisodeEntry.Addon.class, 5, 2);
                registerExtension("dmo", 1, -1);
            }

            @Override
            public void handleDirectory(Directory dir) {
                EpisodeInfo ep = episodeManager.getEpisode(dir.getDirectoryEntry());
                if (ep != null) {
                    // don't show main
                    if (ep.equals(gOriginalEpisode)) {
                        return;
                    }
                    addFile(ep.getEpisodeEntry());
                }
            }

            @Override
            public void handleFile(FileEntry file) {
                switch (file.getExtension()) {
                    case "MAP":
                        Directory dir = file.getParent();
                        EpisodeInfo addon = episodeManager.getEpisode(dir.getDirectoryEntry());
                        if (addon != null) {
                            for (int j = 0; j < addon.maps(); j++) {
                                if (file.getName().equalsIgnoreCase(addon.gMapInfo.get(j).path)) {
                                    return; // This file has added to addon
                                }
                            }
                        }
                        addFile(file);
                        break;
                    case "DMO":
                        if (gDemoScreen.isDemoFile(file)) {
                            addFile(file);
                        }
                        break;
                    // case "CON":
                    case "PK3":
                    case "ZIP":
                        EpisodeInfo ep = episodeManager.getEpisode(file);
                        if (ep != null) {
                            addFile(ep.getEpisodeEntry());
                        }
                        break;
                }
            }

            @Override
            public void invoke(FileEntry fil) {
                switch (fil.getExtension()) {
                    case "MAP":
                        launchMap(fil, skills);
                        app.pMenu.mClose();
                        break;
                    case "GRP":
                    case "ZIP":
                    case "PK3":
                        launchEpisode(episodeManager.getEpisode(fil), skills);
                        break;
                    case "DMO":
                        Directory parent = fil.getParent();
                        List<Entry> demoList = gDemoScreen.checkDemoEntry(parent);
                        gDemoScreen.nDemonum = demoList.indexOf(fil);
                        gDemoScreen.showDemo(fil);
                        app.pMenu.mClose();
                        break;
                    default:
                        if (fil instanceof EpisodeEntry) {
                            launchEpisode(episodeManager.getEpisode(fil), skills);
                        }
                        break;
                }
            }
        };
        list.pathPal = 7;
        addItem(title, false);
        addItem(list, true);
    }

    public static void resetEpisodeResources(EpisodeInfo def) {
        Console.out.println("Resetting custom resources", OsdColor.GREEN);
        if (usergroup != null) {
            game.getCache().removeGroup(usergroup);
        }
        usergroup = null;
        gCurrentEpisode = def;

        if (!usecustomarts) {
            game.setDefs(game.baseDef);
            return;
        }

        System.err.println("Reset to default resources");
        if (engine.loadpics() == 0) {
            throw new AssertException("ART files not found " + game.getCache().getGameDirectory().getPath().resolve(engine.getTileManager().getTilesPath()));
        }

        if (!game.setDefs(game.baseDef)) {
            game.baseDef.apply();
        }

        usecustomarts = false;
    }

    public static boolean checkEpisodeResources(EpisodeInfo addon) {
        resetEpisodeResources(gOriginalEpisode);

        usergroup = new GrpFile("RemovableGroup");
        EpisodeEntry.Addon addonEntry = addon.getEpisodeEntry();
        DefScript addonScript;
        Group parent = addonEntry.getGroup();

        addonScript = new DefScript(game.baseDef, addonEntry.getFileEntry());
        if (!(parent instanceof Directory)) { // is group entry
            try {
                Entry res = parent.getEntry(game.getGameDef()); // load def scripts
                if (res.exists()) {
                    addonScript.loadScript(parent.getName() + " script", res);
                }
                searchEpisodeResources(parent, usergroup);
            } catch (Exception e) {
                throw new WarningException("Error found in " + addonEntry.getName() + "\r\n" + e.getMessage());
            }
        } else {
            if (!game.getCache().isGameDirectory(parent)) {
                searchEpisodeResources(parent, usergroup);
                Entry def = parent.getEntry(game.getGameDef());
                if (def.exists()) {
                    addonScript.loadScript(def);
                }
            }
        }

        // Loading user package files
        game.getCache().addGroup(usergroup, HIGHEST);
        InitGroupResources(addonScript, usergroup.getEntries());
        gCurrentEpisode = addon;
        game.setDefs(addonScript);
        return true;
    }

    private static void searchEpisodeResources(Group container, GrpFile resourceHolder) {
        for (Entry file : container.getEntries()) {
            Group subContainer = DUMMY_DIRECTORY;
            if (file.isDirectory() && file instanceof FileEntry) {
                subContainer = ((FileEntry) file).getDirectory();
            } else if (file.isExtension("pk3") || file.isExtension("zip") || file.isExtension("grp") || file.isExtension("rff")) {
                subContainer = game.getCache().newGroup(file);
            }

            if (!subContainer.equals(DUMMY_DIRECTORY)) {
                searchEpisodeResources(subContainer, resourceHolder);
            } else {
                resourceHolder.addEntry(file);
            }
        }
    }

    public static void InitGroupResources(DefScript addonScript, java.util.List<Entry> list) {
        for (Entry res : list) {
            switch (res.getExtension()) {
                case "ART":
                    engine.loadpic(res);
                    usecustomarts = true;
                    break;
                case "DEF":
                    if (!res.getName().equalsIgnoreCase(game.getGameDef())) {
                        addonScript.loadScript(res.getName() + " script", res);
                        Console.out.println("Found def-script. Loading " + res.getName());
                    }
                    break;
            }
        }
    }

    private void launchEpisode(EpisodeInfo ep, int skill) {
        if (ep == null) {
            return;
        }

        gGameScreen.newgame(ep, 1, skill);
    }

    private void launchMap(final FileEntry fil, int skill) {
        if (fil == null) {
            return;
        }

        gGameScreen.newgame(fil, 0, skill);
    }
}
