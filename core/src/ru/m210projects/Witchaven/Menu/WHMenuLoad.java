package ru.m210projects.Witchaven.Menu;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuLoadSave;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Witchaven.Screens.PrecacheScreen;
import ru.m210projects.Witchaven.Types.DemoFile;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Globals.playerDemoData;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.MAINMENU;
import static ru.m210projects.Witchaven.WHPLR.mapon;
import static ru.m210projects.Witchaven.WHPLR.player;
import static ru.m210projects.Witchaven.WHScreen.showmessage;
import static ru.m210projects.Witchaven.Whldsv.*;
import static ru.m210projects.Witchaven.Whmap.loadnewlevel;

public class WHMenuLoad extends MenuLoadSave {

    public WHMenuLoad(final BuildGame app) {
        super(app, app.getFont(1), 90, 60, 190, 210, 9, 0, 5, MAINMENU, (handler, pItem) -> {
            final MenuSlotList item = (MenuSlotList) pItem;
            if (canLoad(item.getFileEntry()) && (!app.isCurrentScreen(gLoadScreen)
                    && !(app.getScreen() instanceof PrecacheScreen))) {
                app.changeScreen(gLoadScreen);
                gLoadScreen.init(() -> {
                    if (!loadgame(item.getFileEntry())) {
                        handler.mClose();
                        app.setPrevScreen();
                        if (app.isCurrentScreen(gGameScreen)) {
                            showmessage("Incompatible version of saved game found!", 240);
                            app.pNet.ready2send = true;
                        }
                    } else if (item.getFileEntry().getName().equalsIgnoreCase("autosave.sav") && gDemoScreen.isRecordEnabled()) {
                        gDemoScreen.onStopPlaying();
                        PLAYER pp = player[myconnectindex];
                        playerDemoData = new DemoFile.PlayerDemoData();
                        playerDemoData.setFrom(pp);
                        loadnewlevel(mapon);
                    }
                });
            }
        }, false);
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return new WHTitle(text, 90, 0);
    }

    @Override
    public MenuPicnum getPicnum(Engine draw, int x, int y) {
        return new MenuPicnum(draw, x - 68, y - 3, MAINMENU, MAINMENU, 0x8700) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = game.getRenderer();
                if (nTile != defTile) {
                    renderer.rotatesprite(x << 16, y << 16, 2 * nScale, 0, nTile, 0, 0, 10 | 16);
                } else {
                    renderer.rotatesprite(x << 16, y << 16, nScale, 0, nTile, 0, 0, 10 | 16);
                }
            }
        };
    }

    @Override
    public boolean loadData(FileEntry fileEntry) {
        return lsReadLoadData(fileEntry) != -1;
    }

    @Override
    public MenuText getInfo(BuildGame app, int x, int y) {
        return new MenuText(lsInf.info, app.getFont(1), x - 58, y + 90, 0) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = handler.getRenderer();
                int ty = y;
                if (lsInf.addonfile != null && !lsInf.addonfile.isEmpty()) {
                    font.drawTextScaled(renderer, x, ty, lsInf.addonfile, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                    ty -= 10;
                }
                if (lsInf.date != null && !lsInf.date.isEmpty()) {
                    font.drawTextScaled(renderer, x, ty, lsInf.date.toCharArray(), 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                    ty -= 10;
                }
                if (lsInf.info != null) {
                    font.drawTextScaled(renderer, x, ty, lsInf.info.toCharArray(), 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                }
            }
        };
    }

}
