package ru.m210projects.Witchaven.Menu;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Witchaven.Main;

import static ru.m210projects.Witchaven.Main.cfg;

public class MenuInterfaceSet extends BuildMenu {

    public MenuInterfaceSet(Main app) {
        super(app.pMenu);
        int pos = 0;
        WHTitle mLogo = new WHTitle("Interface Setup", 90, pos);

        MenuSwitch messages = new MenuSwitch("Messages:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 60, 240, true, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.MessageState = sw.value;
        }, null, null) {
            @Override
            public void open() {
                value = cfg.MessageState;
            }
        };

        MenuSlider sScreenSize = new MenuSlider(app.pSlider, "Screen size:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, cfg.gViewSize, 0, 1, 1,
                (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.gViewSize = slider.value;
                }, false) {
            @Override
            public void open() {
                value = cfg.gViewSize;
            }
        };

        MenuSlider mCurSize = new MenuSlider(app.pSlider, "Mouse cursor size:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, cfg.getgMouseCursorSize(),
                0x1000, 0x28000, 4096, (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.setgMouseCursorSize(slider.value);
                }, false);

        pos += 5;
        MenuSwitch sCrosshair = new MenuSwitch("Crosshair:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, true, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.gCrosshair = sw.value;
        }, null, null) {
            @Override
            public void open() {
                value = cfg.gCrosshair;
            }
        };

        MenuSlider sCrossSize = new MenuSlider(app.pSlider, "Crosshair size:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, cfg.gCrossSize, 8192,
                3 * 65536, 8192, (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.gCrossSize = slider.value;
                }, true);
        sCrossSize.digitalMax = 65536.0f;

        MenuSlider sHudSize = new MenuSlider(app.pSlider, "HUD size:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, cfg.gHudScale, 32768,
                2 * 65536, 4096, (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.gHudScale = slider.value;
                }, true);
        sHudSize.digitalMax = 65536.0f;

        MenuConteiner sShowStat = new MenuConteiner("Statistics:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, null, 0, (handler, pItem) -> {
            MenuConteiner item = (MenuConteiner) pItem;
            cfg.gShowStat = item.num;
        }) {
            @Override
            public void open() {
                if (this.list == null) {
                    this.list = new char[3][];
                    this.list[0] = "Off".toCharArray();
                    this.list[1] = "Always show".toCharArray();
                    this.list[2] = "Only on a minimap".toCharArray();
                }
                num = cfg.gShowStat;
            }
        };

        MenuSlider sStatSize = new MenuSlider(app.pSlider, "Statistics size:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, cfg.gStatSize, 16384,
                2 * 65536, 4096, (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.gStatSize = slider.value;
                }, true);
        sStatSize.digitalMax = 65536.0f;
        pos += 5;

        MenuSwitch sShowMapName = new MenuSwitch("Info at level startup:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, cfg.showMapInfo, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.showMapInfo = sw.value;
        }, null, null);

        MenuSwitch sShowFPS = new MenuSwitch("Fps counter:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, cfg.isgShowFPS(), (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.setgShowFPS(sw.value);
        }, null, null);

        MenuSlider sFpsSize = new MenuSlider(app.pSlider, "Fps size:", app.WH2 ? app.getFont(0) : app.getFont(1), 47, pos += 10, 240, (int) (cfg.getgFpsScale() * 65536), 32768, 3 * 65536, 8192,
                (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.setgFpsScale(slider.value / 65536.0f);
                }, true);
        sFpsSize.digitalMax = 65536f;

        addItem(mLogo, false);

        addItem(messages, true);
        addItem(sScreenSize, false);
        addItem(mCurSize, false);
        addItem(sCrosshair, false);
        addItem(sCrossSize, false);
        addItem(sHudSize, false);
        addItem(sShowStat, false);
        addItem(sStatSize, false);
        addItem(sShowMapName, false);
        addItem(sShowFPS, false);
        addItem(sFpsSize, false);
    }

}
