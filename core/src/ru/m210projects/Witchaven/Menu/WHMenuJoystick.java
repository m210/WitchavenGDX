package ru.m210projects.Witchaven.Menu;

import ru.m210projects.Build.input.keymap.Keymap;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuJoystick;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Witchaven.Main;

import static com.badlogic.gdx.Input.Keys.*;

public class WHMenuJoystick extends MenuJoystick {

    public WHMenuJoystick(Main app) {
        super(app, 46, 50, 240, app.getFont(0).getSize() - 2, 15,
                app.getFont(0),
                8);

        if (!app.WH2) {
            mList.pal_left = 7;
            mList.font = app.getFont(1);

            mJoyDevices.listFont = app.getFont(1);
            mDeadZone.sliderNumbers = app.getFont(1);
            mLookSpeed.sliderNumbers = app.getFont(1);
            mTurnSpeed.sliderNumbers = app.getFont(1);
            mInvert.switchFont = app.getFont(1);
        }
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return new WHTitle(text, 90, 0);
    }

    @Override
    public String keyNames(int keycode) {
        if (keycode == 0) {
            return "None";
        }

        switch (keycode) {
            case COMMA:
                return "Comma";
            case PERIOD:
                return "Period";
            case MINUS:
                return "Minus";
            case EQUALS:
                return "Equals";
            case LEFT_BRACKET:
                return "L_bracket";
            case RIGHT_BRACKET:
                return "R_bracket";
            case BACKSLASH:
                return "Backslash";
            case SEMICOLON:
                return "Semicolon";
            case APOSTROPHE:
                return "Apostrophe";
            case SLASH:
                return "Slash";
            case AT:
                return "At";
            case COLON:
                return "Colon";
            default:
                return Keymap.toString(keycode);
        }
    }
}
