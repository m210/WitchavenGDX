package ru.m210projects.Witchaven.Menu;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuText;
import ru.m210projects.Build.Pattern.MenuItems.MenuVariants;
import ru.m210projects.Witchaven.Screens.DemoScreen;

import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Witchaven.Globals.boardfilename;
import static ru.m210projects.Witchaven.Main.gDemoScreen;
import static ru.m210projects.Witchaven.Main.gMenuScreen;
import static ru.m210projects.Witchaven.WHSND.sndStopMusic;
import static ru.m210projects.Witchaven.WHSND.stopallsounds;

public class WHMenuQTitle extends BuildMenu {

    public WHMenuQTitle(final BuildGame game) {
        super(game.pMenu);
        MenuText QuitQuestion = new MenuText("Quit to title?", game.getFont(0), 160, 85, 1);
        MenuVariants QuitVariants = new MenuVariants(game.pEngine, "[Y/N]", game.getFont(0), 160, 102) {
            @Override
            public void positive(MenuHandler menu) {
                menu.mClose();

                game.pNet.ready2send = false;
                boardfilename = DUMMY_ENTRY;
                sndStopMusic();
                stopallsounds();

                if (!DemoScreen.isDemoPlaying() && (numplayers > 1 /*|| mFakeMultiplayer*/)) {
                    Gdx.app.postRunnable(() -> game.pNet.NetDisconnect(myconnectindex));
                } else {
                    game.show();
                }
            }
        };

        addItem(QuitQuestion, false);
        addItem(QuitVariants, true);
    }
}
