package ru.m210projects.Witchaven;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Witchaven.Screens.GameScreen;
import ru.m210projects.Witchaven.Types.Item;
import ru.m210projects.Witchaven.Types.MapInfo;
import ru.m210projects.Witchaven.Types.PLAYER;

import java.util.Arrays;

import static ru.m210projects.Build.Engine.MAXSTATUS;
import static ru.m210projects.Build.Engine.automapping;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Witchaven.AI.Ai.aiInit;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Items.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHFX.*;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHTAG.*;
import static ru.m210projects.Witchaven.Whldsv.gAutosaveRequest;

public class Whmap {

    public static boolean nextlevel;

    public static void loadnewlevel(int mapon) {
        gDemoScreen.onStopRecord();

        MapInfo nextmap;
        if (gCurrentEpisode != null && (nextmap = gCurrentEpisode.getMap(mapon)) != null) {
            boardfilename = game.getCache().getEntry(nextmap.path, true);
            Console.out.println("Entering level " + boardfilename);
            nextlevel = true;
            gGameScreen.loadboard(boardfilename, null);
        } else {
            game.show();
        }
    }

    private static void preparesectors(Board board) {
        for (int i = 0; i < board.getSectorCount(); i++) {
            Sector s = board.getSector(i);
            if (s == null) {
                continue;
            }

            ceilingshadearray[i] = s.getCeilingshade();
            floorshadearray[i] = s.getFloorshade();

            if (game.WH2) {
                if (mapon == 13) {
                    if (i == 209 && s.getLotag() == 62) {
                        s.setLotag(0);
                    }

                    if (i == 435 && s.getLotag() == 0) {
                        s.setLotag(62);
                    }
                }
            }

            if (s.getLotag() == 70) {
                skypanlist[skypancnt++] = (short) i;
            }

            if (s.getLotag() >= 80 && s.getLotag() <= 89) {
                floorpanninglist[floorpanningcnt++] = (short) i;
            }

            if (s.getLotag() >= 900 && s.getLotag() <= 999) {
                lavadrylandsector[lavadrylandcnt] = (short) i;
                lavadrylandcnt++;
            }

            if (s.getLotag() >= 2100 && s.getLotag() <= 2199) {
                int dax = 0;
                int day = 0;
                for (ListNode<Wall> wn = s.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    dax += wal.getX();
                    day += wal.getY();
                }

                revolvepivotx[revolvecnt] = dax / s.getWallnum();
                revolvepivoty[revolvecnt] = day / s.getWallnum();

                int k = 0;
                for (ListNode<Wall> wn = s.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    revolvex[revolvecnt][k] = wal.getX();
                    revolvey[revolvecnt][k] = wal.getY();
                    k++;
                }
                revolvesector[revolvecnt] = (short) i;
                revolveang[revolvecnt] = 0;

                revolveclip[revolvecnt] = 1;
                ListNode<Wall> wn = s.getWallNode();

                if (wn != null) {
                    Sector nexts = board.getSector(wn.get().getNextsector());
                    if (nexts != null && s.getCeilingz() == nexts.getCeilingz()) {
                        revolveclip[revolvecnt] = 0;
                    }
                }

                revolvecnt++;
            }

            switch (s.getLotag()) {
                case 131:
                case 132:
                case 133:
                case 134:
                case DOORSWINGTAG:
                    for (ListNode<Wall> wn = s.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        int j = wn.getIndex();

                        if (wal.getLotag() == 4) {
                            Wall w2 = wal.getWall2();
                            Wall w3 = w2.getWall2();
                            Wall w4 = w3.getWall2();
                            if ((wal.getX() == w4.getWall2().getX()) && (wal.getY() == w4.getWall2().getY())) {
                                swingdoor[swingcnt].wall[0] = j;
                                swingdoor[swingcnt].wall[1] = wal.getPoint2();
                                swingdoor[swingcnt].wall[2] = w2.getPoint2();
                                swingdoor[swingcnt].wall[3] = w3.getPoint2();
                                swingdoor[swingcnt].angopen = 1536;
                                swingdoor[swingcnt].angclosed = 0;
                                swingdoor[swingcnt].angopendir = -1;
                            } else {
                                swingdoor[swingcnt].wall[0] = wal.getPoint2();
                                swingdoor[swingcnt].wall[1] = j;
                                swingdoor[swingcnt].wall[2] = engine.lastwall(j);
                                swingdoor[swingcnt].wall[3] = engine.lastwall(swingdoor[swingcnt].wall[2]);
                                swingdoor[swingcnt].angopen = 512;
                                swingdoor[swingcnt].angclosed = 0;
                                swingdoor[swingcnt].angopendir = 1;
                            }
                            for (int k = 0; k < 4; k++) {
                                Wall w = board.getWall(swingdoor[swingcnt].wall[k]);
                                if (w != null) {
                                    swingdoor[swingcnt].x[k] = w.getX();
                                    swingdoor[swingcnt].y[k] = w.getY();
                                }
                            }
                            swingdoor[swingcnt].sector = i;
                            swingdoor[swingcnt].ang = swingdoor[swingcnt].angclosed;
                            swingdoor[swingcnt].anginc = 0;
                            swingcnt++;
                        }
                    }
                    break;
                case 11:
                    xpanningsectorlist[xpanningsectorcnt++] = (short) i;
                    break;
                case 12:
                    int dax = 0x7fffffff;
                    int day = 0x7fffffff;
                    int dax2 = 0x80000000;
                    int day2 = 0x80000000;
                    Wall wallk = null;

                    for (ListNode<Wall> wn = s.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        if (wal.getX() < dax) {
                            dax = wal.getX();
                        }
                        if (wal.getY() < day) {
                            day = wal.getY();
                        }
                        if (wal.getX() > dax2) {
                            dax2 = wal.getX();
                        }
                        if (wal.getY() > day2) {
                            day2 = wal.getY();
                        }
                        if (wal.getLotag() == 3) {
                            wallk = wal;
                        }
                    }

                    Wall w = wallk;
                    if (w != null) {
                        if (w.getX() == dax) {
                            dragxdir[dragsectorcnt] = -16;
                        }
                        if (w.getY() == day) {
                            dragydir[dragsectorcnt] = -16;
                        }
                        if (w.getX() == dax2) {
                            dragxdir[dragsectorcnt] = 16;
                        }
                        if (w.getY() == day2) {
                            dragydir[dragsectorcnt] = 16;
                        }
                    }

                    dragx1[dragsectorcnt] = 0x7fffffff;
                    dragy1[dragsectorcnt] = 0x7fffffff;
                    dragx2[dragsectorcnt] = 0x80000000;
                    dragy2[dragsectorcnt] = 0x80000000;

                    ListNode<Wall> wallPtr = s.getWallNode();
                    if (wallPtr != null) {
                        Wall firstWall = wallPtr.get();
                        Sector nsector = board.getSector(firstWall.getNextsector());
                        if (nsector != null) {
                            for (ListNode<Wall> wn = nsector.getWallNode(); wn != null; wn = wn.getNext()) {
                                Wall wal = wn.get();
                                if (wal.getX() < dragx1[dragsectorcnt]) {
                                    dragx1[dragsectorcnt] = wal.getX();
                                }
                                if (wal.getY() < dragy1[dragsectorcnt]) {
                                    dragy1[dragsectorcnt] = wal.getY();
                                }
                                if (wal.getX() > dragx2[dragsectorcnt]) {
                                    dragx2[dragsectorcnt] = wal.getX();
                                }
                                if (wal.getY() > dragy2[dragsectorcnt]) {
                                    dragy2[dragsectorcnt] = wal.getY();
                                }
                            }
                        }

                        dragx1[dragsectorcnt] += (firstWall.getX() - dax);
                        dragy1[dragsectorcnt] += (firstWall.getY() - day);
                        dragx2[dragsectorcnt] -= (dax2 - firstWall.getX());
                        dragy2[dragsectorcnt] -= (day2 - firstWall.getY());
                        dragfloorz[dragsectorcnt] = s.getFloorz();

                        dragsectorlist[dragsectorcnt++] = (short) i;
                    }
                    break;
                case 10:
                case 14:
                    // case 15:
                    // captureflag sector
                case 4002:
                    warpsectorlist[warpsectorcnt++] = (short) i;
                    break;
                case 10000:
                    System.out.println("bobbingsectorlist");
                    bobbingsectorlist[bobbingsectorcnt++] = (short) i;
            }
            if (s.getFloorpicnum() == TELEPAD && s.getLotag() == 0) {
                warpsectorlist[warpsectorcnt++] = (short) i;
            }
            if (s.getFloorpicnum() == FLOORMIRROR) {
                floormirrorsector[floormirrorcnt++] = (short) i;
            }
        }

        ypanningwallcnt = 0;
        for (int i = 0; i < board.getWallCount(); i++) {
            Wall wal = board.getWall(i);
            if (wal == null) {
                continue;
            }

            wallshadearray[i] = wal.getShade();
            if (wal.getLotag() == 1) {
                if (ypanningwallcnt < ypanningwalllist.length) {
                    ypanningwalllist[ypanningwallcnt++] = (short) i;
                }
            }
        }
    }

    public static Board prepareboard(GameScreen screen, Entry fil) {
        try {
            Board board = engine.loadboard(fil);

            BuildPos out = board.getPos();

            PLAYER plr = player[0];
            lockclock = 0;
            game.pNet.ResetTimers();
            engine.srand(17);

            plr.setX(out.x);
            plr.setY(out.y);
            plr.setZ(out.z);
            plr.setAng(out.ang);
            plr.setSector(out.sectnum);

            swingcnt = 0;
            xpanningsectorcnt = 0;
            ypanningwallcnt = 0;
            floorpanningcnt = 0;
//	    crushsectorcnt=0;
            revolvecnt = 0;
            warpsectorcnt = 0;
            dragsectorcnt = 0;
            ironbarscnt = 0;
            bobbingsectorcnt = 0;
            playertorch = 0;

            plr.setDamage_angvel(0);
            plr.setDamage_svel(0);
            plr.setDamage_vel(0);

            hours = 0;
            minutes = 0;
            seconds = 0;
            fortieth = 0;

//		goblinwarcnt = 0;
            treasurescnt = 0;
            treasuresfound = 0;
            killcnt = 0;
            kills = 0;
            expgained = 0;
            // numinterpolations=0;

            // the new mirror code
            floormirrorcnt = 0;

            ArtEntry artEntry = engine.getTile(FLOORMIRROR);
            if (!(artEntry instanceof DynamicArtEntry) || !artEntry.exists()) {
                artEntry = engine.allocatepermanenttile(FLOORMIRROR, 0, 0);
            }
            ((DynamicArtEntry) artEntry).clearData();

            Arrays.fill(arrowsprite, (short) -1);
            Arrays.fill(throwpikesprite, (short) -1);

            lavadrylandcnt = 0;
            aiInit();

            for (int i = 0; i < board.getSpriteCount(); i++) { // setup sector effect options
                Sprite spr = board.getSprite(i);
                if (spr == null || spr.getStatnum() >= MAXSTATUS) {
                    continue;
                }

                Sector sector = board.getSector(spr.getSectnum());
                if (sector == null) {
                    continue;
                }

                if (!game.WH2 && mapon == 5 && i == 0 && spr.getLotag() == 0 && spr.getHitag() == 0) {
                    spr.setLotag(1);
                    spr.setHitag(34);
                }

                if (spr.getPicnum() == CONE) {
                    sparksx = spr.getX();
                    sparksy = spr.getY();
                    sparksz = spr.getZ();
                    for (int j = 0; j < 10; j++) {
                        makesparks(i, 1);
                    }
                    for (int j = 10; j < 20; j++) {
                        makesparks(i, 2);
                    }
                    for (int j = 20; j < 30; j++) {
                        makesparks(i, 3);
                    }
                    spr.setCstat(spr.getCstat() & ~3);
                    spr.setCstat(spr.getCstat() | 0x8000);
                    spr.setClipdist(4);
                    engine.changespritestat(i, (short) 0);
                    sector.setLotag(50);
                    sector.setHitag(spr.getHitag());
                    if (sector.getHitag() == 0) {
                        sector.setHitag(1);
                    }
                }

                if ((spr.getCstat() & (16 + 32)) == (16 + 32)) {
                    spr.setCstat(spr.getCstat() & ~(16 | 32));
                }

//			if (spr.picnum == RAT) {
//				ratcnt++;
//				if (ratcnt > 10)
//					engine.deletesprite((short) i);
//			}

                if (spr.getPicnum() == SPAWN) {
                    engine.deletesprite(i);
                }

                if (spr.getPicnum() == TORCH) {
                    spr.setCstat(spr.getCstat() & ~3);
                    engine.changespritestat(i, TORCHLIGHT);
                }

                if (spr.getPicnum() == STANDINTORCH || spr.getPicnum() == BOWLOFFIRE) {
                    engine.changespritestat(i, TORCHLIGHT);
                }

                if (spr.getPicnum() == GLOW) {
                    engine.changespritestat(i, GLOWLIGHT);
                }

                if (spr.getPicnum() == SNDEFFECT) {
                    sector.setExtra(spr.getLotag());
                    engine.deletesprite(i);
                }

                if (spr.getPicnum() == SNDLOOP) { // loop on
                    sector.setExtra((short) (32768 | (spr.getLotag() << 1) | 1));
                    engine.deletesprite(i);
                }

                if (spr.getPicnum() == SNDLOOPOFF) { // loop off
                    sector.setExtra((short) (32768 | (spr.getLotag() << 1)));
                    engine.deletesprite(i);
                }

                if (spr.getLotag() == 80) {
                    ironbarsector[ironbarscnt] = spr.getSectnum();
                    ironbarsdone[ironbarscnt] = 0;
                    ironbarsanim[ironbarscnt] = (short) i;
                    ironbarsgoal[ironbarscnt] = 0;
                    ironbarscnt++;
                }

                if (game.WH2) {
                    switch (spr.getPicnum()) {
                        case WH2HELMET:
                            spr.setDetail(HELMETTYPE);
                            break;
                        case WH2PLATEARMOR:
                            spr.setDetail(PLATEARMORTYPE);
                            break;
                        case WH2CHAINMAIL:
                            spr.setDetail(CHAINMAILTYPE);
                            break;
                        case WH2LEATHERARMOR:
                            spr.setDetail(LEATHERARMORTYPE);
                            break;
                        case WH2PENTAGRAM:
                            spr.setDetail(PENTAGRAMTYPE);
                            break;
                        case WH2CRYSTALSTAFF:
                            spr.setDetail(CRYSTALSTAFFTYPE);
                            break;
                        case WH2AMULETOFTHEMIST:
                            spr.setDetail(AMULETOFTHEMISTTYPE);
                            break;
                        case WH2HORNEDSKULL:
                            spr.setDetail(HORNEDSKULLTYPE);
                            break;
                        case WH2THEHORN:
                            spr.setDetail(THEHORNTYPE);
                            break;
                        case WH2BRASSKEY:
                            spr.setDetail(BRASSKEYTYPE);
                            break;
                        case WH2BLACKKEY:
                            spr.setDetail(BLACKKEYTYPE);
                            break;
                        case WH2GLASSKEY:
                            spr.setDetail(GLASSKEYTYPE);
                            break;
                        case WH2IVORYKEY:
                            spr.setDetail(IVORYKEYTYPE);
                            break;
                        case WH2SCROLLSCARE:
                            spr.setDetail(SCROLLSCARETYPE);
                            break;
                        case WH2SCROLLNIGHT:
                            spr.setDetail(SCROLLNIGHTTYPE);
                            break;
                        case WH2SCROLLFREEZE:
                            spr.setDetail(SCROLLFREEZETYPE);
                            break;
                        case WH2SCROLLMAGIC:
                            spr.setDetail(SCROLLMAGICTYPE);
                            break;
                        case WH2SCROLLOPEN:
                            spr.setDetail(SCROLLOPENTYPE);
                            break;
                        case WH2SCROLLFLY:
                            spr.setDetail(SCROLLFLYTYPE);
                            break;
                        case WH2SCROLLFIREBALL:
                            spr.setDetail(SCROLLFIREBALLTYPE);
                            break;
                        case WH2SCROLLNUKE:
                            spr.setDetail(SCROLLNUKETYPE);
                            break;
                        case WH2QUIVER:
                            spr.setDetail(QUIVERTYPE);
                            break;
                        case WALLBOW:
                        case WH2BOW:
                            spr.setDetail(BOWTYPE);
                            break;
                        case WH2WEAPON1:
                            spr.setDetail(WEAPON1TYPE);
                            break;
                        case WH2WEAPON1A:
                            spr.setDetail(WEAPON1ATYPE);
                            break;
                        case WH2GOBWEAPON:
                            spr.setDetail(GOBWEAPONTYPE);
                            break;
                        case WH2WEAPON2:
                            spr.setDetail(WEAPON2TYPE);
                            break;
                        case WALLAXE:
                        case WH2WEAPON4:
                            spr.setDetail(WEAPON4TYPE);
                            break;
                        case WH2THROWHALBERD:
                            spr.setDetail(THROWHALBERDTYPE);
                            break;
                        case WH2WEAPON5:
                            spr.setDetail(WEAPON5TYPE);
                            break;
                        case WH2SHIELD:
                            spr.setDetail(SHIELDTYPE);
                            break;
                        case WH2WEAPON5B:
                            spr.setDetail(WEAPON5BTYPE);
                            break;
                        case WALLPIKE:
                        case WH2THROWPIKE + 1:
                            spr.setDetail(WALLPIKETYPE);
                            break;
                        case WH2WEAPON6:
                            spr.setDetail(WEAPON6TYPE);
                            break;
                        case WH2WEAPON7:
                            spr.setDetail(WEAPON7TYPE);
                            break;
                        case WEAPON8:
                            spr.setDetail(WEAPON8TYPE);
                            break;
                    }
                } else {
                    switch (spr.getPicnum()) {
                        case WH1HELMET:
                            spr.setDetail(HELMETTYPE);
                            break;
                        case WH1PLATEARMOR:
                            spr.setDetail(PLATEARMORTYPE);
                            break;
                        case WH1CHAINMAIL:
                            spr.setDetail(CHAINMAILTYPE);
                            break;
                        case WH1LEATHERARMOR:
                            spr.setDetail(LEATHERARMORTYPE);
                            break;
                        case WH1PENTAGRAM:
                            spr.setDetail(PENTAGRAMTYPE);
                            break;
                        case WH1CRYSTALSTAFF:
                            spr.setDetail(CRYSTALSTAFFTYPE);
                            break;
                        case WH1AMULETOFTHEMIST:
                            spr.setDetail(AMULETOFTHEMISTTYPE);
                            break;
                        case WH1HORNEDSKULL:
                            spr.setDetail(HORNEDSKULLTYPE);
                            break;
                        case WH1THEHORN:
                            spr.setDetail(THEHORNTYPE);
                            break;
                        case WH1BRASSKEY:
                            spr.setDetail(BRASSKEYTYPE);
                            break;
                        case WH1BLACKKEY:
                            spr.setDetail(BLACKKEYTYPE);
                            break;
                        case WH1GLASSKEY:
                            spr.setDetail(GLASSKEYTYPE);
                            break;
                        case WH1IVORYKEY:
                            spr.setDetail(IVORYKEYTYPE);
                            break;
                        case WH1SCROLLSCARE:
                            spr.setDetail(SCROLLSCARETYPE);
                            break;
                        case WH1SCROLLNIGHT:
                            spr.setDetail(SCROLLNIGHTTYPE);
                            break;
                        case WH1SCROLLFREEZE:
                            spr.setDetail(SCROLLFREEZETYPE);
                            break;
                        case WH1SCROLLMAGIC:
                            spr.setDetail(SCROLLMAGICTYPE);
                            break;
                        case WH1SCROLLOPEN:
                            spr.setDetail(SCROLLOPENTYPE);
                            break;
                        case WH1SCROLLFLY:
                            spr.setDetail(SCROLLFLYTYPE);
                            break;
                        case WH1SCROLLFIREBALL:
                            spr.setDetail(SCROLLFIREBALLTYPE);
                            break;
                        case WH1SCROLLNUKE:
                            spr.setDetail(SCROLLNUKETYPE);
                            break;
                        case WH1QUIVER:
                            spr.setDetail(QUIVERTYPE);
                            break;
                        case WALLBOW:
                        case WH1BOW:
                            spr.setDetail(BOWTYPE);
                            break;
                        case WH1WEAPON1:
                            spr.setDetail(WEAPON1TYPE);
                            break;
                        case WH1WEAPON1A:
                            spr.setDetail(WEAPON1ATYPE);
                            break;
                        case WH1GOBWEAPON:
                            spr.setDetail(GOBWEAPONTYPE);
                            break;
                        case WH1WEAPON2:
                            spr.setDetail(WEAPON2TYPE);
                            break;
                        case WALLAXE:
                        case WH1WEAPON4:
                            spr.setDetail(WEAPON4TYPE);
                            break;
                        case WH1THROWHALBERD:
                            spr.setDetail(THROWHALBERDTYPE);
                            break;
                        case WH1WEAPON5:
                            spr.setDetail(WEAPON5TYPE);
                            break;
                        case WH1SHIELD:
                            spr.setDetail(SHIELDTYPE);
                            break;
                        case WH1WEAPON5B:
                            spr.setDetail(WEAPON5BTYPE);
                            break;
                        case WALLPIKE:
                        case WH1THROWPIKE + 1:
                            spr.setDetail(WALLPIKETYPE);
                            break;
                        case WH1WEAPON6:
                            spr.setDetail(WEAPON6TYPE);
                            break;
                        case WH1WEAPON7:
                            spr.setDetail(WEAPON7TYPE);
                            break;
                    }
                }

                switch (spr.getPicnum()) {
                    case SILVERBAG:
                    case SILVERCOINS:
                        spr.setDetail(SILVERBAGTYPE);
                        break;
                    case GOLDBAG:
                    case GOLDBAG2:
                    case GOLDCOINS:
                    case GOLDCOINS2:
                        spr.setDetail(GOLDBAGTYPE);
                        break;
                    case GIFTBOX:
                        spr.setDetail(GIFTBOXTYPE);
                        break;
                    case FLASKBLUE:
                        spr.setDetail(FLASKBLUETYPE);
                        break;
                    case FLASKRED:
                        spr.setDetail(FLASKREDTYPE);
                        break;
                    case FLASKGREEN:
                        spr.setDetail(FLASKGREENTYPE);
                        break;
                    case FLASKOCHRE:
                        spr.setDetail(FLASKOCHRETYPE);
                        break;
                    case FLASKTAN:
                        spr.setDetail(FLASKTANTYPE);
                        break;
                    case DIAMONDRING:
                        spr.setDetail(DIAMONDRINGTYPE);
                        break;
                    case SHADOWAMULET:
                        spr.setDetail(SHADOWAMULETTYPE);
                        break;
                    case GLASSSKULL:
                        spr.setDetail(GLASSSKULLTYPE);
                        break;
                    case AHNK:
                        spr.setDetail(AHNKTYPE);
                        break;
                    case BLUESCEPTER:
                        spr.setDetail(BLUESCEPTERTYPE);
                        break;
                    case YELLOWSCEPTER:
                        spr.setDetail(YELLOWSCEPTERTYPE);
                        break;
                    case ADAMANTINERING:
                        spr.setDetail(ADAMANTINERINGTYPE);
                        break;
                    case ONYXRING:
                        spr.setDetail(ONYXRINGTYPE);
                        break;
                    case SAPHIRERING:
                        spr.setDetail(SAPHIRERINGTYPE);
                        break;
                    case WALLSWORD:
                    case WEAPON3A:
                        spr.setDetail(WEAPON3ATYPE);
                        break;
                    case WEAPON3:
                        spr.setDetail(WEAPON3TYPE);
                        break;
                    case GONZOBSHIELD:
                    case GONZOCSHIELD:
                    case GONZOGSHIELD:
                        spr.setDetail(GONZOSHIELDTYPE);
                        break;
                    case SPIKEBLADE:
                        spr.setDetail(SPIKEBLADETYPE);
                        break;
                    case SPIKE:
                        spr.setDetail(SPIKETYPE);
                        break;
                    case SPIKEPOLE:
                        spr.setDetail(SPIKEPOLETYPE);
                        break;
                    case MONSTERBALL:
                        spr.setDetail(MONSTERBALLTYPE);
                        break;
                    case WH1HANGMAN + 1:
                    case WH2HANGMAN + 1:
                        if ((spr.getPicnum() == (WH1HANGMAN + 1) && game.WH2) || (spr.getPicnum() == WH2HANGMAN + 1 && !game.WH2)) {
                            break;
                        }

                        spr.setXrepeat(28);
                        spr.setYrepeat(28);
                        break;
                    case GOBLINDEAD:
                        if (game.WH2) {
                            break;
                        }
                        spr.setXrepeat(36);
                        spr.setYrepeat(36);
                        break;
                    case STONEGONZOBSH:
                    case STONEGONZOBSW2:
                    case STONEGONZOCHM:
                    case STONEGONZOGSH:
                    case STONEGRONDOVAL:
                    case STONEGONZOBSW:
                        spr.setXrepeat(24);
                        spr.setYrepeat(24);
                        break;
                    case GONZOHMJUMP:
                    case GONZOSHJUMP:
                        spr.setXrepeat(24);
                        spr.setYrepeat(24);
                        spr.setClipdist(32);
                        spr.setExtra(spr.getLotag());
                        spr.setLotag(20);
                        if (spr.getExtra() == 3) {
                            spr.setLotag(80);
                        }
                        spr.setCstat(spr.getCstat() | 0x101);
                        break;

                    case PINE:
                        int treesize = (short) (((engine.krand() % 5) + 3) << 4);
                        spr.setXrepeat(treesize);
                        spr.setYrepeat(treesize);
                        break;

                    case GYSER:
                        if (game.WH2) {
                            break;
                        }
                        spr.setXrepeat(32);
                        spr.setYrepeat(18);
                        spr.setShade(-17);
                        spr.setPal(0);
                        spr.setDetail(GYSERTYPE);
                        // engine.changespritestat(i,DORMANT);
                        break;
                    case PATROLPOINT:
                        spr.setXrepeat(24);
                        spr.setYrepeat(32);

                        spr.setCstat(spr.getCstat() & ~3);
                        spr.setCstat(spr.getCstat() | 0x8000);
                        spr.setClipdist(4);
                        engine.changespritestat(i, APATROLPOINT);
                        break;
                    case BARREL:
                    case VASEA:
                    case VASEB:
                    case VASEC:
                    case STAINGLASS1:
                    case STAINGLASS2:
                    case STAINGLASS3:
                    case STAINGLASS4:
                    case STAINGLASS5:
                    case STAINGLASS6:
                    case STAINGLASS7:
                    case STAINGLASS8:
                    case STAINGLASS9:
                        spr.setCstat(spr.getCstat() | 0x101);
                        spr.setClipdist(64);
                        break;
                    case 2232: // team flags
                        if (!game.WH2) {
                            break;
                        }
//               netmarkflag(i);
                        break;
                    case 2233: // team flags
                        if (game.WH2) {
                            break;
                        }
                        // XXX netmarkflag(i);
                        break;
                }

                if (isItemSprite(spr)) {
                    Item item = items[(spr.getDetail() & 0xFF) - ITEMSBASE];
                    if (item.sizx != -1 && item.sizy != -1) {
                        spr.setXrepeat((short) item.sizx);
                        spr.setYrepeat((short) item.sizy);
                    }

                    if (item.treasures) {
                        treasurescnt++;
                    }
                    spr.setCstat(spr.getCstat() & ~1);
                    if (item.cflag) {
                        spr.setCstat(spr.getCstat() & ~3);
                    }
                }

            }

            preparesectors(board);

            automapping = 1;

            if (game.WH2) {
                if (mapon == 5) {
                    Sprite spr = board.getSprite(185);
                    if (spr != null && spr.getPicnum() == 172 && spr.getX() == -36864 && spr.getY() == -53504) {
                        engine.deletesprite((short) 185);
                    }
                }

                if (mapon == 13) {
                    Sector sec427 = board.getSector(427);
                    if (sec427 != null && sec427.getFloorpicnum() == 291) {
                        int s = engine.insertsprite((short) 427, (short) 0);
                        if (s != -1) {
                            Sprite sp = board.getSprite(s);
                            if (sp != null) {
                                sp.setX(27136);
                                sp.setY(51712);
                                sp.setZ(7168);
                                sp.setPicnum(WH2PENTAGRAM);
                                sp.setCstat(515);
                                sp.setShade(-3);
                                sp.setXrepeat(64);
                                sp.setYrepeat(64);
                            }
                        }
                    }
                }
            }

            if (justteleported) { // next level
                plr.setHvel(0);
                plr.setSpritenum(engine.insertsprite(plr.getSector(), (short) 0));
                Sprite playerSpr = board.getSprite(plr.getSpritenum());
                plr.setOldsector(plr.getSector());

                if (playerSpr == null) {
                    throw new WarningException("Player sprite is NULL");
                }

                Sector playerSec = board.getSector(plr.getSector());
                if (playerSec == null) {
                    throw new WarningException("Player sector is NULL");
                }

                playerSpr.setX(plr.getX());
                playerSpr.setY(plr.getY());
                playerSpr.setZ(playerSec.getFloorz());
                playerSpr.setCstat(1 + 256);
                playerSpr.setPicnum(game.WH2 ? GRONSW : FRED);
                playerSpr.setShade(0);
                playerSpr.setXrepeat(36);
                playerSpr.setYrepeat(36);
                playerSpr.setAng((short) plr.getAng());
                playerSpr.setXvel(0);
                playerSpr.setYvel(0);
                playerSpr.setZvel(0);
                playerSpr.setOwner((short) (4096 + myconnectindex));
                playerSpr.setLotag(0);
                playerSpr.setHitag(0);
                playerSpr.setPal((short) (game.WH2 ? 10 : 1));
                if (game.WH2) {
                    playerSpr.setClipdist(48);
                }

                engine.setsprite(plr.getSpritenum(), plr.getX(), plr.getY(), plr.getZ() + (getPlayerHeight() << 8));

                warpfxsprite(plr.getSpritenum());
                plr.getTreasure()[TBRASSKEY] = plr.getTreasure()[TBLACKKEY] = plr.getTreasure()[TGLASSKEY] = plr.getTreasure()[TIVORYKEY] = 0;
                plr.getTreasure()[TBLUESCEPTER] = plr.getTreasure()[TYELLOWSCEPTER] = 0;

                // protection from missile
                // anit-missile for level only
                // dont forget to cleanup values
                plr.getTreasure()[TONYXRING] = 0;
                SND_CheckLoops();

                justteleported = false;
            } else {
                initplayersprite(plr);
                if (playerDemoData != null) { // prepare to record
                    plr.setHvel(0);
                    plr.setJumphoriz(0);
                    plr.setCurrweapontics(0);
                    plr.setCurrweaponframe(0);
                    plr.setSpellbooktics(0);
                    plr.setSpellbook(0);
                    nextlevel = false;

                    playerDemoData.applyTo(plr);
                    playerDemoData = null;
                }
                dimension = 3;
                SND_Sound(S_SCARYDUDE);
            }

            if (nextlevel) {
                gAutosaveRequest = true;
                nextlevel = false;
            }

            gDemoScreen.onPrepareboard(screen);

            if (mUserFlag != UserFlag.UserMap) {
                startmusic(mapon - 1);
            }

            return board;
        } catch (Exception e) {
            Console.out.println("Load map exception: " + e);
            if (e.getMessage() != null) {
                game.GameMessage("Load map exception:\n" + e.getMessage());
            } else {
                game.GameMessage("Can't load the map " + fil);
            }
        }
        return null;
    }
}
