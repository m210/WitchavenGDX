package ru.m210projects.Witchaven.Factory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Polymost.Polymost;
import ru.m210projects.Build.Render.Polymost.Polymost2D;
import ru.m210projects.Build.Render.TextureHandle.GLTile;
import ru.m210projects.Build.Render.TextureHandle.TileData;
import ru.m210projects.Build.Render.Types.GL10;
import ru.m210projects.Build.Render.Types.ScreenFade;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.settings.GameConfig;

import static java.lang.Math.*;
import static ru.m210projects.Build.Gameutils.BClipLow;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Render.Types.GL10.GL_TEXTURE0;
import static ru.m210projects.Build.Render.Types.GL10.*;
import static ru.m210projects.Witchaven.Main.game;
import static ru.m210projects.Witchaven.WHFX.*;
import static ru.m210projects.Witchaven.WHScreen.*;

public class WHPolymost extends Polymost implements WHRenderer {

    protected int framew;
    protected int frameh;
    protected GLTile frameTexture;
    private boolean drunk;
    private float drunkIntensive = 1.0f;

    public WHPolymost(GameConfig config) {
        super(config);
        globalfog.setFogScale(64);
    }

    @Override
    public void setPrepareMirror(boolean inpreparemirror) {
        this.inpreparemirror = inpreparemirror;
    }

    @Override
    protected Polymost2D allocOrphoRenderer(Engine engine) {
        return new Polymost2D(this, new WHMapSettings(engine.getBoardService()));
    }

    @Override
    public void drawmasks() {
        super.drawmasks();
        if (drunk) {
            Gdx.gl.glActiveTexture(GL_TEXTURE0);
            boolean hasShader = texshader != null && texshader.isBinded();
            if (hasShader) {
                texshader.unbind();
            }

            if (frameTexture == null || framew != xdim || frameh != ydim) {
                int size;
                for (size = 1; size < Math.max(xdim, ydim); size <<= 1) {
                    ;
                }

                if (frameTexture != null) {
                    frameTexture.dispose();
                } else {
                    frameTexture = textureCache.newTile(TileData.PixelFormat.Rgb, size, size);
                }

                frameTexture.bind();

                gl.glTexImage2D(GL_TEXTURE_2D, 0, GL10.GL_RGB, frameTexture.getWidth(), frameTexture.getHeight(), 0,
                        GL10.GL_RGB, GL_UNSIGNED_BYTE, null);
                frameTexture.unsafeSetFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                framew = xdim;
                frameh = ydim;
            }

            textureCache.bind(frameTexture);
            gl.glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, frameTexture.getWidth(), frameTexture.getHeight());

            gl.glDisable(GL_DEPTH_TEST);
            gl.glDisable(GL_ALPHA_TEST);
            gl.glEnable(GL_TEXTURE_2D);

            gl.glMatrixMode(GL_PROJECTION);
            gl.glPushMatrix();
            gl.glLoadIdentity();

            float tiltang = (drunkIntensive * 360) / 2048f;
            float tilt = min(max(tiltang, -MAXDRUNKANGLE), MAXDRUNKANGLE);

            gl.glScalef(1.05f, 1.05f, 1);
            gl.glRotatef(tilt, 0, 0, 1.0f);

            gl.glMatrixMode(GL_MODELVIEW);
            gl.glPushMatrix();
            gl.glLoadIdentity();

            float u = (float) xdim / frameTexture.getWidth();
            float v = (float) ydim / frameTexture.getHeight();

            gl.glColor4f(1, 1, 1, abs(tilt) / (2 * MAXDRUNKANGLE));
            gl.glBegin(GL10.GL_TRIANGLE_FAN);
            gl.glTexCoord2f(0, 0);
            gl.glVertex2f(-1f, -1f);

            gl.glTexCoord2f(0, v);
            gl.glVertex2f(-1f, 1f);

            gl.glTexCoord2f(u, v);
            gl.glVertex2f(1f, 1f);

            gl.glTexCoord2f(u, 0);
            gl.glVertex2f(1f, -1f);
            gl.glEnd();

            gl.glMatrixMode(GL_MODELVIEW);
            gl.glPopMatrix();
            gl.glMatrixMode(GL_PROJECTION);
            gl.glPopMatrix();

            gl.glEnable(GL_DEPTH_TEST);
            gl.glEnable(GL_ALPHA_TEST);
            gl.glDisable(GL_TEXTURE_2D);

            if (hasShader) {
                texshader.bind();
            }
        }
    }

    public void setdrunk(float intensive) {
        if (intensive == 0) {
            drunk = false;
            drunkIntensive = 0;
        } else {
            drunk = true;
            drunkIntensive = intensive;
        }
    }

    public float getdrunk() {
        return drunkIntensive;
    }

    @Override
    public byte[] getGotPic() {
        return gotpic;
    }

    @Override
    public Color getshadefactor(int shade, int method) {
        if (drawfloormirror) {
            switch (renderingType) {
                case Sprite:
                    shade = 20;
                    break;
                case Wall:
                case MaskWall:
                    int wal = renderingType.getIndex();
                    Wall wall = boardService.getWall(wal);
                    if (wall != null) {
                        int dist = klabs(wall.getX() - globalposx);
                        dist += klabs(wall.getY() - globalposy);

                        shade = BClipLow(dist >> 7, 20);
                    }

                    break;
                default:
                    shade = 30;
                    break;
            }
        }
        return super.getshadefactor(shade, method);
    }

    @Override
    public int animateoffs(int tilenum, int nInfo) {
        return super.animateoffs(tilenum, 0);
    }

    private void renderScreenFade() {
        gl.glBegin(GL_TRIANGLES);
        for (int i = 0; i < 6; i += 2) {
            gl.glVertex2f(vertices[i], vertices[i + 1]);
        }
        gl.glEnd();
    }

    @Override
    public void scrSetDac() {
        if (config.isPaletteEmulation()) {
            WHRenderer.super.scrSetDac();
            return;
        }

        if (game.menu.gShowMenu) {
            return;
        }

        boolean hasShader = beginShowFade();
        for (ScreenFade fade : SCREEN_DAC_ARRAY) {
            showScreenFade(fade);
        }
        endShowFade(hasShader);
    }

    @Override
    public void showScreenFade(ScreenFade screenFade) {
        int intensive = screenFade.getIntensive();
        if (intensive == 0) {
            return;
        }

        if (intensive > 32) {
            intensive = 32;
        }

        int r = 0;
        int g = 0;
        int b = 0;
        int a;
        switch (screenFade.getName()) {
            case BLUE_SCREEN_NAME:
                b = a = (int) Math.min(4.2f * (intensive + (intensive < 10 ? ((float) intensive / 2) : 0)), 255);

                gl.glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, b, a);
                renderScreenFade();

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(0, 0, b, 0);
                renderScreenFade();
                break;
            case GREEN_SCREEN_NAME:
                g = a = Math.min(4 * (intensive + (intensive < 10 ? (intensive / 2) : 0)), 255);

                gl.glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, b, a);
                renderScreenFade();

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(0, g, 0, 0);
                renderScreenFade();
                break;
            case WHITE_SCREEN_NAME:
                g = r = intensive;
                a = Math.min(intensive + 32, 255);

                gl.glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, b, a);
                renderScreenFade();

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, 0, 0);
                renderScreenFade();
                break;
            case RED_SCREEN_NAME:
                r = a = Math.min(4 * (intensive + (intensive < 10 ? (intensive / 2) : 0)), 255);
                gl.glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, b, a);
                renderScreenFade();

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, 0, 0, 0);
                renderScreenFade();
                break;
        }
    }
}
