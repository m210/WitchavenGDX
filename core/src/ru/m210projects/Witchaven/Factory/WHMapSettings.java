package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Render.DefaultMapSettings;
import ru.m210projects.Build.Types.Wall;

import static ru.m210projects.Witchaven.Globals.followmode;
import static ru.m210projects.Witchaven.WHPLR.player;
import static ru.m210projects.Witchaven.WHPLR.pyrn;

public class WHMapSettings extends DefaultMapSettings {


    public WHMapSettings(BoardService boardService) {
        super(boardService);
    }

    @Override
    public int getWallColor(int w, int s) {
        Wall wal = boardService.getWall(w);
        if (wal != null && boardService.isValidSector(wal.getNextsector())) // red wall
        {
            return 252;
        }
        return 24; // white wall
    }

    @Override
    public boolean isSpriteVisible(MapView view, int index) {
        return false;
    }

    @Override
    public int getPlayerPicnum(int player) {
        return -1;
    }

    @Override
    public int getSpriteColor(int s) {
        return 31;
    }

    @Override
    public int getPlayerSprite(int i) {
        return player[i].getSpritenum();
    }

    @Override
    public boolean isScrollMode() {
        return followmode;
    }

    @Override
    public int getViewPlayer() {
        return pyrn;
    }
}
