package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Tables;
import ru.m210projects.Build.Types.DefaultPaletteLoader;
import ru.m210projects.Build.Types.DefaultPaletteManager;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.osd.CommandResponse;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.commands.OsdCommand;
import ru.m210projects.Witchaven.Main;

import static ru.m210projects.Witchaven.Globals.TICSPERFRAME;
import static ru.m210projects.Witchaven.Globals.TIMERRATE;

public class WHEngine extends Engine {

    public static String randomStack = "";

    public WHEngine(BuildGame game) throws Exception {
        super(game);
        inittimer(game.pCfg.isLegacyTimer(), TIMERRATE, TICSPERFRAME);

        Console.out.registerCommand(new OsdCommand("fastdemo", "fastdemo \"demo speed\"") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (argv.length != 1) {
                    return CommandResponse.DESCRIPTION_RESPONSE;
                }

                try {
                    getTimer().setSkipTicks(Math.max(0, Integer.parseInt(argv[0])));
                } catch (Exception e) {
                    return CommandResponse.BAD_ARGUMENT_RESPONSE;
                }
                return CommandResponse.OK_RESPONSE;
            }
        });
    }

    @Override
    public int krand() {
//        boolean showLines = false;
//		StringBuilder sb = new StringBuilder();
//		randomStack += "\r\n";
//		randomStack += "bseed: " + randomseed;
//		for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
//			if (element.getMethodName().equalsIgnoreCase("ProcessFrame")) {
//				break;
//			}
//
//            if (!showLines) {
//                sb.append("\t").append(element.getClassName()).append(".").append(element.getMethodName());
//            } else {
//                sb.append("\t").append(element);
//            }
//
//			sb.append("\r\n");
//		}
//		randomStack += sb.toString();

        return super.krand();
    }

    @Override
    protected BoardService createBoardService() {
        return new WHBoardService();
    }

    @Override
    protected Tables loadtables() throws Exception {
        return new WHTables(game.getCache().getEntry("tables.dat", true));
    }

    @Override
    public PaletteManager loadpalette() throws Exception {
        Entry paletteEntry = game.getCache().getEntry("palette.dat", true);
        return new DefaultPaletteManager(this, Main.game.WH2 ? new DefaultPaletteLoader(paletteEntry) : new WHPaletteLoader(paletteEntry));
    }
}
