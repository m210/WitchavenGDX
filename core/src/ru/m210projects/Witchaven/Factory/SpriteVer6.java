package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

public class SpriteVer6 extends Sprite {

    @Override
    public Sprite readObject(InputStream is) throws IOException {
        setX(StreamUtils.readInt(is));
        setY(StreamUtils.readInt(is));
        setZ(StreamUtils.readInt(is));
        setCstat(StreamUtils.readShort(is));
        setShade(StreamUtils.readUnsignedByte(is));
        setPal(StreamUtils.readUnsignedByte(is));
        setClipdist(StreamUtils.readUnsignedByte(is));
        setXrepeat(StreamUtils.readUnsignedByte(is));
        setYrepeat(StreamUtils.readUnsignedByte(is));
        setXoffset(StreamUtils.readUnsignedByte(is));
        setYoffset(StreamUtils.readUnsignedByte(is));
        setPicnum(StreamUtils.readShort(is));
        setAng(StreamUtils.readShort(is));
        setXvel(StreamUtils.readShort(is));
        setYvel(StreamUtils.readShort(is));
        setZvel(StreamUtils.readShort(is));
        setOwner(StreamUtils.readShort(is));
        setSectnum(StreamUtils.readShort(is));
        setStatnum(StreamUtils.readShort(is));
        setLotag(StreamUtils.readShort(is));
        setHitag(StreamUtils.readShort(is));
        setExtra(StreamUtils.readShort(is));
        setDetail(0);

        return this;
    }
}
