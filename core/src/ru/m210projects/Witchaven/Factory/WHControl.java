package ru.m210projects.Witchaven.Factory;

import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.Pattern.BuildNet.NetInput;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Witchaven.Config.WhKeys;
import ru.m210projects.Witchaven.Main;
import ru.m210projects.Witchaven.Types.Input;

import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHScreen.showmessage;
import static ru.m210projects.Witchaven.WHScreen.zoom;

public class WHControl extends GameProcessor {

    public static final int NORMALKEYMOVE = 15;
    public static final int WH2NORMALKEYMOVE = 20;
    public static final int CtrlJump = 1;
    public static final int CtrlCrouch = 1 << 1;
    public static final int CtrlWeapon_Fire = 1 << 2;
    public static final int CtrlAim_Up = 1 << 3;
    public static final int CtrlAim_Down = 1 << 4;
    public static final int CtrlAim_Center = 1 << 5;
    public static final int CtrlRun = 1 << 6;
    public static final int CtrlCastspell = 1 << 7;
    public static final int CtrlInventory_Use = 1 << 19;
    public static final int CtrlMouseAim = 1 << 20;
    public static final int CtrlOpen = 1 << 21;
    public static final int CtrlTurnAround = 1 << 22;
    public static final int CtrlFlyup = 1 << 23;

    //1 << 8 - 1 << 12 - weapons
    //1 << 12 - 1 << 16 - spells
    //1 << 16 - 1 << 18 - potions
    public static final int CtrlFlydown = 1 << 24;
    public static final int CtrlEndflying = 1 << 25;
    private static final int NORMALTURN = 64;
    private static final int MAXVEL = 201;
    private static final int MAXSVEL = 127;
    private static final int MAXANGVEL = 127;
    public static final int YDIM = 200;
    private int nonsharedtimer;
    private int lPlayerXVel, lPlayerYVel;

    public WHControl(Main game) {
        super(game);
    }

    public void reset() {
        lPlayerXVel = lPlayerYVel = 0;
    }

    @Override
    public void fillInput(NetInput input) {
        mouseDelta.y /= 16.0f;

        Input loc = (Input) input;

        if (Console.out.isShowing()) {
            return;
        }

        loc.reset();
        int svel;
        int vel = svel = 0;
        float angvel;
        float horiz = angvel = 0;

        if (game.pMenu.gShowMenu) {
            return;
        }

        if (isGameKeyJustPressed(GameKeys.Mouse_Aiming)) {
            if (!cfg.isgMouseAim()) {
                cfg.setgMouseAim(true);
                showmessage("Mouse aiming on", 240);
            } else {
                cfg.setgMouseAim(false);
                autohoriz = 1;
                showmessage("Mouse aiming off", 240);
            }
        }

        if (isGameKeyJustPressed(WhKeys.Toggle_Crosshair)) {
            if (!cfg.gCrosshair) {
                cfg.gCrosshair = true;
                showmessage("Crosshair on", 240);
            } else {
                cfg.gCrosshair = false;
                showmessage("Crosshair off", 240);
            }
        }

        if (isGameKeyJustPressed(WhKeys.AutoRun)) {
            if (!cfg.gAutoRun) {
                cfg.gAutoRun = true;
                showmessage("Auto run On", 240);
            } else {
                cfg.gAutoRun = false;
                showmessage("Auto run Off", 240);
            }
        }

        if (isGameKeyJustPressed(WhKeys.See_Coop_View)) {
            if (numplayers > 1) {
                pyrn = connectpoint2[pyrn];
                if (pyrn < 0) {
                    pyrn = connecthead;
                }
            }
        }

        if (isGameKeyJustPressed(GameKeys.Map_Toggle)) {
            if (dimension == 3) {
                dimension = 2;
            } else {
                dimension = 3;
            }
        }

        if (dimension == 2) {
            int j = engine.getTotalClock() - nonsharedtimer;
            nonsharedtimer += j;
            if (isGameKeyPressed(GameKeys.Enlarge_Screen)) {
                zoom += mulscale(j, Math.max(zoom, 256), 6);
            }
            if (isGameKeyPressed(GameKeys.Shrink_Screen)) {
                zoom -= mulscale(j, Math.max(zoom, 256), 6);
            }
            zoom = BClipRange(zoom, 48, 4096);

            if (isGameKeyJustPressed(WhKeys.Map_Follow_Mode)) {
                followmode = !followmode;
                if (followmode) {
                    followx = player[myconnectindex].getX();
                    followy = player[myconnectindex].getY();
                    followa = (int) player[myconnectindex].getAng();
                }
            }
        } else {
            if (isGameKeyJustPressed(GameKeys.Enlarge_Screen) && cfg.gViewSize > 0) {
                cfg.gViewSize--;
            }
            if (isGameKeyJustPressed(GameKeys.Shrink_Screen) && cfg.gViewSize < 1) {
                cfg.gViewSize++;
            }
        }

        loc.bits = isGameKeyPressed(GameKeys.Jump) ? CtrlJump : 0;
        loc.bits |= isGameKeyPressed(GameKeys.Crouch) ? CtrlCrouch : 0;
        loc.bits |= isGameKeyPressed(GameKeys.Weapon_Fire) ? CtrlWeapon_Fire : 0;
        loc.bits |= isGameKeyPressed(WhKeys.Aim_Up) ? CtrlAim_Up : 0;
        loc.bits |= isGameKeyPressed(WhKeys.Aim_Down) ? CtrlAim_Down : 0;
        loc.bits |= isGameKeyJustPressed(WhKeys.Aim_Center) ? CtrlAim_Center : 0;
        loc.bits |= isGameKeyPressed(GameKeys.Run) ? CtrlRun : 0;
        loc.bits |= isGameKeyJustPressed(WhKeys.Cast_spell) ? CtrlCastspell : 0;

        for (int i = 0; i < 10; i++) {
            GameKey weapKey = cfg.getKeymap()[i + cfg.weaponIndex];
            if (isGameKeyJustPressed(weapKey)) {
                loc.bits |= (i + 1) << 8; //1 << 12
            }
        }
        if (isGameKeyJustPressed(GameKeys.Previous_Weapon)) {
            loc.bits |= (11) << 8;
        }
        if (isGameKeyJustPressed(GameKeys.Next_Weapon)) {
            loc.bits |= (12) << 8;
        }

        for (int i = 0; i < 8; i++) {
            if (isGameKeyJustPressed(cfg.getKeymap()[i + cfg.spellIndex])) {
                loc.bits |= (i + 1) << 12;
            }
        }
        if (isGameKeyJustPressed(WhKeys.Spell_prev)) {
            loc.bits |= (9) << 12;
        }
        if (isGameKeyJustPressed(WhKeys.Spell_next)) {
            loc.bits |= (10) << 12; //1 << 16
        }

        if (isGameKeyJustPressed(WhKeys.Health_potion)) {
            loc.bits |= (1) << 16;
        }
        if (isGameKeyJustPressed(WhKeys.Strength_potion)) {
            loc.bits |= (2) << 16; // 1 << 17
        }
        if (isGameKeyJustPressed(WhKeys.Curepoison_potion)) {
            loc.bits |= (3) << 16;
        }
        if (isGameKeyJustPressed(WhKeys.Fireresist_potion)) {
            loc.bits |= (4) << 16; // 1 << 18
        }
        if (isGameKeyJustPressed(WhKeys.Invisibility_potion)) {
            loc.bits |= (5) << 16;
        }
        if (isGameKeyJustPressed(WhKeys.Inventory_Left)) {
            loc.bits |= (6) << 16;
        }
        if (isGameKeyJustPressed(WhKeys.Inventory_Right)) {
            loc.bits |= (7) << 16;
        }
        loc.bits |= isGameKeyJustPressed(WhKeys.Inventory_Use) ? CtrlInventory_Use : 0;

        loc.bits |= cfg.isgMouseAim() ? CtrlMouseAim : 0;
        loc.bits |= isGameKeyJustPressed(GameKeys.Open) ? CtrlOpen : 0;
        loc.bits |= isGameKeyJustPressed(GameKeys.Turn_Around) ? CtrlTurnAround : 0;

        loc.bits |= isGameKeyPressed(WhKeys.Fly_up) ? CtrlFlyup : 0;
        loc.bits |= isGameKeyPressed(WhKeys.Fly_down) ? CtrlFlydown : 0;
        loc.bits |= isGameKeyJustPressed(WhKeys.End_flying) ? CtrlEndflying : 0;

        boolean running = ((!cfg.gAutoRun && (loc.bits & CtrlRun) != 0) || ((loc.bits & CtrlRun) == 0 && cfg.gAutoRun));
        int keymove, turnamount;

        if (running) {
            turnamount = NORMALTURN << 1;
            keymove = Main.game.WH2 ? (WH2NORMALKEYMOVE << 1) : (NORMALKEYMOVE << 1);
        } else {
            turnamount = NORMALTURN;
            keymove = Main.game.WH2 ? WH2NORMALKEYMOVE : NORMALKEYMOVE;
        }

        boolean left = isGameKeyPressed(GameKeys.Turn_Left);
        boolean right = isGameKeyPressed(GameKeys.Turn_Right);

        if (cfg.isgMouseAim()) {
            horiz = BClipRange(horiz - ctrlGetMouseLook(cfg.isgInvertmouse()), -(YDIM >> 1), 100 + (YDIM >> 1));
        } else {
            vel = BClipRange(vel - (int) ctrlGetMouseMove(), -MAXVEL, MAXVEL);
        }

        if (!isGameKeyPressed(GameKeys.Strafe)) {
            if (left) {
                angvel = (int) BClipLow(angvel - turnamount, -MAXANGVEL);
            }
            if (right) {
                angvel = (int) BClipHigh(angvel + turnamount, MAXANGVEL);
            }
            angvel = BClipRange(angvel + ctrlGetMouseTurn(), -1024, 1024);
        } else {
            if (left) {
                svel = BClipHigh(svel + keymove, MAXSVEL);
            }
            if (right) {
                svel = BClipLow(svel - keymove, -MAXSVEL);
            }
            svel = BClipRange(svel - (int) ctrlGetMouseStrafe(), -MAXSVEL, MAXSVEL);
        }

        Vector2 stick1 = ctrlGetStick(JoyStick.LOOKING);
        Vector2 stick2 = ctrlGetStick(JoyStick.MOVING);

        float lookx = stick1.x;
        float looky = stick1.y;

        if (looky != 0) {
            float k = 4.0f;
            Renderer renderer = game.getRenderer();
            int ydim = renderer.getHeight();

            horiz = BClipRange(horiz - k * looky * cfg.getJoyLookSpeed(), -(ydim >> 1), 100 + (ydim >> 1));
        }

        if (lookx != 0) {
            float k = 80.0f;
            angvel = (short) BClipRange(angvel + k * lookx * cfg.getJoyTurnSpeed(), -1024, 1024);
        }

        if (stick2.y != 0) {
            vel = (short) BClipRange(vel - ((float) keymove * stick2.y), -keymove, keymove);
        }
        if (stick2.x != 0) {
            svel = (short) BClipRange(svel - ((float) keymove * stick2.x), -keymove, keymove);
        }

        if (isGameKeyPressed(GameKeys.Move_Forward)) {
            vel = BClipHigh(vel + keymove, MAXVEL);
        }
        if (isGameKeyPressed(GameKeys.Move_Backward)) {
            vel = BClipLow(vel - keymove, -MAXVEL);
        }
        if (isGameKeyPressed(GameKeys.Strafe_Left)) {
            svel = BClipHigh(svel + keymove, MAXSVEL);
        }
        if (isGameKeyPressed(GameKeys.Strafe_Right)) {
            svel = BClipLow(svel - keymove, -MAXSVEL);
        }

        float k = 0.92f;
        float angle = BClampAngle(player[myconnectindex].getAng());

        double xvel = (vel * BCosAngle(angle) + svel * BSinAngle(angle));
        double yvel = (vel * BSinAngle(angle) - svel * BCosAngle(angle));

        double len = Math.sqrt(xvel * xvel + yvel * yvel);
        if (len > (keymove << 14)) {
            xvel = (xvel / len) * (keymove << 14);
            yvel = (yvel / len) * (keymove << 14);
        }

        lPlayerXVel += (int) (xvel * k);
        lPlayerYVel += (int) (yvel * k);

        lPlayerXVel = mulscale(lPlayerXVel, 0xD000, 16);
        lPlayerYVel = mulscale(lPlayerYVel, 0xD000, 16);

        if (klabs(lPlayerXVel) < 2048 && klabs(lPlayerYVel) < 2048) {
            lPlayerXVel = lPlayerYVel = 0;
        }

        if (followmode && dimension == 2) {
            followvel = vel;
            followsvel = svel;
            followang = angvel;

            loc.fvel = 0;
            loc.svel = 0;
            loc.angvel = 0;
            loc.horiz = 0;
            return;
        }

        loc.fvel = lPlayerXVel; //vel;
        loc.svel = lPlayerYVel; //svel;
        loc.angvel = angvel;
        loc.horiz = horiz;
    }

}
