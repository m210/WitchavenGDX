package ru.m210projects.Witchaven.Factory;

import org.jetbrains.annotations.NotNull;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.osd.Console;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static ru.m210projects.Build.Engine.MAXPALOOKUPS;

public class WHPaletteLoader implements PaletteManager.PaletteLoader {

    private final byte[] basePalette;
    private final int shadesCount;
    private final byte[][] palookup;
    private final byte[] transluc;

    public WHPaletteLoader(@NotNull Entry entry) throws IOException {
        if (!entry.exists()) {
            throw new FileNotFoundException("Failed to load \"palette.dat\"!");
        }

        boolean hastransluc = false;
        int numshades = (short) ((entry.getSize() - 768) >> 7);
        if ((numshades & 1) <= 0) {
            numshades >>= 1;
        } else {
            numshades = (short) ((numshades - 255) >> 1);
            hastransluc = true;
        }

        this.palookup = new byte[MAXPALOOKUPS][];
        this.transluc = new byte[65536];
        this.shadesCount = numshades;

        try (InputStream is = entry.getInputStream()) {
            System.out.println("Loading palettes");
            basePalette = StreamUtils.readBytes(is, 768);

            Console.out.println("Loading palookup tables");
            this.palookup[0] = StreamUtils.readBytes(is, numshades << 8);

            if (hastransluc) {
                Console.out.println("Loading translucency table");
                byte[] tmp = new byte[256];
                for (int i = 0; i < 255; i++) {
                    StreamUtils.readBytes(is, tmp, 255 - i);
                    System.arraycopy(tmp, 0, transluc, (i << 8) + i + 1, 255 - i);
                    for (int j = i + 1; j < 256; j++) {
                        transluc[(j << 8) + i] = transluc[(i << 8) + j];
                    }
                }

                for (int i = 0; i < 256; i++) {
                    transluc[(i << 8) + i] = (byte) i;
                }
            }
        }
    }

    @Override
    public byte[] getBasePalette() {
        return basePalette;
    }

    @Override
    public int getShadeCount() {
        return shadesCount;
    }

    @Override
    public byte[][] getPalookup() {
        return palookup;
    }

    @Override
    public byte[] getTransluc() {
        return transluc;
    }
}
