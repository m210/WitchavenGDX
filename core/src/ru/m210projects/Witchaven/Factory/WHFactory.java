package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.FontHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.SliderDrawable;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Renderer.RenderType;
import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.OsdFunc;
import ru.m210projects.Witchaven.Fonts.*;
import ru.m210projects.Witchaven.Main;

import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.Main.game;
import static ru.m210projects.Witchaven.Names.MAINMENU;

public class WHFactory extends BuildFactory {

    private final Main app;

    public WHFactory(Main app) {
        super("stuff.dat");
        this.app = app;

        OsdColor.DEFAULT.setPal(30);
    }

    @Override
    public void drawInitScreen() {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, MAINMENU, 0, 0, 2 | 8);
    }

    @Override
    public Engine engine() throws Exception {
        return engine = new WHEngine(app);
    }

    @Override
    public Renderer renderer(RenderType type) {
        if (type == RenderType.Software) {
            return new WHSoftware(app.pCfg);
        } else if (type == RenderType.PolyGDX) {
            return new WHPolyGDX(app.pCfg);
        } else {
            return new WHPolymost(app.pCfg);
        }
    }

    @Override
    public DefScript getBaseDef(Engine engine) {
        return new DefScript(engine);
    }

    @Override
    public OsdFunc getOsdFunc() {
        return new WHOSDFunc();
    }

    @Override
    public MenuHandler menus() {
        return app.menu = (game.WH2 ? new WH2MenuHandler(app) : new WHMenuHandler(app));
    }

    @Override
    public FontHandler fonts() {
        return new FontHandler(6) {
            @Override
            protected Font init(int i) {
                if (i == 0) {
                    if (game.WH2) {
                        return new WH2Fonts();
                    }
                    return new MenuFont(app.pEngine);
                }
                if (i == 1) {
                    return new TheFont();
                }
                if (i == 2) {
                    return new HealthFont();
                }
                if (i == 3) {
                    return new PotionFont();
                }
                if (i == 4) {
                    return new ScoreFont();
                }

                return EngineUtils.getLargeFont();
            }
        };
    }

    @Override
    public BuildNet net() {
        return new WHNetwork(app);
    }

    @Override
    public SliderDrawable slider() {
        return new WHSliderDrawable();
    }

}
