package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.SpriteMap;
import ru.m210projects.Build.Types.collections.ValueSetter;

import java.util.List;

public class WHBoardService extends BoardService {

    public WHBoardService() {
        registerBoard(6, SectorVer6.class, WallVer6.class, SpriteVer6.class);
    }

    @Override
    protected SpriteMap createSpriteMap(int listCount, List<Sprite> spriteList, int spriteCount, ValueSetter<Sprite> valueSetter) {
        return new SpriteMap(listCount, spriteList, spriteCount, valueSetter) {
            @Override
            protected Sprite getInstance() {
                return new SpriteVer6();
            }
        };
    }

    @Override
    public int insertsprite(int sectnum, int statnum) {
        int j = super.insertsprite(sectnum, statnum);
        Sprite spr = board.getSprite(j);
        if (spr != null) {
            spr.setDetail(0);
        }
        return j;
    }

    @Override
    public boolean deletesprite(int spritenum) {
        boolean deleted = super.deletesprite(spritenum);
        if (deleted) {
            Sprite spr = board.getSprite(spritenum);
            if (spr != null) {
                spr.setDetail(0);
            }
        }
        return deleted;
    }
}
