package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

public class WallVer6 extends Wall {

    @Override
    public Wall readObject(InputStream is) throws IOException {
        setX(StreamUtils.readInt(is));
        setY(StreamUtils.readInt(is));
        setPoint2(StreamUtils.readShort(is));
        setNextsector(StreamUtils.readShort(is));
        setNextwall(StreamUtils.readShort(is));
        setPicnum(StreamUtils.readShort(is));
        setOverpicnum(StreamUtils.readShort(is));
        setShade(StreamUtils.readUnsignedByte(is));
        setPal(StreamUtils.readUnsignedByte(is));
        setCstat(StreamUtils.readShort(is));
        setXrepeat(StreamUtils.readUnsignedByte(is));
        setYrepeat(StreamUtils.readUnsignedByte(is));
        setXpanning(StreamUtils.readUnsignedByte(is));
        setYpanning(StreamUtils.readUnsignedByte(is));
        setLotag(StreamUtils.readShort(is));
        setHitag(StreamUtils.readShort(is));
        setExtra(StreamUtils.readShort(is));
        return this;
    }
}
