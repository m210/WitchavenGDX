package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.PaletteManager;

import static ru.m210projects.Build.Gameutils.BClipHigh;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.WHFX.*;
import static ru.m210projects.Witchaven.WHScreen.*;

public interface WHRenderer extends Renderer {

    void setdrunk(float intensive);

    float getdrunk();

    byte[] getGotPic();

    default void scrSetDac() {
        int red = 0, white = 0, green = 0, blue = 0;

        if (WHITE_DAC.getIntensive() != 0) {
            white = BClipHigh(WHITE_DAC.getIntensive() / WHITETICS + 1, NUMWHITESHIFTS);
        }

        if (RED_DAC.getIntensive() != 0) {
            red = BClipHigh(RED_DAC.getIntensive() / 10 + 1, NUMREDSHIFTS);
        }

        if (GREEN_DAC.getIntensive() != 0) {
            green = BClipHigh(GREEN_DAC.getIntensive() / 10 + 1, NUMGREENSHIFTS);
        }

        if (BLUE_DAC.getIntensive() != 0) {
            blue = BClipHigh(BLUE_DAC.getIntensive() / 10 + 1, NUMBLUESHIFTS);
        }

        PaletteManager manager = engine.getPaletteManager();
        if (red != 0) {
            manager.changePalette(redshifts[red - 1]);
            palshifted = true;
        } else if (white != 0) {
            manager.changePalette(whiteshifts[white - 1]);
            palshifted = true;
        } else if (green != 0) {
            manager.changePalette(greenshifts[green - 1]);
            palshifted = true;
        } else if (blue != 0) {
            manager.changePalette(blueshifts[blue - 1]);
            palshifted = true;
        } else if (palshifted) {
            engine.setbrightness(manager.getPaletteGamma(), manager.getBasePalette());
            palshifted = false;
        }
    }

    void setPrepareMirror(boolean inpreparemirror);
}
