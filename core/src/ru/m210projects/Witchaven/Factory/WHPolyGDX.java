package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.GdxRender.GDXOrtho;
import ru.m210projects.Build.Render.GdxRender.GDXRenderer;
import ru.m210projects.Build.settings.GameConfig;

public class WHPolyGDX extends GDXRenderer implements WHRenderer {

    public WHPolyGDX(GameConfig config) {
        super(config);
    }

    @Override
    public void setPrepareMirror(boolean inpreparemirror) {
        this.inpreparemirror = inpreparemirror;
    }

    @Override
    protected GDXOrtho allocOrphoRenderer(Engine engine) {
        return new GDXOrtho(this, new WHMapSettings(engine.getBoardService()));
    }

    @Override
    public void setdrunk(float intensive) {
        // TODO
    }

    @Override
    public float getdrunk() {
        return 0; // TODO
    } @Override

    public int animateoffs(int tilenum, int nInfo) {
        return super.animateoffs(tilenum, 0);
    }

    @Override
    public byte[] getGotPic() {
        return gotpic;
    }

//    @Override
//    public void scrSetDac() {
//        if (config.isPaletteEmulation()) {
//            WHRenderer.super.scrSetDac();
//            return;
//        }
//    }
}

