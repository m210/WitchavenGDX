package ru.m210projects.Witchaven.Factory;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.ScreenAdapters.InitScreen;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.DefaultOsdFunc;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Witchaven.Globals.WH2LOGO;
import static ru.m210projects.Witchaven.Globals.WHLOGO;
import static ru.m210projects.Witchaven.Main.engine;
import static ru.m210projects.Witchaven.Main.game;
import static ru.m210projects.Witchaven.Names.BACKGROUND;

public class WHOSDFunc extends DefaultOsdFunc {

    public WHOSDFunc() {
        super(game.getRenderer());

        BGTILE = BACKGROUND;
        BGCTILE = game.WH2 ? WH2LOGO : WHLOGO; //game.WH2 ? 500 : 679;
        BORDTILE = BACKGROUND;

        BITSTH = 1 + 8 + 16;

        OsdColor.WHITE.setPal(30);
        OsdColor.RED.setPal(170);
        OsdColor.BLUE.setPal(233);
        OsdColor.YELLOW.setPal(143);
        OsdColor.BROWN.setPal(40);
        OsdColor.GREEN.setPal(125);
    }

    @Override
    protected Font getFont() {
        return EngineUtils.getLargeFont();
    }

    @Override
    public void showOsd(boolean captured) {
        super.showOsd(captured);
        if (!(game.getScreen() instanceof InitScreen) && !game.pMenu.gShowMenu) {
            Gdx.input.setCursorCatched(!captured);
        }
    }

    @Override
    public void drawlogo(int daydim) {
        ArtEntry pic = engine.getTile(BGCTILE);
        if (pic.exists()) {
            int xsiz = pic.getWidth() / 2;
            int ysiz = pic.getHeight() / 2;

            if (xsiz > 0 && ysiz > 0) {
                int xdim = renderer.getWidth();

                renderer.rotatesprite((xdim - xsiz) << 15, (daydim - ysiz) << 16, 32768, 0, BGCTILE, SHADE - 32, PALETTE, BITSTL, 0, 0, xdim, daydim);
            }
        }
    }
}
