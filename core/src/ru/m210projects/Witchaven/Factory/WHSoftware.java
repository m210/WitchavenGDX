package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Software.Software;
import ru.m210projects.Build.Render.Software.SoftwareOrpho;
import ru.m210projects.Build.Tables;
import ru.m210projects.Build.settings.GameConfig;

import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Witchaven.Globals.lockclock;

public class WHSoftware extends Software implements WHRenderer {

    public WHSoftware(GameConfig config) {
        super(config);
    }

    @Override
    protected SoftwareOrpho allocOrphoRenderer(Engine engine) {
        return new SoftwareOrpho(this, new WHMapSettings(engine.getBoardService()));
    }

    @Override
    public void dosetaspect() {
        int i, j, k, x, xinc;

        if (xyaspect != oxyaspect) {
            oxyaspect = xyaspect;
            j = xyaspect * 320;
            lookups[horizlookup2 + horizycent - 1] = divscale(131072, j, 26);
            for (i = ydim * 4 - 1; i >= 0; i--) {
                if (i != (horizycent - 1)) {
                    lookups[i] = divscale(1, i - (horizycent - 1), 28);
                    lookups[horizlookup2 + i] = divscale(klabs(lookups[i]), j, 14);
                }
            }
        }
        if ((xdimen != oxdimen) || (viewingrange != oviewingrange)) {
            oxdimen = xdimen;
            oviewingrange = viewingrange;

            x = 0;
            xinc = xdimscale;
            Tables tables = EngineUtils.getTables();
            for (i = 0; i < xdimen; i++) {
                j = x & 65535;
                k = x >> 16;
                x += xinc;
                if (k < 0 || k >= 1279) {
                    break;
                }

                if (j != 0) {
                    j = mulscale(tables.getRadarAng(k + 1) - tables.getRadarAng(k), j, 16);
                }
                radarang2[i] = (short) ((tables.getRadarAng(k) + j) >> 6);
            }

            for (i = 1; i < 65536; i++) {
                distrecip[i] = divscale(xdimen, i, 20);
            }
            nytooclose = xdimen * 2100;
            nytoofar = 65536 * 16384 - 1048576;
        }
    }

    public void TempHorizon(int horiz) {
        long l = scale(horiz - 100, xdim, 320) + (ydim >> 1);
        for (int y1 = 0, y2 = ydim - 1; y1 < y2; y1++, y2--) {
            int ptr = ylookup[y1];
            int ptr2 = ylookup[y2];
            int ptr3 = (int) (Math.min(klabs(y1 - l) >> 2, 31) << 8);
            int ptr4 = (int) (Math.min(klabs(y2 - l) >> 2, 31) << 8);

            int j = EngineUtils.sin(((y2 + lockclock) << 7) & 2047);
            j += EngineUtils.sin(((y2 - lockclock) << 8) & 2047);

            j >>= 14;
            ptr2 += j;

            byte[] frameplace = a.getframeplace();
            for (int x1 = 0; x1 < xdim - j; x1++) {
                byte ch = frameplace[ptr + x1];
                frameplace[ptr + x1] = paletteManager.getPalookupBuffer()[0][(frameplace[ptr2 + x1] & 0xFF) + ptr3];
                frameplace[ptr2 + x1] = paletteManager.getPalookupBuffer()[0][(ch & 0xFF) + ptr4];
            }
            a.setframeplace(frameplace);
        }
    }

    @Override
    public void setdrunk(float intensive) {
    }

    @Override
    public float getdrunk() {
        return 0;
    }

    @Override
    public int animateoffs(int tilenum, int nInfo) {
        return super.animateoffs(tilenum, 0);
    }

    @Override
    public byte[] getGotPic() {
        return gotpic;
    }

    @Override
    public void setPrepareMirror(boolean inpreparemirror) {
        this.inpreparemirror = inpreparemirror;
    }
}
