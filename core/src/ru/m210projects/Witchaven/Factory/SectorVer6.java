package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class SectorVer6 extends Sector {

    @Override
    public Sector readObject(InputStream is) throws IOException {
        this.setWallptr(StreamUtils.readShort(is));
        this.setWallnum(StreamUtils.readShort(is));
        this.setCeilingpicnum(StreamUtils.readShort(is));
        this.setFloorpicnum(StreamUtils.readShort(is));
        int ceilingHeinum = StreamUtils.readShort(is);
        int floorHeinum = StreamUtils.readShort(is);
        this.setCeilingz(StreamUtils.readInt(is));
        this.setFloorz(StreamUtils.readInt(is));
        this.setCeilingshade(StreamUtils.readUnsignedByte(is));
        this.setFloorshade(StreamUtils.readUnsignedByte(is));
        this.setCeilingxpanning(StreamUtils.readUnsignedByte(is));
        this.setFloorxpanning(StreamUtils.readUnsignedByte(is));
        this.setCeilingypanning(StreamUtils.readUnsignedByte(is));
        this.setFloorypanning(StreamUtils.readUnsignedByte(is));
        int ceilingstat = StreamUtils.readUnsignedByte(is);
        if ((ceilingstat & 2) == 0) {
            ceilingHeinum = 0;
        } else {
            ceilingHeinum = max(min(ceilingHeinum << 5, Short.MAX_VALUE), Short.MIN_VALUE);
        }
        this.setCeilingheinum(ceilingHeinum);
        this.setCeilingstat(ceilingstat);
        int floorstat = StreamUtils.readUnsignedByte(is);
        if ((floorstat & 2) == 0) {
            floorHeinum = 0;
        } else {
            floorHeinum = max(min(floorHeinum << 5, Short.MAX_VALUE), Short.MIN_VALUE);
        }
        this.setFloorheinum(floorHeinum);
        this.setFloorstat(floorstat);
        this.setCeilingpal(StreamUtils.readUnsignedByte(is));
        this.setFloorpal(StreamUtils.readUnsignedByte(is));
        this.setVisibility(StreamUtils.readUnsignedByte(is));
        this.setLotag(StreamUtils.readShort(is));
        this.setHitag(StreamUtils.readShort(is));
        this.setExtra(StreamUtils.readShort(is));

        return this;
    }
}
