package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.Tables;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.scale;

public class WHTables extends Tables {

    public WHTables(Entry entry) throws IOException {
        super(entry);
    }

    @Override
    protected void read(Entry entry) throws IOException {
        if(!entry.exists()) {
            throw new FileNotFoundException("Failed to load \"tables.dat\"!");
        }

        try(InputStream is = entry.getInputStream()) {
            ByteBuffer.wrap(StreamUtils.readBytes(is, sintable.length * 2)).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(sintable);
            StreamUtils.skip(is, 4096); // tantable
            ByteBuffer.wrap(StreamUtils.readBytes(is, 640)).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(radarang, 0,  320);
            radarang[320] = 0x4000;
            StreamUtils.readBytes(is, textfont, 1024);
            StreamUtils.readBytes(is, smalltextfont, 1024);
        }
    }

    @Override
    public int getAngle(int xvect, int yvect) {
        if ((xvect | yvect) == 0) {
            return (0);
        }
        if (xvect == 0) {
            return (short) (512 + ((yvect < 0 ? 1 : 0) << 10));
        }
        if (yvect == 0) {
            return (short) ((xvect < 0 ? 1 : 0) << 10);
        }
        if (xvect == yvect) {
            return (short) (256 + ((xvect < 0 ? 1 : 0) << 10));
        }
        if (xvect == -yvect) {
            return (short) (768 + ((xvect > 0 ? 1 : 0) << 10));
        }

        if (klabs(xvect) > klabs(yvect)) {
            return (short) (((radarang[160 + scale(160, yvect, xvect)] >> 6) + ((xvect < 0 ? 1 : 0) << 10)) & 2047);
        }
        return (short) (((radarang[160 - scale(160, xvect, yvect)] >> 6) + 512 + ((yvect < 0 ? 1 : 0) << 10)) & 2047);
    }
}
