package ru.m210projects.Witchaven.Factory;

import ru.m210projects.Build.Render.DummyRenderer;

public class WHDummyRenderer extends DummyRenderer implements WHRenderer {

    @Override
    public void setPrepareMirror(boolean inpreparemirror) {
    }

    @Override
    public void setdrunk(float intensive) {
    }

    @Override
    public float getdrunk() {
        return 0;
    }

    @Override
    public byte[] getGotPic() {
        return new byte[0];
    }

    @Override
    public void scrSetDac() {

    }
}
