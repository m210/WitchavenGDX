package ru.m210projects.Witchaven;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Point;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Witchaven.Types.Delayitem;
import ru.m210projects.Witchaven.Types.PLAYER;
import ru.m210projects.Witchaven.Types.SWINGDOOR;

import static ru.m210projects.Build.Engine.MAXSECTORS;
import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Witchaven.Animate.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Types.ANIMATION.*;
import static ru.m210projects.Witchaven.WH1Names.GOBLIN;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHFX.shards;
import static ru.m210projects.Witchaven.WHOBJ.newstatus;
import static ru.m210projects.Witchaven.WHOBJ.trowajavlin;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.showmessage;

public class WHTAG {

    public static final int[] opwallfind = new int[2];
    public static int delaycnt;
    public static final Delayitem[] delayitem = new Delayitem[MAXSECTORS];
    public static final short[] warpsectorlist = new short[64];
    public static short warpsectorcnt;
    public static final short[] xpanningsectorlist = new short[16];
    public static short xpanningsectorcnt;
    public static final short[] ypanningwalllist = new short[128];
    public static short ypanningwallcnt;
    public static final short[] floorpanninglist = new short[64];
    public static short floorpanningcnt;
    public static final SWINGDOOR[] swingdoor = new SWINGDOOR[MAXSWINGDOORS];
    public static short swingcnt;
    public static final short[] dragsectorlist = new short[16];
    public static final short[] dragxdir = new short[16];
    public static final short[] dragydir = new short[16];
    public static short dragsectorcnt;
    public static final int[] dragx1 = new int[16];
    public static final int[] dragy1 = new int[16];
    public static final int[] dragx2 = new int[16];
    public static final int[] dragy2 = new int[16];
    public static final int[] dragfloorz = new int[16];
    static final short[] ironbarsector = new short[16];
    static short ironbarscnt;
    static final int[] ironbarsgoal1 = new int[16];
    static final int[] ironbarsgoal2 = new int[16];
    static final short[] ironbarsdone = new short[16];
    static final short[] ironbarsanim = new short[16];
    static final int[] ironbarsgoal = new int[16];
    private static int d_soundplayed = 0;

    public static void operatesprite(PLAYER plr, final int s) {
        Sprite spr = boardService.getSprite(s);
        if (spr == null) {
            return;
        }

        if (spr.getPicnum() == SPAWNFIREBALL) {
            newstatus(s, DEVILFIRE);
        }
        if (spr.getPicnum() == SPAWNJAVLIN) {
            trowajavlin(s);
        }

        switch (spr.getPicnum()) {
            case STONEGONZOCHM:
            case STONEGONZOGSH:
            case STONEGRONDOVAL:
            case STONEGONZOBSW:
                spr.setLotag(spr.getLotag() * 120);
                engine.changespritestat(s, STONETOFLESH);
                break;
            case GONZOHMJUMP:
            case GONZOSHJUMP:
                newstatus(s, AMBUSH);
                break;
            case STAINGLASS1:
            case STAINGLASS2:
            case STAINGLASS3:
            case STAINSKULL:
            case STAINHEAD:
            case STAINSNAKE:
            case STAINCIRCLE:
            case STAINQ:
            case STAINSCENE:
                if (spr.getLotag() == 2) {
                    playsound_loc(S_GLASSBREAK1 + (engine.rand() % 3), spr.getX(), spr.getY());
                    for (int j = 0; j < 20; j++) {
                        shards(s, 2);
                    }
                    engine.deletesprite(s);
                }
                break;
        }

        if ((spr.getLotag() == 1800 || spr.getLotag() == 1810 || spr.getLotag() == 1820)
                && spr.getSectnum() == plr.getSector()) {
            for (int j = 0; j < MAXSPRITES; j++) {
                Sprite spr2 = boardService.getSprite(j);
                if (spr2 != null && spr.getSectnum() == spr2.getSectnum() && (spr2.getLotag() >= 1800 && spr2.getLotag() <= 1899)) {
                    newstatus(j, LIFTDN);
                }
            }
        }
        if ((spr.getLotag() == 1801 || spr.getLotag() == 1811 || spr.getLotag() == 1821)
                && spr.getSectnum() == plr.getSector()) {
            for (int j = 0; j < MAXSPRITES; j++) {
                Sprite spr2 = boardService.getSprite(j);
                if (spr2 != null && spr.getSectnum() == spr2.getSectnum() && (spr2.getLotag() >= 1800 && spr2.getLotag() <= 1899)) {
                    newstatus(j, LIFTUP);
                }
            }
        }
    }

    public static void operatesector(PLAYER plr, final int s) {
        final Sector sector = boardService.getSector(s);
        if (sector == null) {
            return;
        }

        int keysok = 0;
        int datag = sector.getLotag();
        int startwall = sector.getWallptr();
        int endwall = startwall + sector.getWallnum() - 1;
        int centx = 0, centy = 0;

        for (ListNode<Wall> wn = sector.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wal = wn.get();
            centx += wal.getX();
            centy += wal.getY();
        }
        centx /= (endwall - startwall + 1);
        centy /= (endwall - startwall + 1);

        switch (datag) {
            case 61:
            case 131:
            case 71:
                // check for proper key
                if (plr.getTreasure()[TBRASSKEY] == 0) {
                    showmessage("BRASS KEY NEEDED", 360);
                } else {
                    keysok = 1;
                }
                break;
            case 62:
            case 132:
                // check for proper key
                if (plr.getTreasure()[TBLACKKEY] == 0) {
                    showmessage("Black KEY NEEDED", 360);
                } else {
                    keysok = 1;
                }
                break;
            case 63:
            case 133:
            case 73:
                // check for proper key
                if (plr.getTreasure()[TGLASSKEY] == 0) {
                    showmessage("glass KEY NEEDED", 360);
                } else {
                    keysok = 1;
                }
                break;
            case 64:
            case 134:
                if (plr.getTreasure()[TIVORYKEY] == 0) {
                    showmessage("ivory KEY NEEDED", 360);
                } else {
                    keysok = 1;
                }
                break;
            case 72:
                // check for proper key
                if (plr.getTreasure()[TBLACKKEY] == 0) {
                    showmessage("black KEY NEEDED", 360);
                } else {
                    keysok = 1;
                }
                break;
            case 74:
                if (plr.getTreasure()[TIVORYKEY] == 0) {
                    showmessage("Ivory KEY NEEDED", 360);
                } else {
                    keysok = 1;
                }
                break;
        }

        switch (datag) {
            case DOORBOX:
                System.out.println("DOORBOX");
                opwallfind[0] = -1;
                opwallfind[1] = -1;
                for (ListNode<Wall> wn = sector.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    if (wal.getLotag() == 6) {
                        if (opwallfind[0] == -1) {
                            opwallfind[0] = wn.getIndex();
                        } else {
                            opwallfind[1] = wn.getIndex();
                        }
                    }
                }

                for (int j = 0; j < 2; j++) {
                    Wall opwall = boardService.getWall(opwallfind[j]);
                    if (opwall == null) {
                        break;
                    }

                    if ((((opwall.getX() + opwall.getWall2().getX()) >> 1) == centx)
                            && (((opwall.getY() + opwall.getWall2().getY()) >> 1) == centy)) {
                        int i = opwallfind[j] - 1;
                        if (i < startwall) {
                            i = endwall;
                        }
                        Wall w = boardService.getWall(i);
                        if (w != null) {
                            int dax2 = w.getX() - opwall.getX();
                            int day2 = w.getY() - opwall.getY();
                            if (dax2 != 0) {
                                dax2 = opwall.getWall2().getWall2().getWall2().getX();
                                dax2 -= opwall.getWall2().getWall2().getX();
                                setanimation(opwallfind[j], opwall.getX() + dax2, 4, 0, WALLX);
                                setanimation(i, w.getX() + dax2, 4, 0, WALLX);
                                setanimation(opwall.getPoint2(), opwall.getWall2().getX() + dax2, 4, 0,
                                        WALLX);
                                setanimation(opwall.getWall2().getPoint2(),
                                        opwall.getWall2().getWall2().getX() + dax2, 4, 0, WALLX);
                            } else if (day2 != 0) {
                                day2 = opwall.getWall2().getWall2().getWall2().getY();
                                day2 -= opwall.getWall2().getWall2().getY();
                                setanimation(opwallfind[j], opwall.getY() + day2, 4, 0, WALLY);
                                setanimation(i, w.getY() + day2, 4, 0, WALLY);
                                setanimation(opwall.getPoint2(), opwall.getWall2().getY() + day2, 4, 0,
                                        WALLY);
                                setanimation(opwall.getWall2().getPoint2(),
                                        opwall.getWall2().getWall2().getY() + day2, 4, 0, WALLY);
                            }
                        }
                    } else {
                        int i = opwallfind[j] - 1;
                        if (i < startwall) {
                            i = endwall;
                        }
                        Wall w = boardService.getWall(i);
                        if (w != null) {
                            int dax2 = w.getX() - opwall.getX();
                            int day2 = w.getY() - opwall.getY();
                            if (dax2 != 0) {
                                setanimation(opwallfind[j], centx, 4, 0, WALLX);
                                setanimation(i, centx + dax2, 4, 0, WALLX);
                                setanimation(opwall.getPoint2(), centx, 4, 0, WALLX);
                                setanimation(opwall.getWall2().getPoint2(), centx + dax2, 4, 0, WALLX);
                            } else if (day2 != 0) {
                                setanimation(opwallfind[j], centy, 4, 0, WALLY);
                                setanimation(i, centy + day2, 4, 0, WALLY);
                                setanimation(opwall.getPoint2(), centy, 4, 0, WALLY);
                                setanimation(opwall.getWall2().getPoint2(), centy + day2, 4, 0, WALLY);
                            }
                        }
                    }
                }

                SND_Sound(S_DOOR2);

                break;
            case DOORUPTAG: {// a door that opens up
                int i = getanimationgoal(sector, 2);
                if (i >= 0) {
                    Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                    if (nextsec != null) {
                        int goalz = nextsec.getCeilingz();
                        if (gAnimationData[i].goal == goalz) {
                            gAnimationData[i].goal = sector.getFloorz();
                        } else {
                            gAnimationData[i].goal = goalz;
                        }
                    }
                } else {
                    int goalz = sector.getFloorz();
                    if (sector.getCeilingz() == sector.getFloorz()) {
                        Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                        if (nextsec != null) {
                            goalz = nextsec.getCeilingz();
                        } else {
                            goalz = sector.getFloorz();
                        }
                    }
                    setanimation(s, goalz, DOORSPEED, 0, CEILZ);
                }
                SND_Sound(S_DOOR2);
                break;
            }
            case DOORDOWNTAG: { // a door that opens down
                int i = getanimationgoal(sector, 1);
                if (i >= 0) {
                    Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz(), 1, 1));
                    if (nextsec != null) {
                        int goalz = nextsec.getFloorz();
                        if (gAnimationData[i].goal == goalz) {
                            gAnimationData[i].goal = sector.getCeilingz();
                        } else {
                            gAnimationData[i].goal = goalz;
                        }
                    }
                } else {
                    int goalz = sector.getCeilingz();
                    if (sector.getCeilingz() == sector.getFloorz()) {
                        Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz(), 1, 1));
                        goalz = nextsec != null ? nextsec.getFloorz() : 0;
                    }
                    setanimation(s, goalz, DOORSPEED, 0, FLOORZ);
                }
                SND_Sound(S_DOOR1);
                break;
            }
            case PLATFORMELEVTAG: {
                int i = getanimationgoal(sector, 1);

                Sector nextsec = boardService.getSector(plr.getSector());
                int goalz = nextsec != null ? nextsec.getFloorz() : 0;
                if (i >= 0) {
                    gAnimationData[i].goal = goalz;
                } else {
                    setanimation(s, goalz, ELEVSPEED, 0, FLOORZ);
                }
                break;}
            case BOXELEVTAG: {
                int i = getanimationgoal(sector, 1);
                int j = getanimationgoal(sector, 2);
                int size = sector.getCeilingz() - sector.getFloorz();

                Sector nextsec = boardService.getSector(plr.getSector());
                int goalz = nextsec != null ? nextsec.getFloorz() : 0;
                if (i >= 0) {

                    gAnimationData[i].goal = goalz;
                } else {
                    setanimation(s, goalz, ELEVSPEED, 0, FLOORZ);
                }
                goalz = goalz + size;
                if (j >= 0) {
                    gAnimationData[j].goal = goalz;
                } else {
                    setanimation(s, goalz, ELEVSPEED, 0, CEILZ);
                }
                break;}
            case DOORSPLITHOR:{
                int i = getanimationgoal(sector, 1);
                int j = getanimationgoal(sector, 2);
                if (i >= 0) {
                    Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz(), 1, 1));
                    int botz = nextsec != null ? nextsec.getFloorz() : 0;
                    if (gAnimationData[i].goal == botz) {
                        gAnimationData[i].goal = (sector.getCeilingz() + sector.getFloorz()) >> 1;
                    } else {
                        gAnimationData[i].goal = botz;
                    }
                } else {
                    int botz = (sector.getCeilingz() + sector.getFloorz()) >> 1;
                    if (sector.getCeilingz() == sector.getFloorz()) {
                        Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz(), 1, 1));
                        botz = nextsec != null ? nextsec.getFloorz() : 0;
                    }
                    setanimation(s, botz, ELEVSPEED, 0, FLOORZ);
                }
                if (j >= 0) {
                    Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                    int topz = nextsec != null ? nextsec.getCeilingz() : 0;
                    if (gAnimationData[j].goal == topz) {
                        gAnimationData[j].goal = (sector.getCeilingz() + sector.getFloorz()) >> 1;
                    } else {
                        gAnimationData[j].goal = topz;
                    }
                } else {
                    int topz = (sector.getCeilingz() + sector.getFloorz()) >> 1;
                    if (sector.getCeilingz() == sector.getFloorz()) {
                        Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                        topz = nextsec != null ? nextsec.getCeilingz() : 0;
                    }
                    setanimation(s, topz, ELEVSPEED, 0, CEILZ);
                }
                SND_Sound(S_DOOR1 + (engine.krand() % 3));
                break;}
            case DOORSPLITVER:
                opwallfind[0] = -1;
                opwallfind[1] = -1;
                for (ListNode<Wall> wn = sector.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    if ((wal.getX() == centx) || (wal.getY() == centy)) {
                        if (opwallfind[0] == -1) {
                            opwallfind[0] = wn.getIndex();
                        } else {
                            opwallfind[1] = wn.getIndex();
                        }
                    }
                }

                for (int j = 0; j < 2; j++) {
                    Wall opwall = boardService.getWall(opwallfind[j]);
                    if (opwall == null) {
                        continue;
                    }

                    if ((opwall.getX() == centx) && (opwall.getY() == centy)) {
                        int i = opwallfind[j] - 1;
                        if (i < startwall) {
                            i = endwall;
                        }
                        Wall w = boardService.getWall(i);
                        if (w != null) {
                            int dax2 = ((w.getX() + opwall.getWall2().getX()) >> 1) - opwall.getX();
                            int day2 = ((w.getY() + opwall.getWall2().getY()) >> 1) - opwall.getY();
                            if (dax2 != 0) {
                                dax2 = opwall.getWall2().getWall2().getX();
                                dax2 -= opwall.getWall2().getX();
                                setanimation(opwallfind[j], opwall.getX() + dax2, 4, 0, WALLX);
                                setanimation(i, w.getX() + dax2, 4, 0, WALLX);
                                setanimation(opwall.getPoint2(), opwall.getWall2().getX() + dax2, 4, 0,
                                        WALLX);
                            } else if (day2 != 0) {
                                day2 = opwall.getWall2().getWall2().getY();
                                day2 -= opwall.getWall2().getY();
                                setanimation(opwallfind[j], opwall.getY() + day2, 4, 0, WALLY);
                                setanimation(i, w.getY() + day2, 4, 0, WALLY);
                                setanimation(opwall.getPoint2(), opwall.getWall2().getY() + day2, 4, 0,
                                        WALLY);
                            }
                        }
                    } else {
                        int i = opwallfind[j] - 1;
                        if (i < startwall) {
                            i = endwall;
                        }
                        Wall w = boardService.getWall(i);
                        if (w != null) {
                            int dax2 = ((w.getX() + opwall.getWall2().getX()) >> 1) - opwall.getX();
                            int day2 = ((w.getY() + opwall.getWall2().getY()) >> 1) - opwall.getY();
                            if (dax2 != 0) {
                                setanimation(opwallfind[j], centx, 4, 0, WALLX);
                                setanimation(i, centx + dax2, 4, 0, WALLX);
                                setanimation(opwall.getPoint2(), centx + dax2, 4, 0, WALLX);
                            } else if (day2 != 0) {
                                setanimation(opwallfind[j], centy, 4, 0, WALLY);
                                setanimation(i, centy + day2, 4, 0, WALLY);
                                setanimation(opwall.getPoint2(), centy + day2, 4, 0, WALLY);
                            }
                        }
                    }
                }
                break;

            case 131:
                if (keysok == 0) {
                    break;
                } else {
                    playsound(S_CREAKDOOR2, 0, 0, 0);
                    d_soundplayed = 1;
                }

            case 132:
                if (keysok == 0) {
                    break;
                } else {
                    playsound(S_CREAKDOOR2, 0, 0, 0);
                    d_soundplayed = 1;
                }

            case 133:
                if (keysok == 0) {
                    break;
                } else {
                    playsound(S_CREAKDOOR2, 0, 0, 0);
                    d_soundplayed = 1;
                }

            case 134:
                if (keysok == 0) {
                    break;
                } else {
                    playsound(S_CREAKDOOR2, 0, 0, 0);
                    d_soundplayed = 1;
                }

            case DOORSWINGTAG:
                if (game.WH2 && d_soundplayed == 0) {
                    playsound(S_SWINGDOOR, 0, 0, 0);
                } else {
                    d_soundplayed = 1;
                }

                for (int i = 0; i < swingcnt; i++) {
                    if (swingdoor[i].sector == s) {
                        if (swingdoor[i].anginc == 0) {
                            if (swingdoor[i].ang == swingdoor[i].angclosed) {
                                swingdoor[i].anginc = swingdoor[i].angopendir;
                            } else {
                                swingdoor[i].anginc = -swingdoor[i].angopendir;
                            }
                        } else {
                            swingdoor[i].anginc = -swingdoor[i].anginc;
                        }
                    }
                }
                break;
        } // switch
        //
        // LOWER FLOOR ANY AMOUNT
        //
        if (datag >= 1100 && datag <= 1199) {

            int speed = 32;
            if (sector.getHitag() > 100) {
                speed = 64;
            }

            sector.setHitag(0);

            int daz = sector.getFloorz() + (1024 * (sector.getLotag() - 1100));
            if ((setanimation(s, daz, speed, 0, FLOORZ)) >= 0) {
                playsound(S_STONELOOP1, 0, 0, (sector.getLotag() - 1100) / 10);
            }
            sector.setLotag(0);
        }

        //
        // RAISE FLOOR 1-99
        //
        if (datag >= 1200 && datag <= 1299) {
            int speed = 32;
            if (sector.getHitag() > 100) {
                speed = 64;
            }

            sector.setHitag(0);

            switch (sector.getFloorpicnum()) {
                case LAVA:
                case ANILAVA:
                case LAVA1:
                    sector.setFloorpicnum(COOLLAVA);
                    break;
                case SLIME:
                    sector.setFloorpicnum(DRYSLIME);
                    break;
                case WATER:
                case HEALTHWATER:
                    sector.setFloorpicnum(DRYWATER);
                    break;
                case LAVA2:
                    sector.setFloorpicnum(COOLLAVA2);
                    break;
            }

            int daz = sector.getFloorz() - (1024 * (sector.getLotag() - 1200));
            if ((setanimation(s, daz, speed, 0, FLOORZ)) >= 0) {
                playsound(S_STONELOOP1, 0, 0, (sector.getLotag() - 1200) / 10);
            }
            sector.setLotag(0);
        }

        if (datag >= 1300 && datag <= 1399) {
            int speed = 32;
            if (sector.getHitag() > 100) {
                speed = 64;
            }

            sector.setHitag(0);

            int daz = sector.getCeilingz() + (1024 * (sector.getLotag() - 1300));
            if ((setanimation(s, daz, speed, 0, CEILZ)) >= 0) {
                playsound(S_STONELOOP1, 0, 0, (sector.getLotag() - 1300) / 10);
            }
            sector.setLotag(0);
        }

        // RAISE CEILING ANY AMOUNT
        if (datag >= 1400 && datag <= 1499) {
            sector.setHitag(0);
            int speed = 32;
            if (sector.getHitag() > 100) {
                speed = 64;
            }

            int daz = sector.getCeilingz() - (1024 * (sector.getLotag() - 1400));
            if ((setanimation(s, daz, speed, 0, CEILZ)) >= 0) {
                playsound(S_STONELOOP1, 0, 0, (sector.getLotag() - 1400) / 10);
            }
            sector.setLotag(0);
        }

         // *********
         // * LOWER FLOOR AND CEILING ANY AMOUNT
         // *********

        if (datag >= 1500 && datag <= 1599) {
            int speed = 32;
            if (sector.getHitag() > 100) {
                speed = 64;
            }

            sector.setHitag(0);

            int daz = sector.getFloorz() + (1024 * (sector.getLotag() - 1500));
            setanimation(s, daz, speed, 0, FLOORZ);
            daz = sector.getCeilingz() + (1024 * (sector.getLotag() - 1500));
            if ((setanimation(s, daz, speed, 0, CEILZ)) >= 0) {
                playsound(S_STONELOOP1, 0, 0, (sector.getLotag() - 1500) / 10);
            }
            sector.setLotag(0);
        }

        //
        // RAISE FLOOR AND CEILING ANY AMOUNT
        //
        if (datag >= 1600 && datag <= 1699) {

            int speed = 32;
            if (sector.getHitag() > 100) {
                speed = 64;
            }

            sector.setHitag(0);

            int daz = sector.getFloorz() - (1024 * (sector.getLotag() - 1600));
            setanimation(s, daz, speed, 0, FLOORZ);
            daz = sector.getCeilingz() - (1024 * (sector.getLotag() - 1600));
            if ((setanimation(s, daz, speed, 0, CEILZ)) >= 0) {
                playsound(S_STONELOOP1, 0, 0, (sector.getLotag() - 1600) / 10);
            }
            sector.setLotag(0);
        }

        if (datag >= 1800 && datag <= 1899) {
            int i = getanimationgoal(sector, 1);
            if (i >= 0) {
                int daz = sector.getCeilingz() + (1024 * 16);
                if (gAnimationData[i].goal == daz) {
                    Sector sec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz() - (1024 * 16), -1, -1));
                    if (sec != null) {
                        gAnimationData[i].goal = sec.getFloorz();
                    }
                } else {
                    gAnimationData[i].goal = daz;
                }
            } else {
                int daz;
                if (sector.getFloorz() == sector.getCeilingz() + (1024 * 16)) {
                    Sector sec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz() - (1024 * 16), -1, -1));
                    if (sec != null) {
                        daz = sec.getFloorz();
                    } else {
                        daz = 0;
                    }
                } else {
                    daz = sector.getCeilingz() + (1024 * 16);
                }
                if ((setanimation(s, daz, 32, 0, FLOORZ)) >= 0) {
                    playsound(S_STONELOOP1, 0, 0, (sector.getLotag() - 1800) / 10);
                }
            }
        }

        if (datag >= 1900 && datag <= 1999) {
            sector.setHitag(0);
            int temp1 = sector.getLotag() - 1900;
            int temp2 = temp1 / 10;
            int temp3 = temp1 - temp2;

            SND_Sound(S_STONELOOP1);

            switch (temp3) { // type of crush
                case 0:
                    sector.setLotag(DOORDOWNTAG);
                    setanimation(s, sector.getCeilingz(), 64, 0, FLOORZ);
                    break;
                case 1:
                    setanimation(s, sector.getCeilingz(), 64, 0, FLOORZ);
                    sector.setLotag(0);
                    break;
                case 2:
                    setanimation(s, sector.getFloorz(), 64, 0, CEILZ);
                    sector.setLotag(0);
                    break;
                case 3:
                case 6:
                case 4:
                    sector.setLotag(0);
                    break;
                case 5:
                    int daz = (sector.getCeilingz() + sector.getFloorz()) >> 1;
                    setanimation(s, daz, 64, 0, FLOORZ);
                    setanimation(s, daz, 64, 0, CEILZ);
                    break;
            }
        }

        // BRASS KEY
        // BLACK KEY
        // GLASS KEY
        // IVORY KEY

        if (datag >= 2000 && datag <= 2999) {
            System.err.println("WHTAG.java: 712 check this place keychecking"); //XXX
            int doorkey = (sector.getLotag() - 2000) / 100;
            int doorantic = (sector.getLotag() - (2000 + (doorkey * 100))) / 10;
            int doortype = sector.getLotag() - (2000 + (doorkey * 100) + (doorantic * 10));
            boolean checkforkey = false;
            for (int i = 0; i < MAXKEYS; i++) {
                if (plr.getTreasure()[i] == doorkey) {
                    checkforkey = true;
                    break;
                }
            }

            if (checkforkey) {
                if (doorantic == 0) {
                    sector.setLotag(0);
                }
                switch (doortype) {
                    case 0: {// up
                        int i = getanimationgoal(sector, 2);
                        if (i >= 0) {
                            Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                            int goalz = nextsec != null ? nextsec.getCeilingz() : 0;
                            if (gAnimationData[i].goal == goalz) {
                                gAnimationData[i].goal = sector.getFloorz();
                                if (doorantic == 2 || doorantic == 3) {
                                    setdelayfunc(s, 0);
                                }
                            } else {
                                gAnimationData[i].goal = goalz;
                                if (doorantic == 2) {
                                    setdelayfunc(s, DOORDELAY);
                                } else if (doorantic == 3) {
                                    setdelayfunc(s, DOORDELAY << 3);
                                }
                            }
                        } else {
                            int goalz = sector.getFloorz();
                            if (sector.getCeilingz() == sector.getFloorz()) {
                                Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                                goalz = nextsec != null ? nextsec.getCeilingz() : 0;
                                if (doorantic == 2) {
                                    setdelayfunc(s, DOORDELAY);
                                } else if (doorantic == 3) {
                                    setdelayfunc(s, DOORDELAY << 3);
                                }
                            } else {
                                if (doorantic == 2 || doorantic == 3) {
                                    setdelayfunc(s, 0);
                                }
                            }
                            setanimation(s, goalz, DOORSPEED, 0, CEILZ);
                        }
                        break;
                    }
                    case 1: { // dn
                        int i = getanimationgoal(sector, 1);
                        if (i >= 0) {
                            Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz(), 1, 1));
                            int goalz = nextsec != null ? nextsec.getFloorz() : 0;
                            if (gAnimationData[i].goal == goalz) {
                                gAnimationData[i].goal = sector.getCeilingz();
                                if (doorantic == 2 || doorantic == 3) {
                                    setdelayfunc(s, 0);
                                }
                            } else {
                                gAnimationData[i].goal = goalz;
                                if (doorantic == 2) {
                                    setdelayfunc(s, DOORDELAY);
                                } else if (doorantic == 3) {
                                    setdelayfunc(s, DOORDELAY << 3);
                                }
                            }
                        } else {
                            int goalz = sector.getCeilingz();
                            if (sector.getCeilingz() == sector.getFloorz()) {
                                Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz(), 1, 1));
                                goalz = nextsec != null ? nextsec.getFloorz() : 0;
                                if (doorantic == 2) {
                                    setdelayfunc(s, DOORDELAY);
                                } else if (doorantic == 3) {
                                    setdelayfunc(s, DOORDELAY << 3);
                                }
                            } else {
                                if (doorantic == 2 || doorantic == 3) {
                                    setdelayfunc(s, 0);
                                }
                            }
                            setanimation(s, goalz, DOORSPEED, 0, FLOORZ);
                        }
                        break;
                    }
                    case 2: { // middle
                        int i = getanimationgoal(sector, 1);
                        int j = getanimationgoal(sector, 2);
                        if (i >= 0) {
                            Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz(), 1, 1));
                            int botz = nextsec != null ? nextsec.getFloorz() : 0;
                            if (gAnimationData[i].goal == botz) {
                                gAnimationData[i].goal = (sector.getCeilingz() + sector.getFloorz()) >> 1;
                                if (doorantic == 2 || doorantic == 3) {
                                    setdelayfunc(s, 0);
                                }
                            } else {
                                gAnimationData[i].goal = botz;
                                if (doorantic == 2) {
                                    setdelayfunc(s, DOORDELAY);
                                } else if (doorantic == 3) {
                                    setdelayfunc(s, DOORDELAY << 3);
                                }
                            }
                        } else {
                            int botz = (sector.getCeilingz() + sector.getFloorz()) >> 1;
                            if (sector.getCeilingz() == sector.getFloorz()) {
                                Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getCeilingz(), 1, 1));
                                botz = nextsec != null ? nextsec.getFloorz() : 0;
                                if (doorantic == 2) {
                                    setdelayfunc(s, DOORDELAY);
                                } else if (doorantic == 3) {
                                    setdelayfunc(s, DOORDELAY << 3);
                                }
                            } else {
                                if (doorantic == 2 || doorantic == 3) {
                                    setdelayfunc(s, 0);
                                }
                            }
                            setanimation(s, botz, DOORSPEED, 0, FLOORZ);
                        }
                        if (j >= 0) {
                            Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                            int topz = nextsec != null ? nextsec.getCeilingz() : 0;
                            if (gAnimationData[j].goal == topz) {
                                gAnimationData[j].goal = (sector.getCeilingz() + sector.getFloorz()) >> 1;
                            } else {
                                gAnimationData[j].goal = topz;
                            }
                        } else {
                            int  topz = (sector.getCeilingz() + sector.getFloorz()) >> 1;
                            if (sector.getCeilingz() == sector.getFloorz()) {
                                Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                                topz = nextsec != null ? nextsec.getCeilingz() : 0;
                            }
                            setanimation(s, topz, DOORSPEED, 0, CEILZ);
                        }
                        break;
                    }
                    case 3: // vert
                    case 4: // swing
                        break;
                }
            }
        } // end of complexdoors

        if (datag == 3000) {
            for (int k = 0; k < ironbarscnt; k++) {
                if (ironbarsector[k] == s) {
                    ironbarsdone[k] = 1;
                    Sprite spr = boardService.getSprite(ironbarsanim[k]);
                    if (spr != null) {
                        switch (spr.getPicnum()) {

                            case SWINGDOOR:

                            case SWINGDOOR2:

                            case SWINGDOOR3:

                            case TALLSWING:

                            case TALLSWING2:
                                SND_Sound(S_CREAKDOOR2);
                                break;

                            case SWINGGATE:
                                SND_Sound(S_CREAKDOOR3);
                                break;

                            case SWINGHOLE:

                            case SWINGGATE2:

                            case ROPEDOOR:

                            case GEARSSTART:

                            case WOODGEARSSTART:
                                SND_Sound(S_CREAKDOOR1);
                                break;
                        }

                        int pic = spr.getPicnum();
                        if (pic == SWINGGATE3 || pic == SWINGGATE4 || pic == SWINGGATE5 || pic == GEARS2START) {
                            SND_Sound(S_CREAKDOOR1);
                        }
                    }
                }
            }
        }

        if (datag == 4000) {
            for (int k = 0; k < MAXSPRITES; k++) {
                Sprite spr = boardService.getSprite(k);

                if (spr != null && sector.getHitag() == spr.getHitag() && spr.getExtra() < 1) {
                    newstatus(k, FLOCKSPAWN);
                    if (batsnd == -1) {
                        batsnd = playsound(S_BATSLOOP, spr.getX(), spr.getY(), -1);
                    }
                }
            }
        }
        if (datag == 4001) {
            for (int k = 0; k < MAXSPRITES; k++) {
                Sprite spr = boardService.getSprite(k);
                if (spr != null && sector.getHitag() == spr.getHitag() && spr.getPicnum() == GOBLIN) {
                    newstatus(k, WAR);
                }
            }
        }

        switch (datag) {
            case 61:
            case 62:
            case 63:
            case 64:
                if (keysok == 0) {
                    break;
                } else {
                    int i = getanimationgoal(sector, 2);
                    if (i >= 0) {
                        Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                        int goalz = nextsec != null ? nextsec.getCeilingz() : 0;
                        if (gAnimationData[i].goal == goalz) {
                            gAnimationData[i].goal = sector.getFloorz();
                        } else {
                            gAnimationData[i].goal = goalz;
                        }
                    } else {
                        int goalz = sector.getFloorz();
                        if (sector.getCeilingz() == sector.getFloorz()) {
                            Sector nextsec = boardService.getSector(engine.nextsectorneighborz(s, sector.getFloorz(), -1, -1));
                            goalz = nextsec != null ? nextsec.getCeilingz() : 0;
                        }
                        setanimation(s, goalz, DOORSPEED, 0, CEILZ);
                    }
                    SND_Sound(S_DOOR2);
                }
        }
    }

    public static void animatetags(int nPlayer) {
        PLAYER plr = player[nPlayer];
        Sector plrSec = boardService.getSector(plr.getSector());

        if (plrSec != null) {
            if (plrSec.getLotag() == 2) {
                for (int i = 0; i < boardService.getSectorCount(); i++) {
                    Sector sec = boardService.getSector(i);
                    if (sec != null && sec.getHitag() == plrSec.getHitag()) {
                        if (sec.getLotag() != 2) {
                            operatesector(plr, i);
                        }
                    }
                }

                ListNode<Sprite> node = boardService.getStatNode(0);
                while (node != null) {
                    ListNode<Sprite> next = node.getNext();
                    if (node.get().getHitag() == plrSec.getHitag()) {
                        operatesprite(plr, node.getIndex());
                    }
                    node = next;
                }

                plrSec.setLotag(0);
                plrSec.setHitag(0);
            }

            if ((plrSec.getLotag() == 1) && (plr.getSector() != plr.getOldsector())) {
                for (int i = 0; i < boardService.getSectorCount(); i++) {
                    Sector sec = boardService.getSector(i);
                    if (sec != null && sec.getHitag() == plrSec.getHitag()) {
                        if (sec.getLotag() != 2) {
                            operatesector(plr, i);
                        }
                    }
                }


                ListNode<Sprite> node = boardService.getStatNode(0);
                while (node != null) {
                    ListNode<Sprite> next = node.getNext();
                    if (node.get().getHitag() == plrSec.getHitag()) {
                        operatesprite(plr, node.getIndex());
                    }
                    node = next;
                }
            }
        }

        for (int i = 0; i < dragsectorcnt; i++) {
            int dasector = dragsectorlist[i];
            Sector dragSec = boardService.getSector(dasector);
            if (dragSec == null) {
                continue;
            }

            Wall startwall = boardService.getWall(dragSec.getWallptr());
            if (startwall != null) {
                if (startwall.getX() + dragxdir[i] < dragx1[i]) {
                    dragxdir[i] = 16;
                }
                if (startwall.getY() + dragydir[i] < dragy1[i]) {
                    dragydir[i] = 16;
                }
                if (startwall.getX() + dragxdir[i] > dragx2[i]) {
                    dragxdir[i] = -16;
                }
                if (startwall.getY() + dragydir[i] > dragy2[i]) {
                    dragydir[i] = -16;
                }
            }

            for (ListNode<Wall> wn = dragSec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                engine.dragpoint(wn.getIndex(), wal.getX() + dragxdir[i], wal.getY() + dragydir[i]);
            }

            int j = dragSec.getFloorz();
            game.pInt.setceilinterpolate(dasector, dragSec);
            dragSec.setFloorz(dragfloorz[i] + (EngineUtils.sin((lockclock << 4) & 2047) >> 3));

            if (plr.getSector() == dasector) {
                viewBackupPlayerLoc(nPlayer);

                plr.setX(plr.getX() + dragxdir[i]);
                plr.setY(plr.getY() + dragydir[i]);
                plr.setZ(plr.getZ() + (dragSec.getFloorz() - j));

                // Update sprite representation of player

                game.pInt.setsprinterpolate(plr.getSpritenum(), plr.getSprite());
                engine.setsprite(plr.getSpritenum(), plr.getX(), plr.getY(), plr.getZ() + (plr.getHeight()));
                plr.getSprite().setAng((short) plr.getAng());
            }
        }

        for (int i = 0; i < swingcnt; i++) {
            if (swingdoor[i].anginc != 0) {
                int oldang = swingdoor[i].ang;
                for (int j = 0; j < ((TICSPERFRAME) << 2); j++) {
                    swingdoor[i].ang = ((swingdoor[i].ang + 2048 + swingdoor[i].anginc) & 2047);
                    if (swingdoor[i].ang == swingdoor[i].angclosed) {
                        swingdoor[i].anginc = 0;
                    }
                    if (swingdoor[i].ang == swingdoor[i].angopen) {
                        swingdoor[i].anginc = 0;
                    }
                }
                for (int k = 1; k <= 3; k++) {
                    Point out = EngineUtils.rotatepoint(swingdoor[i].x[0], swingdoor[i].y[0], swingdoor[i].x[k],
                            swingdoor[i].y[k], (short) swingdoor[i].ang);

                    engine.dragpoint((short) swingdoor[i].wall[k], out.getX(), out.getY());
                }
                if (swingdoor[i].anginc != 0) {
                    if (plr.getSector() == swingdoor[i].sector) {
                        int good = 1;
                        for (int k = 1; k <= 3; k++) {
                            if (engine.clipinsidebox(plr.getX(), plr.getY(), (short) swingdoor[i].wall[k], 512) != 0) {
                                good = 0;
                                break;
                            }
                        }
                        if (good == 0) {
                            swingdoor[i].ang = oldang;
                            for (int k = 1; k <= 3; k++) {
                                Point out = EngineUtils.rotatepoint(swingdoor[i].x[0], swingdoor[i].y[0], swingdoor[i].x[k],
                                        swingdoor[i].y[k], (short) swingdoor[i].ang);

                                engine.dragpoint((short) swingdoor[i].wall[k], out.getX(), out.getY());
                            }
                            swingdoor[i].anginc = -swingdoor[i].anginc;
                            break;
                        }
                    }
                }
            }
        }
    }

    public static void dodelayitems(int tics) {
        int cnt = delaycnt;
        for (int i = 0; i < cnt; i++) {
            if (!delayitem[i].func) {
                int j = delaycnt - 1;
                delayitem[i].memmove(delayitem[j]);
                delaycnt = j;
            }
            if (delayitem[i].timer > 0) {
                if ((delayitem[i].timer -= tics) <= 0) {
                    delayitem[i].timer = 0;
                    operatesector(player[pyrn], delayitem[i].item);
                    delayitem[i].func = false;
                }
            }
        }
    }

    public static void setdelayfunc(int item, int delay) {
        System.out.println("setdelayfunc");
        for (int i = 0; i < delaycnt; i++) {
            if (delayitem[i].item == item && delayitem[i].func) {
                if (delay == 0) {
                    delayitem[i].func = false;
                }
                delayitem[i].timer = delay;
                return;
            }
        }
        if (delay > 0) {
            delayitem[delaycnt].func = true;
            delayitem[delaycnt].item = item;
            delayitem[delaycnt].timer = delay;
            delaycnt++;
        }
    }

}
