package ru.m210projects.Witchaven;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Witchaven.Types.PLAYER;

import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Witchaven.Globals.netgame;
import static ru.m210projects.Witchaven.Items.FLASKBLUETYPE;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WHPLR.addhealth;
import static ru.m210projects.Witchaven.WHPLR.player;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.*;

public class Potions {

    public static final int MAXPOTIONS = 5;

    public static int potiontilenum;
    public static final char[] potionbuf = new char[50];

    public static void potiontext(PLAYER plr) {
        if (plr.getPotion()[plr.getCurrentpotion()] > 0) {
            switch (plr.getCurrentpotion()) {
                case 0:
                    showmessage("Health Potion", 240);
                    break;
                case 1:
                    showmessage("Strength Potion", 240);
                    break;
                case 2:
                    showmessage("Cure Poison Potion", 240);
                    break;
                case 3:
                    showmessage("Resist Fire Potion", 240);
                    break;
                case 4:
                    showmessage("Invisibility Potion", 240);
                    break;
            }
        }
    }

    public static void potionchange(int snum) {
        PLAYER plr = player[snum];

        int key = ((plr.getInput().bits & (15 << 16)) >> 16) - 1;
        if (key != -1 && key < 7) {
            if (key == 5 || key == 6) {
                key = (key == 5 ? -1 : 1);
                plr.setCurrentpotion(plr.getCurrentpotion() + key);
                if (plr.getCurrentpotion() < 0) {
                    plr.setCurrentpotion(4);
                }
                if (plr.getCurrentpotion() >= MAXPOTIONS) {
                    plr.setCurrentpotion(0);
                }
            } else {
                plr.setCurrentpotion(key);
            }
            SND_Sound(S_BOTTLES);
            potiontext(plr);
        }
    }

    public static void usapotion(PLAYER plr) {

        if (plr.getCurrentpotion() == 0 && plr.getHealth() >= plr.getMaxhealth()) {
            return;
        }

        if (plr.getCurrentpotion() == 2 && plr.getPoisoned() == 0) {
            return;
        }

        if (plr.getPotion()[plr.getCurrentpotion()] <= 0) {
            return;
        } else {
            plr.getPotion()[plr.getCurrentpotion()]--;
        }

        switch (plr.getCurrentpotion()) {
            case 0: // health potion
                if (plr.getHealth() + 25 > plr.getMaxhealth()) {
                    plr.setHealth(0);
                    SND_Sound(S_DRINK);
                    addhealth(plr, plr.getMaxhealth());
                } else {
                    SND_Sound(S_DRINK);
                    addhealth(plr, 25);
                }
                startblueflash(10);
                break;
            case 1: // strength
                plr.setStrongtime(3200);
                SND_Sound(S_DRINK);
                startredflash(10);
                break;
            case 2: // anti venom
                SND_Sound(S_DRINK);
                plr.setPoisoned(0);
                plr.setPoisontime(0);
                startwhiteflash(10);
                showmessage("poison cured", 360);
                addhealth(plr, 0);
                break;
            case 3: // fire resist
                SND_Sound(S_DRINK);
                plr.setManatime(3200);
                startwhiteflash(10);
                if (lavasnd != -1) {
                    stopsound(lavasnd);
                    lavasnd = -1;
                }
                break;
            case 4: // invisi
                SND_Sound(S_DRINK);
                plr.setInvisibletime(3200);
                startgreenflash(10);
                break;
        }
    }

    public static boolean potionspace(PLAYER plr, int vial) {
        return plr.getPotion()[vial] <= 9;
    }

    public static void updatepotion(PLAYER plr, int vial) {
        switch (vial) {
            case HEALTHPOTION:
                plr.getPotion()[0]++;
                break;
            case STRENGTHPOTION:
                plr.getPotion()[1]++;
                break;
            case ARMORPOTION:
                plr.getPotion()[2]++;
                break;
            case FIREWALKPOTION:
                plr.getPotion()[3]++;
                break;
            case INVISIBLEPOTION:
                plr.getPotion()[4]++;
                break;
        }
    }

    public static void potionpic(PLAYER plr, int currentpotion, int x, int y, int scale) {
        int tilenum = SFLASKBLUE;

        if (netgame) {
            return;
        }

        Renderer renderer = game.getRenderer();
        x = x + mulscale(200, scale, 16);
        y = y - mulscale(94, scale, 16);
        renderer.rotatesprite(x << 16, y << 16, scale, 0, SPOTIONBACKPIC, 0, 0, 8 | 16);
        renderer.rotatesprite((x - mulscale(4, scale, 16)) << 16, (y - mulscale(7, scale, 16)) << 16, scale, 0, SPOTIONARROW + currentpotion, 0, 0, 8 | 16);

        x += mulscale(4, scale, 16);
        for (int i = 0; i < MAXPOTIONS; i++) {
            if (plr.getPotion()[i] < 0) {
                plr.getPotion()[i] = 0;
            }
            if (plr.getPotion()[i] > 0) {
                switch (i) {
                    case 1:
                        tilenum = SFLASKGREEN;
                        break;
                    case 2:
                        tilenum = SFLASKOCHRE;
                        break;
                    case 3:
                        tilenum = SFLASKRED;
                        break;
                    case 4:
                        tilenum = SFLASKTAN;
                        break;
                }
                potiontilenum = tilenum;

                renderer.rotatesprite((x + mulscale(i * 20, scale, 16)) << 16, (y + mulscale(19, scale, 16)) << 16, scale, 0, potiontilenum, 0, 0, 8 | 16);
                Bitoa(plr.getPotion()[i], potionbuf);

                game.getFont(3).drawText(renderer, x + mulscale(7 + (i * 20), scale, 16), y + mulscale(7, scale, 16), potionbuf, scale / 65536.0f, 0, 0, TextAlign.Left, Transparent.None, false);
            } else {
                renderer.rotatesprite((x + mulscale(i * 20, scale, 16)) << 16, (y + mulscale(19, scale, 16)) << 16, scale, 0, SFLASKBLACK, 0, 0, 8 | 16);
            }
        }
    }

    public static void randompotion(int i) {
        if ((engine.krand() % 100) > 20) {
            return;
        }
        
        Sprite srcSprite = boardService.getSprite(i);
        if (srcSprite == null) {
            return;
        }
        
        int j = engine.insertsprite(srcSprite.getSectnum(), 0);
        Sprite potionSprite = boardService.getSprite(j);
        if (potionSprite == null) {
            return;
        }

        potionSprite.setX(srcSprite.getX());
        potionSprite.setY(srcSprite.getY());
        potionSprite.setZ(srcSprite.getZ() - (12 << 8));
        potionSprite.setShade(-12);
        potionSprite.setPal(0);
        potionSprite.setCstat(0);
        potionSprite.setCstat(potionSprite.getCstat() & ~3);
        potionSprite.setXrepeat(64);
        potionSprite.setYrepeat(64);
        int type = engine.krand() % 4;
        potionSprite.setPicnum((short) (FLASKBLUE + type));
        potionSprite.setDetail((short) (FLASKBLUETYPE + type));
    }
}
