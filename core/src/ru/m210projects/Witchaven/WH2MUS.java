package ru.m210projects.Witchaven;

import static ru.m210projects.Witchaven.WHSND.*;

public class WH2MUS {

    public static final int NUMLEVELS = 17;
    private static final int SONGSPERLEVEL = 4;
    public static int attacktheme = 0;
    private static int oldsong;

    public static void loadlevelsongs(int which) {
//		int index = (which * SONGSPERLEVEL);
//		for (int i = 0; i < SONGSPERLEVEL; i++) {
//			songptr.offset = songlist[(index + i) * 3] * 4096;
//			songptr.length = songlist[(index + i) * 3 + 1];
//
//			// read data from file
//			if (songptr.buffer == null) {
//				fhsongs.seek(songptr.offset, Whence.Set);
//				byte[] buffer = new byte[songptr.length];
//				int rv = fhsongs.read(buffer);
//
//				songptr.buffer = hmpinit(buffer);
//
//				if (songptr.buffer == null || rv != songptr.length)
//					return false;
//			}
//		}

        oldsong = which;
    }

    public static void startsong(int which) // 0, 1, 2 or 3
    {
//        cfg.setMusicVolume(!cfg.isMuteMusic() ? cfg.getMusicVolume() : 0);
        int index = (oldsong * SONGSPERLEVEL);
        songptr.entry = getMusicEntry(which + index);

        if (sndPlayTrack(index + which)) {
            return;
        }

        if (!songptr.entry.exists()) {
            return;
        }

        if (currSong.equals(songptr.entry)) {
            return;
        }

        sndStopMusic();

        songptr.handle = newMusic(songptr.entry);
        if (songptr.handle != null) {
            currSong = songptr.entry;
            songptr.handle.setLooping(attacktheme == 0);
            songptr.handle.play();
            if (which < 2) {
                attacktheme = 0;
            }
        }

    }
}
