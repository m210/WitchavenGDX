package ru.m210projects.Witchaven;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Gameutils;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Witchaven.Factory.WHRenderer;
import ru.m210projects.Witchaven.Factory.WHSoftware;
import ru.m210projects.Witchaven.Types.PLAYER;

import java.util.concurrent.atomic.AtomicInteger;

import static ru.m210projects.Build.Engine.MAXSPRITESONSCREEN;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Build.Strhandler.buildString;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Items.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Potions.potionpic;
import static ru.m210projects.Witchaven.Spellbooks.sspellbookanim;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHFX.*;
import static ru.m210projects.Witchaven.WHOBJ.justwarpedcnt;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.Weapons.*;

public class WHScreen {

//	public static int jumphoriz;

    public static final Vector3[] sflag = {new Vector3(260, 387, 0), new Vector3(286, 387, 10), new Vector3(260, 417, 11), new Vector3(286, 417, 12),};
    public static final int NUMWHITESHIFTS = 3;
    public static final int WHITESTEPS = 20;
    public static final int WHITETICS = 6;
    public static final int NUMREDSHIFTS = 4;
    public static final int REDSTEPS = 8;
    public static final int NUMGREENSHIFTS = 4;
    public static final int GREENSTEPS = 8;
    public static final int NUMBLUESHIFTS = 4;
    public static final int BLUESTEPS = 8;
    public static final byte[][] whiteshifts = new byte[NUMREDSHIFTS][768];
    public static final byte[][] redshifts = new byte[NUMREDSHIFTS][768];
    public static final byte[][] greenshifts = new byte[NUMGREENSHIFTS][768];
    public static final byte[][] blueshifts = new byte[NUMBLUESHIFTS][768];
    private static final char[] buffer = new char[256];
    private static final AtomicInteger fz = new AtomicInteger();
    private static final AtomicInteger cz = new AtomicInteger();
    public static final char[] armorbuf = new char[50];
    public static final char[] scorebuf = new char[50];
    public static final char[] healthbuf = new char[50];
    public static boolean drawfloormirror = false;
    public static int zoom = 256;
    public static final char[] displaybuf = new char[128];
    public static int redcount, whitecount, greencount, bluecount;
    public static boolean palshifted;

    public static int displaytime;
    public static String lastmessage;

    public static void initpaletteshifts() {
        int delta;

        byte[] palette = engine.getPaletteManager().getBasePalette();
        for (int i = 1; i <= NUMREDSHIFTS; i++) {
            int workptr = 0;
            int baseptr = 0;
            for (int j = 0; j <= 255; j++) {
                delta = 64 - (palette[baseptr] & 0xFF);
                redshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / REDSTEPS) << 2);
                delta = -(palette[baseptr] & 0xFF);
                redshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / REDSTEPS) << 2);
                delta = -(palette[baseptr] & 0xFF);
                redshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / REDSTEPS) << 2);
            }
        }

        for (int i = 1; i <= NUMWHITESHIFTS; i++) {
            int workptr = 0;
            int baseptr = 0;
            for (int j = 0; j <= 255; j++) {
                delta = 64 - (palette[baseptr] & 0xFF);
                whiteshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / WHITESTEPS) << 2);
                delta = 62 - (palette[baseptr] & 0xFF);
                whiteshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / WHITESTEPS) << 2);
                delta = -(palette[baseptr] & 0xFF);
                whiteshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / WHITESTEPS) << 2);
            }
        }

        for (int i = 1; i <= NUMGREENSHIFTS; i++) {
            int workptr = 0;
            int baseptr = 0;
            for (int j = 0; j <= 255; j++) {
                delta = -(palette[baseptr] & 0xFF);
                greenshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / GREENSTEPS) << 2);
                delta = 64 - (palette[baseptr] & 0xFF);
                greenshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / GREENSTEPS) << 2);
                delta = -(palette[baseptr] & 0xFF);
                greenshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / GREENSTEPS) << 2);
            }
        }

        for (int i = 1; i <= NUMBLUESHIFTS; i++) {
            int workptr = 0;
            int baseptr = 0;
            for (int j = 0; j <= 255; j++) {
                delta = -(palette[baseptr] & 0xFF);
                blueshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / BLUESTEPS) << 2);
                delta = -(palette[baseptr] & 0xFF);
                blueshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / BLUESTEPS) << 2);
                delta = 64 - (palette[baseptr] & 0xFF);
                blueshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / BLUESTEPS) << 2);
            }
        }
    }

    public static void updatepaletteshifts() {
        if (whitecount != 0) {
            whitecount = BClipLow(whitecount - TICSPERFRAME, 0);
        }

        if (redcount != 0) {
            redcount = BClipLow(redcount - TICSPERFRAME, 0);
        }

        if (bluecount != 0) {
            bluecount = BClipLow(bluecount - TICSPERFRAME, 0);
        }

        if (greencount != 0) {
            greencount = BClipLow(greencount - TICSPERFRAME, 0);
        }
    }

    public static void startredflash(int damage) {
        redcount = BClipHigh(redcount + 3 * damage, 100);
    }

    public static void startblueflash(int bluetime) {
        bluecount = BClipHigh(bluecount + 3 * bluetime, 100);
    }

    public static void startwhiteflash(int whitetime) {
        whitecount = BClipHigh(whitecount + 3 * whitetime, 100);
    }

    public static void startgreenflash(int greentime) {
        greencount = BClipHigh(greencount + 3 * greentime, 100);
    }

    public static void resetflash() {
        redcount = 0;
        whitecount = 0;
        greencount = 0;
        bluecount = 0;

        WHITE_DAC.setIntensive(0);
        RED_DAC.setIntensive(0);
        GREEN_DAC.setIntensive(0);
        BLUE_DAC.setIntensive(0);
    }

    public static void showmessage(String message, int time) {
        buildString(displaybuf, 0, message);
        displaytime = time;
        if (!message.equals(lastmessage)) {
            Console.out.println(message);
        }
        lastmessage = message;
    }

    public static void spikeanimation(PLAYER plr) {
        if (plr.getSpiketics() < 0) {
            plr.setCurrspikeframe(plr.getCurrspikeframe() + 1);
            if (plr.getCurrspikeframe() > 4) {
                plr.setCurrspikeframe(4);
            }
            plr.setSpiketics(spikeanimtics[plr.getCurrspikeframe()].daweapontics);
            plr.setSpikeframe(spikeanimtics[plr.getCurrspikeframe()].daweaponframe);
        }
    }

    public static void drawscreen(int num, int dasmoothratio) {
        WHRenderer renderer = game.getRenderer();
        PLAYER plr = player[num];

        int cposx = plr.getX();
        int cposy = plr.getY();
        int cposz = plr.getZ();
        float cang = plr.getAng();
        float choriz = plr.getHoriz() + plr.getJumphoriz();

        if (!game.menu.gShowMenu && !Console.out.isShowing()) {

            int ix = gPrevPlayerLoc[num].x;
            int iy = gPrevPlayerLoc[num].y;
            int iz = gPrevPlayerLoc[num].z;
            float iHoriz = gPrevPlayerLoc[num].horiz;
            float inAngle = gPrevPlayerLoc[num].ang;

            ix += mulscale(cposx - gPrevPlayerLoc[num].x, dasmoothratio, 16);
            iy += mulscale(cposy - gPrevPlayerLoc[num].y, dasmoothratio, 16);
            iz += mulscale(cposz - gPrevPlayerLoc[num].z, dasmoothratio, 16);
            iHoriz += ((choriz - gPrevPlayerLoc[num].horiz) * dasmoothratio) / 65536.0f;
            inAngle += ((BClampAngle(cang - gPrevPlayerLoc[num].ang + 1024) - 1024) * dasmoothratio) / 65536.0f;

            cposx = ix;
            cposy = iy;
            cposz = iz;

            choriz = iHoriz;
            cang = inAngle;
        }

        // wango
        if ((renderer.getGotPic()[FLOORMIRROR >> 3] & (1 << (FLOORMIRROR & 7))) != 0) {
            int dist = 0x7fffffff;
            int i = 0, j;
            for (int k = floormirrorcnt - 1; k >= 0; k--) {
                int sect = floormirrorsector[k];
                if (!renderer.gotSector(sect)) {
                    continue;
                }

                Sector sec = boardService.getSector(sect);
                if (sec != null) {
                    Wall wal = boardService.getWall(sec.getWallptr());
                    if (wal != null) {
                        j = klabs(wal.getX() - plr.getX());
                        j += klabs(wal.getY() - plr.getY());
                        if (j < dist) {
                            dist = j;
                            i = k;
                        }
                    }
                }
            }

            if (!game.isSoftwareRenderer()) {
                renderer.setPrepareMirror(true);
                drawfloormirror = true;
                renderer.settiltang(1024);
                renderer.drawrooms(plr.getX(), plr.getY(), plr.getZ(), plr.getAng(), 201 - plr.getHoriz(), floormirrorsector[i]);
                analizesprites(plr, dasmoothratio);
                renderer.drawmasks();
                renderer.settiltang(0);
                renderer.setPrepareMirror(false);
            } else {
                Sector sec = boardService.getSector(floormirrorsector[i]);
                if (sec != null) {
                    renderer.drawrooms(plr.getX(), plr.getY(), (sec.getFloorz() << 1) - plr.getZ(), plr.getAng(), 201 - plr.getHoriz(), floormirrorsector[i]);
                    analizesprites(plr, dasmoothratio);
                    renderer.drawmasks();

                    // Temp horizon
                    WHSoftware rend = (WHSoftware) game.getRenderer();
                    rend.TempHorizon((int) plr.getHoriz());
                }
            }

            renderer.getGotPic()[FLOORMIRROR >> 3] &= ~(1 << (FLOORMIRROR & 7));
        }

        engine.getzsofslope(plr.getSector(), cposx, cposy, fz, cz);
        int lz = 4 << 8;
        if (cposz < cz.get() + lz) {
            cposz = cz.get() + lz;
        }
        if (cposz > fz.get() - lz) {
            cposz = fz.get() - lz;
        }

        renderer.drawrooms(cposx, cposy, cposz, cang, choriz, plr.getSector());
        analizesprites(plr, dasmoothratio);
        renderer.drawmasks();

        if (dimension == 2) {

            if (followmode) {
                cposx = followx;
                cposy = followy;
                cang = followa;

                game.getFont(1).drawTextScaled(renderer, 5, 25, "follow mode", 1.0f, 0, 7, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
            }

            if (mUserFlag == UserFlag.UserMap) {
                game.getFont(1).drawTextScaled(renderer, 5, 15, "user map: " + boardfilename, 1.0f, 0, 7, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
            } else {
                if (gCurrentEpisode != null && gCurrentEpisode.getMap(mapon) != null) {
                    game.getFont(1).drawTextScaled(renderer, 5, 15, gCurrentEpisode.getMap(mapon).title, 1.0f, 0, 7, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
                } else {
                    game.getFont(1).drawTextScaled(renderer, 5, 15, "Map " + mapon, 1.0f, 0, 7, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
                }
            }

            renderer.drawoverheadmap(cposx, cposy, zoom, (short) cang);
        }
    }

    public static void drawscary() {
        Renderer renderer = game.getRenderer();
        if (scarytime > 140 && scarytime < 180) {
            renderer.rotatesprite(320 << 15, 200 << 15, scarysize << 9, 0, SCARY, 0, 0, 1 + 2);
        }
        if (scarytime > 120 && scarytime < 139) {
            renderer.rotatesprite(320 << 15, 200 << 15, scarysize << 9, 0, SCARY + 1, 0, 0, 1 + 2);
        }
        if (scarytime > 100 && scarytime < 119) {
            renderer.rotatesprite(320 << 15, 200 << 15, scarysize << 9, 0, SCARY + 2, 0, 0, 1 + 2);
        }
        if (scarytime > 0 && scarytime < 99) {
            renderer.rotatesprite(320 << 15, 200 << 15, scarysize << 9, 0, SCARY + 3, 0, 0, 1 + 2);
        }
    }

    public static void overwritesprite(int thex, int they, int tilenum, int shade, int stat, int dapalnum) {
        WHRenderer renderer = game.getRenderer();
        renderer.rotatesprite(thex << 16, they << 16, 65536, (stat & 8) << 7, tilenum, shade, dapalnum, (((stat & 1) ^ 1) << 4) + (stat & 2) + ((stat & 4) >> 2) + (((stat & 16) >> 2) ^ ((stat & 8) >> 1)) + 8 + (stat & 256) + (stat & 512));
    }

    public static void spikeheart(PLAYER plr) {
        int dax = spikeanimtics[plr.getCurrspikeframe()].currx;
        int day = spikeanimtics[plr.getCurrspikeframe()].curry;
        Sector sec = boardService.getSector(plr.getSector());
        if (sec != null) {
            overwritesprite(dax, day, plr.getSpikeframe(), sec.getCeilingshade(), 0x02, 0);
            startredflash(10);
        }
    }

    public static void levelpic(PLAYER plr, int x, int y, int scale) {
        float fscale = scale / 65536.0f;
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        if (plr.getSelectedgun() == 6) {
            Bitoa(plr.getAmmo()[6], tempchar);

            if (game.WH2) {
                int px = (x - mulscale(314, scale, 16));
                int py = (y - mulscale(43, scale, 16));
                renderer.rotatesprite(px << 16, py << 16, 2 * scale, 0, 1916, 0, 0, 8 | 16, px + mulscale(4, scale, 16), py + mulscale(4, scale, 16), xdim, py + mulscale(20, scale, 16));
            } else {
                renderer.rotatesprite(x - mulscale(313, scale, 16) << 16, y - mulscale(43, scale, 16) << 16, scale, 0, SARROWS, 0, 0, 8 | 16);
            }
            game.getFont(4).drawText(renderer, x - mulscale(235, scale, 16), y - mulscale(40, scale, 16), tempchar, fscale, 0, 0, TextAlign.Left, Transparent.None, false);
        } else if (plr.getSelectedgun() == 7 && plr.getWeapon()[7] == 2) {
            Bitoa(plr.getAmmo()[7], tempchar);
            renderer.rotatesprite(x - mulscale(game.WH2 ? 314 : 313, scale, 16) << 16, y - mulscale(46, scale, 16) << 16, scale, 0, SPIKES, 0, 0, 8 | 16);
            game.getFont(4).drawText(renderer, x - mulscale(235, scale, 16), y - mulscale(40, scale, 16), tempchar, fscale, 0, 0, TextAlign.Left, Transparent.None, false);
        } else {
            if (game.WH2) {
                int tilenum = 1917 + (plr.getLvl() - 1);
                int px = (x - mulscale(314, scale, 16));
                int py = (y - mulscale(43, scale, 16));
                renderer.rotatesprite(px << 16, py << 16, 2 * scale, 0, tilenum, 0, 0, 8 | 16, px + mulscale(4, scale, 16), py + mulscale(4, scale, 16), xdim, py + mulscale(20, scale, 16));
            } else {
                int tilenum = SPLAYERLVL + (plr.getLvl() - 1);
                renderer.rotatesprite((x - mulscale(313, scale, 16)) << 16, (y - mulscale(43, scale, 16)) << 16, scale, 0, tilenum, 0, 0, 8 | 16);
            }
        }
    }

    public static void drawscore(PLAYER plr, int x, int y, int scale) {
        float fscale = scale / 65536.0f;
        Renderer renderer = game.getRenderer();
        Bitoa(plr.getScore(), scorebuf);
        renderer.rotatesprite((x - mulscale(game.WH2 ? 314 : 313, scale, 16)) << 16, (y - mulscale(85, scale, 16)) << 16, scale, 0, SSCOREBACKPIC, 0, 0, 8 | 16);
        game.getFont(4).drawText(renderer, x - mulscale(259, scale, 16), y - mulscale(81, scale, 16), scorebuf, fscale, 0, 0, TextAlign.Left, Transparent.None, false);
    }

    public static void updatepics(PLAYER plr, int x, int y, int scale) {
        drawscore(plr, x, y, scale);
        if (netgame) {
            if (game.nNetMode == NetMode.Multiplayer) {
                captureflagpic(scale);
            } else {
                fragspic(plr, scale);
            }
        } else {
            potionpic(plr, plr.getCurrentpotion(), x, y, scale);
        }

        levelpic(plr, x, y, scale);
        drawhealth(plr, x, y, scale);
        drawarmor(plr, x, y, scale);
        keyspic(plr, x, y, scale);
    }

    public static void captureflagpic(int ignoredScale) {
        overwritesprite(260 << 1, 387, SPOTIONBACKPIC, 0, 0, 0);

        for (int i = 0; i < 4; i++) {
//			if( teaminplay[i] ) { XXX
            overwritesprite(((int) sflag[i].x << 1) + 6, (int) sflag[i].y + 8, STHEFLAG, 0, 0, (int) sflag[i].z);
//				 Bitoa(teamscore[i],tempchar);
//			fancyfont(((int) sflag[i].x << 1) + 16, (int) sflag[i].y + 16, SPOTIONFONT - 26, tempchar, 0);
//			}
        }
    }

    public static void fragspic(PLAYER ignoredPlr, int ignoredScale) {
        if (cfg.gViewSize == 320) {
            WHRenderer renderer = game.getRenderer();
            int x = renderer.getWidth() / 2 + 200;
            int y = renderer.getHeight() - 94;
            overwritesprite(x, y, SPOTIONBACKPIC, 0, 0, 0);

//			Bitoa(teamscore[pyrn],tempchar); XXX
            game.getFont(2).drawText(renderer, x + 10, y + 10, tempchar, 1.0f, 0, 0, TextAlign.Left, Transparent.None, false);
        }
    }

    public static void keyspic(PLAYER plr, int x, int y, int scale) {
        y -= mulscale(85, scale, 16);
        Renderer renderer = game.getRenderer();
        if (plr.getTreasure()[TBRASSKEY] == 1) {
            renderer.rotatesprite((x + mulscale(180, scale, 16)) << 16, y << 16, scale, 0, SKEYBRASS, 0, 0, 8);
        } else {
            renderer.rotatesprite((x + mulscale(180, scale, 16)) << 16, y << 16, scale, 0, SKEYBLANK, 0, 0, 8);
        }

        y += mulscale(22, scale, 16);
        if (plr.getTreasure()[TBLACKKEY] == 1) {
            renderer.rotatesprite((x + mulscale(180, scale, 16)) << 16, y << 16, scale, 0, SKEYBLACK, 0, 0, 8);
        } else {
            renderer.rotatesprite((x + mulscale(180, scale, 16)) << 16, y << 16, scale, 0, SKEYBLANK, 0, 0, 8);
        }

        y += mulscale(22, scale, 16);
        if (plr.getTreasure()[TGLASSKEY] == 1) {
            renderer.rotatesprite((x + mulscale(180, scale, 16)) << 16, y << 16, scale, 0, SKEYGLASS, 0, 0, 8);
        } else {
            renderer.rotatesprite((x + mulscale(180, scale, 16)) << 16, y << 16, scale, 0, SKEYBLANK, 0, 0, 8);
        }

        y += mulscale(22, scale, 16);
        if (plr.getTreasure()[TIVORYKEY] == 1) {
            renderer.rotatesprite((x + mulscale(180, scale, 16)) << 16, y << 16, scale, 0, SKEYIVORY, 0, 0, 8);
        } else {
            renderer.rotatesprite((x + mulscale(180, scale, 16)) << 16, y << 16, scale, 0, SKEYBLANK, 0, 0, 8);
        }
    }

    public static void drawhealth(PLAYER plr, int x, int y, int scale) {
        float fscale = scale / 65536.0f;
        Bitoa(plr.getHealth(), healthbuf);
        Renderer renderer = game.getRenderer();
        if (plr.getPoisoned() == 1) {
            int flag;
            switch (EngineUtils.sin((10 * engine.getTotalClock()) & 2047) / 4096) {
                case 0:
                    flag = 0;
                    break;
                case 1:
                case -1:
                    flag = 1;
                    break;
                case -2:
                case 2:
                    flag = 33;
                    break;
                default:
                    game.getFont(2).drawText(renderer, x - mulscale(167, scale, 16), y - mulscale(70, scale, 16), healthbuf, fscale, 0, 0, TextAlign.Left, Transparent.None, false);
                    return;
            }

            renderer.rotatesprite((x - mulscale(171, scale, 16)) << 16, (y - mulscale(75, scale, 16)) << 16, scale, 0, SHEALTHBACK, 0, 6, 8 | 16 | flag);
            game.getFont(2).drawText(renderer,x - mulscale(167, scale, 16), y - mulscale(70, scale, 16), healthbuf, fscale, 0, 0, TextAlign.Left, Transparent.None, false);
        } else {
            renderer.rotatesprite((x - mulscale(171, scale, 16)) << 16, (y - mulscale(75, scale, 16)) << 16, scale, 0, SHEALTHBACK, 0, 0, 8 | 16);
            game.getFont(2).drawText(renderer,x - mulscale(167, scale, 16), y - mulscale(70, scale, 16), healthbuf, fscale, 0, 0, TextAlign.Left, Transparent.None, false);
        }
    }

    public static void drawarmor(PLAYER plr, int x, int y, int scale) {
        Bitoa(plr.getArmor(), armorbuf);
        float fscale = scale / 65536.0f;
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite((x + mulscale(81, scale, 16)) << 16, (y - mulscale(75, scale, 16)) << 16, scale, 0, SHEALTHBACK, 0, 0, 8 | 16);
        game.getFont(2).drawText(renderer, x + mulscale(89, scale, 16), y - mulscale(70, scale, 16), armorbuf, fscale, 0, 0, TextAlign.Left, Transparent.None, false);
    }

    public static void drawweapons(int snum) {
        PLAYER plr = player[snum];
        Sector sec = boardService.getSector(plr.getSector());
        if (sec == null) {
            return;
        }

        int dashade = sec.getCeilingshade();
        if (plr.getShadowtime() > 0 || plr.getSector() == -1) {
            dashade = 31;
        }

        int dapalnum = 0;
        int dabits = 0x02;
        if (plr.getInvisibletime() > 0) {
            dabits = 0x06;
        }

        if (plr.getCurrweaponflip() == 1) {
            dabits |= 0x08;
        }

        switch (plr.getCurrweapon()) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
            case 9:
                dabits |= 512;
                break;
        }

        switch (plr.getCurrweaponfired()) {
            case 6: {
                int dax, day;
                if (game.WH2) {
                    if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                        {
                            dax = lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                            day = lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].curry + 8;
                        }
                    } else {
                        dax = zlefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                        day = zlefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].curry + 8;
                    }
                } else {
                    dax = lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                    day = lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].curry + 8;
                }

                dabits &= ~512;
                dabits |= 257; // #GDX 12.07.2024 second hand animation fix
                if (plr.getCurrweapon() == 0 && plr.getCurrweaponframe() != 0) {
                    if (plr.getDahand() == 1) {
                        overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                    } else if (plr.getDahand() == 2) {
                        dax = lefthandanimtics[0][plr.getCurrweaponanim()].currx;
                        day = lefthandanimtics[0][plr.getCurrweaponanim()].curry + 8;
                        overwritesprite(dax, day + 5, plr.getCurrweaponframe() + 6, dashade, dabits, dapalnum);
                    }
                } else {
                    if (plr.getCurrweaponframe() != 0) {
                        if (game.WH2) {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1) {
                                dax = lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                            } else {
                                dax = zlefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                            }
                        } else {
                            dax = lefthandanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                        }
                        overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                    }
                }
                break;
            }
            case 1: {// fire
                int dax, day;
                if (plr.getCurrweaponattackstyle() == 0) {
                    if (game.WH2) {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            dax = weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx + 8;
                            day = weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].curry;
                        } else {
                            dax = zweaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx + 8;
                            day = zweaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].curry;
                        }
                    } else {
                        dax = weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                        day = weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].curry + 4;
                    }
                } else {
                    if (game.WH2) {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            dax = weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].currx + 8;
                            day = weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].curry;
                        } else {
                            dax = zweaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].currx + 8;
                            day = zweaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].curry;
                        }
                    } else {
                        dax = weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                        day = weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].curry + 4;
                    }
                }

                if (plr.getCurrweapon() == 0 && plr.getCurrweaponframe() != 0) {
                    if (plr.getDahand() == 1) {
                        overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                    } else if (plr.getDahand() == 2) {
                        dax = lefthandanimtics[0][plr.getCurrweaponanim()].currx;
                        day = lefthandanimtics[0][plr.getCurrweaponanim()].curry + 8;
                        overwritesprite(dax, day + 5, plr.getCurrweaponframe() + 6, dashade, dabits, dapalnum);
                    }
                } else {
                    if (plr.getCurrweaponframe() != 0) {
                        if (plr.getCurrweaponattackstyle() == 0) {
                            if (game.WH2) {
                                // flip
                                if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                    dax = weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                                } else {
                                    dax = zweaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                                }
                            } else {
                                dax = weaponanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                            }
                        } else {
                            if (game.WH2) {
                                if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                    dax = weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                                } else {
                                    dax = zweaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                                }
                            } else {
                                dax = weaponanimtics2[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                            }
                        }
                        overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                    }
                }
                break;
            }
            case 0: { // walking
                int dax, day;
                if ((plr.getInput().fvel | plr.getInput().svel) != 0) {
                    if (plr.getCurrweaponframe() == BOWREADYEND) {
                        if (game.WH2) {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1) {
                                day = readyanimtics[plr.getCurrweapon()][6].curry + snakey + 8;
                                dax = readyanimtics[plr.getCurrweapon()][6].currx + snakex + 8;
                            } else {
                                day = zreadyanimtics[plr.getCurrweapon()][6].curry + snakey + 8;
                                dax = zreadyanimtics[plr.getCurrweapon()][6].currx + snakex + 8;
                            }
                        } else {
                            day = readyanimtics[plr.getCurrweapon()][6].curry + snakey + 8;
                            dax = readyanimtics[plr.getCurrweapon()][6].currx + snakex + 8;
                        }
                    } else {
                        if (game.WH2) {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                day = weaponanimtics[plr.getCurrweapon()][0].curry + snakey + 8;
                                dax = weaponanimtics[plr.getCurrweapon()][0].currx + snakex + 8;
                            } else {
                                day = zweaponanimtics[plr.getCurrweapon()][0].curry + snakey + 8;
                                dax = zweaponanimtics[plr.getCurrweapon()][0].currx + snakex + 8;
                            }
                        } else {
                            day = weaponanimtics[plr.getCurrweapon()][0].curry + snakey + 8;
                            dax = weaponanimtics[plr.getCurrweapon()][0].currx + snakex + 8;
                        }
                    }
                } else {
                    if (game.WH2) {
                        if (plr.getCurrweaponframe() == BOWREADYEND) {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1) {
                                day = readyanimtics[plr.getCurrweapon()][6].curry + 3;
                                dax = readyanimtics[plr.getCurrweapon()][6].currx + 3;
                            } else {
                                day = zreadyanimtics[plr.getCurrweapon()][6].curry + 3;
                                dax = zreadyanimtics[plr.getCurrweapon()][6].currx + 3;
                            }
                        } else {
                            if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                                dax = weaponanimtics[plr.getCurrweapon()][0].currx + 3;
                                day = weaponanimtics[plr.getCurrweapon()][0].curry + 3;
                            } else {
                                dax = zweaponanimtics[plr.getCurrweapon()][0].currx + 3;
                                day = zweaponanimtics[plr.getCurrweapon()][0].curry + 3;
                            }
                        }
                    } else {
                        if (plr.getCurrweaponframe() == BOWREADYEND) {

                            day = readyanimtics[plr.getCurrweapon()][6].curry + 3;
                            dax = readyanimtics[plr.getCurrweapon()][6].currx + 3;
                        } else {
                            dax = weaponanimtics[plr.getCurrweapon()][0].currx + 3;
                            day = weaponanimtics[plr.getCurrweapon()][0].curry + 3;
                        }
                    }
                }

                if (plr.getCurrweapon() == 0 && plr.getCurrweaponframe() != 0) {
                    overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                    overwritesprite(0, day + 8, plr.getCurrweaponframe() + 6, dashade, dabits, dapalnum);
                } else if (plr.getCurrweaponframe() != 0) {
                    overwritesprite(dax + snakex, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                }
                break;
            }
            case 2: { // unready
                int dax, day;
                if (game.WH2) {
                    if (plr.getCurrweaponframe() == BOWREADYEND) {
                        day = readyanimtics[plr.getCurrweapon()][6].curry + (plr.getWeapondrop());
                        dax = readyanimtics[plr.getCurrweapon()][6].currx;
                    } else if (plr.getCurrweaponframe() == ZBOWWALK) {
                        day = zreadyanimtics[plr.getCurrweapon()][6].curry + (plr.getWeapondrop());
                        dax = zreadyanimtics[plr.getCurrweapon()][6].currx;
                    } else {
                        if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                            dax = weaponanimtics[plr.getCurrweapon()][0].currx;
                            day = weaponanimtics[plr.getCurrweapon()][0].curry + (plr.getWeapondrop());
                        } else {
                            dax = zweaponanimtics[plr.getCurrweapon()][0].currx;
                            day = zweaponanimtics[plr.getCurrweapon()][0].curry + (plr.getWeapondrop());
                        }
                    }
                } else {
                    if (plr.getCurrweaponframe() == BOWREADYEND) {
                        day = readyanimtics[plr.getCurrweapon()][6].curry + (plr.getWeapondrop());
                        dax = readyanimtics[plr.getCurrweapon()][6].currx;
                    } else {
                        dax = weaponanimtics[plr.getCurrweapon()][0].currx;
                        day = weaponanimtics[plr.getCurrweapon()][0].curry + (plr.getWeapondrop());
                    }
                }

                if (plr.getCurrweapon() == 0 && plr.getCurrweaponframe() != 0) {
                    overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                    overwritesprite(0, day, plr.getCurrweaponframe() + 6, dashade, dabits, dapalnum);
                } else if (plr.getCurrweaponframe() != 0) {
                    dax = weaponanimtics[plr.getCurrweapon()][0].currx;
                    overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                }
                break;
            }
            case 3: { // ready
                int dax, day;
                if (game.WH2) {
                    if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                        dax = readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                        day = readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].curry + 8;
                    } else {
                        dax = zreadyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                        day = zreadyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].curry + 8;
                    }
                } else {
                    dax = readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].currx;
                    day = readyanimtics[plr.getCurrweapon()][plr.getCurrweaponanim()].curry + 8;
                }

                if (plr.getCurrweapon() == 0 && plr.getCurrweaponframe() != 0) {
                    overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                    overwritesprite(0, day, plr.getCurrweaponframe() + 6, dashade, dabits, dapalnum);
                } else if (plr.getCurrweaponframe() != 0) {
                    overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                }
                break;
            }
            case 5: { // cock
                int dax, day;
                if (game.WH2) {
                    if (plr.getWeapon()[plr.getCurrweapon()] == 1 || plr.getWeapon()[plr.getCurrweapon()] == 2) { // #GDX 09.06.2024 was weapon[7] == 2
                        dax = cockanimtics[plr.getCurrweaponanim()].currx;
                        day = cockanimtics[plr.getCurrweaponanim()].curry + 8;
                    } else {
                        dax = zcockanimtics[plr.getCurrweaponanim()].currx;
                        day = zcockanimtics[plr.getCurrweaponanim()].curry + 8;
                    }
                } else {
                    dax = cockanimtics[plr.getCurrweaponanim()].currx;
                    day = cockanimtics[plr.getCurrweaponanim()].curry + 8;
                }
                if (plr.getCurrweaponframe() != 0) {
                    overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                }
                break;
            }
            case 4: { // throw the orb
                int dax, day;
                if (game.WH2) {
                    dax = wh2throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].currx;
                    day = wh2throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].curry + 8;
                } else {
                    dax = throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].currx;
                    day = throwanimtics[plr.getCurrentorb()][plr.getCurrweaponanim()].curry + 8;
                }
                if (plr.getCurrweaponframe() != 0) {
                    overwritesprite(dax, day, plr.getCurrweaponframe(), dashade, dabits, dapalnum);
                }
                break;
            }
        }

        // shield stuff
        if (plr.getShieldpoints() > 0 && (plr.getCurrweaponfired() == 0 || plr.getCurrweaponfired() == 1) && plr.getSelectedgun() > 0 && plr.getSelectedgun() < 5) {
            dabits &= ~512;
            dabits |= 256; // #GDX 12.07.2024 Shield draw fix
            if (plr.getShieldtype() == 1) {
                if (plr.getShieldpoints() > 75) {
                    overwritesprite(-40 + snakex, 100 + snakey, GRONSHIELD, dashade, dabits, dapalnum);
                } else if (plr.getShieldpoints() > 50) {
                    overwritesprite(-40 + snakex, 100 + snakey, GRONSHIELD + 1, dashade, dabits, dapalnum);
                } else if (plr.getShieldpoints() > 25) {
                    overwritesprite(-40 + snakex, 100 + snakey, GRONSHIELD + 2, dashade, dabits, dapalnum);
                } else {
                    overwritesprite(-40 + snakex, 100 + snakey, GRONSHIELD + 3, dashade, dabits, dapalnum);
                }
            } else {
                if (plr.getShieldpoints() > 150) {
                    overwritesprite(-40 + snakex, 100 + snakey, ROUNDSHIELD, dashade, dabits, dapalnum);
                } else if (plr.getShieldpoints() > 100) {
                    overwritesprite(-40 + snakex, 100 + snakey, ROUNDSHIELD + 1, dashade, dabits, dapalnum);
                } else if (plr.getShieldpoints() > 50) {
                    overwritesprite(-40 + snakex, 100 + snakey, ROUNDSHIELD + 2, dashade, dabits, dapalnum);
                } else {
                    overwritesprite(-40 + snakex, 100 + snakey, ROUNDSHIELD + 3, dashade, dabits, dapalnum);
                }
            }
        }
    }

    public static void analizesprites(PLAYER plr, int dasmoothratio) {
        WHRenderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        for (int i = renderedSpriteList.getSize() - 1; i >= 0; i--) {
            Sprite tspr = renderedSpriteList.get(i);

            if (isItemSprite(tspr) && !isWeaponSprite(tspr)) { // #GDX 12.07.2024 Items shouldn't be wall oriented, instead of weapons (WH2)
                int cstat = tspr.getCstat();
                tspr.setCstat(cstat & ~16);
            }

            if (plr.getNightglowtime() <= 0 && (tspr.getDetail() != 0 && tspr.getDetail() < MAXTYPES)
                    || tspr.getStatnum() == DEAD) { // #GDX 12.07.2024 Dead enemies too
                //enemies' detail only
                if (((tspr.getDetail() != GONZOTYPE && tspr.getPicnum() != GONZOGSHDEAD) || tspr.getShade() != 31) && tspr.getStatnum() != EVILSPIRIT) {
                    Sector sec = boardService.getSector(tspr.getSectnum());
                    if (sec != null) {
                        tspr.setShade(sec.getFloorshade());
                    }
                }
            }

            ILoc oldLoc = game.pInt.getsprinterpolate(tspr.getOwner());
            // only interpolate certain moving things
            if (oldLoc != null && (tspr.getHitag() & 0x0200) == 0) {
                int x = oldLoc.x;
                int y = oldLoc.y;
                int z = oldLoc.z;
                short nAngle = oldLoc.ang;

                // interpolate sprite position
                x += mulscale(tspr.getX() - oldLoc.x, dasmoothratio, 16);
                y += mulscale(tspr.getY() - oldLoc.y, dasmoothratio, 16);
                z += mulscale(tspr.getZ() - oldLoc.z, dasmoothratio, 16);
                nAngle += (short) mulscale(((tspr.getAng() - oldLoc.ang + 1024) & 2047) - 1024, dasmoothratio, 16);

                tspr.setX(x);
                tspr.setY(y);
                tspr.setZ(z);
                tspr.setAng(nAngle);
            }

            Sprite spr = boardService.getSprite(tspr.getOwner());
            if (spr != null) {
                switch (spr.getDetail()) {
                    case GRONTYPE:
                        if (tspr.getPicnum() == GRONHAL || tspr.getPicnum() == GRONSW || tspr.getPicnum() == GRONSWATTACK || tspr.getPicnum() == GRONMU) {
                            int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                            k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                            if (k <= 4) {
                                tspr.setPicnum(tspr.getPicnum() + (k << 2));
                                tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                            } else {
                                tspr.setPicnum(tspr.getPicnum() + ((8 - k) << 2));
                                tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                            }
                        } else {
                            switch (tspr.getPicnum()) {
                                case WH1GRONMUATTACK:
                                case WH2GRONMUATTACK: {
                                    if (tspr.getPicnum() != GRONMUATTACK) {
                                        break;
                                    }
                                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                                    if (k <= 4) {
                                        tspr.setPicnum(tspr.getPicnum() + (k * 6));
                                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                                    } else {
                                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 6));
                                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                                    }
                                    break;
                                }
                                case WH1GRONHALATTACK:
                                case WH2GRONHALATTACK: {
                                    if (tspr.getPicnum() != GRONHALATTACK) {
                                        break;
                                    }
                                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                                    if (k <= 4) {
                                        tspr.setPicnum(tspr.getPicnum() + (k * 7));
                                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                                    } else {
                                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 7));
                                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                                    }
                                    break;
                                }
                            }
                        }

                        continue;
                    case GOBLINTYPE:
                        if (game.WH2) {
                            continue;
                        }

                        switch (tspr.getPicnum()) {
                            case GOBLINSTAND:
                            case GOBLIN:
                            case GOBLINATTACK:
                                int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                                k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                                if (k <= 4) {
                                    tspr.setPicnum(tspr.getPicnum() + (k << 2));
                                    tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                                } else {
                                    tspr.setPicnum(tspr.getPicnum() + ((8 - k) << 2));
                                    tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                                }
                                break;
                        }
                        continue;
                    case IMPTYPE:
                        if (!game.WH2) {
                            continue;
                        }

                        int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                        k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                        if (tspr.getPicnum() == IMP) {
                            if (k <= 4) {
                                tspr.setPicnum(tspr.getPicnum() + (k * 6));
                                tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                            } else {
                                tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 6));
                                tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                            }
                        }
                        continue;
                }
            }
            switch (tspr.getPicnum()) {
                case MONSTERBALL:
                case EXPLOSION:
                case PLASMA:
                case ICECUBE:
                case BULLET:
                case DISTORTIONBLAST:
                case WH1WILLOW:
                case WH2WILLOW:
                    if ((tspr.getPicnum() == WH1WILLOW && game.WH2) || (tspr.getPicnum() == WH2WILLOW && !game.WH2)) {
                        break;
                    }
                    tspr.setShade(-128);
                    break;
                case DEVILSTAND:
                case DEVIL:
                case DEVILATTACK:
                case SKULLY:
                case FATWITCH:
                case JUDY:
                case FREDSTAND:
                case FRED:
                case FREDATTACK:
                case MINOTAUR:
                case MINOTAURATTACK: {
                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k << 2));
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) << 2));
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    }
                    break;
                }
                case WH2SKELETON:
                case WH2SKELETONATTACK: {
                    if (game.WH2) {
                        int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                        k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                        if (k <= 4) {
                            tspr.setPicnum(tspr.getPicnum() + (k * 6));
                            tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                        } else {
                            tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 6));
                            tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                        }
                    }
                    break;
                }
                case WH1SKELETON:
                case WH1SKELETONATTACK:
                case KOBOLD:
                case KURTAT: {
                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k * 5));
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 5));
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    }
                    break;
                }
                case KATIE:
                case KATIEAT: {
                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k * 5));
                        // tspr.cstat &= ~4; //clear x-flipping bit
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 5));
                        // tspr.cstat |= 4; //set x-flipping bit
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    }
                    break;
                }
                case NEWGUY:
                case NEWGUYBOW:
                case NEWGUYMACE:

                case GONZOCSW:
                case GONZOCSWAT: {
                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k * 6));
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 6));
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    }
                    break;
                }
                case GONZOGSW:
                case GONZOGHM: {
                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k * 6));
                        // tspr.cstat &= ~4; //clear x-flipping bit
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 6));
                        // tspr.cstat |= 4; //set x-flipping bit
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit

                    }
                    break;
                }
                case GONZOGSH: {
                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;

                    tspr.setPicnum(tspr.getPicnum() + (k * 6));
                    tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    break;
                }
                case NEWGUYCAST:
                case NEWGUYPUNCH:
                case KURTPUNCH: {
                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k * 3));
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 3));
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    }
                    break;
                }
                case WH1RAT:
                case WH2RAT: {
                    if ((tspr.getPicnum() == WH1RAT && game.WH2) || (tspr.getPicnum() == WH2RAT && !game.WH2)) {
                        break;
                    }
                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k * 2));
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 2));
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    }
                    break;
                }
                case SPIDER: {
                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k << 3));
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) << 3));
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    }
                    break;
                }
                case NEWGUYSTAND:
                case NEWGUYKNEE:
                case KURTSTAND:
                case KURTKNEE:
                case WH1GUARDIAN:
                case WH2GUARDIAN: {
                    if ((tspr.getPicnum() == WH1GUARDIAN && game.WH2) || (tspr.getPicnum() == WH2GUARDIAN && !game.WH2)) {
                        break;
                    }

                    int k = EngineUtils.getAngle(tspr.getX() - plr.getX(), tspr.getY() - plr.getY());
                    k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + k);
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + (8 - k));
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    }
                    break;
                }
                // ITEMS

                case SILVERBAG:
                case SILVERCOINS:
                case GOLDBAG:
                case GOLDBAG2:
                case GOLDCOINS:
                case GOLDCOINS2:
                case GIFTBOX:
                case FLASKBLUE:
                case FLASKRED:
                case FLASKGREEN:
                case FLASKOCHRE:
                case FLASKTAN:
                case DIAMONDRING:
                case SHADOWAMULET:
                case GLASSSKULL:
                case AHNK:
                case BLUESCEPTER:
                case YELLOWSCEPTER:
                case ADAMANTINERING:
                case ONYXRING:
                case SAPHIRERING:
                case WALLBOW:
                case WALLSWORD:
                case WEAPON3A:
                case WEAPON3:
                case WALLAXE:
                case GONZOBSHIELD:
                case GONZOCSHIELD:
                case GONZOGSHIELD:
                case WALLPIKE:
                    tspr.setShade(tspr.getShade() - 16);
                    break;
                default:
                    int p = tspr.getPicnum();
                    if (p == CRYSTALSTAFF || p == AMULETOFTHEMIST || p == HORNEDSKULL || p == THEHORN || p == HELMET || p == PLATEARMOR || p == CHAINMAIL || p == LEATHERARMOR || p == BRASSKEY || p == BLACKKEY || p == GLASSKEY || p == IVORYKEY || p == SCROLLSCARE || p == SCROLLNIGHT || p == SCROLLFREEZE || p == SCROLLMAGIC || p == SCROLLOPEN || p == SCROLLFLY || p == SCROLLFIREBALL || p == SCROLLNUKE || p == QUIVER || p == BOW || p == WEAPON1 || p == WEAPON1A || p == GOBWEAPON || p == WEAPON2 || p == WEAPON4 || p == THROWHALBERD || p == WEAPON5 || p == SHIELD || p == WEAPON5B || p == THROWPIKE || p == WEAPON6 || p == WEAPON7 || p == PENTAGRAM) {
                        tspr.setShade(tspr.getShade() - 16);
                        if (p == PENTAGRAM) {
                            Sector sec = boardService.getSector(tspr.getSectnum());
                            if (sec != null && sec.getLotag() == 4002 && plr.getTreasure()[TPENTAGRAM] == 0) {
                                tspr.setCstat(tspr.getCstat() | 514);
                            }
                        }
                    }
                    break;
            }

            if (tspr.getDetail() != 0 && tspr.getStatnum() != 99 && (tspr.getDetail() & 0xFF) != SPIKEBLADETYPE) {
                if (renderedSpriteList.getSize() < (MAXSPRITESONSCREEN - 2)) {
                    int fz = engine.getflorzofslope(tspr.getSectnum(), tspr.getX(), tspr.getY());
                    if (fz > plr.getZ()) {
                        short siz = (short) Math.max((tspr.getXrepeat() - ((fz - tspr.getZ()) >> 10)), 1);
                        if (siz > 4) {
                            Sprite tshadow = renderedSpriteList.obtain();
                            tshadow.set(tspr);
                            int camangle = EngineUtils.getAngle(plr.getX() - tshadow.getX(), plr.getY() - tshadow.getY());
                            tshadow.setX(tshadow.getX() - mulscale(EngineUtils.sin((camangle + 512) & 2047), 100, 16));
                            tshadow.setY(tshadow.getY() + mulscale(EngineUtils.sin((camangle + 1024) & 2047), 100, 16));
                            tshadow.setZ(fz + 1);
                            tshadow.setStatnum(99);

                            tshadow.setXrepeat(siz);
                            tshadow.setYrepeat((short) (tspr.getYrepeat() >> 3));
                            if (tshadow.getYrepeat() < 4) {
                                tshadow.setYrepeat(4);
                            }

                            tshadow.setShade(127);
                            tshadow.setCstat(tshadow.getCstat() | 2);
                        }
                    }
                }
            }
        }
    }

    public static void drawInterface(PLAYER plr) {
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();
        int hudscale = cfg.gHudScale;
        if (cfg.gViewSize == 1) {
            drawhud(plr, renderer.getWidth() / 2, renderer.getHeight() + 1, hudscale);
        }
        if (plr.getPotion()[0] == 0 && plr.getHealth() > 0 && plr.getHealth() < 21) {
            game.getFont(1).drawTextScaled(renderer, 160, 5, "health critical", 1.0f, 0, 7, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        if (justwarpedfx > 0) {
            renderer.rotatesprite(320 << 15, 200 << 15, justwarpedcnt << 9, 0, ANNIHILATE, 0, 0, 1 + 2);
        }

        int pwpos = 0;

        if (plr.getHelmettime() > 0) {
            renderer.rotatesprite(300 << 16, (pwpos += engine.getTile(HELMET).getHeight() >> 2) << 16, 16384, 0, HELMET, 0, 0, 2 + 8 + 512);
            pwpos += 10;
        }

        if (plr.getVampiretime() > 0) {
            renderer.rotatesprite(300 << 16, (pwpos += engine.getTile(THEHORN).getHeight() / 6) << 16, 12000, 0, THEHORN, 0, 0, 2 + 8 + 512);
            pwpos += 10;
        }

        if (plr.getOrbactive()[5] > 0) {
            renderer.rotatesprite(300 << 16, (pwpos += engine.getTile(SCROLLFLY).getHeight() >> 2) << 16, 16384, 0, SCROLLFLY, 0, 0, 2 + 8 + 512);
            pwpos += 10;
        }

        if (plr.getShadowtime() > 0) {
            renderer.rotatesprite(300 << 16, (pwpos += engine.getTile(SCROLLSCARE).getHeight() >> 2) << 16, 16384, 0, SCROLLSCARE, 0, 0, 2 + 8 + 512);
            pwpos += 10;
        }

        if (plr.getNightglowtime() > 0) {
            renderer.rotatesprite(300 << 16, pwpos + (engine.getTile(SCROLLNIGHT).getHeight() >> 2) << 16, 16384, 0, SCROLLNIGHT, 0, 0, 2 + 8 + 512);
        }

        boolean message = cfg.MessageState && displaytime > 0;

        if (message) {
            game.getFont(1).drawTextScaled(renderer, 5, 5, displaybuf, 1.0f, 0, 7, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
        }

        int amposx = 10;
        int amposy = 7;
        if (message) {
            amposy += 15;
            if (dimension == 2) {
                amposy += 10;
            }
            if (dimension == 2 && followmode) {
                amposy += 10;
            }
        }

        if (plr.getTreasure()[TONYXRING] != 0) {
            renderer.rotatesprite(amposx << 16, amposy << 16, 16384, 0, ONYXRING, 0, 0, 2 | 8 | 256);
            amposx += 20;
        }

        if (plr.getTreasure()[TAMULETOFTHEMIST] != 0 && plr.getInvisibletime() > 0) {
            renderer.rotatesprite(amposx << 16, amposy << 16, 16384, 0, AMULETOFTHEMIST, 0, 0, 2 | 8 | 256);
            amposx += 20;
        }

        if (plr.getTreasure()[TADAMANTINERING] != 0) {
            renderer.rotatesprite(amposx << 16, amposy << 16, 16384, 0, ADAMANTINERING, 0, 0, 2 | 8 | 256);
            amposx += 20;
        }

        if (plr.getTreasure()[TBLUESCEPTER] != 0) {
            renderer.rotatesprite(amposx << 16, (amposy + 9) << 16, 16384, 0, BLUESCEPTER, 0, 0, 2 | 8 | 256, 0, 0, xdim - 1, Gameutils.coordsConvertYScaled(amposy + 4));
            amposx += 20;
        }

        if (plr.getTreasure()[TYELLOWSCEPTER] != 0) {
            renderer.rotatesprite(amposx << 16, (amposy + 9) << 16, 16384, 0, YELLOWSCEPTER, 0, 0, 2 | 8 | 256, 0, 0, xdim - 1, Gameutils.coordsConvertYScaled(amposy + 4));
        }

        if (cfg.gCrosshair) {
            int col = 17;
            renderer.drawline256((xdim - mulscale(cfg.gCrossSize, 16, 16)) << 11, ydim << 11, (xdim - mulscale(cfg.gCrossSize, 4, 16)) << 11, ydim << 11, col);
            renderer.drawline256((xdim + mulscale(cfg.gCrossSize, 4, 16)) << 11, ydim << 11, (xdim + mulscale(cfg.gCrossSize, 16, 16)) << 11, ydim << 11, col);
            renderer.drawline256(xdim << 11, (ydim - mulscale(cfg.gCrossSize, 16, 16)) << 11, xdim << 11, (ydim - mulscale(cfg.gCrossSize, 4, 16)) << 11, col);
            renderer.drawline256(xdim << 11, (ydim + mulscale(cfg.gCrossSize, 4, 16)) << 11, xdim << 11, (ydim + mulscale(cfg.gCrossSize, 16, 16)) << 11, col);
        }

        if (plr.getSpiked() != 0) {
            spikeanimation(plr);
        }

        int y = renderer.getHeight() - 20;
        if (cfg.gViewSize == 1) {
            y -= mulscale(engine.getTile(SSTATUSBAR).getHeight(), hudscale, 16);
        }

        if (cfg.gShowStat == 1 || (cfg.gShowStat == 2 && dimension == 2)) {
            drawStatistics(10, y, cfg.gStatSize);
        }

        if (game.gPaused) {
            game.getFont(0).drawTextScaled(renderer, 160, 5, "Pause", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
        }
    }

    public static void drawStatistics(int x, int y, int zoom) {
        float viewzoom = (zoom / 65536.0f);
        Font font = game.getFont(1);
        Renderer renderer = game.getRenderer();

        buildString(buffer, 0, "kills: ");

        int yoffset = (int) (4 * (font.getSize()) * viewzoom);
        y -= yoffset;

        int staty = y;

        font.drawText(renderer, x, staty, buffer, viewzoom, 0, 7, TextAlign.Left, Transparent.None, false);

        int alignx = font.getWidth(buffer, viewzoom);

        int offs = Bitoa(kills, buffer);
        buildString(buffer, offs, " / ", killcnt);
        font.drawText(renderer, x + (alignx + 2), staty, buffer, viewzoom, 0, 0, TextAlign.Left, Transparent.None, false);

        staty = y + (int) (15 * viewzoom);

        buildString(buffer, 0, "treasures: ");

        font.drawText(renderer, x, staty, buffer, viewzoom, 0, 7, TextAlign.Left, Transparent.None, false);

        alignx = font.getWidth(buffer, viewzoom);

        offs = Bitoa(treasuresfound, buffer, 2);
        buildString(buffer, offs, " / ", treasurescnt, 2);

        font.drawText(renderer, x + (alignx + 2), staty, buffer, viewzoom, 0, 0, TextAlign.Left, Transparent.None, false);

        staty = y + (int) (30 * viewzoom);

        buildString(buffer, 0, "time: ");

        font.drawText(renderer, x, staty, buffer, viewzoom, 0, 7, TextAlign.Left, Transparent.None, false);
        alignx = font.getWidth(buffer, viewzoom);

        offs = Bitoa(minutes, buffer, 2);
        buildString(buffer, offs, " : ", seconds, 2);

        font.drawText(renderer, x + (alignx + 2), staty, buffer, viewzoom, 0, 0, TextAlign.Left, Transparent.None, false);
    }

    public static void drawhud(PLAYER plr, int x, int y, int scale) {
//		int frames = xdim / tilesizx[2278];
//		int hx = 0;
//		for (int i = 0; i <= (frames + 1); i++) {
//			renderer.rotatesprite(hx << 16, (windowy2 - engine.getTile(2278).getHeight() / 2 + 5) << 16, 0x10000, 0, 2278, 0, 0, 8 + 256, 0,
//					0, xdim - 1, ydim - 1);
//			hx += tilesizx[2278];
//		}
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(x << 16, (y << 16) - renderer.getTile(SSTATUSBAR).getHeight() * scale / 2, scale, 0, SSTATUSBAR, 0, 0, 8);
        updatepics(plr, x, y, scale);

        int bookpic = plr.getSpellbook();
        if (plr.getSpellbookflip() == 0) {
            bookpic = 8;
        }

        if (bookpic < sspellbookanim[plr.getCurrentorb()].length && (plr.getOrbammo()[plr.getCurrentorb()] > 0 || plr.getCurrweaponfired() == 4)) {
            int spellbookframe = sspellbookanim[plr.getCurrentorb()][bookpic].daweaponframe;
            int dax = x + mulscale(sspellbookanim[plr.getCurrentorb()][bookpic].currx, scale, 16);
            int day = y + mulscale(sspellbookanim[plr.getCurrentorb()][bookpic].curry, scale, 16);

            renderer.rotatesprite(dax << 16, day << 16, scale, 0, spellbookframe, 0, 0, 8 | 16);
            Bitoa(plr.getOrbammo()[plr.getCurrentorb()], tempchar);
            game.getFont(4).drawText(renderer, x - mulscale(67, scale, 16), y - mulscale(39, scale, 16), tempchar, scale / 65536.0f, 0, 0, TextAlign.Left, Transparent.None, false);
        }
    }
}
