package ru.m210projects.Witchaven;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Witchaven.Types.PLAYER;
import ru.m210projects.Witchaven.Types.WEAPON;

import static ru.m210projects.Build.Gameutils.BClampAngle;
import static ru.m210projects.Witchaven.AI.Ai.*;
import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.WH1Names.*;
import static ru.m210projects.Witchaven.WH2Names.*;
import static ru.m210projects.Witchaven.WHOBJ.newstatus;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.showmessage;
import static ru.m210projects.Witchaven.Weapons.*;

public class Spellbooks {

    public static final WEAPON[][] sspellbookanim = {
            // SCARE
            {new WEAPON(8, SSPELLBOOK8, -78, -96), new WEAPON(8, SSPELLBOOK8 + 1, -78, -104),
                    new WEAPON(8, SSPELLBOOK8 + 2, -78, -100), new WEAPON(8, SSPELLBOOK8 + 3, -78, -98),
                    new WEAPON(8, SSPELLBOOK8 + 4, -78, -99), new WEAPON(8, SSPELLBOOK8 + 5, -78, -99),
                    new WEAPON(8, SSPELLBOOK8 + 6, -78, -99), new WEAPON(8, SSPELLBOOK8 + 7, -78, -98),
                    new WEAPON(8, SSPELLBOOK8 + 7, -78, -98),},
            // NIGHT VISION
            {new WEAPON(8, SSPELLBOOK6, -78, -96), new WEAPON(8, SSPELLBOOK6 + 1, -78, -104),
                    new WEAPON(8, SSPELLBOOK6 + 2, -78, -101), new WEAPON(8, SSPELLBOOK6 + 3, -78, -99),
                    new WEAPON(8, SSPELLBOOK6 + 4, -78, -98), new WEAPON(8, SSPELLBOOK6 + 5, -78, -99),
                    new WEAPON(8, SSPELLBOOK6 + 6, -78, -99), new WEAPON(8, SSPELLBOOK6 + 7, -78, -98),
                    new WEAPON(8, SSPELLBOOK6 + 7, -78, -98),},
            // FREEZE
            {new WEAPON(8, SSPELLBOOK3, -78, -99), new WEAPON(8, SSPELLBOOK3 + 1, -78, -104),
                    new WEAPON(8, SSPELLBOOK3 + 2, -78, -101), new WEAPON(8, SSPELLBOOK3 + 3, -78, -99),
                    new WEAPON(8, SSPELLBOOK3 + 4, -78, -98), new WEAPON(8, SSPELLBOOK3 + 5, -78, -97),
                    new WEAPON(8, SSPELLBOOK3 + 6, -78, -99), new WEAPON(8, SSPELLBOOK3 + 7, -78, -99),
                    new WEAPON(8, SSPELLBOOK3 + 7, -78, -99),},
            // MAGIC ARROW
            {new WEAPON(8, SSPELLBOOKBLANK, -78, -98), new WEAPON(8, SSPELLBOOKBLANK + 1, -78, -104),
                    new WEAPON(8, SSPELLBOOKBLANK + 2, -78, -101), new WEAPON(8, SSPELLBOOKBLANK + 3, -78, -99),
                    new WEAPON(8, SSPELLBOOKBLANK + 4, -78, -98), new WEAPON(8, SSPELLBOOKBLANK + 5, -78, -97),
                    new WEAPON(8, SSPELLBOOKBLANK + 6, -78, -97), new WEAPON(8, SSPELLBOOKBLANK + 7, -78, -97),
                    new WEAPON(8, SSPELLBOOKBLANK + 7, -78, -97),},
            // OPEN DOORS
            {new WEAPON(8, SSPELLBOOK7, -78, -98), new WEAPON(8, SSPELLBOOK7 + 1, -78, -104),
                    new WEAPON(8, SSPELLBOOK7 + 2, -78, -101), new WEAPON(8, SSPELLBOOK7 + 3, -78, -99),
                    new WEAPON(8, SSPELLBOOK7 + 4, -78, -99), new WEAPON(8, SSPELLBOOK7 + 5, -78, -99),
                    new WEAPON(8, SSPELLBOOK7 + 6, -78, -97), new WEAPON(8, SSPELLBOOK7 + 7, -78, -98),
                    new WEAPON(8, SSPELLBOOK7 + 7, -78, -98),},
            // FLY
            {new WEAPON(8, SSPELLBOOK2, -78, -98), new WEAPON(8, SSPELLBOOK2 + 1, -78, -104),
                    new WEAPON(8, SSPELLBOOK2 + 2, -78, -101), new WEAPON(8, SSPELLBOOK2 + 3, -78, -99),
                    new WEAPON(8, SSPELLBOOK2 + 4, -78, -96), new WEAPON(8, SSPELLBOOK2 + 5, -78, -98),
                    new WEAPON(8, SSPELLBOOK2 + 6, -78, -96), new WEAPON(8, SSPELLBOOK2 + 7, -78, -98),
                    new WEAPON(8, SSPELLBOOK2 + 7, -78, -98),},
            // FIRE BALL
            {new WEAPON(8, SSPELLBOOK4, -78, -97), new WEAPON(8, SSPELLBOOK4 + 1, -78, -104),
                    new WEAPON(8, SSPELLBOOK4 + 2, -78, -101), new WEAPON(8, SSPELLBOOK4 + 3, -78, -99),
                    new WEAPON(8, SSPELLBOOK4 + 4, -78, -94), new WEAPON(8, SSPELLBOOK4 + 5, -78, -95),
                    new WEAPON(8, SSPELLBOOK4 + 6, -78, -97), new WEAPON(8, SSPELLBOOK4 + 6, -78, -97),
                    new WEAPON(8, SSPELLBOOK4 + 6, -78, -97),},
            // NUKE!
            {new WEAPON(8, SSPELLBOOK5, -78, -99), new WEAPON(8, SSPELLBOOK5 + 1, -78, -104),
                    new WEAPON(8, SSPELLBOOK5 + 2, -78, -99), new WEAPON(8, SSPELLBOOK5 + 3, -78, -99),
                    new WEAPON(8, SSPELLBOOK5 + 4, -78, -98), new WEAPON(8, SSPELLBOOK5 + 5, -78, -96),
                    new WEAPON(8, SSPELLBOOK5 + 6, -78, -100), new WEAPON(8, SSPELLBOOK5 + 6, -78, -100),
                    new WEAPON(8, SSPELLBOOK5 + 6, -78, -100)}};

    public static void activatedaorb(PLAYER plr) {
        if (plr.getOrbammo()[plr.getCurrentorb()] <= 0) {
            return;
        }

        switch (plr.getCurrentorb()) {
            case 0: // SCARE
                // shadowtime=1200+(plr.lvl*120);
                break;
            case 1: // NIGHT VISION
                // nightglowtime=2400+(plr.lvl*600);
                break;
            case 2: // FREEZE
            case 4: // OPEN DOORS
            case 6: // FIREBALL
            case 7: // NUKE
            case 3: // MAGIC ARROW
                plr.getOrbactive()[plr.getCurrentorb()] = -1;
                break;
            case 5: // FLY
                // plr.orbactive[currentorb]=3600+(plr.lvl*600);
                break;
        }

        if (plr.getOrbammo()[plr.getCurrentorb()] <= 0) {
            plr.getOrb()[plr.getCurrentorb()] = 0;
            return;
        } else {
            plr.getOrbammo()[plr.getCurrentorb()]--;
        }

        plr.setCurrweaponfired(4);
        plr.setCurrweapontics(game.WH2 ? wh2throwanimtics[plr.getCurrentorb()][0].daweapontics : throwanimtics[plr.getCurrentorb()][0].daweapontics);
    }

    public static void castaorb(PLAYER plr) {
        switch (plr.getCurrentorb()) {
            case 0: // SCARE
                if (game.WH2) {
                    playsound_loc(S_GENERALMAGIC4, plr.getX(), plr.getY());
                }
                plr.setShadowtime(((plr.getLvl() + 1) * 120) << 2);
                break;
            case 1: // NIGHTVISION
                plr.setNightglowtime(3600 + (plr.getLvl() * 120));
                break;
            case 2: // FREEZE
                if (game.WH2) {
                    playsound_loc(S_GENERALMAGIC3, plr.getX(), plr.getY());
                } else {
                    playsound_loc(S_SPELL1, plr.getX(), plr.getY());
                }
                shootgun(plr, plr.getAng(), 6);
                break;
            case 3: // MAGIC ARROW
                if (game.WH2) {
                    lockon(plr, 10, 2);
                    playsound_loc(S_GENERALMAGIC2, plr.getX(), plr.getY());
                } else {
                    float daang = BClampAngle(plr.getAng() - 36);
                    for (int k = 0; k < 10; k++) {
                        daang = BClampAngle(daang + (k << 1));
                        shootgun(plr, daang, 2);
                    }
                    playsound_loc(S_SPELL1, plr.getX(), plr.getY());
                }
                break;
            case 4: // OPEN DOORS
                shootgun(plr, plr.getAng(), 7);
                if (game.WH2) {
                    playsound_loc(S_DOORSPELL, plr.getX(), plr.getY());
                } else {
                    playsound_loc(S_SPELL1, plr.getX(), plr.getY());
                }
                break;
            case 5: // FLY
                plr.getOrbactive()[plr.getCurrentorb()] = 3600 + (plr.getLvl() * 120);
                if (game.WH2) {
                    playsound_loc(S_GENERALMAGIC1, plr.getX(), plr.getY());
                } else {
                    playsound_loc(S_SPELL1, plr.getX(), plr.getY());
                }
                break;
            case 6: // FIREBALL
                if (game.WH2) {
                    lockon(plr, 3, 3);
                    playsound_loc(S_FIRESPELL, plr.getX(), plr.getY());
                } else {
                    shootgun(plr, plr.getAng(), 3);
                    playsound_loc(S_SPELL1, plr.getX(), plr.getY());
                }
                break;
            case 7: // NUKE
                shootgun(plr, plr.getAng(), 4);
                if (game.WH2) {
                    playsound_loc(S_NUKESPELL, plr.getX(), plr.getY());
                } else {
                    playsound_loc(S_SPELL1, plr.getX(), plr.getY());
                }
                break;
        }
    }

    public static void spellswitch(PLAYER plr, int j) {
        int i = plr.getCurrentorb();
        while (i >= 0 && i < MAXNUMORBS) {
            i += j;

            if (i == -1) {
                i = MAXNUMORBS - 1;
            } else if (i == MAXNUMORBS) {
                i = 0;
            }

            if (plr.getSpellbookflip() != 0 || i == plr.getCurrentorb()) {
                break;
            }

            if (changebook(plr, i)) {
                displayspelltext(plr);
                plr.setSpelltime(360);
                break;
            }
        }
    }

    public static void bookprocess(int snum) {
        PLAYER plr = player[snum];

        if (plr.getCurrweaponanim() == 0 && plr.getCurrweaponflip() == 0) {
            int bits = plr.getInput().bits;
            int spell = ((bits & (15 << 12)) >> 12) - 1;

            if (spell != -1 && spell < 10) {
                if (spell != 9 && spell != 8) {
                    if (changebook(plr, spell)) {
                        displayspelltext(plr);
                        plr.setSpelltime(360);
                    } else {
                        return;
                    }
                } else {
                    spellswitch(plr, spell == 9 ? -1 : 1);
                }
                plr.setOrbshot(0);
            }
        }

        for (int j = 0; j < MAXNUMORBS; j++) {
            if (plr.getOrbactive()[j] > -1) {
                plr.getOrbactive()[j] -= TICSPERFRAME;
            }
        }
    }

    public static boolean changebook(PLAYER plr, int i) {
        if (plr.getOrbammo()[i] <= 0 || plr.getCurrentorb() == i) {
            return false;
        }
        plr.setCurrentorb(i);
        if (plr.getSpellbookflip() == 0) {
            plr.setSpellbook(0);
            plr.setSpellbooktics(10);
            plr.setSpellbookflip(1);
            SND_Sound(S_PAGE);
            return true;
        }
        return false;
    }

    public static boolean lvlspellcheck(PLAYER plr) {
        if (game.WH2) {
            return true;
        }

        switch (plr.getCurrentorb()) {
            case 0:
            case 1:
                return true;
            case 2:
            case 3:
                if (plr.getLvl() > 1) {
                    return true;
                } else {
                    showmessage("must attain 2nd level", 360);
                }
                break;
            case 4:
            case 5:
                if (plr.getLvl() > 2) {
                    return true;
                } else {
                    showmessage("must attain 3rd level", 360);
                }
                break;

            case 6:
                if (plr.getLvl() > 3) {
                    return true;
                } else {
                    showmessage("must attain 4th level", 360);
                }
                break;
            case 7:
                if (plr.getLvl() > 4) {
                    return true;
                } else {
                    showmessage("must attain 5th level", 360);
                }
                break;
        }
        return false;
    }

    public static void speelbookprocess(PLAYER plr) {
        if (plr.getSpelltime() > 0) {
            plr.setSpelltime(plr.getSpelltime() - TICSPERFRAME);
        }

        if (plr.getSpellbookflip() == 1) {
            plr.setSpellbooktics(plr.getSpellbooktics() - TICSPERFRAME);
            if (plr.getSpellbooktics() < 0) {
                plr.setSpellbook(plr.getSpellbook() + 1);
            }
            if (plr.getSpellbook() > 8) {
                plr.setSpellbook(8);
            }
            if (plr.getSpellbook() == 8) {
                plr.setSpellbookflip(0);
            }
        }
    }

    public static void nukespell(PLAYER plr, final int j) {
        Sprite sprite = boardService.getSprite(j);
        if (sprite == null || sprite.getDetail() != WILLOWTYPE && sprite.getPal() == 6) { //don't nuke freezed enemies
            return;
        }

        if (game.WH2) {
            // dont nuke a shade
            if (sprite.getShade() > 30) {
                return;
            }

            newstatus(j, NUKED);
            sprite.setPal(0);
            sprite.setCstat(sprite.getCstat() | 1);
            sprite.setCstat(sprite.getCstat() & ~3);
            sprite.setShade(6);
            sprite.setLotag(360);
            sprite.setAng((short) plr.getAng());
            sprite.setHitag(0);
            addscore(plr, 150);

            int k = engine.insertsprite(sprite.getSectnum(), NUKED);
            Sprite spr = boardService.getSprite(k);
            if (spr != null) {
                spr.setLotag(360);
                spr.setXrepeat(30);
                spr.setYrepeat(12);
                spr.setPicnum(ZFIRE);
                spr.setPal(0);
                spr.setAng(sprite.getAng());
                spr.setX(sprite.getX());
                spr.setY(sprite.getY());
                spr.setZ(sprite.getZ());
                spr.setCstat(sprite.getCstat());
            }
            return;
        }

        switch (sprite.getDetail()) {
            case WILLOWTYPE:
            case SPIDERTYPE:
                engine.deletesprite((short) j);
                addscore(plr, 10);
                break;
            case KOBOLDTYPE:
                sprite.setPicnum(KOBOLDCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case DEVILTYPE:
                sprite.setPicnum(DEVILCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case GOBLINTYPE:
            case IMPTYPE:
                sprite.setPicnum(GOBLINCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case MINOTAURTYPE:
                sprite.setPicnum(MINOTAURCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case SKELETONTYPE:
                sprite.setPicnum(SKELETONCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case GRONTYPE:
                sprite.setPicnum(GRONCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case DRAGONTYPE:
                sprite.setPicnum(DRAGONCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case GUARDIANTYPE:
                sprite.setPicnum(GUARDIANCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case FATWITCHTYPE:
                sprite.setPicnum(FATWITCHCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case SKULLYTYPE:
                sprite.setPicnum(SKULLYCHAR);
                newstatus(j, NUKED);
                sprite.setPal(0);
                sprite.setCstat(sprite.getCstat() | 1);
                addscore(plr, 150);
                break;
            case JUDYTYPE:
                if (mapon < 24) {
                    sprite.setPicnum(JUDYCHAR);
                    newstatus(j, NUKED);
                    sprite.setPal(0);
                    sprite.setCstat(sprite.getCstat() | 1);
                    addscore(plr, 150);
                }
                break;
        }
    }

    public static void medusa(PLAYER plr, final int j) {
        Sprite sprite = boardService.getSprite(j);
        if (sprite == null || sprite.getHitag() <= 0) //don't freeze dead enemies
        {
            return;
        }

        newstatus(j, FROZEN);
        int pic = sprite.getPicnum();
        switch (sprite.getDetail()) {

            case NEWGUYTYPE:
                sprite.setPicnum(NEWGUYPAIN);
                break;
            case KURTTYPE:
                sprite.setPicnum(GONZOCSWPAIN);
                break;
            case GONZOTYPE:
                if (pic == GONZOCSW || pic == GONZOCSWAT) {
                    sprite.setPicnum(GONZOCSWPAIN);
                } else if (pic == GONZOGSW || pic == GONZOGSWAT) {
                    sprite.setPicnum(GONZOGSWPAIN);
                } else if (pic == GONZOGHM || pic == GONZOGHMAT
                        || pic == GONZOGSH || pic == GONZOGSHAT) {
                    sprite.setPicnum(GONZOGHMPAIN);
                }
                break;
            case KATIETYPE:
                sprite.setPicnum(KATIEPAIN);
                break;
            case KOBOLDTYPE:
                sprite.setPicnum(KOBOLDDIE);
                break;
            case DEVILTYPE:
                sprite.setPicnum(DEVILDIE);
                break;
            case FREDTYPE:
                sprite.setPicnum(FREDDIE);
                break;
            case GOBLINTYPE:
            case IMPTYPE:
                if (game.WH2) {
                    sprite.setPicnum(IMPDIE);
                } else {
                    sprite.setPicnum(GOBLINDIE);
                }
                break;
            case MINOTAURTYPE:
                sprite.setPicnum(MINOTAURDIE);
                break;
            case SPIDERTYPE:
                sprite.setPicnum(SPIDERDIE);
                break;
            case SKELETONTYPE:
                sprite.setPicnum(SKELETONDIE);
                break;
            case GRONTYPE:
                if (pic == GRONHAL || pic == GRONHALATTACK) {
                    sprite.setPicnum(GRONHALDIE);
                } else if (pic == GRONMU || pic == GRONMUATTACK) {
                    sprite.setPicnum(GRONMUDIE);
                } else if (pic == GRONSW || pic == GRONSWATTACK) {
                    sprite.setPicnum(GRONSWDIE);
                }
                break;
        }
        sprite.setPal(6);
        sprite.setCstat(sprite.getCstat() | 1);
        addscore(plr, 100);
    }

    public static void displayspelltext(PLAYER plr) {
        switch (plr.getCurrentorb()) {
            case 0:
                showmessage("scare spell", 360);
                break;
            case 1:
                showmessage("night vision spell", 360);
                break;
            case 2:
                showmessage("freeze spell", 360);
                break;
            case 3:
                showmessage("magic arrow spell", 360);
                break;
            case 4:
                showmessage("open door spell", 360);
                break;
            case 5:
                showmessage("fly spell", 360);
                break;
            case 6:
                showmessage("fireball spell", 360);
                break;
            case 7:
                showmessage("nuke spell", 360);
                break;
        }
    }
}
