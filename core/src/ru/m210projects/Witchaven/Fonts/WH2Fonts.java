package ru.m210projects.Witchaven.Fonts;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.game;

public class WH2Fonts extends Font {

    public WH2Fonts() {
        this.size = 12;

        Renderer renderer = game.getRenderer();
        this.addCharInfo(' ', new CharInfo(this, -1, 0.5f, 10));

        int nTile;
        for (int i = 0; i < 26; i++) {
            nTile = i + WH2FONTBIGLETTERS;
            addChar((char) ('A' + i), nTile, (renderer.getTile(nTile).getWidth()) + 1);
            nTile = i + WH2FONTLILLETTERS;
            addChar((char) ('a' + i), nTile, (renderer.getTile(nTile).getWidth()) + 1);
        }
        for (int i = 0; i < 10; i++) {
            nTile = i + WH2FONTNUMBERS;
            addChar((char) ('0' + i), nTile, (renderer.getTile(nTile).getWidth()) + 1);
        }

        addChar('.', 9318, (renderer.getTile(9318).getWidth()) + 1);
        addChar(',', 9319, (renderer.getTile(9319).getWidth()) + 1);
        addChar(';', 9320, (renderer.getTile(9320).getWidth()) + 1);
        addChar(':', 9321, (renderer.getTile(9321).getWidth()) + 1);
        addChar('"', 9322, (renderer.getTile(9322).getWidth()) + 1);
        addChar('!', 9323, (renderer.getTile(9323).getWidth()) + 1);
        addChar('(', 9324, (renderer.getTile(9324).getWidth()) + 1);
        addChar(')', 9325, (renderer.getTile(9325).getWidth()) + 1);
        addChar('[', 9326, (renderer.getTile(9326).getWidth()) + 1);
        addChar(']', 9327, (renderer.getTile(9327).getWidth()) + 1);
        addChar('%', 9328, (renderer.getTile(9328).getWidth()) + 1);
        addChar('&', 9329, (renderer.getTile(9329).getWidth()) + 1);
        addChar('/', 9330, (renderer.getTile(9330).getWidth()) + 1);
        addChar('\\', 9331, (renderer.getTile(9331).getWidth()) + 1);
        addChar('-', 9332, (renderer.getTile(9332).getWidth()) + 1);
        addChar('+', 9333, (renderer.getTile(9333).getWidth()) + 1);
        addChar('?', 9334, (renderer.getTile(9334).getWidth()) + 1);
    }

    protected void addChar(char ch, int nTile, int width) {
        this.addCharInfo(ch, new CharInfo(this, nTile, 0.5f, width, width, 0, 0));
    }
}
