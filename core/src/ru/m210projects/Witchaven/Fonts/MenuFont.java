package ru.m210projects.Witchaven.Fonts;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Witchaven.Globals.MENUFONT;
import static ru.m210projects.Witchaven.Main.game;

public class MenuFont extends Font {

    public MenuFont(Engine engine) {
        this.size = 13;

        this.addCharInfo(' ', new CharInfo(this, -1, 0.5f, 12));

        for (int i = 0; i < 26; i++) {
            int nTile = i + MENUFONT + 1;
            addChar((char) ('A' + i), nTile);
            addChar((char) ('a' + i), nTile);
        }

        this.addChar('/', MENUFONT + 32);
        this.addChar('&', MENUFONT);
        final int width = game.getRenderer().getTile(MENUFONT + 27).getWidth();
        this.addCharInfo('?', new CharInfo(this, MENUFONT + 27, 0.5f, width, width, 0, 1));
        this.addChar('[', MENUFONT + 28);
        this.addChar(']', MENUFONT + 29);
        this.addChar(':', MENUFONT + 30);
        this.addChar('.', MENUFONT + 31);

        byte[] remapbuf = new byte[256];
        for (int i = 242; i < 252; i++) //yellow to green
        {
            remapbuf[i] = (byte) (368 - i);
        }
        for (int i = 117; i < 127; i++) //green to yellow
        {
            remapbuf[i] = (byte) (368 - i);
        }
        engine.getPaletteManager().makePalookup(20, remapbuf, 0, 0, 0, 1);
    }

    protected void addChar(char ch, int nTile) {
        this.addCharInfo(ch, new CharInfo(this, nTile, 0.5f, game.getRenderer().getTile(nTile).getWidth()));
    }
}
