package ru.m210projects.Witchaven.Fonts;

import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Witchaven.Main.game;
import static ru.m210projects.Witchaven.Names.SPOTIONFONT;

public class PotionFont extends Font {

    public PotionFont() {
        this.size = 9;
        this.setVerticalScaled(false);

        this.addCharInfo(' ', new CharInfo(this, -1, 1.0f, 20));

        for (int i = 0; i < 10; i++) {
            int nTile = i + SPOTIONFONT;
            this.addCharInfo((char) ('0' + i), new CharInfo(this, nTile, 1.0f, game.getRenderer().getTile(nTile).getWidth() + 1));
        }
    }
}
