package ru.m210projects.Witchaven.Fonts;

import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Witchaven.Main.game;
import static ru.m210projects.Witchaven.Names.SSCOREFONT;

public class ScoreFont extends Font {

    public ScoreFont() {
        this.size = 19;
        this.setVerticalScaled(false);

        this.addCharInfo(' ', new CharInfo(this, -1, 1.0f, 20));
        for (int i = 0; i < 10; i++) {
            int nTile = i + SSCOREFONT;
            this.addCharInfo((char) ('0' + i), new CharInfo(this, nTile, 1.0f, game.getRenderer().getTile(nTile).getWidth() + 1));
        }
    }
}
