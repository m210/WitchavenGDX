package ru.m210projects.Witchaven.Fonts;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Witchaven.Globals.MINITEXTPOINT;
import static ru.m210projects.Witchaven.Globals.MINITEXTSLASH;
import static ru.m210projects.Witchaven.Main.game;
import static ru.m210projects.Witchaven.Names.THEFONT;

public class TheFont extends Font {

    public TheFont() {
        this.size = 9;

        Renderer renderer = game.getRenderer();
        this.addCharInfo(' ', new CharInfo(this, -1, 0.5f, 20));

        for (int i = 0; i < 26; i++) {
            int nTile = i + THEFONT;
            addChar((char) ('A' + i), nTile, (renderer.getTile(nTile).getWidth()) + 1);
            addChar((char) ('a' + i), nTile, (renderer.getTile(nTile).getWidth()) + 1);
        }

        for (int i = 0; i < 10; i++) {
            int nTile = i + THEFONT + 26;
            addChar((char) ('0' + i), nTile, (renderer.getTile(nTile).getWidth()) + 1);
        }

        if (game.WH2) {
            addChar('!', 1546, (renderer.getTile(1546).getWidth()) + 1);
            addChar('?', 1547, (renderer.getTile(1547).getWidth()) + 1);
            addChar('-', 1548, (renderer.getTile(1548).getWidth()) + 1, 2);
            addChar('_', 1548, (renderer.getTile(1548).getWidth()) + 1, 4);
            addChar(':', 1549, (renderer.getTile(1549).getWidth()) + 1);
            addChar('.', MINITEXTPOINT, (renderer.getTile(MINITEXTPOINT).getWidth()) + 1, -2);
            addChar('/', MINITEXTSLASH, (renderer.getTile(MINITEXTSLASH).getWidth()) + 1, -2);
            addChar('\\', MINITEXTSLASH, (renderer.getTile(MINITEXTSLASH).getWidth()) + 1, -2);
        } else {
            addChar('!', 1547, (renderer.getTile(1547).getWidth()) + 1);
            addChar('?', 1548, (renderer.getTile(1548).getWidth()) + 1);
            addChar('-', 1549, (renderer.getTile(1549).getWidth()) + 1);
            addChar('_', 1549, (renderer.getTile(1549).getWidth()) + 1, 4);
            addChar(':', 1550, (renderer.getTile(1550).getWidth()) + 1);
            addChar('.', MINITEXTPOINT, (renderer.getTile(MINITEXTPOINT).getWidth()) + 1);
            addChar('/', MINITEXTSLASH, (renderer.getTile(MINITEXTSLASH).getWidth()) + 1);
            addChar('\\', MINITEXTSLASH, (renderer.getTile(MINITEXTSLASH).getWidth()) + 1);
        }
    }

    protected void addChar(char ch, int nTile, int width, int yoffset) {
        this.addCharInfo(ch, new CharInfo(this, nTile, 0.5f, width, width, 0, yoffset));
    }

    protected void addChar(char ch, int nTile, int width) {
        this.addCharInfo(ch, new CharInfo(this, nTile, 0.5f, width, width, 0, 0));
    }
}
