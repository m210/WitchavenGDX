package ru.m210projects.Witchaven;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Witchaven.Types.Item;

import static ru.m210projects.Witchaven.Globals.*;
import static ru.m210projects.Witchaven.Main.*;
import static ru.m210projects.Witchaven.Names.*;
import static ru.m210projects.Witchaven.Potions.potionspace;
import static ru.m210projects.Witchaven.Potions.updatepotion;
import static ru.m210projects.Witchaven.Screens.DemoScreen.checkCodeVersion;
import static ru.m210projects.Witchaven.Spellbooks.changebook;
import static ru.m210projects.Witchaven.WHOBJ.explosion;
import static ru.m210projects.Witchaven.WHPLR.*;
import static ru.m210projects.Witchaven.WHSND.*;
import static ru.m210projects.Witchaven.WHScreen.showmessage;
import static ru.m210projects.Witchaven.WHScreen.startredflash;
import static ru.m210projects.Witchaven.Weapons.autoweaponchange;

public class Items {

    public static final int ITEMSBASE = 101;
    public static final int SILVERBAGTYPE = 101;
    public static final int GOLDBAGTYPE = 102;
    public static final int HELMETTYPE = 103;
    public static final int PLATEARMORTYPE = 104;
    public static final int CHAINMAILTYPE = 105;
    public static final int LEATHERARMORTYPE = 106;
    public static final int GIFTBOXTYPE = 107;
    public static final int FLASKBLUETYPE = 108;
    public static final int FLASKGREENTYPE = 109;
    public static final int FLASKOCHRETYPE = 110;
    public static final int FLASKREDTYPE = 111;
    public static final int FLASKTANTYPE = 112;
    public static final int DIAMONDRINGTYPE = 113;
    public static final int SHADOWAMULETTYPE = 114;
    public static final int GLASSSKULLTYPE = 115;
    public static final int AHNKTYPE = 116;
    public static final int BLUESCEPTERTYPE = 117;
    public static final int YELLOWSCEPTERTYPE = 118;
    public static final int ADAMANTINERINGTYPE = 119;
    public static final int ONYXRINGTYPE = 120;
    public static final int PENTAGRAMTYPE = 121;
    public static final int CRYSTALSTAFFTYPE = 122;
    public static final int AMULETOFTHEMISTTYPE = 123;
    public static final int HORNEDSKULLTYPE = 124;
    public static final int THEHORNTYPE = 125;
    public static final int SAPHIRERINGTYPE = 126;
    public static final int BRASSKEYTYPE = 127;
    public static final int BLACKKEYTYPE = 128;
    public static final int GLASSKEYTYPE = 129;
    public static final int IVORYKEYTYPE = 130;
    public static final int SCROLLSCARETYPE = 131;
    public static final int SCROLLNIGHTTYPE = 132;
    public static final int SCROLLFREEZETYPE = 133;
    public static final int SCROLLMAGICTYPE = 134;
    public static final int SCROLLOPENTYPE = 135;
    public static final int SCROLLFLYTYPE = 136;
    public static final int SCROLLFIREBALLTYPE = 137;
    public static final int SCROLLNUKETYPE = 138;
    public static final int QUIVERTYPE = 139;
    public static final int BOWTYPE = 140;
    public static final int WEAPON1TYPE = 141;
    public static final int WEAPON1ATYPE = 142;
    public static final int GOBWEAPONTYPE = 143;
    public static final int WEAPON2TYPE = 144;
    public static final int WEAPON3ATYPE = 145;
    public static final int WEAPON3TYPE = 146;
    public static final int WEAPON4TYPE = 147;
    public static final int THROWHALBERDTYPE = 148;
    public static final int WEAPON5TYPE = 149;
    public static final int GONZOSHIELDTYPE = 150;
    public static final int SHIELDTYPE = 151;
    public static final int WEAPON5BTYPE = 152;
    public static final int WALLPIKETYPE = 153;
    public static final int WEAPON6TYPE = 154;
    public static final int WEAPON7TYPE = 155;
    public static final int GYSERTYPE = 156; //WH1
    public static final int SPIKEBLADETYPE = 157;
    public static final int SPIKETYPE = 158;
    public static final int SPIKEPOLETYPE = 159;
    public static final int MONSTERBALLTYPE = 160;
    public static final int WEAPON8TYPE = 161;
    public static final int MAXITEMS = 162;

    public static final Item[] items = {new Item(-1, -1, false, false, (plr, i) -> {  // SILVERBAG
        showmessage("Silver!", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, engine.krand() % 100 + 10);
    }), new Item(-1, -1, false, false, (plr, i) -> { // GOLDBAG
        showmessage("Gold!", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, engine.krand() % 100 + 10);
    }), new Item(27, 28, true, false, (plr, i) -> { // HELMET
        showmessage("Hero Time for 60 sec", 360);
        engine.deletesprite(i);
        if (!game.WH2) {
            addarmor(plr, 10);
        }
        plr.setHelmettime(7200);
        // JSA_DEMO3
        treasuresfound++;
        SND_Sound(S_STING1 + engine.krand() % 2);
        addscore(plr, 10);
    }), new Item(26, 26, true, false, (plr, i) -> { // PLATEARMOR
        if (plr.getArmor() <= 149) {
            showmessage("Plate Armor (150 Armor type3)", 360);
            engine.deletesprite(i);
            plr.setArmortype(3);
            plr.setArmor(0);
            addarmor(plr, 150);
            SND_Sound(S_POTION1);
            addscore(plr, 40);
            treasuresfound++;
        }
    }), new Item(26, 26, true, false, (plr, i) -> { // CHAINMAIL
        if (plr.getArmor() <= 99) {
            showmessage("Chain Mail (100 Armor type2)", 360);
            engine.deletesprite(i);
            plr.setArmortype(2);
            plr.setArmor(0);
            addarmor(plr, 100);
            SND_Sound(S_POTION1);
            addscore(plr, 20);
            treasuresfound++;
        }
    }), new Item(47, 50, false, false, (plr, i) -> { // LEATHERARMOR
        if (plr.getArmor() <= 49) {
            showmessage("Leather Armor (50 Armor type1)", 360);
            engine.deletesprite(i);
            plr.setArmortype(1);
            plr.setArmor(0);
            addarmor(plr, 50);
            SND_Sound(S_POTION1);
            addscore(plr, 10);
            treasuresfound++;
        }
    }), new Item(56, 49, true, false, (plr, i) -> { // GIFTBOX
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        spr.setDetail(0);
        treasuresfound++;
        playsound_loc(S_TREASURE1, spr.getX(), spr.getY());
        int j = engine.krand() % 8;
        switch (j) {
            case 0:
                switch (engine.krand() % 5) {
                    case 0:
                        if (!potionspace(plr, 0)) {
                            break;
                        }
                        showmessage("Health Potion", 360);
                        updatepotion(plr, HEALTHPOTION);
                        plr.setCurrentpotion(0);
                        SND_Sound(S_POTION1);
                        addscore(plr, 10);
                        break;
                    case 1:
                        if (!potionspace(plr, 1)) {
                            break;
                        }
                        showmessage("Strength Potion", 360);
                        updatepotion(plr, STRENGTHPOTION);
                        plr.setCurrentpotion(1);
                        SND_Sound(S_POTION1);
                        addscore(plr, 20);
                        break;
                    case 2:
                        if (!potionspace(plr, 2)) {
                            break;
                        }
                        showmessage("Cure Poison Potion", 360);
                        updatepotion(plr, ARMORPOTION);
                        plr.setCurrentpotion(2);
                        SND_Sound(S_POTION1);
                        addscore(plr, 15);
                        break;
                    case 3:
                        if (!potionspace(plr, 3)) {
                            break;
                        }
                        showmessage("Resist Fire Potion", 360);
                        updatepotion(plr, FIREWALKPOTION);
                        plr.setCurrentpotion(3);
                        SND_Sound(S_POTION1);
                        addscore(plr, 15);
                        break;
                    case 4:
                        if (!potionspace(plr, 4)) {
                            break;
                        }
                        showmessage("Invisibility Potion", 360);
                        updatepotion(plr, INVISIBLEPOTION);
                        plr.setCurrentpotion(4);
                        SND_Sound(S_POTION1);
                        addscore(plr, 30);
                        break;
                }
                spr.setPicnum(OPENCHEST);
                break;
            case 1:
                switch (engine.krand() % 8) {
                    case 0:
                        if (plr.getOrbammo()[0] < 10) {
                            plr.getOrb()[0] = 1;
                            plr.getOrbammo()[0]++;
                            showmessage("Scare Scroll", 360);
                            SND_Sound(S_POTION1);
                        }
                        break;
                    case 1:
                        if (plr.getOrbammo()[1] < 10) {
                            plr.getOrb()[1] = 1;
                            plr.getOrbammo()[1]++;
                            showmessage("Night Vision Scroll", 360);
                            SND_Sound(S_POTION1);
                        }
                        break;
                    case 2:
                        if (plr.getOrbammo()[2] < 10) {
                            plr.getOrb()[2] = 1;
                            plr.getOrbammo()[2]++;
                            showmessage("Freeze Scroll", 360);
                            SND_Sound(S_POTION1);
                        }
                        break;
                    case 3:
                        if (plr.getOrbammo()[3] < 10) {
                            plr.getOrb()[3] = 1;
                            plr.getOrbammo()[3]++;
                            showmessage("Magic Arrow Scroll", 360);
                            SND_Sound(S_POTION1);
                        }
                        break;
                    case 4:
                        if (plr.getOrbammo()[4] < 10) {
                            plr.getOrb()[4] = 1;
                            plr.getOrbammo()[4]++;
                            showmessage("Open Door Scroll", 360);
                            SND_Sound(S_POTION1);
                        }
                        break;
                    case 5:
                        if (plr.getOrbammo()[5] < 10) {
                            plr.getOrb()[5] = 1;
                            plr.getOrbammo()[5]++;
                            showmessage("Fly Scroll", 360);
                            SND_Sound(S_POTION1);
                        }
                        break;
                    case 6:
                        if (plr.getOrbammo()[6] < 10) {
                            plr.getOrb()[6] = 1;
                            plr.getOrbammo()[6]++;
                            showmessage("Fireball Scroll", 360);
                            SND_Sound(S_POTION1);
                        }
                        break;
                    case 7:
                        if (plr.getOrbammo()[7] < 10) {
                            plr.getOrb()[7] = 1;
                            plr.getOrbammo()[7]++;
                            showmessage("Nuke Scroll", 360);
                            SND_Sound(S_POTION1);
                        }
                        break;
                }
                spr.setPicnum(OPENCHEST);
                break;
            case 2:
                spr.setPicnum(OPENCHEST);
                addscore(plr, (engine.krand() % 400) + 100);
                showmessage("Treasure Chest", 360);
                SND_Sound(S_POTION1);
                break;
            case 3:
                // random weapon
                switch ((engine.krand() % 5) + 1) {
                    case 1:
                        if (plr.getAmmo()[1] < 12) {
                            plr.getWeapon()[1] = 1;
                            plr.getAmmo()[1] = 40;
                            showmessage("Dagger", 360);
                            SND_Sound(S_POTION1);
                            if (plr.getSelectedgun() < 1) {
                                autoweaponchange(plr, 1);
                            }
                            addscore(plr, 10);
                        }
                        break;
                    case 2:
                        if (plr.getAmmo()[3] < 12) {
                            plr.getWeapon()[3] = 1;
                            plr.getAmmo()[3] = 55;
                            showmessage("Morning Star", 360);
                            SND_Sound(S_POTION1);
                            if (plr.getSelectedgun() < 3) {
                                autoweaponchange(plr, 3);
                            }
                            addscore(plr, 20);
                        }
                        break;
                    case 3:
                        if (plr.getAmmo()[2] < 12) {
                            plr.getWeapon()[2] = 1;
                            plr.getAmmo()[2] = 30;
                            showmessage("Short Sword", 360);
                            SND_Sound(S_POTION1);
                            if (plr.getSelectedgun() < 2) {
                                autoweaponchange(plr, 2);
                            }
                            addscore(plr, 10);
                        }
                        break;
                    case 4:
                        if (plr.getAmmo()[5] < 12) {
                            plr.getWeapon()[5] = 1;
                            plr.getAmmo()[5] = 100;
                            showmessage("Battle axe", 360);
                            SND_Sound(S_POTION1);
                            if (plr.getSelectedgun() < 5) {
                                autoweaponchange(plr, 5);
                            }
                            addscore(plr, 30);
                        }
                        break;
                    case 5:
                        if (plr.getWeapon()[7] == 1) {
                            plr.getWeapon()[7] = 2;
                            plr.getAmmo()[7] = 1;
                            showmessage("Pike axe", 360);
                            engine.deletesprite(i);
                            SND_Sound(S_POTION1);
                            addscore(plr, 30);
                        }
                        if (plr.getWeapon()[7] == 2) {
                            plr.getAmmo()[7]++;
                            showmessage("Pike axe", 360);
                            engine.deletesprite(i);
                            SND_Sound(S_POTION1);
                            addscore(plr, 30);
                        }
                        if (plr.getWeapon()[7] < 1) {
                            if (plr.getAmmo()[7] < 12) {
                                plr.getWeapon()[7] = 1;
                                plr.getAmmo()[7] = 30;
                                showmessage("Pike axe", 360);
                                engine.deletesprite(i);
                                SND_Sound(S_POTION1);
                                if (plr.getSelectedgun() < 7) {
                                    autoweaponchange(plr, 7);
                                }
                                addscore(plr, 30);
                            }
                        }
                        break;
                }
                spr.setPicnum(OPENCHEST);
                break;
            case 4:
                // random armor
                int randomVar = engine.krand();
                int switchVar = randomVar & 4;
                if (checkCodeVersion(game.getScreen(), 101)) {
                    switchVar = randomVar % 4;
                }

                switch (switchVar) {
                    case 0:
                        showmessage("Hero Time for 60 sec", 360);
                        addarmor(plr, 10);
                        plr.setHelmettime(7200);
                        SND_Sound(S_STING1 + engine.krand() % 2);
                        break;
                    case 1:
                        if (plr.getArmor() <= 149) {
                            showmessage("Plate Armor (150 Armor type3)", 360);
                            plr.setArmortype(3);
                            plr.setArmor(0);
                            addarmor(plr, 150);
                            SND_Sound(S_POTION1);
                            addscore(plr, 40);
                        }
                        break;
                    case 2:
                        if (plr.getArmor() <= 99) {
                            showmessage("Chain Mail (100 Armor type2)", 360);
                            plr.setArmortype(2);
                            plr.setArmor(0);
                            addarmor(plr, 100);
                            SND_Sound(S_POTION1);
                            addscore(plr, 20);
                        }
                        break;
                    case 3:
                        if (plr.getArmor() <= 49) {
                            showmessage("Leather Armor (50 Armor type1)", 360);
                            plr.setArmortype(1);
                            plr.setArmor(0);
                            addarmor(plr, 50);
                            SND_Sound(S_POTION1);
                            addscore(plr, 20);
                        }
                        break;
                }
                spr.setPicnum(OPENCHEST);
                break;
            case 5:
                // poison chest
                if ((engine.krand() & 2) == 0) {
                    plr.setPoisoned(1);
                    plr.setPoisontime(7200);
                    spr.setDetail(GIFTBOXTYPE);
                    addhealth(plr, -10);
                    showmessage("Poisoned Chest", 360);
                } else {
                    engine.deletesprite(i);
                    addscore(plr, (engine.krand() & 400) + 100);
                    showmessage("Treasure Chest", 360);
                    SND_Sound(S_POTION1);
                }
                break;
            case 6:
                for (j = 0; j < 8; j++) {
                    explosion(i, spr.getX(), spr.getY(), spr.getZ(), spr.getOwner());
                }
                playsound_loc(S_EXPLODE, spr.getX(), spr.getY());
                engine.deletesprite(i);
                break;
            default:
                spr.setPicnum(OPENCHEST);
                addscore(plr, (engine.krand() % 400) + 100);
                showmessage("Experience Gained", 360);
                SND_Sound(S_POTION1);
                break;
        }
    }), new Item(-1, -1, true, false, (plr, i) -> { // FLASKBLUE
        if (!potionspace(plr, 0)) {
            return;
        }
        showmessage("Health Potion", 360);
        updatepotion(plr, HEALTHPOTION);
        plr.setCurrentpotion(0);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 10);
        treasuresfound++;
    }), new Item(-1, -1, true, false, (plr, i) -> { // FLASKGREEN
        if (!potionspace(plr, 1)) {
            return;
        }
        showmessage("Strength Potion", 360);
        updatepotion(plr, STRENGTHPOTION);
        plr.setCurrentpotion(1);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 15);
        treasuresfound++;
    }), new Item(-1, -1, true, false, (plr, i) -> { // FLASKOCHRE
        if (!potionspace(plr, 2)) {
            return;
        }
        showmessage("Cure Poison Potion", 360);
        updatepotion(plr, ARMORPOTION);
        plr.setCurrentpotion(2);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 15);
        treasuresfound++;
    }), new Item(-1, -1, true, false, (plr, i) -> { // FLASKRED
        if (!potionspace(plr, 3)) {
            return;
        }
        showmessage("Resist Fire Potion", 360);
        updatepotion(plr, FIREWALKPOTION);
        plr.setCurrentpotion(3);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 20);
        treasuresfound++;
    }), new Item(-1, -1, true, false, (plr, i) -> { // FLASKTAN
        if (!potionspace(plr, 4)) {
            return;
        }
        showmessage("Invisibility Potion", 360);
        updatepotion(plr, INVISIBLEPOTION);
        plr.setCurrentpotion(4);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 30);
        treasuresfound++;
    }), new Item(14, 14, true, false, (plr, i) -> { // DIAMONDRING
        plr.getTreasure()[TDIAMONDRING] = 1;
        showmessage("DIAMOND RING (200 Armor type3)", 360);
        plr.setArmor(0);
        addarmor(plr, 200);
        plr.setArmortype(3);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 25);
        treasuresfound++;
    }), new Item(30, 23, true, false, (plr, i) -> { // SHADOWAMULET
        plr.getTreasure()[TSHADOWAMULET] = 1;
        showmessage("SHADOW AMULET (Shadow bonus for 60 sec)", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        plr.setShadowtime(7500);
        addscore(plr, 50);
        treasuresfound++;
    }), new Item(-1, -1, true, false, (plr, i) -> { // GLASSSKULL
        plr.getTreasure()[TGLASSSKULL] = 1;
        showmessage("GLASS SKULL", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        treasuresfound++;
        switch (plr.getLvl()) {
            case 1:
                plr.setScore(2300);
                break;
            case 2:
                plr.setScore(4550);
                break;
            case 3:
                plr.setScore(9050);
                break;
            case 4:
                plr.setScore(18050);
                break;
            case 5:
                plr.setScore(36050);
                break;
            case 6:
                plr.setScore(75050);
                break;
            case 7:
                plr.setScore(180500);
                break;
            case 8:
                plr.setScore(280500);
                break;
        }
        addscore(plr, 10);
    }), new Item(51, 54, true, false, (plr, i) -> { // AHNK
        plr.getTreasure()[TAHNK] = 1;
        showmessage("ANKH (250 Health)", 360);
        plr.setHealth(0);
        addhealth(plr, 250);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 100);
        treasuresfound++;
    }), new Item(32, 32, true, false, (plr, i) -> { // BLUESCEPTER
        plr.getTreasure()[TBLUESCEPTER] = 1;
        showmessage("Water walk scepter", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 10);
        treasuresfound++;
    }), new Item(32, 32, true, false, (plr, i) -> { // YELLOWSCEPTER
        plr.getTreasure()[TYELLOWSCEPTER] = 1;
        showmessage("Fire walk scepter", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 10);
        treasuresfound++;
    }), new Item(14, 14, true, false, (plr, i) -> { // ADAMANTINERING
        // ring or protection +5
        plr.getTreasure()[TADAMANTINERING] = 1;
        showmessage("ADAMANTINE RING (Attack protection)", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 30);
        treasuresfound++;
    }), new Item(42, 28, true, false, (plr, i) -> { // ONYXRING
        // protection from missile
        // anit-missile for level only
        // dont forget to cleanup values
        plr.getTreasure()[TONYXRING] = 1;
        showmessage("ONYX RING (Missile protection)", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 35);
        treasuresfound++;
    }), new Item(-1, -1, true, true, (plr, i) -> { // PENTAGRAM
        Sector sec = boardService.getSector(plr.getSector());
        if (sec == null || sec.getLotag() == 4002) {
            return;
        }

        plr.getTreasure()[TPENTAGRAM] = 1;
        showmessage("PENTAGRAM", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        treasuresfound++;
        addscore(plr, 100);
    }), new Item(64, 64, true, false, (plr, i) -> { // CRYSTALSTAFF
        plr.getTreasure()[TCRYSTALSTAFF] = 1;
        showmessage("CRYSTAL STAFF (250 Health, 300 Armor type2) ", 360);
        plr.setHealth(0);
        addhealth(plr, 250);
        plr.setArmortype(2);
        plr.setArmor(0);
        addarmor(plr, 300);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        treasuresfound++;
        addscore(plr, 150);
    }), new Item(26, 28, true, false, (plr, i) -> { // AMULETOFTHEMIST
        plr.getTreasure()[TAMULETOFTHEMIST] = 1;
        showmessage("AMULET OF THE MIST (Invisible bonus for 30sec)", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        plr.setInvisibletime(3200);
        addscore(plr, 75);
        treasuresfound++;
    }), new Item(64, 64, true, false, (plr, i) -> { // HORNEDSKULL
        if (game.WH2) {
            Runnable ending3 = () -> {
                if (gCutsceneScreen.init("ending3.smk")) {
                    game.changeScreen(gCutsceneScreen.setSkipping(game.main).escSkipping(true));
                } else {
                    game.changeScreen(gMenuScreen);
                }
            };

            Runnable ending2 = () -> {
                if (gCutsceneScreen.init("ending2.smk")) {
                    game.changeScreen(gCutsceneScreen.setSkipping(ending3).escSkipping(true));
                } else {
                    game.changeScreen(gMenuScreen);
                }
            };

            if (gCutsceneScreen.init("ending1.smk")) {
                game.changeScreen(gCutsceneScreen.setSkipping(ending2).escSkipping(true));
            } else {
                game.changeScreen(gMenuScreen);
            }
            return;
        }
        plr.getTreasure()[THORNEDSKULL] = 1;
        showmessage("HORNED SKULL", 360);
        engine.deletesprite(i);
        SND_Sound(S_STING2);
        addscore(plr, 750);
        treasuresfound++;
    }), new Item(32, 32, true, false, (plr, i) -> { // THEHORN
        plr.getTreasure()[TTHEHORN] = 1;
        showmessage("Ornate Horn (Vampire bonus for 60sec)", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        plr.setVampiretime(7200);
        // gain 5-10 hp when you kill something
        // for 60 seconds
        addscore(plr, 350);
        treasuresfound++;
    }), new Item(30, 20, true, false, (plr, i) -> { // SAPHIRERING
        plr.getTreasure()[TSAPHIRERING] = 1;
        showmessage("SAPPHIRE RING (Armor type 3)", 360);
        plr.setArmortype(3);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 25);
        treasuresfound++;
    }), new Item(24, 24, true, false, (plr, i) -> { // BRASSKEY
        plr.getTreasure()[TBRASSKEY] = 1;
        showmessage("BRASS KEY", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 15);
        treasuresfound++;
    }), new Item(24, 24, true, false, (plr, i) -> { // BLACKKEY
        plr.getTreasure()[TBLACKKEY] = 1;
        showmessage("BLACK KEY", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 15);
    }), new Item(24, 24, true, false, (plr, i) -> { // GLASSKEY
        plr.getTreasure()[TGLASSKEY] = 1;
        showmessage("GLASS KEY", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 15);
        treasuresfound++;
    }), new Item(24, 24, true, false, (plr, i) -> { // IVORYKEY

        plr.getTreasure()[TIVORYKEY] = 1;
        showmessage("IVORY KEY", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        addscore(plr, 15);
        treasuresfound++;
    }), new Item(35, 36, true, true, (plr, i) -> { // SCROLLSCARE

        if (plr.getOrbammo()[0] < 10) {
            plr.getOrb()[0] = 1;
            plr.getOrbammo()[0]++;
            changebook(plr, 0);
            showmessage("Scare Scroll", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            treasuresfound++;
        }
    }), new Item(35, 36, true, true, (plr, i) -> { // SCROLLNIGHT

        if (plr.getOrbammo()[1] < 10) {
            plr.getOrb()[1] = 1;
            plr.getOrbammo()[1]++;
            changebook(plr, 1);
            showmessage("Night Vision Scroll", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            treasuresfound++;
        }
    }), new Item(35, 36, true, true, (plr, i) -> { // SCROLLFREEZE

        if (plr.getOrbammo()[2] < 10) {
            plr.getOrb()[2] = 1;
            plr.getOrbammo()[2]++;
            changebook(plr, 2);
            showmessage("Freeze Scroll", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            treasuresfound++;
        }
    }), new Item(35, 36, true, true, (plr, i) -> { // SCROLLMAGIC

        if (plr.getOrbammo()[3] < 10) {
            plr.getOrb()[3] = 1;
            plr.getOrbammo()[3]++;
            changebook(plr, 3);
            showmessage("Magic Arrow Scroll", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            treasuresfound++;
        }
    }), new Item(35, 36, true, true, (plr, i) -> { // SCROLLOPEN

        if (plr.getOrbammo()[4] < 10) {
            plr.getOrb()[4] = 1;
            plr.getOrbammo()[4]++;
            changebook(plr, 4);
            showmessage("Open Door Scroll", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            treasuresfound++;
        }
    }), new Item(35, 36, true, true, (plr, i) -> { // SCROLLFLY

        if (plr.getOrbammo()[5] < 10) {
            plr.getOrb()[5] = 1;
            plr.getOrbammo()[5]++;
            changebook(plr, 5);
            showmessage("Fly Scroll", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            treasuresfound++;
        }
    }), new Item(35, 36, true, true, (plr, i) -> { // SCROLLFIREBALL

        if (plr.getOrbammo()[6] < 10) {
            plr.getOrb()[6] = 1;
            plr.getOrbammo()[6]++;
            changebook(plr, 6);
            showmessage("Fireball Scroll", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            treasuresfound++;
        }
    }), new Item(35, 36, true, true, (plr, i) -> { // SCROLLNUKE

        if (plr.getOrbammo()[7] < 10) {
            plr.getOrb()[7] = 1;
            plr.getOrbammo()[7]++;
            changebook(plr, 7);
            showmessage("Nuke Scroll", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            treasuresfound++;
        }
    }), new Item(27, 27, false, false, (plr, i) -> { // QUIVER

        if (plr.getAmmo()[6] < 100) {
            plr.getAmmo()[6] += 20;
            if (plr.getAmmo()[6] > 100) {
                plr.getAmmo()[6] = 100;
            }
            showmessage("Quiver of magic arrows", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            addscore(plr, 10);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // WALLBOW BOW

        plr.getWeapon()[6] = 1;
        plr.getAmmo()[6] += 10;
        if (plr.getAmmo()[6] > 100) {
            plr.getAmmo()[6] = 100;
        }
        showmessage("Magic bow", 360);
        engine.deletesprite(i);
        SND_Sound(S_POTION1);
        if (plr.getSelectedgun() < 6) {
            autoweaponchange(plr, 6);
        }
        addscore(plr, 10);
    }), new Item(34, 21, false, false, (plr, i) -> { // WEAPON1

        if (plr.getAmmo()[1] < 12) {
            plr.getWeapon()[1] = 1;
            plr.getAmmo()[1] = 40;
            showmessage("Dagger", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            if (plr.getSelectedgun() < 1) {
                autoweaponchange(plr, 1);
            }
            addscore(plr, 10);
        }
    }), new Item(34, 21, false, false, (plr, i) -> { // WEAPON1A

        if (plr.getAmmo()[1] < 12) {
            plr.getWeapon()[1] = 3;
            plr.getAmmo()[1] = 80;
            showmessage("Jeweled Dagger", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            autoweaponchange(plr, 1);
            addscore(plr, 30);
        }
    }), new Item(34, 21, false, false, (plr, i) -> { // GOBWEAPON

        if (plr.getAmmo()[2] < 12) {
            plr.getWeapon()[2] = 1;
            plr.getAmmo()[2] = 20;
            showmessage("Short sword", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            if (plr.getSelectedgun() < 2) {
                autoweaponchange(plr, 2);
            }
            addscore(plr, 10);
        }
    }), new Item(26, 26, false, false, (plr, i) -> { // WEAPON2

        if (plr.getAmmo()[3] < 12) {
            plr.getWeapon()[3] = 1;
            plr.getAmmo()[3] = 55;
            showmessage("Morning Star", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            if (plr.getSelectedgun() < 3) {
                autoweaponchange(plr, 3);
            }
            addscore(plr, 20);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // WALLSWORD WEAPON3A

        if (plr.getAmmo()[4] < 12) {
            plr.getWeapon()[4] = 1;
            plr.getAmmo()[4] = 160;
            showmessage("Broad Sword", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            autoweaponchange(plr, 4);
            addscore(plr, 60);
        }
    }), new Item(44, 39, false, false, (plr, i) -> { // WEAPON3

        if (plr.getAmmo()[4] < 12) {
            plr.getWeapon()[4] = 1;
            plr.getAmmo()[4] = 80;
            showmessage("Broad Sword", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            if (plr.getSelectedgun() < 4) {
                autoweaponchange(plr, 4);
            }
            addscore(plr, 30);
        }
    }), new Item(25, 20, false, false, (plr, i) -> { // WALLAXE WEAPON4

        if (plr.getAmmo()[5] < 12) {
            plr.getWeapon()[5] = 1;
            plr.getAmmo()[5] = 100;
            showmessage("Battle axe", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            if (plr.getSelectedgun() < 5) {
                autoweaponchange(plr, 5);
            }
            addscore(plr, 30);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // THROWHALBERD

        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        // EG fix: don't collect moving halberds, be hurt by them as you should be
        // ...but only if you don't have the Onyx Ring
        if (spr.getStatnum() != INACTIVE && plr.getTreasure()[TONYXRING] == 0) {
            addhealth(plr, -((engine.krand() % 20) + 5)); // Inflict pain
            // make it look and sound painful, too
            if ((engine.krand() % 9) == 0) {
                playsound_loc(S_PLRPAIN1 + (engine.rand() % 2), spr.getX(), spr.getY());
            }
            startredflash(10);
            engine.deletesprite(i);
            return;
        }
        if (spr.getStatnum() != INACTIVE && plr.getTreasure()[TONYXRING] != 0) {
            // Can we grab?
            if (plr.getAmmo()[9] < 12 && plr.getWeapon()[9] != 3) {
                // You grabbed a halberd out of midair, so go ahead and be smug about it
                if ((engine.rand() % 10) > 6) { // playsound
                    SND_Sound(S_PICKUPAXE);
                }
                // fall through to collect
            } else {
                // Can't grab, so just block getting hit
                engine.deletesprite(i);
                return;
            }
        }

        if (plr.getAmmo()[9] < 12) {
            plr.getWeapon()[9] = 1;
            plr.getAmmo()[9] = 30;
            showmessage("Halberd", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            if (plr.getSelectedgun() < 9) {
                autoweaponchange(plr, 9);
            }
            addscore(plr, 30);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // WEAPON5

        if (plr.getAmmo()[9] < 12) {
            plr.getWeapon()[9] = 1;
            plr.getAmmo()[9] = 30;
            showmessage("Halberd", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            if (plr.getSelectedgun() < 9) {
                autoweaponchange(plr, 9);
            }
            addscore(plr, 30);
        }
    }), new Item(12, 12, false, false, (plr, i) -> { // GONZOBSHIELD

        if (plr.getShieldpoints() < 100) {
            plr.setShieldtype(2);
            plr.setShieldpoints(200);
            plr.setDroptheshield(false);
            engine.deletesprite(i);
            showmessage("Magic Shield", 360);
            SND_Sound(S_POTION1);
            addscore(plr, 50);
        }
    }), new Item(26, 26, false, false, (plr, i) -> { // SHIELD

        if (plr.getShieldpoints() < 100) {
            plr.setShieldpoints(100);
            plr.setShieldtype(1);
            engine.deletesprite(i);
            showmessage("Shield", 360);
            plr.setDroptheshield(false); // EG 17 Oct 2017
            SND_Sound(S_POTION1);
            addscore(plr, 10);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // WEAPON5B

        if (plr.getAmmo()[9] < 12) { // XXX orly?
            engine.deletesprite(i);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // WALLPIKE

        if (plr.getWeapon()[7] == 1) {
            plr.getWeapon()[7] = 2;
            plr.getAmmo()[7] = 2;
            showmessage("Pike axe", 360);
            engine.deletesprite(i);
            SND_Sound(S_PICKUPAXE);
            addscore(plr, 30);
        }
        if (plr.getWeapon()[7] == 2) {
            plr.getAmmo()[7]++;
            showmessage("Pike axe", 360);
            engine.deletesprite(i);
            SND_Sound(S_PICKUPAXE);
            // score(plr, 30);
        }
        if (plr.getWeapon()[7] < 1) {
            if (plr.getAmmo()[7] < 12) {
                plr.getWeapon()[7] = 1;
                plr.getAmmo()[7] = 30;
                showmessage("Pike axe", 360);
                engine.deletesprite(i);
                SND_Sound(S_POTION1);
                if (plr.getSelectedgun() < 7) {
                    autoweaponchange(plr, 7);
                }
                addscore(plr, 30);
            }
        }
    }), new Item(20, 15, false, true, (plr, i) -> { // WEAPON6

        if (plr.getWeapon()[7] == 1) {
            plr.getWeapon()[7] = 2;
            plr.getAmmo()[7] = 10;
            showmessage("Pike axe", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            addscore(plr, 30);
        }
        if (plr.getWeapon()[7] == 2) {
            plr.getAmmo()[7] += 10;
            showmessage("Pike axe", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            // score(plr, 30);
        }
        if (plr.getWeapon()[7] < 1) {
            if (plr.getAmmo()[7] < 12) {
                plr.getWeapon()[7] = 2;
                plr.getAmmo()[7] = 10;
                showmessage("Pike axe", 360);
                engine.deletesprite(i);
                SND_Sound(S_POTION1);
                if (plr.getSelectedgun() < 7) {
                    autoweaponchange(plr, 7);
                }
                addscore(plr, 30);
            }
        }
    }), new Item(41, 36, false, true, (plr, i) -> { // WEAPON7

        if (plr.getAmmo()[8] < 12) {
            plr.getWeapon()[8] = 1;
            plr.getAmmo()[8] = 250;
            showmessage("Magic sword", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            if (plr.getSelectedgun() < 8) {
                autoweaponchange(plr, 8);
            }
            addscore(plr, 30);
        }
    }), new Item(32, 18, false, false, (plr, i) -> { // GYSER
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        if (plr.getManatime() < 1 && plr.getInvincibletime() <= 0 && !plr.isGodMode()) {
            playsound_loc(S_FIREBALL, spr.getX(), spr.getY());
            addhealth(plr, -1);
            startredflash(30);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // SPIKEBLADE

        if (plr.getInvincibletime() <= 0 && !plr.isGodMode() && !justteleported) {
            addhealth(plr, -plr.getHealth());
            plr.setHoriz(200);
            plr.setSpiked(1);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // SPIKE

        if (plr.getInvincibletime() <= 0 && !plr.isGodMode() && !justteleported) {
            addhealth(plr, -plr.getHealth());
            plr.setHoriz(200);
            plr.setSpiked(1);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // SPIKEPOLE

        if (plr.getInvincibletime() <= 0 && !plr.isGodMode() && !justteleported) {
            addhealth(plr, -plr.getHealth());
            plr.setHoriz(200);
            plr.setSpiked(1);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // MONSTERBALL

        if (plr.getManatime() < 1 && plr.getInvincibletime() <= 0 && !plr.isGodMode()) {
            addhealth(plr, -1);
        }
    }), new Item(-1, -1, false, false, (plr, i) -> { // WEAPON8

        if (plr.getAmmo()[8] < 12) {
            plr.getWeapon()[8] = 1;
            plr.getAmmo()[8] = 250;
            showmessage("Two Handed Sword", 360);
            engine.deletesprite(i);
            SND_Sound(S_POTION1);
            autoweaponchange(plr, 8);
            addscore(plr, 30);
        }
    })

    };

    public static boolean isItemSprite(Sprite spr) {
        return (spr.getDetail() & 0xFF) >= ITEMSBASE && (spr.getDetail() & 0xFF) < MAXITEMS;
    }

    public static boolean isWeaponSprite(Sprite spr) {
        switch (spr.getDetail() & 0xFF) {
            case WEAPON1TYPE:
            case WEAPON1ATYPE:
            case GOBWEAPONTYPE:
            case WEAPON2TYPE:
            case WEAPON3ATYPE:
            case WEAPON3TYPE:
            case WEAPON4TYPE:
            case THROWHALBERDTYPE:
            case WEAPON5TYPE:
            case WEAPON5BTYPE:
            case WALLPIKETYPE:
            case WEAPON6TYPE:
            case WEAPON7TYPE:
            case WEAPON8TYPE:
                return true;
        }
        return false;
    }

}
