package ru.m210projects.Witchaven;

import com.badlogic.gdx.Input.Keys;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.settings.*;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.stream.IntStream;

public class Config extends GameConfig {

    public static final int[] defkeys = {
            Keys.W,    //Move_Forward
            Keys.S,    //Move_Backward
            Keys.LEFT,    //Turn_Left
            Keys.RIGHT,    //Turn_Right
            Keys.BACKSPACE, //Turn_Around
            Keys.ALT_LEFT,    //Strafe
            Keys.A,        //Strafe_Left
            Keys.D,    //Strafe_Right
            Keys.SPACE,        //Jump
            Keys.CONTROL_LEFT,    //Crouch
            Keys.SHIFT_LEFT,    //Run
            Keys.E,            //Open
            0,                //Weapon_Fire
            0,                //Next_Weapon
            0,                //Previous_Weapon
            0,                  // Look_Up
            0,                  // Look_Down
            Keys.TAB,            //Map_Toggle
            Keys.EQUALS,    //Enlarge_Screen
            Keys.MINUS,    //Shrink_Screen
            Keys.T,             //Send_message
            Keys.U,            //Mouse_Aiming
            Keys.ESCAPE,    //Menu_Toggle
            Keys.GRAVE,         //Show_Console

            Keys.CAPS_LOCK,        //AutoRun
            Keys.PAGE_UP,        //Aim_Up
            Keys.PAGE_DOWN,        //Aim_Down
            Keys.HOME,        //Aim_Center
            Keys.NUM_1,    //Weapon_1
            Keys.NUM_2,        //Weapon_2
            Keys.NUM_3,        //Weapon_3
            Keys.NUM_4,        //Weapon_4
            Keys.NUM_5,        //Weapon_5
            Keys.NUM_6,        //Weapon_6
            Keys.NUM_7,        //Weapon_7
            Keys.NUM_8,        //Weapon_8
            Keys.NUM_9,        //Weapon_9
            Keys.NUM_0,        //Weapon_10
            Keys.Q,            //Cast_spell
            Keys.NUMPAD_1,        //Scare_Spell
            Keys.NUMPAD_2,        //Nightvision_Spell
            Keys.NUMPAD_3,        //Freeze_Spell
            Keys.NUMPAD_4,        //Arrow_Spell
            Keys.NUMPAD_5,        //Opendoor_Spell
            Keys.NUMPAD_6,        //Fly_Spell
            Keys.NUMPAD_7,        //Fire_Spell
            Keys.NUMPAD_8,        //Nuke_Spell
            Keys.APOSTROPHE,    //Spell_next
            Keys.SEMICOLON,        //Spell_prev
            Keys.ENTER,        //Inventory_Use
            Keys.LEFT_BRACKET, //Inventory_Left
            Keys.RIGHT_BRACKET, //Inventory_Right
            Keys.INSERT,     //Fly_up
            Keys.FORWARD_DEL, //Fly_down
            Keys.END,        //End_flying
            0,                    //Health_potion
            0,                    //Strength_potion
            0,                    //Curepoison_potion
            0,                    //Fireresist_potion
            0,                    //Invisibility_potion
            Keys.F,            //Map_Follow_Mode
            Keys.I,            //Toggle_Crosshair
            Keys.K,                //See_coop
            Keys.F9,        //Quickload
            Keys.F6,        //Quicksave
            Keys.F2,    //Show_SaveMenu
            Keys.F3,    //Show_LoadMenu
            Keys.F4,    //Show_SoundSetup
            Keys.F5,    //Show_Options
            Keys.F10,    //Quit
            Keys.F11,    //Gamma
            Keys.F12,    //Make_Screenshot
    };
    public static final int[] defclassickeys = {
            Keys.UP,    //Move_Forward
            Keys.DOWN,    //Move_Backward
            Keys.LEFT,    //Turn_Left
            Keys.RIGHT,    //Turn_Right
            0,            //Turn_Around
            Keys.ALT_LEFT,    //Strafe
            Keys.COMMA,        //Strafe_Left
            Keys.PERIOD,    //Strafe_Right
            Keys.X,        //Jump
            Keys.C,        //Crouch
            Keys.SHIFT_LEFT,    //Run
            Keys.SPACE,            //Open
            Keys.CONTROL_LEFT, //Weapon_Fire
            0,                //Next_Weapon
            0,                //Previous_Weapon
            0,
            0,
            Keys.TAB,            //Map_Toggle
            Keys.EQUALS,    //Enlarge_Screen
            Keys.MINUS,    //Shrink_Screen
            0,                //Mouse_Aiming
            0,                //Send_message
            Keys.ESCAPE, //Open/Close_menu
            0,  //Show_Console


            0,                //AutoRun
            Keys.PAGE_UP,    //Aim_Up
            Keys.PAGE_DOWN,    //Aim_Down
            Keys.HOME,        //Aim_Center
            Keys.NUM_1,    //Weapon_1
            Keys.NUM_2,        //Weapon_2
            Keys.NUM_3,        //Weapon_3
            Keys.NUM_4,        //Weapon_4
            Keys.NUM_5,        //Weapon_5
            Keys.NUM_6,        //Weapon_6
            Keys.NUM_7,        //Weapon_7
            Keys.NUM_8,        //Weapon_8
            Keys.NUM_9,        //Weapon_9
            Keys.NUM_0,        //Weapon_10
            Keys.GRAVE,        //Cast_spell
            Keys.F1,        //Scare_Spell
            Keys.F2,        //Nightvision_Spell
            Keys.F3,        //Freeze_Spell
            Keys.F4,        //Arrow_Spell
            Keys.F5,        //Opendoor_Spell
            Keys.F6,        //Fly_Spell
            Keys.F7,        //Fire_Spell
            Keys.F8,        //Nuke_Spell
            0,                //Spell_next
            0,                //Spell_prev
            Keys.ENTER,    //Inventory_Use
            Keys.LEFT_BRACKET, //Inventory_Left
            Keys.RIGHT_BRACKET, //Inventory_Right
            Keys.INSERT,     //Fly_up
            Keys.FORWARD_DEL, //Fly_down
            Keys.END,        //End_flying
            0,                    //Health_potion
            0,                    //Strength_potion
            0,                    //Curepoison_potion
            0,                    //Fireresist_potion
            0,                    //Invisibility_potion
            Keys.F,            //Map_Follow_Mode
            0,                //Toggle_Crosshair
            0,                //See_coop
            0,            //Quickload
            0,            //Quicksave
            0,        //Show_SaveMenu
            0,        //Show_LoadMenu
            0,        //Show_SoundSetup
            0,        //Show_Options
            0,        //Quit
            0,        //Gamma
            0,        //Make_Screenshot
    };
    public int weaponIndex = -1;
    public int spellIndex = -1;
    public int gViewSize = 1;
    public boolean MessageState = true;
    public boolean gCrosshair = true;
    public boolean gAutoRun = false;
    public int gStatSize = 32768;
    public int gCrossSize = 65536;
    public int gShowStat = 1;
    public int gHudScale = 65536;
    public boolean showCutscenes = true;
    public boolean gGameGore = true;
    public boolean showMapInfo = true;
    public int gDemoSeq = 1;

    public Config(Path path) {
        super(path);
    }

    public GameKey[] getKeyMap() {
        return new GameKey[]{
                GameKeys.Move_Forward,
                GameKeys.Move_Backward,
                GameKeys.Turn_Left,
                GameKeys.Turn_Right,
                GameKeys.Turn_Around,
                GameKeys.Strafe,
                GameKeys.Strafe_Left,
                GameKeys.Strafe_Right,
                GameKeys.Jump,
                GameKeys.Crouch,
                GameKeys.Run,
                WhKeys.AutoRun,
                GameKeys.Open,
                GameKeys.Weapon_Fire,
                WhKeys.Cast_spell,
                WhKeys.Aim_Up,
                WhKeys.Aim_Down,
                WhKeys.Aim_Center,
                WhKeys.Weapon_1,
                WhKeys.Weapon_2,
                WhKeys.Weapon_3,
                WhKeys.Weapon_4,
                WhKeys.Weapon_5,
                WhKeys.Weapon_6,
                WhKeys.Weapon_7,
                WhKeys.Weapon_8,
                WhKeys.Weapon_9,
                WhKeys.Weapon_10,
                GameKeys.Next_Weapon,
                GameKeys.Previous_Weapon,
                WhKeys.Scare_Spell,
                WhKeys.Nightvision_Spell,
                WhKeys.Freeze_Spell,
                WhKeys.Arrow_Spell,
                WhKeys.Opendoor_Spell,
                WhKeys.Fly_Spell,
                WhKeys.Fire_Spell,
                WhKeys.Nuke_Spell,
                WhKeys.Spell_next,
                WhKeys.Spell_prev,
                WhKeys.Health_potion,
                WhKeys.Strength_potion,
                WhKeys.Curepoison_potion,
                WhKeys.Fireresist_potion,
                WhKeys.Invisibility_potion,
                WhKeys.Inventory_Use,
                WhKeys.Inventory_Left,
                WhKeys.Inventory_Right,
                GameKeys.Map_Toggle,
                WhKeys.Map_Follow_Mode,
                GameKeys.Shrink_Screen,
                GameKeys.Enlarge_Screen,
                GameKeys.Mouse_Aiming,
                WhKeys.Toggle_Crosshair,
                WhKeys.Fly_up,
                WhKeys.Fly_down,
                WhKeys.End_flying,
                GameKeys.Send_Message,
                WhKeys.See_Coop_View,
                GameKeys.Show_Console,
                WhKeys.Quickload,
                WhKeys.Quicksave,
                WhKeys.Show_Savemenu,
                WhKeys.Show_Loadmenu,
                WhKeys.Show_Sounds,
                WhKeys.Show_Options,
                WhKeys.Quit,
                WhKeys.Gamma,
                WhKeys.Make_Screenshot,
                GameKeys.Menu_Toggle
        };
    }

    @Override
    protected InputContext createDefaultInputContext() {
        GameKey[] keymap = getKeyMap();

        return new InputContext(keymap, defkeys, defclassickeys) {

            @Override
            protected void clearInput() {
                super.clearInput();
                weaponIndex = IntStream.range(0, keymap.length).filter(i -> keymap[i].equals(WhKeys.Weapon_1)).findFirst().orElse(-1);
                spellIndex = IntStream.range(0, keymap.length).filter(i -> keymap[i].equals(WhKeys.Scare_Spell)).findFirst().orElse(-1);
            }
        };
    }

    @Override
    protected ConfigContext createDefaultGameContext() {
        return new ConfigContext() {
            @Override
            public void load(Properties prop) {
                if (prop.setContext("Options")) {
                    gViewSize = prop.getIntValue("ViewSize", gViewSize);
                    gCrosshair = prop.getBooleanValue("Crosshair", gCrosshair);
                    MessageState = prop.getBooleanValue("MessageState", MessageState);
                    gAutoRun = prop.getBooleanValue("AutoRun", gAutoRun);
                    gStatSize = Math.max(prop.getIntValue("StatSize", gStatSize), 16384);
                    gCrossSize = Math.max(prop.getIntValue("CrossSize", gCrossSize), 16384);
                    gHudScale = Math.max(prop.getIntValue("HUDScale", gHudScale), 32768);
                    gShowStat = prop.getIntValue("ShowStat", gShowStat);
                    showCutscenes = prop.getBooleanValue("showCutscenes", showCutscenes);
                    gGameGore = prop.getBooleanValue("Gore", gGameGore);
                    showMapInfo = prop.getBooleanValue("ShowMapInfo", showMapInfo);
                    gDemoSeq = prop.getIntValue("DemoSequence", gDemoSeq);
                }
            }

            @Override
            public void save(OutputStream os) throws IOException {
                putString(os, "[Options]\r\n");
                //Options
                putInteger(os, "ViewSize", gViewSize);
                putBoolean(os, "Crosshair", gCrosshair);
                putBoolean(os, "MessageState", MessageState);

                putBoolean(os, "AutoRun", gAutoRun);
                putInteger(os, "StatSize", gStatSize);
                putInteger(os, "CrossSize", gCrossSize);
                putInteger(os, "HUDScale", gHudScale);
                putInteger(os, "ShowStat", gShowStat);
                putBoolean(os, "showCutscenes", showCutscenes);
                putBoolean(os, "Gore", gGameGore);
                putBoolean(os, "ShowMapInfo", showMapInfo);
                putInteger(os, "DemoSequence", gDemoSeq);
            }
        };
    }


    public enum WhKeys implements GameKey {

        AutoRun,
        Aim_Up,
        Aim_Down,
        Aim_Center,
        Weapon_1,
        Weapon_2,
        Weapon_3,
        Weapon_4,
        Weapon_5,
        Weapon_6,
        Weapon_7,
        Weapon_8,
        Weapon_9,
        Weapon_10,

        Cast_spell,
        Scare_Spell,
        Nightvision_Spell,
        Freeze_Spell,
        Arrow_Spell,
        Opendoor_Spell,
        Fly_Spell,
        Fire_Spell,
        Nuke_Spell,
        Spell_next,
        Spell_prev,
        Inventory_Use,
        Inventory_Left,
        Inventory_Right,
        Fly_up,
        Fly_down,
        End_flying,

        Health_potion,
        Strength_potion,
        Curepoison_potion,
        Fireresist_potion,
        Invisibility_potion,

        Map_Follow_Mode,
        Toggle_Crosshair,
        See_Coop_View,
        Quickload,
        Quicksave,
        Show_Savemenu,
        Show_Loadmenu,
        Show_Sounds,
        Show_Options,
        Quit,
        Gamma,
        Make_Screenshot;

        public int getNum() {
            return GameKeys.values().length + ordinal();
        }

        public String getName() {
            return name();
        }

    }
}
