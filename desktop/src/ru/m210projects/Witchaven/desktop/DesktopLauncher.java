package ru.m210projects.Witchaven.desktop;

import com.badlogic.gdx.backends.LwjglLauncherUtil;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Witchaven.Config;
import ru.m210projects.Witchaven.Main;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Locale;

public class DesktopLauncher {

    public static final String appname = "WitchavenGDX";

    public static void main(final String[] arg) throws IOException {
        GameConfig cfg = new Config(Paths.get(arg[0], (appname + ".ini").toLowerCase(Locale.ROOT)));
        cfg.load();
        cfg.setGamePath(cfg.getCfgPath().getParent());
        cfg.addMidiDevices(LwjglLauncherUtil.getMidiDevices());
        boolean isWH2 = false;
        LwjglLauncherUtil.launch(new Main(cfg, appname, "?.??",false, isWH2), null);
    }
}
